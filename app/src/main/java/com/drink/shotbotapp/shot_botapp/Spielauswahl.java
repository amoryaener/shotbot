package com.drink.shotbotapp.shot_botapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class Spielauswahl extends AppCompatActivity {

    ImageButton btnQuizStart;
    ImageButton btnRouletteStart;
    ImageButton btnBombeStart;
    ImageButton btnRouletteStart2;
    ImageButton btnBalkenSaufenStart;
    ImageButton btnHoeherTieferStart;
    ImageButton btnMemoryStart;
    ImageButton btnWahrFalschStart;
    ImageButton btnDiceStart;
    ImageButton btnMeiernStart;
    ImageButton btnINeverStart;
    ImageButton btnGameStart;
    ImageButton btnNichtLachenStart;
    ImageButton btnBusStart;
    ImageButton btnTronStart;
    ImageButton btnDrawStart;
    ImageButton btnRofStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spielauswahl);
        btnQuizStart = (ImageButton)findViewById(R.id.btnQuizStart);
        btnRouletteStart = (ImageButton)findViewById(R.id.btnRouletteStart);
        btnRouletteStart2 = (ImageButton)findViewById(R.id.btnRouletteStart2);
        btnBombeStart = (ImageButton)findViewById(R.id.btnBombeStart);
        btnBalkenSaufenStart = (ImageButton)findViewById(R.id.btnBalkenSaufenStart);
        btnHoeherTieferStart = (ImageButton)findViewById(R.id.btnHoeherStart);
        btnMemoryStart = (ImageButton)findViewById(R.id.btnMemoryStart);
        btnWahrFalschStart = (ImageButton)findViewById(R.id.btnWahrFalschStart);
        btnDiceStart = (ImageButton)findViewById(R.id.btnDiceStart);
        btnMeiernStart = (ImageButton)findViewById(R.id.btnMeiernStart);
        btnINeverStart = (ImageButton)findViewById(R.id.btnINeverStart);
        btnGameStart = (ImageButton)findViewById(R.id.btnGameStart);
        btnNichtLachenStart = (ImageButton)findViewById(R.id.btnNichtLachenStart);
        btnBusStart = (ImageButton)findViewById(R.id.btnBusStart);
        btnTronStart = (ImageButton)findViewById(R.id.btnTronStart);
        btnDrawStart = (ImageButton)findViewById(R.id.btnDrawStart);
        btnRofStart = (ImageButton)findViewById(R.id.btnRofStart);

        btnQuizStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Spielauswahl.this, Quiz.class);
                startActivity(i);
            }
        });

        btnRouletteStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Spielauswahl.this, Gluecksrad.class);
                startActivity(i);
            }
        });

        btnBombeStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Spielauswahl.this, TimerSpiel.class);
                startActivity(i);
            }
        });

        btnRouletteStart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Spielauswahl.this, Roulette.class);
                startActivity(i);
            }
        });

        btnBalkenSaufenStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Spielauswahl.this, BalkenSaufen.class);
                startActivity(i);
            }
        });

        btnHoeherTieferStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Spielauswahl.this, HoeherTiefer.class);
                startActivity(i);
            }
        });

        btnMemoryStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Spielauswahl.this, Memory.class);
                startActivity(i);
            }
        });

        btnWahrFalschStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Spielauswahl.this, WahrFalsch.class);
                startActivity(i);
            }
        });

        btnDiceStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Spielauswahl.this, Hausnummer.class);
                startActivity(i);
            }
        });

        btnMeiernStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Spielauswahl.this, Meiern.class);
                startActivity(i);
            }
        });

        btnINeverStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Spielauswahl.this, INever.class);
                startActivity(i);
            }
        });

        btnGameStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent i = new Intent(Spielauswahl.this, game.class);
               // startActivity(i);
            }
        });

        btnNichtLachenStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Spielauswahl.this, NichtLachen.class);
                startActivity(i);
            }
        });

        btnBusStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Spielauswahl.this, Busfahrer.class);
                startActivity(i);
            }
        });

        btnTronStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PlayersClass.getPlayerList().size()<2){
                    Toast.makeText(getApplicationContext(), "Mindestens 2 Spieler benötigt", Toast.LENGTH_LONG).show();
                }else{
                    Intent i = new Intent(Spielauswahl.this, Tron.class);
                    startActivity(i);
                }
            }
        });

        btnDrawStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PlayersClass.getPlayerList().size()<3){
                    Toast.makeText(getApplicationContext(), "Mindestens 3 Spieler benötigt", Toast.LENGTH_LONG).show();
                }else{
                    Intent i = new Intent(Spielauswahl.this, Draw.class);
                    startActivity(i);
                }
            }
        });

        btnRofStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Spielauswahl.this, DragDrop.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(Spielauswahl.this, SpielerEinstellungen.class);
        startActivity(i);
    }
}
