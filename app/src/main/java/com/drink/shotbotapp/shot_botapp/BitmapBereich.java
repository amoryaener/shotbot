package com.drink.shotbotapp.shot_botapp;

/**
 * Created by Alexander on 18.02.2018.
 */

public class BitmapBereich {
    public int startx;
    public int starty;
    public int width;
    public int height;

    public BitmapBereich(int x, int y, int w, int h){
        this.startx = x;
        this.starty = y;
        this.width = w;
        this.height = h;
    }
}
