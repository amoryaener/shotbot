package com.drink.shotbotapp.shot_botapp;

import java.util.ArrayList;

/**
 * Created by Alexander on 19.01.2018.
 */

public class PlayersClass {

    public static void clean(){
        playerlist = new ArrayList<Player>();
    }

    public static ArrayList<Player> playerlist = new ArrayList<Player>();

    public static void addPlayer(Player p){
        playerlist.add(p);
        Player[] pArray = new Player[playerlist.size()];
        for(int i=0; i<playerlist.size(); i++){
            pArray[i]=playerlist.get(i);
        }
        for(int i=0; i<14; i++){
            for(int j=0; j<pArray.length-1; j++){
                int a = pArray[j].id;
                int b = pArray[j+1].id;
                if(a>b){
                    Player tmp = pArray[j];
                    pArray[j]=pArray[j+1];
                    pArray[j+1]=tmp;
                }
            }
        }
        playerlist.clear();
        for(int i=0; i<pArray.length; i++){
            playerlist.add(pArray[i]);
        }
    }

    public static void removePlayer(Player p){
        playerlist.remove(p);
    }

    public static ArrayList<Player> getPlayerList(){
        return playerlist;
    }

    public static int maxShots(){
        int max =0;
        for(int i=0;i<playerlist.size();i++){
            if(playerlist.get(i).getShotCount()>max){
                max = playerlist.get(i).getShotCount();
            }
        }
        return max;
    }

}
