package com.drink.shotbotapp.shot_botapp;

import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Meiern extends AppCompatActivity {

    int count2 =0;
    boolean begrenzt = false;
    int rounds;
    String Status;
    Boolean Getrunken = true;
    ImageView ampel;
    int oldDice1;
    int oldDice2;
    ImageView playerPic;
    ImageView dice1;
    ImageView dice2;
    TextView playerName;
    Button aufdecken;
    Button wuerfeln;
    Button weiter;
    int currentPlayer;
    ArrayList<Player> players;
    Resources res;
    Drawable[] dice= new Drawable[6];
    private final int[] diceImages = new int[] { R.drawable.one, R.drawable.two, R.drawable.three, R.drawable.four, R.drawable.five, R.drawable.six, R.drawable.dicesb };
    int roll1;
    int roll2;
    private Handler animationHandler;
    boolean hasRolled;
    private final int rollAnimations = 50;
    private final int delayTime = 15;
    private final Random randomGen = new Random();
    LinearLayout popup;
    TextView ja_nein_text;
    Button ja;
    Button nein;
    int status =0;
    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meiern);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        rounds = getIntent().getIntExtra("Rounds",0);
        if(rounds >0){
            begrenzt=true;
        }
        GlobalStats.gameStartet("Meiern");
        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });
        ampel = findViewById(R.id.ampel);
        popup = (LinearLayout)findViewById(R.id.meiern_popup);
        ja_nein_text = (TextView)findViewById(R.id.meiern_janeinText);
        ja = (Button)findViewById(R.id.meiern_ja);
        nein = (Button)findViewById(R.id.meiern_nein);
        playerPic= (ImageView)findViewById(R.id.meiern_playerpic);
        dice1= (ImageView)findViewById(R.id.meiern_dice1);
        dice2= (ImageView)findViewById(R.id.meiern_dice2);
        playerName =(TextView)findViewById(R.id.meiern_playername);
        aufdecken = (Button)findViewById(R.id.meiern_aufdecken);
        wuerfeln = (Button)findViewById(R.id.meiern_wuerfeln);
        weiter = (Button)findViewById(R.id.meiern_weiter);
        Typeface KreideSchrift = Typeface.createFromAsset(getAssets(), "Gravur.ttf");
        BluetoothAdapterClass.setHandler(mHandler);
        ja_nein_text.setTypeface(KreideSchrift);
        popup.setVisibility(View.INVISIBLE);
        dice1.setImageResource(diceImages[6]);
        dice2.setImageResource(diceImages[6]);
        hasRolled =false;
        currentPlayer =0;
        players = PlayersClass.getPlayerList();
        playerName.setText(players.get(currentPlayer).getName());
        playerPic.setImageResource(players.get(currentPlayer).getColor());
        if(players.get(currentPlayer).bild!=null){
            playerPic.setImageBitmap(players.get(currentPlayer).bild);
            playerPic.setForeground(null);
        }else{
            playerPic.setForeground(players.get(currentPlayer).playerPic);
        }
        weiter.setVisibility(View.INVISIBLE);
        aufdecken.setVisibility(View.INVISIBLE);

        res = getResources();
        for (int i = 0; i < 6; i++) {
            dice[i] = res.getDrawable(diceImages[i]);
        }
        animationHandler = new Handler() {
            public void handleMessage(Message msg) {
                dice1.setImageDrawable(dice[roll1]);
                dice2.setImageDrawable(dice[roll2]);
            }
        };

        wuerfeln.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!hasRolled){
                    hasRolled = true;
                    rollDice();
                    wuerfeln.setVisibility(View.INVISIBLE);
                    aufdecken.setVisibility(View.INVISIBLE);
                    weiter.setVisibility(View.VISIBLE);
                }
            }
        });

        weiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Getrunken==true) {
                    count2++;
                    hasRolled = false;
                    oldDice1 = roll1;
                    oldDice2 = roll2;
                    wuerfeln.setVisibility(View.VISIBLE);
                    aufdecken.setVisibility(View.VISIBLE);
                    weiter.setVisibility(View.INVISIBLE);
                    dice1.setImageResource(diceImages[6]);
                    dice2.setImageResource(diceImages[6]);
                    if (currentPlayer < players.size() - 1) {
                        currentPlayer++;
                    } else {
                        currentPlayer = 0;
                    }
                    if(begrenzt && rounds == count2){
                        finish();
                    }
                    playerName.setText(players.get(currentPlayer).getName());
                    playerPic.setImageResource(players.get(currentPlayer).getColor());
                    if (players.get(currentPlayer).bild != null) {
                        playerPic.setImageBitmap(players.get(currentPlayer).bild);
                        playerPic.setForeground(null);
                    } else {
                        playerPic.setForeground(players.get(currentPlayer).playerPic);
                    }
                }
            }
        });

        aufdecken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hasRolled=false;
                dice1.setImageResource(diceImages[oldDice1]);
                dice2.setImageResource(diceImages[oldDice2]);
                Player prev=null;
                if(currentPlayer==0){
                    prev =players.get(players.size()-1);
                }else{
                    prev= players.get(currentPlayer-1);
                }
                ja_nein_text.setText("Hat "+prev.getName()+" gelogen?");
                popup.setVisibility(View.VISIBLE);
            }
        });

        ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status==1){
                    status=0;
                    popup.setVisibility(View.INVISIBLE);
                    ja.setText("JA");
                    if(currentPlayer<players.size()-1){
                        currentPlayer ++;
                    }else{
                        currentPlayer =0;
                    }
                    playerName.setText(players.get(currentPlayer).getName());
                    playerPic.setImageResource(players.get(currentPlayer).getColor());
                    if(players.get(currentPlayer).bild!=null){
                        playerPic.setImageBitmap(players.get(currentPlayer).bild);
                        playerPic.setForeground(null);
                    }else{
                        playerPic.setForeground(players.get(currentPlayer).playerPic);
                    }
                    dice1.setImageResource(diceImages[6]);
                    dice2.setImageResource(diceImages[6]);
                    weiter.setVisibility(View.INVISIBLE);
                    aufdecken.setVisibility(View.INVISIBLE);
                    wuerfeln.setVisibility(View.VISIBLE);
                    hasRolled=false;
                    nein.setVisibility(View.VISIBLE);
                }else{
                    Player prev=null;
                    if(currentPlayer==0){
                        prev =players.get(players.size()-1);
                    }else{
                        prev= players.get(currentPlayer+1);
                    }
                    prev.oneShot();
                    if(!SystemState.isOffline()) {
                        Getrunken = false;
                        ampel.setImageResource(R.drawable.ampel_rot);
                    }
                    popup.setVisibility(View.INVISIBLE);
                    weiter.setVisibility(View.INVISIBLE);
                    aufdecken.setVisibility(View.INVISIBLE);
                    wuerfeln.setVisibility(View.VISIBLE);
                }
            }
        });
        nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                players.get(currentPlayer).oneShot();
                if(!SystemState.isOffline()) {
                    Getrunken = false;
                    ampel.setImageResource(R.drawable.ampel_rot);
                }
                popup.setVisibility(View.INVISIBLE);
                weiter.setVisibility(View.INVISIBLE);
                aufdecken.setVisibility(View.INVISIBLE);
                wuerfeln.setVisibility(View.VISIBLE);
            }
        });
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bedienung.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Status = readMessage;
                    if(Status.contains("1")){
                        Getrunken = true;
                        ampel.setImageResource(R.drawable.ampel_gruen);
                        Status="";
                    }
                    break;
            }
        }
    };

    private final Handler mHandler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            meier();
        }
    };

    public void meier(){
        status =1;
        String shots ="";
        for(int i=0; i<players.size(); i++){
            if(i!=currentPlayer){
                shots = shots+"T"+players.get(i).id+"#";
                players.get(i).shotCountPlus();
            }
        }
        shots = shots+"TB";
        if(players.size()>1){
            BluetoothAdapterClass.WriteBT(shots);
        }
        nein.setVisibility(View.INVISIBLE);
        popup.setVisibility(View.VISIBLE);
        ja_nein_text.setText("Meier!\nAlle außer "+ players.get(currentPlayer).getName()+" bekommen einen Shot!");
        ja.setText("Weiter");

        if(!SystemState.isOffline()) {
            Getrunken = false;
            ampel.setImageResource(R.drawable.ampel_rot);
        }
    }

    private void rollDice() {
        hasRolled =true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < rollAnimations; i++) {
                    doRoll(i);
                }
            }
        }).start();
        MediaPlayer mp = MediaPlayer.create(this, R.raw.roll);
        try {
            mp.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mp.start();
    }

    private void doRoll(int a) { // only does a single roll
        roll1 = randomGen.nextInt(6);
        roll2 = randomGen.nextInt(6);
        synchronized (getLayoutInflater()) {
            animationHandler.sendEmptyMessage(0);
        }
        try { // delay to alloy for smooth animation
            Thread.sleep(delayTime);
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
        if((roll1==0 && roll2 ==1 && a== rollAnimations-1) || (roll1==1 && roll2 ==0) && a== rollAnimations-1){
            mHandler2.obtainMessage(0).sendToTarget();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }
}
