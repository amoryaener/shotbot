package com.drink.shotbotapp.shot_botapp;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by Alexander on 17.02.2018.
 */

public class GameState {
    public static int[] positions = new int[10];
    public static ArrayList<GameFeld> felder = new ArrayList<GameFeld>();
    public static Bitmap feld;
    public static ArrayList<Player> players = PlayersClass.getPlayerList();
    public static int currentPlayer=0;
    public static boolean hasRolled;
    public static boolean diceVisible;
    public static int statusFab;
    public static boolean playerPopupOpen;
    public static BitmapBereich[] bmB = new BitmapBereich[54];
    public static ColorSave[] save = new ColorSave[10];
    public static boolean[] saved = new boolean[10];
    public static boolean statsVisible;
    public static boolean legendeVisible;

    public static void setFeld(Bitmap b){
        feld = b;
    }

    public static void setupGame(){
        for(int i=0; i<players.size(); i++){
            positions[i]=0;
            saved[i]=false;
        }
        bmB[0]=new BitmapBereich(40+0*feld.getWidth()/9,80, 110,110);
        bmB[1]=new BitmapBereich(40+1*feld.getWidth()/9,80, 110,110);
        bmB[2]=new BitmapBereich(40+2*feld.getWidth()/9,80, 110,110);
        bmB[3]=new BitmapBereich(40+3*feld.getWidth()/9,80, 110,110);
        bmB[4]=new BitmapBereich(40+4*feld.getWidth()/9,80, 110,110);
        bmB[5]=new BitmapBereich(40+5*feld.getWidth()/9,80, 110,110);
        bmB[6]=new BitmapBereich(40+6*feld.getWidth()/9,80, 110,110);
        bmB[7]=new BitmapBereich(40+7*feld.getWidth()/9,80, 110,110);
        bmB[8]=new BitmapBereich(40+8*feld.getWidth()/9,80, 110,110);
        bmB[9]=new BitmapBereich(40+8*feld.getWidth()/9,80+1*feld.getHeight()/6, 110,110);
        bmB[10]=new BitmapBereich(40+8*feld.getWidth()/9,80+2*feld.getHeight()/6, 110,110);
        bmB[11]=new BitmapBereich(40+8*feld.getWidth()/9,80+3*feld.getHeight()/6, 110,110);
        bmB[12]=new BitmapBereich(40+8*feld.getWidth()/9,80+4*feld.getHeight()/6, 110,110);
        bmB[13]=new BitmapBereich(40+8*feld.getWidth()/9,80+5*feld.getHeight()/6, 110,110);
        bmB[14]=new BitmapBereich(40+7*feld.getWidth()/9,80+5*feld.getHeight()/6, 110,110);
        bmB[15]=new BitmapBereich(40+6*feld.getWidth()/9,80+5*feld.getHeight()/6, 110,110);
        bmB[16]=new BitmapBereich(40+5*feld.getWidth()/9,80+5*feld.getHeight()/6, 110,110);
        bmB[17]=new BitmapBereich(40+4*feld.getWidth()/9,80+5*feld.getHeight()/6, 110,110);
        bmB[18]=new BitmapBereich(40+3*feld.getWidth()/9,80+5*feld.getHeight()/6, 110,110);
        bmB[19]=new BitmapBereich(40+2*feld.getWidth()/9,80+5*feld.getHeight()/6, 110,110);
        bmB[20]=new BitmapBereich(40+1*feld.getWidth()/9,80+5*feld.getHeight()/6, 110,110);
        bmB[21]=new BitmapBereich(40+0*feld.getWidth()/9,80+5*feld.getHeight()/6, 110,110);
        bmB[22]=new BitmapBereich(40+0*feld.getWidth()/9,80+4*feld.getHeight()/6, 110,110);
        bmB[23]=new BitmapBereich(40+0*feld.getWidth()/9,80+3*feld.getHeight()/6, 110,110);
        bmB[24]=new BitmapBereich(40+0*feld.getWidth()/9,80+2*feld.getHeight()/6, 110,110);
        bmB[25]=new BitmapBereich(40+0*feld.getWidth()/9,80+1*feld.getHeight()/6, 110,110);
        bmB[26]=new BitmapBereich(40+1*feld.getWidth()/9,80+1*feld.getHeight()/6, 110,110);
        bmB[27]=new BitmapBereich(40+2*feld.getWidth()/9,80+1*feld.getHeight()/6, 110,110);
        bmB[28]=new BitmapBereich(40+3*feld.getWidth()/9,80+1*feld.getHeight()/6, 110,110);
        bmB[29]=new BitmapBereich(40+4*feld.getWidth()/9,80+1*feld.getHeight()/6, 110,110);
        bmB[30]=new BitmapBereich(40+5*feld.getWidth()/9,80+1*feld.getHeight()/6, 110,110);
        bmB[31]=new BitmapBereich(40+6*feld.getWidth()/9,80+1*feld.getHeight()/6, 110,110);
        bmB[32]=new BitmapBereich(40+7*feld.getWidth()/9,80+1*feld.getHeight()/6, 110,110);
        bmB[33]=new BitmapBereich(40+7*feld.getWidth()/9,80+2*feld.getHeight()/6, 110,110);
        bmB[34]=new BitmapBereich(40+7*feld.getWidth()/9,80+3*feld.getHeight()/6, 110,110);
        bmB[35]=new BitmapBereich(40+7*feld.getWidth()/9,80+4*feld.getHeight()/6, 110,110);
        bmB[36]=new BitmapBereich(40+6*feld.getWidth()/9,80+4*feld.getHeight()/6, 110,110);
        bmB[37]=new BitmapBereich(40+5*feld.getWidth()/9,80+4*feld.getHeight()/6, 110,110);
        bmB[38]=new BitmapBereich(40+4*feld.getWidth()/9,80+4*feld.getHeight()/6, 110,110);
        bmB[39]=new BitmapBereich(40+3*feld.getWidth()/9,80+4*feld.getHeight()/6, 110,110);
        bmB[40]=new BitmapBereich(40+2*feld.getWidth()/9,80+4*feld.getHeight()/6, 110,110);
        bmB[41]=new BitmapBereich(40+1*feld.getWidth()/9,80+4*feld.getHeight()/6, 110,110);
        bmB[42]=new BitmapBereich(40+1*feld.getWidth()/9,80+3*feld.getHeight()/6, 110,110);
        bmB[43]=new BitmapBereich(40+1*feld.getWidth()/9,80+2*feld.getHeight()/6, 110,110);
        bmB[44]=new BitmapBereich(40+2*feld.getWidth()/9,80+2*feld.getHeight()/6, 110,110);
        bmB[45]=new BitmapBereich(40+3*feld.getWidth()/9,80+2*feld.getHeight()/6, 110,110);
        bmB[46]=new BitmapBereich(40+4*feld.getWidth()/9,80+2*feld.getHeight()/6, 110,110);
        bmB[47]=new BitmapBereich(40+5*feld.getWidth()/9,80+2*feld.getHeight()/6, 110,110);
        bmB[48]=new BitmapBereich(40+6*feld.getWidth()/9,80+2*feld.getHeight()/6, 110,110);
        bmB[49]=new BitmapBereich(40+6*feld.getWidth()/9,80+3*feld.getHeight()/6, 110,110);
        bmB[50]=new BitmapBereich(40+5*feld.getWidth()/9,80+3*feld.getHeight()/6, 110,110);
        bmB[51]=new BitmapBereich(40+4*feld.getWidth()/9,80+3*feld.getHeight()/6, 110,110);
        bmB[52]=new BitmapBereich(40+3*feld.getWidth()/9,80+3*feld.getHeight()/6, 110,110);
        bmB[53]=new BitmapBereich(40+2*feld.getWidth()/9,80+3*feld.getHeight()/6, 110,110);


        felder.add(new GameFeld("Start Feld", "Du bist auf dem Startfeld", 1, ""));
        felder.add(new GameFeld("Shot Feld", "Du bekommst einen Shot vom Shotbot", 2, "Zug beenden"));
        felder.add(new GameFeld("Leeres Feld", "Es passiert nichts", 0, "Zug beenden"));
        felder.add(new GameFeld("Leiter Feld", "Du nutzt die Leiter als Abkürzung", 3, "Zug beenden"));
        felder.add(new GameFeld("Minispiel", "Ihr spielt ein Minispiel!", 5, "Minispiel starten"));
        felder.add(new GameFeld("Leeres Feld", "Es passiert nichts", 0, "Zug beenden"));
        felder.add(new GameFeld("Glücksrad", "Du darfst einmal am Glücksrad drehen", 6, "Zum Glücksrad"));
        felder.add(new GameFeld("Leeres Feld", "Es passiert nichts", 0, "Zug beenden"));
        felder.add(new GameFeld("Shot Feld", "Du bekommst einen Shot vom Shotbot", 2, "Zug beenden"));
        felder.add(new GameFeld("Minispiel", "Ihr spielt ein Minispiel!", 5, "Minispiel starten"));
        felder.add(new GameFeld("Leeres Feld", "Es passiert nichts", 0, "Zug beenden"));
        felder.add(new GameFeld("Leeres Feld", "Es passiert nichts", 0, "Zug beenden"));
        for(int i=0; i<50; i++){
            felder.add(new GameFeld("Leeres Feld", "Es passiert nichts", 0, "Zug beenden"));
        }
    }
}
