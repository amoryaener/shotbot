package com.drink.shotbotapp.shot_botapp;

import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.ArrayList;

public class BalkenSaufen extends AppCompatActivity {

    ImageView einstellungen;
    ImageView difficulty;
    LinearLayout popup;
    ImageView marker;
    int m_left;
    int m_right;
    boolean left_down;
    boolean right_down;
    ImageView start;
    CoordinatorLayout overall;
    Runner r;
    int status;
    ArrayList<Player> players;
    int currentPlayer;
    ImageView check;
    TextView playerName;
    boolean next;
    ArrayList<Player> loosers;
    LinearLayout nameAndCheck;
    ImageView playerPic;
    ImageView ampel;
    String Status;
    Boolean Getrunken = true;
    int diffi =1; // 0=easy, 1=medium, 2=hard
    boolean einstellungenSichtbar = false;
    int step =25;
    int count =0;
    boolean begrenzt = false;
    int rounds;
    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balken_saufen);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        rounds = getIntent().getIntExtra("Rounds",0);
        if(rounds >0){
            begrenzt=true;
        }
        nameAndCheck = (LinearLayout)findViewById(R.id.balken_nameAndCheck);
        next = false;
        BluetoothAdapterClass.setHandler(mHandler2);
        ampel = findViewById(R.id.ampel);
        difficulty = findViewById(R.id.btn_balkenDIff);
        difficulty.setVisibility(View.GONE);
        einstellungen = findViewById(R.id.btn_balkenEins);
        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });
        playerPic = findViewById(R.id.balkenplayerPic);
        difficulty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status!=55){
                    ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) marker.getLayoutParams();
                    params.rightMargin = 0;
                    params.leftMargin = 0;
                    m_left =0;
                    m_right =0;
                    marker.setLayoutParams(params);
                    left_down = false;
                    right_down = false;
                    if(diffi==0){
                        diffi =1;
                        difficulty.setImageResource(R.drawable.balkensaufen_medium);
                        step =25;
                    }else if(diffi==1){
                        diffi =2;
                        difficulty.setImageResource(R.drawable.balkensaufen_hard);
                        step =35;
                    }else{
                        diffi=0;
                        difficulty.setImageResource(R.drawable.balkensaufen_easy);
                        step =15;
                    }
                }
            }
        });
        loosers = new ArrayList<Player>();
        einstellungen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(einstellungenSichtbar){
                    einstellungenSichtbar=false;
                    difficulty.setVisibility(View.GONE);
                    difficulty.setClickable(false);
                    difficulty.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_hide));
                }else {
                    einstellungenSichtbar=true;
                    difficulty.setVisibility(View.VISIBLE);
                    difficulty.setClickable(true);
                    difficulty.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_show));
                }
            }
        });
        playerName = (TextView) findViewById(R.id.balken_playername);
        popup = findViewById(R.id.balkenPopup);
        popup.setVisibility(View.VISIBLE);
        check = (ImageView) findViewById(R.id.balken_check);
        check.setImageDrawable(null);
        status =-1;
        players = PlayersClass.getPlayerList();
        currentPlayer = 0;
        overall = (CoordinatorLayout)findViewById(R.id.overallLayout);
        marker = (ImageView)findViewById(R.id.marker);
        m_left =0;
        m_right =0;
        left_down = false;
        right_down = false;
        start = (ImageView)findViewById(R.id.btn_balkenStart);

        playerName.setText(players.get(currentPlayer).getName());
        playerPic.setImageResource(players.get(currentPlayer).getColor());
        if(players.get(currentPlayer).bild!=null){
            playerPic.setImageBitmap(players.get(currentPlayer).bild);
            playerPic.setForeground(null);
        }else{
            playerPic.setForeground(players.get(currentPlayer).playerPic);
        }

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status ==0){
                    status =1;
                    r= new Runner();
                    r.start();
                    status = 55;
                    start.setImageResource(R.drawable.balkensaufen_stop);
                }else if(status ==3){
                    if(Getrunken){
                        if(currentPlayer<players.size()-1) {
                            currentPlayer++;
                        }else{
                            currentPlayer=0;
                        }
                        check.setImageDrawable(null);
                        playerName.setText(players.get(currentPlayer).getName());
                        playerName.setText(players.get(currentPlayer).getName());
                        playerPic.setImageResource(players.get(currentPlayer).getColor());
                        if(players.get(currentPlayer).bild!=null){
                            playerPic.setImageBitmap(players.get(currentPlayer).bild);
                            playerPic.setForeground(null);
                        }else{
                            playerPic.setForeground(players.get(currentPlayer).playerPic);
                        }
                        start.setImageResource(R.drawable.balkensaufen_start);
                        status =0;
                        next = false;
                    }
                }else if(status == 55){
                        r.stoppen();
                        if(m_left<180 && m_right<180){
                            check.setImageResource(R.drawable.check);
                        }else if(m_left<260 && m_right<260){
                            check.setImageResource(R.drawable.kreuz_transparent);
                            //Füllmenge klein
                            //BluetoothAdapterClass.WriteBT("ST#X "+players.get(currentPlayer).id+"#GO#Y 500#VM#X "+"-"+players.get(currentPlayer).id+"#GO#P "+players.get(currentPlayer).id+"#QUIT");
                            players.get(currentPlayer).oneShot();
                            if(!SystemState.isOffline()){
                                Getrunken=false;
                                ampel.setImageResource(R.drawable.ampel_rot);
                            }
                            //players.get(currentPlayer).oneShot();
                        }else if(m_left<390 && m_right<390){
                            check.setImageResource(R.drawable.kreuz_transparent);
                            //Füllmenge groß
                            //BluetoothAdapterClass.WriteBT("ST#X "+players.get(currentPlayer).id+"#GO#Y 1500#VM#X "+"-"+players.get(currentPlayer).id+"#GO#P "+players.get(currentPlayer).id+"#QUIT");
                            players.get(currentPlayer).oneShot();
                            if(!SystemState.isOffline()){
                                Getrunken=false;
                                ampel.setImageResource(R.drawable.ampel_rot);
                            }
                            //players.get(currentPlayer).oneShot();
                        }else if(m_left<560 && m_right<560){
                            check.setImageResource(R.drawable.kreuz_transparent);
                            //Füllmenge Voll
                            //BluetoothAdapterClass.WriteBT("ST#X "+players.get(currentPlayer).id+"#GO#Y 2300#VM#X "+"-"+players.get(currentPlayer).id+"#GO#P "+players.get(currentPlayer).id+"#QUIT");
                            players.get(currentPlayer).oneShot();
                            if(!SystemState.isOffline()){
                                Getrunken=false;
                                ampel.setImageResource(R.drawable.ampel_rot);
                            }
                            //players.get(currentPlayer).oneShot();
                        }else {
                            check.setImageResource(R.drawable.kreuz_transparent);
                            //Füllmenge Emsland
                            //BluetoothAdapterClass.WriteBT("ST#X "+players.get(currentPlayer).id+"#GO#Y 3200#VM#X "+"-"+players.get(currentPlayer).id+"#GO#P "+players.get(currentPlayer).id+"#QUIT");
                            players.get(currentPlayer).oneShot();
                            if(!SystemState.isOffline()){
                                Getrunken=false;
                                ampel.setImageResource(R.drawable.ampel_rot);
                            }
                            //players.get(currentPlayer).oneShot();
                        }
                        next = true;
                        start.setImageResource(R.drawable.balkensaufen_weiter);
                        status=3;
                        count++;
                        if(begrenzt && rounds == count){
                            finish();
                        }
                }
            }
        });

        popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.setVisibility(View.INVISIBLE);
                status =0;
            }
        });

    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            step();
        }
    };

    public void step(){
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) marker.getLayoutParams();
        if(m_left==0 && m_right==0){
            if(!left_down){
                m_left=step;
                right_down= false;
            }else{
                left_down = false;
                m_right=step;
            }
        }else if (m_left>0){
            if(!left_down){
                if(m_left>790){
                    m_left=m_left-step;
                    left_down =true;
                }else{
                    m_left=m_left+step;
                }
            }else{
                m_left=m_left-step;
            }
        }else{
            if(!right_down){
                if(m_right>790){
                    m_right=m_right-step;
                    right_down =true;
                }else{
                    m_right=m_right+step;
                }
            }else{
                m_right=m_right-step;
            }
        }
        params.rightMargin = m_right;
        params.leftMargin = m_left;
        marker.setLayoutParams(params);
    }

    public class Runner extends Thread{

        public boolean running;
        public Runner(){
            running =true;
        }

        public void run(){
            while(running){
                mHandler.obtainMessage(1).sendToTarget();
                try {
                    Thread.sleep(4);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void stoppen() {
            running=false;
        }
    }

    private final Handler mHandler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bedienung.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Status = readMessage;
                    if(Status.contains("1")){
                        Getrunken = true;
                        ampel.setImageResource(R.drawable.ampel_gruen);
                        Status="";
                    }
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }
}
