package com.drink.shotbotapp.shot_botapp;

import java.util.ArrayList;

/**
 * Created by Alexander on 26.04.2018.
 */

public class MultiLight {
    static ArrayList<Player> players;
    static timerClass tc;
    static int hoehe = 500;

    public void shots(ArrayList<Player> p){
        players = p;
        tc = new timerClass();
        tc.start();
    }

    public class timerClass extends Thread{

        public void run(){
            String senden = "";
            for(int i=0; i<players.size();i++){
                players.get(i).shotCountPlus();
                senden = senden+"T"+players.get(i).id+"#";
            }
            senden = senden + "TB";
            BluetoothAdapterClass.WriteBT(senden);
        }
    }
}