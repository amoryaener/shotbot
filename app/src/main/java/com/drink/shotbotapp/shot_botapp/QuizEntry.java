package com.drink.shotbotapp.shot_botapp;

/**
 * Created by Alexander on 16.01.2018.
 */

public class QuizEntry {
    String question;
    String ans1;
    String ans2;
    String ans3;
    String ans4;
    int correctAns;

    public QuizEntry(String q, String a1, String a2, String a3, String a4, int correct){
        this.question = q;
        this.ans1 = a1;
        this.ans2 = a2;
        this.ans3 = a3;
        this.ans4 = a4;
        correctAns = correct;
    }

    public String getQuestion() {
        return question;
    }

    public String getAns1() {
        return ans1;
    }

    public String getAns2() {
        return ans2;
    }

    public String getAns3() {
        return ans3;
    }

    public String getAns4() {
        return ans4;
    }

    public int getCorrectAns() {
        return correctAns;
    }
}
