package com.drink.shotbotapp.shot_botapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class Gluecksrad extends AppCompatActivity {

    Button btn_spin;
    ImageView iv_wheel;
    ImageView iv_arrow;
    TextView spin_result;
    boolean is_spinning;
    int count =0;
    boolean begrenzt = false;
    int rounds;

    Random Random;
    int grad = 0, grad_alt = 0;

    private static final float FACTOR = 10.00f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gluecksrad);
        rounds = getIntent().getIntExtra("Rounds",0);
        if(rounds >0){
            begrenzt=true;
        }
        is_spinning =false;
        btn_spin = (Button)findViewById(R.id.btn_spin);
        iv_wheel = (ImageView)findViewById(R.id.iv_wheel);
        iv_arrow = (ImageView)findViewById(R.id.iv_arrow);
        spin_result = (TextView)findViewById(R.id.spin_result);

        Random = new Random();

        btn_spin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                grad = grad_alt % 360;
                grad = Random.nextInt(3600) + 439;
                RotateAnimation rotate= new RotateAnimation(grad_alt, grad,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f );
                rotate.setDuration(4215);
                rotate.setFillAfter(true);
                rotate.setInterpolator(new DecelerateInterpolator());
                rotate.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        spin_result.setText("");
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        spin_result.setText(CurrentNumber(360 - (grad % 360)));
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                iv_wheel.startAnimation(rotate);
            }
        });

        iv_wheel.setOnTouchListener(new OnSwipeTouchListener(Gluecksrad.this) {
            public void onSwipeTop() {
                if(!is_spinning){
                    rotate();
                }
            }
            public void onSwipeRight() {
                if(!is_spinning){
                    rotate();
                }
            }
            public void onSwipeLeft() {
                if(!is_spinning){
                    rotate();
                }
            }
            public void onSwipeBottom() {
                if(!is_spinning){
                    rotate();
                }
            }

        });
    }

    public void rotate(){
        is_spinning = true;
        grad = grad_alt % 360;
        grad = Random.nextInt(3600) + 439;
        RotateAnimation rotate= new RotateAnimation(grad_alt, grad,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f );
        rotate.setDuration(4215);
        rotate.setFillAfter(true);
        rotate.setInterpolator(new DecelerateInterpolator());
        rotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                spin_result.setText("");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                spin_result.setText(CurrentNumber(360 - (grad % 360)));
                is_spinning =false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        iv_wheel.startAnimation(rotate);
    }

    private String CurrentNumber(int grad){
        String text = "";

        if(grad >= (FACTOR * 0) && grad <= (FACTOR * 2)){
            text = "1 Shot!";
            BluetoothAdapterClass.WriteBT("1S");
        }
        if(grad >= (FACTOR * 2) && grad <= (FACTOR * 4)){
            text = "2 Shots!";
            BluetoothAdapterClass.WriteBT("2S");
        }
        if(grad >= (FACTOR * 4) && grad <= (FACTOR * 6)){
            text = "Alle Trinken!";
        }
        if(grad >= (FACTOR * 6) && grad <= (FACTOR * 8)){
            text = "3 Shots!";
            BluetoothAdapterClass.WriteBT("3S");
        }
        if(grad >= (FACTOR * 8) && grad <= (FACTOR * 10)){
            text = "1 Shot!";
            BluetoothAdapterClass.WriteBT("1S");
        }
        if(grad >= (FACTOR * 10) && grad <= (FACTOR * 12)){
            text = "Glückspilz!";
        }
        if(grad >= (FACTOR * 12) && grad <= (FACTOR * 14)){
            text = "3 Shots!";
            BluetoothAdapterClass.WriteBT("3S");
        }
        if(grad >= (FACTOR * 14) && grad <= (FACTOR * 16)){
            text = "Wiederholung! Dreh noch einmal.";
        }
        if(grad >= (FACTOR * 16) && grad <= (FACTOR * 18)){
            text = "Alle Trinken!";
        }
        if(grad >= (FACTOR * 18) && grad <= (FACTOR * 20)){
            text = "1 Shot!";
            BluetoothAdapterClass.WriteBT("1S");
        }
        if(grad >= (FACTOR * 20) && grad <= (FACTOR * 22)){
            text = "3 Shots!";
            BluetoothAdapterClass.WriteBT("3S");
        }
        if(grad >= (FACTOR * 22) && grad <= (FACTOR * 24)){
            text = "1 Shot!";
            BluetoothAdapterClass.WriteBT("1S");
        }
        if(grad >= (FACTOR * 24) && grad <= (FACTOR * 26)){
            text = "2 Shots!";
            BluetoothAdapterClass.WriteBT("2S");
        }
        if(grad >= (FACTOR * 26) && grad <= (FACTOR * 28)){
            text = "Glückspilz!";
        }
        if(grad >= (FACTOR * 28) && grad <= (FACTOR * 30)){
            text = "4 Shots!";
            BluetoothAdapterClass.WriteBT("4S");
        }
        if(grad >= (FACTOR * 30) && grad <= (FACTOR * 32)){
            text = "2 Shots!";
            BluetoothAdapterClass.WriteBT("2S");
        }
        if(grad >= (FACTOR * 32) && grad <= (FACTOR * 34)){
            text = "Alle Trinken!";
        }
        if(grad >= (FACTOR * 34) && grad <= (FACTOR * 36)){
            text = "Wiederholung! Dreh noch einmal.";
        }
        count++;
        if(begrenzt && rounds == count){
            finish();
        }
        return text;
    }
}
