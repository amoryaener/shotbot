package com.drink.shotbotapp.shot_botapp;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class NichtLachen extends AppCompatActivity {

    TextView p1T;
    TextView p2T;
    TextView p3T;
    TextView p4T;
    TextView p5T;
    TextView p6T;
    TextView p7T;
    TextView p8T;
    TextView p9T;
    TextView p10T;
    ImageView p1P;
    ImageView p2P;
    ImageView p3P;
    ImageView p4P;
    ImageView p5P;
    ImageView p6P;
    ImageView p7P;
    ImageView p8P;
    ImageView p9P;
    ImageView p10P;
    ImageView NL_Getrunken;
    ImageView[] pics;
    TextView[] names;
    ArrayList<Player> players;
    boolean[] shots;
    LinearLayout p1l;
    LinearLayout p2l;
    LinearLayout p3l;
    LinearLayout p4l;
    LinearLayout p5l;
    LinearLayout p6l;
    LinearLayout p7l;
    LinearLayout p8l;
    LinearLayout p9l;
    LinearLayout p10l;
    LinearLayout[] layouts;
    Button trink_button;
    String Status;
    Boolean Getrunken = true;
    int actualEntry;
    TextView shottext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nicht_lachen);
        players = PlayersClass.getPlayerList();
        actualEntry=0;

        shottext = findViewById(R.id.shottext);

        shots = new boolean[players.size()];
        for(int i=0; i<shots.length;i++){
            shots[i]=false;
        }

        names = new TextView[10];
        pics = new ImageView[10];
        layouts = new LinearLayout[10];
        p1l= (LinearLayout)findViewById(R.id.NL_P1L);
        p2l= (LinearLayout)findViewById(R.id.NL_P2L);
        p3l= (LinearLayout)findViewById(R.id.NL_P3L);
        p4l= (LinearLayout)findViewById(R.id.NL_P4L);
        p5l= (LinearLayout)findViewById(R.id.NL_P5L);
        p6l= (LinearLayout)findViewById(R.id.NL_P6L);
        p7l= (LinearLayout)findViewById(R.id.NL_P7L);
        p8l= (LinearLayout)findViewById(R.id.NL_P8L);
        p9l= (LinearLayout)findViewById(R.id.NL_P9L);
        p10l= (LinearLayout)findViewById(R.id.NL_P10L);
        p1T = (TextView)findViewById(R.id.NL_P1N);
        p2T = (TextView)findViewById(R.id.NL_P2N);
        p3T = (TextView)findViewById(R.id.NL_P3N);
        p4T = (TextView)findViewById(R.id.NL_P4N);
        p5T = (TextView)findViewById(R.id.NL_P5N);
        p6T = (TextView)findViewById(R.id.NL_P6N);
        p7T = (TextView)findViewById(R.id.NL_P7N);
        p8T = (TextView)findViewById(R.id.NL_P8N);
        p9T = (TextView)findViewById(R.id.NL_P9N);
        p10T = (TextView)findViewById(R.id.NL_P10N);
        p1P = (ImageView)findViewById(R.id.NL_P1);
        p2P = (ImageView)findViewById(R.id.NL_P2);
        p3P = (ImageView)findViewById(R.id.NL_P3);
        p4P = (ImageView)findViewById(R.id.NL_P4);
        p5P = (ImageView)findViewById(R.id.NL_P5);
        p6P = (ImageView)findViewById(R.id.NL_P6);
        p7P = (ImageView)findViewById(R.id.NL_P7);
        p8P = (ImageView)findViewById(R.id.NL_P8);
        p9P = (ImageView)findViewById(R.id.NL_P9);
        p10P = (ImageView)findViewById(R.id.NL_P10);
        NL_Getrunken = (ImageView)findViewById(R.id.NL_Getrunken);
        trink_button = (Button)findViewById(R.id.trink_button);
        BluetoothAdapterClass.setHandler(mHandler);
        names[0]=p1T;
        names[1]=p2T;
        names[2]=p3T;
        names[3]=p4T;
        names[4]=p5T;
        names[5]=p6T;
        names[6]=p7T;
        names[7]=p8T;
        names[8]=p9T;
        names[9]=p10T;
        pics[0]=p1P;
        pics[1]=p2P;
        pics[2]=p3P;
        pics[3]=p4P;
        pics[4]=p5P;
        pics[5]=p6P;
        pics[6]=p7P;
        pics[7]=p8P;
        pics[8]=p9P;
        pics[9]=p10P;
        layouts[0]=p1l;
        layouts[1]=p2l;
        layouts[2]=p3l;
        layouts[3]=p4l;
        layouts[4]=p5l;
        layouts[5]=p6l;
        layouts[6]=p7l;
        layouts[7]=p8l;
        layouts[8]=p9l;
        layouts[9]=p10l;

        for(int i=0; i<10; i++){
            names[i].setVisibility(View.INVISIBLE);
            pics[i].setVisibility(View.INVISIBLE);
        }
        for(int i=0; i<players.size(); i++){
            names[i].setText(players.get(i).getName());
            pics[i].setImageResource(players.get(i).getColor());
            if(players.get(i).bild!=null){
                pics[i].setImageBitmap(players.get(i).bild);
                pics[i].setForeground(null);
            }else{
                pics[i].setForeground(players.get(i).playerPic);
            }
            names[i].setVisibility(View.VISIBLE);
            pics[i].setVisibility(View.VISIBLE);
        }
        for(int i=0; i<players.size(); i++){
            final int a =i;
            pics[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!shots[a]){
                        shots[a]=true;
                        layouts[a].setForeground(getResources().getDrawable(R.drawable.kreuz_transparent));
                    }else{
                        shots[a]=false;
                        layouts[a].setForeground(null);
                    }
                }
            });
        }
        trink_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MultiShot ms = new MultiShot();
                for(int i=0; i<players.size(); i++){
                    players.get(i).hoehe=400*i;
                }
                MultiLight ml = new MultiLight();
                ml.shots(players);
                //ms.shots(players);
                /**
                if(Getrunken==true) {
                    String shottext = "";
                    boolean minEinPlayer = false;
                    for (int i = 0; i < players.size(); i++) {
                        if (shots[i]) {
                            minEinPlayer = true;
                            shottext = shottext + "T" + players.get(i).id + "#";
                            players.get(i).shotCountPlus();
                        }
                    }
                    shottext = shottext + "TB";
                    if (minEinPlayer) {
                        BluetoothAdapterClass.WriteBT(shottext);
                        Getrunken = false;
                        NL_Getrunken.setForeground(getResources().getDrawable(R.drawable.kreuz_transparent));
                    }

                    reset();
                }else {
                    Toast.makeText(getApplicationContext(), "Warte auf Bestätigung", Toast.LENGTH_LONG).show();
                }**/
            }
        });
    }

    public void reset(){
        for(int i=0; i<players.size(); i++){
            shots[i]=false;
            layouts[i].setForeground(null);
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bedienung.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Status = readMessage;
                    Toast.makeText(getApplicationContext(), Status, Toast.LENGTH_LONG).show();
                    if(Status.contains("1")){
                        Getrunken = true;
                        NL_Getrunken.setForeground(null);
                        Status="";
                    }
                    break;
            }
        }
    };
}
