package com.drink.shotbotapp.shot_botapp;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.ArrayList;

public class Tron extends AppCompatActivity {

    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;
    ImageView looserPic;
    ImageView replay;
    PopupWindow mpopupLooser;
    View popUpViewLooser;
    TextView looserName;
    Button startButton;
    int playerblaupos;
    int playergruenpos;
    PopupWindow mpopupBlau;
    View popUpViewBlau;
    ListView lvPlayer;
    PopupWindow mpopupGruen;
    View popUpViewGruen;
    ListView lvPlayer2;
    ArrayList<Player>players;
    ArrayList<Player>players2;
    TextView playerb;
    TextView playerg;
    int countdown=3;
    TextView countdownView;
    boolean greenReady=false;
    boolean blueReady=false;
    ImageView btnReadyBlue;
    ImageView btnReadyGreen;
    boolean stop = false;
    int bluePosx;
    int bluePosy;
    int greenPosx;
    int greenPosy;
    ImageView blueUp;
    ImageView blueDown;
    ImageView greenUp;
    ImageView greenDown;
    ImageView spielfeld;
    Bitmap feldMap;
    Bitmap feldMap2;
    long count=0;
    /**
     * 0 = Leer
     * 1 = Blau aktiv
     * 2 = Blau passiv
     * 3 = Grün aktiv
     * 4 = Grün passiv
     */
    int[][] matrix;
    int[][] matrix2;
    /**
     * 0 = oben
     * 1 = rechts
     * 2 = unten
     * 3 = links
     */
    int dirGreen = 3;
    int dirBlue = 1;
    int width;
    int height;
    int feldcolor;
    boolean threadrunning = false;
    int colorBlue;
    int colorGreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tron);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        colorBlue = getResources().getColor(R.color.blueTron);
        colorGreen = getResources().getColor(R.color.greenTron);
        countdownView = findViewById(R.id.tronCountdown);
        countdownView.setVisibility(View.INVISIBLE);
        btnReadyBlue= findViewById(R.id.tronReadyBlau);
        btnReadyGreen= findViewById(R.id.tronReadyGreen);
        players = PlayersClass.getPlayerList();
        players2 = PlayersClass.getPlayerList();
        Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
        feldMap = BitmapFactory.decodeResource(getResources(), R.drawable.tron_feld).copy( Bitmap.Config.ARGB_8888 , true);
        feldMap2 = BitmapFactory.decodeResource(getResources(), R.drawable.tron_feld).copy( Bitmap.Config.ARGB_8888 , true);
        feldMap = (Bitmap.createScaledBitmap(feldMap, 1200, 900, false));
        feldMap2 = (Bitmap.createScaledBitmap(feldMap2, 1200, 900, false));
        spielfeld = (ImageView)findViewById(R.id.tronImage);
        blueUp = (ImageView)findViewById(R.id.tronBlueUp);
        blueDown = (ImageView)findViewById(R.id.tronBlueDown);
        greenUp = (ImageView)findViewById(R.id.tronGreenUp);
        greenDown = (ImageView)findViewById(R.id.tronGreenDown);
        width= feldMap.getWidth();
        height = feldMap.getHeight();
        matrix = new int[width][height];
        matrix2 = new int[width][height];
        startButton = findViewById(R.id.tronStartButton);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startButton.setVisibility(View.INVISIBLE);
                mpopupBlau.showAtLocation(popUpViewBlau, Gravity.CENTER, 0, 0); // Displaying popup
            }
        });

        popUpViewBlau = getLayoutInflater().inflate(R.layout.tron_popup_blau,
                null); // inflating popup layout
        mpopupBlau = new PopupWindow(popUpViewBlau, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopupBlau.setAnimationStyle(android.R.style.Animation_Dialog);
        popUpViewGruen = getLayoutInflater().inflate(R.layout.tron_popup_gruen,
                null); // inflating popup layout
        mpopupGruen = new PopupWindow(popUpViewGruen, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopupGruen.setAnimationStyle(android.R.style.Animation_Dialog);

        popUpViewLooser = getLayoutInflater().inflate(R.layout.tron_popup_looser,
                null); // inflating popup layout
        mpopupLooser = new PopupWindow(popUpViewLooser, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopupLooser.setAnimationStyle(android.R.style.Animation_Dialog);

        replay = popUpViewLooser.findViewById(R.id.tronEndRematch);
        looserPic = popUpViewLooser.findViewById(R.id.tronEndPic);

        replay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dirGreen = 3;
                dirBlue = 1;
                blueReady = false;
                greenReady = false;
                btnReadyBlue.setImageResource(R.drawable.notready);
                btnReadyGreen.setImageResource(R.drawable.notready);
                feldMap = BitmapFactory.decodeResource(getResources(), R.drawable.tron_feld).copy( Bitmap.Config.ARGB_8888 , true);
                feldMap2 = BitmapFactory.decodeResource(getResources(), R.drawable.tron_feld).copy( Bitmap.Config.ARGB_8888 , true);
                feldMap = (Bitmap.createScaledBitmap(feldMap, 1200, 900, false));
                feldMap2 = (Bitmap.createScaledBitmap(feldMap2, 1200, 900, false));
                bluePosx= (int)(width*0.1);
                bluePosy= (int)(height*0.5);
                greenPosx = (int)(width*0.9);
                greenPosy = (int)(height*0.5);
                for(int i=bluePosx-5; i<bluePosx+6; i++){
                    for(int j=bluePosy-5; j<bluePosy+6; j++){
                        feldMap.setPixel(i,j,colorBlue);
                    }
                }

                for(int i=greenPosx-5; i<greenPosx+6; i++){
                    for(int j=greenPosy-5; j<greenPosy+6; j++){
                        feldMap.setPixel(i,j,colorGreen);
                    }
                }
                spielfeld.setImageBitmap(feldMap);
                count =0;
                countdown = 3;
                countdownView.setText(""+3);
                btnReadyBlue.setClickable(true);
                btnReadyGreen.setClickable(true);
                mpopupLooser.dismiss();
            }
        });

        feldcolor= R.color.back;

        bluePosx= (int)(width*0.1);
        bluePosy= (int)(height*0.5);
        greenPosx = (int)(width*0.9);
        greenPosy = (int)(height*0.5);

        lvPlayer = (ListView)popUpViewBlau.findViewById(R.id.lv_tron_blau);
        lvPlayer2 = (ListView)popUpViewGruen.findViewById(R.id.lv_tron_gruen);
        setupListview();

        for(int i=bluePosx-5; i<bluePosx+6; i++){
            for(int j=bluePosy-5; j<bluePosy+6; j++){
                feldMap.setPixel(i,j,colorBlue);
            }
        }

        for(int i=greenPosx-5; i<greenPosx+6; i++){
            for(int j=greenPosy-5; j<greenPosy+6; j++){
                feldMap.setPixel(i,j,colorGreen);
            }
        }

        spielfeld.setImageBitmap(feldMap);
        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });
        btnReadyBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(blueReady){
                    blueReady = false;
                    btnReadyBlue.setImageResource(R.drawable.notready);
                }else{
                    blueReady = true;
                    btnReadyBlue.setImageResource(R.drawable.ready);
                    if(greenReady){
                        countdownView.setVisibility(View.VISIBLE);
                        btnReadyGreen.setClickable(false);
                        btnReadyBlue.setClickable(false);
                        timerClass tc = new timerClass();
                        tc.start();
                    }
                }
            }
        });

        btnReadyGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(greenReady){
                    greenReady = false;
                    btnReadyGreen.setImageResource(R.drawable.notready);
                }else{
                    greenReady = true;
                    btnReadyGreen.setImageResource(R.drawable.ready);
                    if(blueReady){
                        countdownView.setVisibility(View.VISIBLE);
                        btnReadyGreen.setClickable(false);
                        btnReadyBlue.setClickable(false);
                        timerClass tc = new timerClass();
                        tc.start();
                    }
                }
            }
        });

        greenDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dirGreen==0){
                    dirGreen = 3;
                }else if(dirGreen==1){
                    dirGreen = 0;
                }else if(dirGreen==2){
                    dirGreen = 1;
                }else if(dirGreen==3){
                    dirGreen = 2;
                }
            }
        });
        greenUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dirGreen==0){
                    dirGreen = 1;
                }else if(dirGreen==1){
                    dirGreen = 2;
                }else if(dirGreen==2){
                    dirGreen = 3;
                }else if(dirGreen==3){
                    dirGreen = 0;
                }
            }
        });
        blueDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dirBlue==0){
                    dirBlue = 1;
                }else if(dirBlue==1){
                    dirBlue = 2;
                }else if(dirBlue==2){
                    dirBlue = 3;
                }else if(dirBlue==3){
                    dirBlue = 0;
                }
            }
        });
        blueUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dirBlue==0){
                    dirBlue = 3;
                }else if(dirBlue==1){
                    dirBlue = 0;
                }else if(dirBlue==2){
                    dirBlue = 1;
                }else if(dirBlue==3){
                    dirBlue = 2;
                }
            }
        });
    }

    public boolean checkCrash(int color){
        if(color == getResources().getColor(R.color.back)){
            return false;
        }else{
            return true;
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what==1){
                if(!stop){
                    newnew();
                }
            }else if(msg.what==0){
                countdown--;
                countdownView.setText(""+countdown);
            }else if(msg.what==2){
                if(threadrunning){
                    stop = false;
                }else{
                    stop = false;
                    countdownView.setVisibility(View.INVISIBLE);
                    threadrunning = true;
                    timerClass2 tc2 = new timerClass2();
                    tc2.start();
                }

            }
        }
    };
    public class timerClass extends Thread{

        public void run(){
            for(int i=0; i<3; i++){
                try {
                    Thread.sleep(1000);
                    mHandler.obtainMessage(0).sendToTarget();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            mHandler.obtainMessage(2).sendToTarget();
        }
    }

    public class timerClass2 extends Thread{

        public void run(){
            while(true){
                try {
                    Thread.sleep(70);
                    if(!stop){
                        mHandler.obtainMessage(1).sendToTarget();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void newnew(){
        if(count%2==0){
            if(dirBlue==1){
                for(int i=bluePosx-5; i<bluePosx+16; i++){
                    for(int j=bluePosy-5; j<bluePosy+6; j++){
                        if(i==bluePosx+15){
                            if(checkCrash(feldMap.getPixel(i,j))) {
                                stop = true;
                                loose(0);
                                break;
                            }
                        }
                        feldMap2.setPixel(i,j,colorBlue);
                    }
                }
                bluePosx+=10;
            }else if(dirBlue==2){
                for(int i=bluePosx-5; i<bluePosx+6; i++){
                    for(int j=bluePosy-5; j<bluePosy+16; j++){
                        if(j==bluePosy+15){
                            if(checkCrash(feldMap.getPixel(i,j))) {
                                stop = true;
                                loose(0);
                                break;
                            }
                        }
                        feldMap2.setPixel(i,j,colorBlue);
                    }
                }
                bluePosy+=10;
            }else if(dirBlue==0){
                for(int i=bluePosx-5; i<bluePosx+6; i++){
                    for(int j=bluePosy-15; j<bluePosy+6; j++){
                        if(j==bluePosy-15){
                            if(checkCrash(feldMap.getPixel(i,j))) {
                                stop = true;
                                loose(0);
                                break;
                            }
                        }
                        feldMap2.setPixel(i,j,colorBlue);
                    }
                }
                bluePosy-=10;
            }else if(dirBlue==3){
                for(int i=bluePosx-15; i<bluePosx+6; i++){
                    for(int j=bluePosy-5; j<bluePosy+6; j++){
                        if(i==bluePosx-15){
                            if(checkCrash(feldMap.getPixel(i,j))) {
                                stop = true;
                                loose(0);
                                break;
                            }
                        }
                        feldMap2.setPixel(i,j,colorBlue);
                    }
                }
                bluePosx-=10;
            }
            if(dirGreen==1){
                for(int i=greenPosx-5; i<greenPosx+16; i++){
                    for(int j=greenPosy-5; j<greenPosy+6; j++){
                        if(i==greenPosx+15){
                            if(checkCrash(feldMap.getPixel(i,j))) {
                                stop = true;
                                loose(1);
                                break;
                            }
                        }
                        feldMap2.setPixel(i,j,colorGreen);
                    }
                }
                greenPosx+=10;
            }else if(dirGreen==2){
                for(int i=greenPosx-5; i<greenPosx+6; i++){
                    for(int j=greenPosy-5; j<greenPosy+16; j++){
                        if(j==greenPosy+15){
                            if(checkCrash(feldMap.getPixel(i,j))) {
                                stop = true;
                                loose(1);
                                break;
                            }
                        }
                        feldMap2.setPixel(i,j,colorGreen);
                    }
                }
                greenPosy+=10;
            }else if(dirGreen==0){
                for(int i=greenPosx-5; i<greenPosx+6; i++){
                    for(int j=greenPosy-15; j<greenPosy+6; j++){
                        if(j==greenPosy-15){
                            if(checkCrash(feldMap.getPixel(i,j))) {
                                stop = true;
                                loose(1);
                                break;
                            }
                        }
                        feldMap2.setPixel(i,j,colorGreen);
                    }
                }
                greenPosy-=10;
            }else if(dirGreen==3){
                for(int i=greenPosx-15; i<greenPosx+6; i++){
                    for(int j=greenPosy-5; j<greenPosy+6; j++){
                        if(i==greenPosx-15){
                            if(checkCrash(feldMap.getPixel(i,j))) {
                                stop = true;
                                loose(1);
                                break;
                            };
                        }
                        feldMap2.setPixel(i,j,colorGreen);
                    }
                }
                greenPosx-=10;
            }
            spielfeld.setImageBitmap(feldMap2);
        }else{
            if(dirBlue==1){
                for(int i=bluePosx-5; i<bluePosx+16; i++){
                    for(int j=bluePosy-5; j<bluePosy+6; j++){
                        if(i==bluePosx+15){
                            if(checkCrash(feldMap2.getPixel(i,j))) {
                                stop = true;
                                loose(0);
                                break;
                            };
                        }
                        feldMap.setPixel(i,j,colorBlue);
                    }
                }
                bluePosx+=10;
            }else if(dirBlue==2){
                for(int i=bluePosx-5; i<bluePosx+6; i++){
                    for(int j=bluePosy-5; j<bluePosy+16; j++){
                        if(j==bluePosy+15){
                            if(checkCrash(feldMap2.getPixel(i,j))) {
                                stop = true;
                                loose(0);
                                break;
                            };;
                        }
                        feldMap.setPixel(i,j,colorBlue);
                    }
                }
                bluePosy+=10;
            }else if(dirBlue==0){
                for(int i=bluePosx-5; i<bluePosx+6; i++){
                    for(int j=bluePosy-15; j<bluePosy+6; j++){
                        if(j==bluePosy-15){
                            if(checkCrash(feldMap2.getPixel(i,j))) {
                                stop = true;
                                loose(0);
                                break;
                            };
                        }
                        feldMap.setPixel(i,j,colorBlue);
                    }
                }
                bluePosy-=10;
            }else if(dirBlue==3){
                for(int i=bluePosx-15; i<bluePosx+6; i++){
                    for(int j=bluePosy-5; j<bluePosy+6; j++){
                        if(i==bluePosx-15){
                            if(checkCrash(feldMap2.getPixel(i,j))) {
                                stop = true;
                                loose(0);
                                break;
                            };
                        }
                        feldMap.setPixel(i,j,colorBlue);
                    }
                }
                bluePosx-=10;
            }
            if(dirGreen==1){
                for(int i=greenPosx-5; i<greenPosx+16; i++){
                    for(int j=greenPosy-5; j<greenPosy+6; j++){
                        if(i==greenPosx+15){
                            if(checkCrash(feldMap2.getPixel(i,j))) {
                                stop = true;
                                loose(1);
                                break;
                            };
                        }
                        feldMap.setPixel(i,j,colorGreen);
                    }
                }
                greenPosx+=10;
            }else if(dirGreen==2){
                for(int i=greenPosx-5; i<greenPosx+6; i++){
                    for(int j=greenPosy-5; j<greenPosy+16; j++){
                        if(j==greenPosy+15){
                            if(checkCrash(feldMap2.getPixel(i,j))) {
                                stop = true;
                                loose(1);
                                break;
                            };
                        }
                        feldMap.setPixel(i,j,colorGreen);
                    }
                }
                greenPosy+=10;
            }else if(dirGreen==0){
                for(int i=greenPosx-5; i<greenPosx+6; i++){
                    for(int j=greenPosy-15; j<greenPosy+6; j++){
                        if(j==greenPosy-15){
                            if(checkCrash(feldMap2.getPixel(i,j))) {
                                stop = true;
                                loose(1);
                                break;
                            };
                        }
                        feldMap.setPixel(i,j,colorGreen);
                    }
                }
                greenPosy-=10;
            }else if(dirGreen==3){
                for(int i=greenPosx-15; i<greenPosx+6; i++){
                    for(int j=greenPosy-5; j<greenPosy+6; j++){
                        if(i==greenPosx-15){
                            if(checkCrash(feldMap2.getPixel(i,j))) {
                                stop = true;
                                loose(1);
                                break;
                            };
                        }
                        feldMap.setPixel(i,j,colorGreen);
                    }
                }
                greenPosx-=10;
            }
            spielfeld.setImageBitmap(feldMap);
        }
        count++;
    }

    private class PlayerListAdapter extends ArrayAdapter<Player> {
        public PlayerListAdapter() {
            super(Tron.this, R.layout.tronentryblau, players);


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.tronentryblau, parent, false);
            }

            //Find Item
            final Player player = players.get(position);

            //Insert in View
            TextView b = (TextView) itemView.findViewById(R.id.r_listentrytron);
            if(b != null){
                b.setText(player.getName());

                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        playerblaupos = position;
                        players2.remove(position);
                        mpopupBlau.dismiss();
                        mpopupGruen.showAtLocation(popUpViewGruen, Gravity.CENTER, 0, 0); // Displaying popup
                    }
                });
            }
            return itemView;
        }
    }

    private class PlayerListAdapter2 extends ArrayAdapter<Player> {
        public PlayerListAdapter2() {
            super(Tron.this, R.layout.tronentrygruen, players2);


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.tronentrygruen, parent, false);
            }

            //Find Item

                final Player player = players2.get(position);
                //Insert in View
                TextView b = (TextView) itemView.findViewById(R.id.r_listentrytron2);
                if(b != null){
                    b.setText(player.getName());

                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            playergruenpos = position;
                            mpopupGruen.dismiss();
                        }
                    });
                }



            return itemView;
        }
    }

    public void setupListview(){
        try {
            ArrayAdapter<Player> adapter = new Tron.PlayerListAdapter();
            lvPlayer.setAdapter(adapter);
            ArrayAdapter<Player> adapter2 = new Tron.PlayerListAdapter2();
            lvPlayer2.setAdapter(adapter2);

        } catch (Exception e) {
        }
    }

    public void loose(int loosernr){
        if(loosernr==0){
            //Blau verloren
            players.get(playerblaupos).oneShot();
            looserPic.setImageResource(R.drawable.trongruengewinnt);
            replay.setImageResource(R.drawable.tronrematchgruen);
        }else if(loosernr==1){
            //Grün verloren
            players.get(playergruenpos).oneShot();
            looserPic.setImageResource(R.drawable.tronblaugewinnt);
            replay.setImageResource(R.drawable.tronrematchblau);
        }
        mpopupLooser.showAtLocation(popUpViewLooser, Gravity.CENTER, 0, 0); // Displaying popup
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }
}
