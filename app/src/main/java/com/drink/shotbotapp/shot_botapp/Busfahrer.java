package com.drink.shotbotapp.shot_botapp;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.wajahatkarim3.easyflipview.EasyFlipView;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Busfahrer extends AppCompatActivity {

    String Status;
    Boolean Getrunken = true;
    ImageView ampel;
    boolean locked = true; //lock während Animation
    EasyFlipView ev1;
    EasyFlipView ev2;
    EasyFlipView ev3;
    EasyFlipView ev4;
    EasyFlipView ev5;
    EasyFlipView[] flips;
    TextView card1vorne;
    TextView card1hinten;
    TextView card2vorne;
    TextView card2hinten;
    TextView card3vorne;
    TextView card3hinten;
    TextView card4vorne;
    TextView card4hinten;
    TextView card5vorne;
    TextView card5hinten;
    ImageButton btn_left;
    ImageButton btn_right;
    CardDeck cardDeck;
    ArrayList<Player> players;
    int currentPlayer;
    Card card1;
    Card card2;
    Card card3;
    Card card4;
    Card card5;
    PopupWindow mpopup;
    View popUpView;
    ImageView playerPic;
    TextView playerName;
    /**
     * 0: Startzustand
     * 1: Farbe richtig
     * 2: Höher/Tiefer richtig
     * 3: Zwischen richtig
     * 4: Gleich richtig
     * 5: Ass richtig
     * 10: Vor Start
     */
    int state =10;

    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busfahrer);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ampel = findViewById(R.id.ampel);
        BluetoothAdapterClass.setHandler(mHandler);
        popUpView = getLayoutInflater().inflate(R.layout.bus_popup,
                null); // inflating popup layout
        mpopup = new PopupWindow(popUpView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopup.setAnimationStyle(android.R.style.Animation_Dialog);
        playerPic = popUpView.findViewById(R.id.busPopupPic);
        playerName = popUpView.findViewById(R.id.busPopupName);

        playerPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpopup.dismiss();
                locked = false;
            }
        });

        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });

        ev1 = (EasyFlipView)findViewById(R.id.bus_flip1);
        ev2 = (EasyFlipView)findViewById(R.id.bus_flip2);
        ev3 = (EasyFlipView)findViewById(R.id.bus_flip3);
        ev4 = (EasyFlipView)findViewById(R.id.bus_flip4);
        ev5 = (EasyFlipView)findViewById(R.id.bus_flip5);
        flips = new EasyFlipView[5];
        flips[0] = ev1;
        flips[1] = ev2;
        flips[2] = ev3;
        flips[3] = ev4;
        flips[4] = ev5;
        card1vorne = (TextView)findViewById(R.id.bus_card1_1);
        card1hinten = (TextView)findViewById(R.id.bus_card1_2);
        card2vorne = (TextView)findViewById(R.id.bus_card2_1);
        card2hinten = (TextView)findViewById(R.id.bus_card2_2);
        card3vorne = (TextView)findViewById(R.id.bus_card3_1);
        card3hinten = (TextView)findViewById(R.id.bus_card3_2);
        card4vorne = (TextView)findViewById(R.id.bus_card4_1);
        card4hinten = (TextView)findViewById(R.id.bus_card4_2);
        card5vorne = (TextView)findViewById(R.id.bus_card5_1);
        card5hinten = (TextView)findViewById(R.id.bus_card5_2);
        btn_left = (ImageButton)findViewById(R.id.busfahren_btnLeft);
        btn_right = (ImageButton)findViewById(R.id.busfahren_btnRight);

        cardDeck = new CardDeck();
        cardDeck.shuffle();
        Random rand = new Random();
        int randomNum = rand.nextInt(6) ;
        for(int i=0; i<randomNum; i++){
            cardDeck.shuffle();
        }
        players = PlayersClass.getPlayerList();
        currentPlayer =0;

        card1 = cardDeck.getCard(0);
        card2 = cardDeck.getCard(1);
        card3 = cardDeck.getCard(2);
        card4 = cardDeck.getCard(3);
        card5 = cardDeck.getCard(4);
        String uri = card1.getPic();
        int imageResource = getResources().getIdentifier(uri, null, getPackageName());
        Drawable draw = getResources().getDrawable(imageResource);
        card1vorne.setBackground(draw);
        String uri2 = card2.getPic();
        int imageResource2 = getResources().getIdentifier(uri2, null, getPackageName());
        Drawable draw2 = getResources().getDrawable(imageResource2);
        card2vorne.setBackground(draw2);
        String uri3 = card3.getPic();
        int imageResource3 = getResources().getIdentifier(uri3, null, getPackageName());
        Drawable draw3 = getResources().getDrawable(imageResource3);
        card3vorne.setBackground(draw3);
        String uri4 = card4.getPic();
        int imageResource4 = getResources().getIdentifier(uri4, null, getPackageName());
        Drawable draw4 = getResources().getDrawable(imageResource4);
        card4vorne.setBackground(draw4);
        String uri5 = card5.getPic();
        int imageResource5 = getResources().getIdentifier(uri5, null, getPackageName());
        Drawable draw5 = getResources().getDrawable(imageResource5);
        card5vorne.setBackground(draw5);
        btn_right.setVisibility(View.INVISIBLE);
        btn_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Getrunken==true) {
                    if(state == 10){
                        state =0;
                        mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
                        btn_left.setBackground(getResources().getDrawable(R.drawable.busfahrer_rot_button));
                        btn_right.setBackground(getResources().getDrawable(R.drawable.busfahrer_schwarz_button));
                        btn_right.setVisibility(View.VISIBLE);
                    }else{
                        if(!locked){
                            calculate(0);
                        }
                    }
                }
            }
        });
        btn_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Getrunken==true) {
                    if(!locked){
                        calculate(1);
                    }
                }
            }
        });

        playerName.setText(players.get(currentPlayer).getName());
        playerPic.setImageResource(players.get(currentPlayer).getColor());
        if(players.get(currentPlayer).bild!=null){
            playerPic.setImageBitmap(players.get(currentPlayer).bild);
            playerPic.setForeground(null);
        }else{
            playerPic.setForeground(players.get(currentPlayer).playerPic);
        }


    }

    public void calculate(int a){
        locked = true;
        if(state==0){
            for(int i=0; i<= ThreadLocalRandom.current().nextInt(0, 5); i++){
                cardDeck.shuffle();
            }
            cardDeck.shuffle();
            card1 = cardDeck.getCard(0);
            card2 = cardDeck.getCard(1);
            card3 = cardDeck.getCard(2);
            card4 = cardDeck.getCard(3);
            card5 = cardDeck.getCard(4);
            String uri = card1.getPic();
            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
            Drawable draw = getResources().getDrawable(imageResource);
            card1vorne.setBackground(draw);
            String uri2 = card2.getPic();
            int imageResource2 = getResources().getIdentifier(uri2, null, getPackageName());
            Drawable draw2 = getResources().getDrawable(imageResource2);
            card2vorne.setBackground(draw2);
            String uri3 = card3.getPic();
            int imageResource3 = getResources().getIdentifier(uri3, null, getPackageName());
            Drawable draw3 = getResources().getDrawable(imageResource3);
            card3vorne.setBackground(draw3);
            String uri4 = card4.getPic();
            int imageResource4 = getResources().getIdentifier(uri4, null, getPackageName());
            Drawable draw4 = getResources().getDrawable(imageResource4);
            card4vorne.setBackground(draw4);
            String uri5 = card5.getPic();
            int imageResource5 = getResources().getIdentifier(uri5, null, getPackageName());
            Drawable draw5 = getResources().getDrawable(imageResource5);
            card5vorne.setBackground(draw5);
            flips[state].flipTheView();
            //rot
            if(a==0){
                if(card1.getColor().equals("Herz")||card1.getColor().equals("Karo")){
                    MediaPlayer quiz_right = MediaPlayer.create(this, R.raw.richtige_antwort);
                    quiz_right.start();
                    step();
                }else{
                    MediaPlayer quiz_false = MediaPlayer.create(this, R.raw.falsche_antwort);
                    quiz_false.start();
                    players.get(currentPlayer).oneShot();
                    if(!SystemState.isOffline()) {
                        Getrunken = false;
                        ampel.setImageResource(R.drawable.ampel_rot);
                    }
                    timerClass tc = new timerClass();
                    tc.start();
                }
            }
            //schwarz
            else{
                if(card1.getColor().equals("Pik")||card1.getColor().equals("Kreuz")){
                    MediaPlayer quiz_right = MediaPlayer.create(this, R.raw.richtige_antwort);
                    quiz_right.start();
                    step();
                }else{
                    MediaPlayer quiz_false = MediaPlayer.create(this, R.raw.falsche_antwort);
                    quiz_false.start();
                    players.get(currentPlayer).oneShot();
                    if(!SystemState.isOffline()) {
                        Getrunken = false;
                        ampel.setImageResource(R.drawable.ampel_rot);
                    }
                    timerClass tc = new timerClass();
                    tc.start();
                }
            }
        }else if(state==1){
            flips[state].flipTheView();
            //höher
            if(a==0){
                if(card1.getValue()<card2.getValue()){
                    MediaPlayer quiz_right = MediaPlayer.create(this, R.raw.richtige_antwort);
                    quiz_right.start();
                    step();
                }else{
                    MediaPlayer quiz_false = MediaPlayer.create(this, R.raw.falsche_antwort);
                    quiz_false.start();
                    players.get(currentPlayer).oneShot();
                    if(!SystemState.isOffline()) {
                        Getrunken = false;
                        ampel.setImageResource(R.drawable.ampel_rot);
                    }
                    timerClass tc = new timerClass();
                    tc.start();
                }
            }
            //tiefer
            else{
                if(card1.getValue()>card2.getValue()){
                    MediaPlayer quiz_right = MediaPlayer.create(this, R.raw.richtige_antwort);
                    quiz_right.start();
                    step();
                }else{
                    MediaPlayer quiz_false = MediaPlayer.create(this, R.raw.falsche_antwort);
                    quiz_false.start();
                    players.get(currentPlayer).oneShot();
                    if(!SystemState.isOffline()) {
                        Getrunken = false;
                        ampel.setImageResource(R.drawable.ampel_rot);
                    }
                    timerClass tc = new timerClass();
                    tc.start();
                }
            }
        }else if(state==2){
            flips[state].flipTheView();
            //zwischen
            if(a==0){
                if((card1.getValue()<card3.getValue() && card3.getValue() < card2.getValue()) ||
                        (card1.getValue()>card3.getValue() && card3.getValue() > card2.getValue())){
                    MediaPlayer quiz_right = MediaPlayer.create(this, R.raw.richtige_antwort);
                    quiz_right.start();
                    step();
                }else{
                    MediaPlayer quiz_false = MediaPlayer.create(this, R.raw.falsche_antwort);
                    quiz_false.start();
                    players.get(currentPlayer).oneShot();
                    if(!SystemState.isOffline()) {
                        Getrunken = false;
                        ampel.setImageResource(R.drawable.ampel_rot);
                    }
                    timerClass tc = new timerClass();
                    tc.start();
                }
            }
            //nicht zwischen
            else{
                if(!((card1.getValue()<card3.getValue() && card3.getValue() < card2.getValue()) ||
                        (card1.getValue()>card3.getValue() && card3.getValue() > card2.getValue()))){
                    MediaPlayer quiz_right = MediaPlayer.create(this, R.raw.richtige_antwort);
                    quiz_right.start();
                    step();
                }else{
                    MediaPlayer quiz_false = MediaPlayer.create(this, R.raw.falsche_antwort);
                    quiz_false.start();
                    players.get(currentPlayer).oneShot();
                    if(!SystemState.isOffline()) {
                        Getrunken = false;
                        ampel.setImageResource(R.drawable.ampel_rot);
                    }
                    timerClass tc = new timerClass();
                    tc.start();
                }
            }
        }else if(state==3){
            flips[state].flipTheView();
            //gleich
            if(a==0){
                if(card3.getValue() == card4.getValue()){
                    MediaPlayer quiz_right = MediaPlayer.create(this, R.raw.richtige_antwort);
                    quiz_right.start();
                    step();
                }else{
                    MediaPlayer quiz_false = MediaPlayer.create(this, R.raw.falsche_antwort);
                    quiz_false.start();
                    players.get(currentPlayer).oneShot();
                    if(!SystemState.isOffline()) {
                        Getrunken = false;
                        ampel.setImageResource(R.drawable.ampel_rot);
                    }
                    timerClass tc = new timerClass();
                    tc.start();
                }
            }
            //ungleich
            else{
                if(card3.getValue() != card4.getValue()){
                    MediaPlayer quiz_right = MediaPlayer.create(this, R.raw.richtige_antwort);
                    quiz_right.start();
                    step();
                }else{
                    MediaPlayer quiz_false = MediaPlayer.create(this, R.raw.falsche_antwort);
                    quiz_false.start();
                    players.get(currentPlayer).oneShot();
                    if(!SystemState.isOffline()) {
                        Getrunken = false;
                        ampel.setImageResource(R.drawable.ampel_rot);
                    }
                    timerClass tc = new timerClass();
                    tc.start();
                }
            }
        }
        else if(state==4){
            flips[state].flipTheView();
            //Ass
            if(a==0){
                if(card5.getValue() == 14){
                    MediaPlayer quiz_right = MediaPlayer.create(this, R.raw.richtige_antwort);
                    quiz_right.start();
                    step();
                }else{
                    MediaPlayer quiz_false = MediaPlayer.create(this, R.raw.falsche_antwort);
                    quiz_false.start();
                    players.get(currentPlayer).oneShot();
                    if(!SystemState.isOffline()) {
                        Getrunken = false;
                        ampel.setImageResource(R.drawable.ampel_rot);
                    }
                    timerClass tc = new timerClass();
                    tc.start();
                }
            }
            //kein Ass
            else{
                if(card5.getValue() != 14){
                    MediaPlayer quiz_right = MediaPlayer.create(this, R.raw.richtige_antwort);
                    quiz_right.start();
                    step();
                }else{
                    MediaPlayer quiz_false = MediaPlayer.create(this, R.raw.falsche_antwort);
                    quiz_false.start();
                    players.get(currentPlayer).oneShot();
                    if(!SystemState.isOffline()) {
                        Getrunken = false;
                        ampel.setImageResource(R.drawable.ampel_rot);
                    }
                    timerClass tc = new timerClass();
                    tc.start();
                }
            }
        }
    }

    public void step(){
        locked = false;
        if(state==0){
            state =1;
            btn_left.setBackground(getResources().getDrawable(R.drawable.busfahrer_hoeher_button));
            btn_right.setBackground(getResources().getDrawable(R.drawable.busfahrer_tiefer_button));
        }
        else if(state==1){
            state =2;
            btn_left.setBackground(getResources().getDrawable(R.drawable.busfahrer_dazwischen_button));
            btn_right.setBackground(getResources().getDrawable(R.drawable.busfahrer_nichtdazwischen_button));
        }
        else if(state==2){
            state =3;
            btn_left.setBackground(getResources().getDrawable(R.drawable.busfahrer_gleich_button));
            btn_right.setBackground(getResources().getDrawable(R.drawable.busfahrer_ungleich_button));
        }
        else if(state==3){
            state =4;
            btn_left.setBackground(getResources().getDrawable(R.drawable.busfahrer_ass_button));
            btn_right.setBackground(getResources().getDrawable(R.drawable.busfahrer_keinass_button));
        }
        else if(state==4){
            timerClass tc = new timerClass();
            tc.start();
            //PopUp
            if(currentPlayer<players.size()-1){
                currentPlayer++;
            }else{
                currentPlayer =0;
            }
            playerName.setText(players.get(currentPlayer).getName());
            playerPic.setImageResource(players.get(currentPlayer).getColor());
            if(players.get(currentPlayer).bild!=null){
                playerPic.setImageBitmap(players.get(currentPlayer).bild);
                playerPic.setForeground(null);
            }else{
                playerPic.setForeground(players.get(currentPlayer).playerPic);
            }
            mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
        }
    }

    public void reset(int anzahl){
        locked = false;
        state =0;
        btn_left.setBackground(getResources().getDrawable(R.drawable.busfahrer_rot_button));
        btn_right.setBackground(getResources().getDrawable(R.drawable.busfahrer_schwarz_button));
        for(int i=0; i<anzahl; i++){
            flips[i].flipTheView();
        }
        cardDeck.shuffle();
    }
    private final Handler mHandler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what==0){
                mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
            }else{
                reset(state+1);
            }
        }
    };
    public class timerClass extends Thread{
        public void run(){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mHandler2.obtainMessage(1).sendToTarget();
        }
    }

    public class timerClass2 extends Thread{
        public void run(){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mHandler2.obtainMessage(0).sendToTarget();
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bedienung.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Status = readMessage;
                    if(Status.contains("1")){
                        Getrunken = true;
                        ampel.setImageResource(R.drawable.ampel_gruen);
                        Status="";
                    }
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }

}
