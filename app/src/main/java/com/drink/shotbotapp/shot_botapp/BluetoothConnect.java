package com.drink.shotbotapp.shot_botapp;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.Set;

public class BluetoothConnect extends AppCompatActivity {
    ImageView b0;
    ImageView b1;
    ImageView b2;
    ImageView b3;
    LinearLayout board;
    int status=0;
    private BluetoothAdapter myBluetooth = null;
    private Set<BluetoothDevice> pairedDevices;
    public static String EXTRA_ADDRESS = "Device_Adresse";
    String[] arr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bluetooth_connection);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        b0 = findViewById(R.id.bluetooth_0);
        b1 = findViewById(R.id.bluetooth_1);
        b2 = findViewById(R.id.bluetooth_2);
        b3 = findViewById(R.id.bluetooth_3);
        board = findViewById(R.id.board);
        Runner runner = new Runner();
        runner.start();
        myBluetooth = BluetoothAdapter.getDefaultAdapter();
        if(myBluetooth == null)
        {
            Toast.makeText(getApplicationContext(), "Bluetooth Gerät nicht verfügbar", Toast.LENGTH_LONG).show();
            finish();
        }
        else if(!myBluetooth.isEnabled())
        {
            Intent turnBTon = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnBTon,1);
        }
        pairedDevices = myBluetooth.getBondedDevices();
        arr= new String[3];
        arr[0]= null;
        arr[1]= null;
        arr[2]= null;

        if(pairedDevices.size()>0){
            for(BluetoothDevice bt : pairedDevices) {
                if(bt.getName().equals("ShotBoard")){
                    board.setAlpha(1);
                    arr[0]= bt.getAddress();
                }
            }
        }
        board.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(arr[0]!=null){
                    Intent i = new Intent(BluetoothConnect.this, MainMenu.class);
                    BluetoothAdapterClass.setAddress_bt(arr[0]);
                    BluetoothAdapterClass.start();
                    startActivity(i);
                    finish();
                }
            }
        });
    }

    public class Runner extends Thread{

        public void run(){
            while (true){
                try {
                    mHandler.obtainMessage(1).sendToTarget();
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(status==0){
                b1.setVisibility(View.GONE);
                b2.setVisibility(View.GONE);
                b3.setVisibility(View.GONE);
            }else if(status==1){
                b1.setVisibility(View.VISIBLE);
                b2.setVisibility(View.GONE);
                b3.setVisibility(View.GONE);
            }else if(status==2){
                b1.setVisibility(View.VISIBLE);
                b2.setVisibility(View.VISIBLE);
                b3.setVisibility(View.GONE);
            }else if(status==3){
                b1.setVisibility(View.VISIBLE);
                b2.setVisibility(View.VISIBLE);
                b3.setVisibility(View.VISIBLE);
            }

            if(status <3){
                status++;
            }else{
                status=0;
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == Activity.RESULT_OK) {
                    finish();
                    startActivity(getIntent());
                } else {
                    // User did not enable Bluetooth or an error occurred
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(BluetoothConnect.this, MainMenu.class);
        startActivity(i);
        finish();
    }
}
