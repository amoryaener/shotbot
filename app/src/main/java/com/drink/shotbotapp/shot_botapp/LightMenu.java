package com.drink.shotbotapp.shot_botapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class LightMenu extends AppCompatActivity {

    Button ja;
    Button nein;
    LinearLayout werkPopup;
    ImageView farben;
    ImageView modi;
    RelativeLayout farbWahl;
    ImageView[] farbButtons;
    ImageView back;
    String[] colorArray;
    EditText codeField;
    ImageView codeSend;
    ImageView werkseinstellungen;
    SeekBar seekBar;
    LinearLayout codePopup;
    Button codeFertig;
    TextView codePText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_light_menu);
        farben = findViewById(R.id.lightMenuFarbe);
        modi = findViewById(R.id.lightMenuModi);
        farbWahl = findViewById(R.id.farbWahl);
        back = findViewById(R.id.colorChooseBack);
        codeField = findViewById(R.id.codeText);
        codeSend = findViewById(R.id.codeSend);
        codeField.getBackground().mutate().setColorFilter(getResources().getColor(R.color.seek_bar_progress), PorterDuff.Mode.SRC_ATOP);
        werkseinstellungen = findViewById(R.id.werk);
        werkPopup = findViewById(R.id.werkPopup);
        ja = findViewById(R.id.werkja);
        nein = findViewById(R.id.werknein);
        codePopup = findViewById(R.id.codePopup);
        codeFertig = findViewById(R.id.codePopupFertig);
        codePText = findViewById(R.id.codePopupText);
        colorArray = new String[20];
        colorArray[0]= "8003fe";
        colorArray[1]= "c003fe";
        colorArray[2]= "fe03fe";
        colorArray[3]= "fe03c0";
        colorArray[4]= "fe0380";
        colorArray[5]= "fec003";
        colorArray[6]= "fe8003";
        colorArray[7]= "fe4003";
        colorArray[8]= "fe0303";
        colorArray[9]= "fe0340";
        colorArray[10]= "fefe03";
        colorArray[11]= "c0fe03";
        colorArray[12]= "80fe03";
        colorArray[13]= "04fe03";
        colorArray[14]= "04fe40";
        colorArray[15]= "0303fe";
        colorArray[16]= "0380fe";
        colorArray[17]= "03c0fe";
        colorArray[18]= "04fec0";
        colorArray[19]= "04fe80";
        farbButtons = new ImageView[20];
        farbButtons[0]= findViewById(R.id.farbe0);
        farbButtons[1]= findViewById(R.id.farbe1);
        farbButtons[2]= findViewById(R.id.farbe2);
        farbButtons[3]= findViewById(R.id.farbe3);
        farbButtons[4]= findViewById(R.id.farbe4);
        farbButtons[5]= findViewById(R.id.farbe5);
        farbButtons[6]= findViewById(R.id.farbe6);
        farbButtons[7]= findViewById(R.id.farbe7);
        farbButtons[8]= findViewById(R.id.farbe8);
        farbButtons[9]= findViewById(R.id.farbe9);
        farbButtons[10]= findViewById(R.id.farbe10);
        farbButtons[11]= findViewById(R.id.farbe11);
        farbButtons[12]= findViewById(R.id.farbe12);
        farbButtons[13]= findViewById(R.id.farbe13);
        farbButtons[14]= findViewById(R.id.farbe14);
        farbButtons[15]= findViewById(R.id.farbe15);
        farbButtons[16]= findViewById(R.id.farbe16);
        farbButtons[17]= findViewById(R.id.farbe17);
        farbButtons[18]= findViewById(R.id.farbe18);
        farbButtons[19]= findViewById(R.id.farbe19);
        seekBar = findViewById(R.id.speedBar);
        seekBar.setMax(20);
        seekBar.setProgress(5);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                BluetoothAdapterClass.WriteBT("S "+seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        final SharedPreferences sharedPrefs = getSharedPreferences("saveCodes", 0);
        codeSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((""+codeField.getText()).equals("christmas")){
                    if(!sharedPrefs.getBoolean("christmas",false)){
                        SharedPreferences.Editor editor = getSharedPreferences("saveCodes", 0).edit();
                        editor.putBoolean("christmas", true);
                        editor.commit();
                        codePText.setText("Weihnachtsmodus jetzt unter Modi verfügbar!");
                        codePopup.setVisibility(View.VISIBLE);
                    }
                }else if ((""+codeField.getText()).equals("silvester")){
                    if(!sharedPrefs.getBoolean("silvester",false)){
                        SharedPreferences.Editor editor = getSharedPreferences("saveCodes", 0).edit();
                        editor.putBoolean("silvester", true);
                        editor.commit();
                        codePText.setText("Silvestermodus jetzt unter Modi verfügbar!");
                        codePopup.setVisibility(View.VISIBLE);
                    }
                }else if ((""+codeField.getText()).equals("halloween")){
                    if(!sharedPrefs.getBoolean("halloween",false)){
                        SharedPreferences.Editor editor = getSharedPreferences("saveCodes", 0).edit();
                        editor.putBoolean("halloween", true);
                        editor.commit();
                        codePText.setText("Halloweenmodus jetzt unter Modi verfügbar!");
                        codePopup.setVisibility(View.VISIBLE);
                    }
                }else{
                    BluetoothAdapterClass.WriteBT(""+codeField.getText());
                }
                codeField.setText("");
            }
        });

        codeFertig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                codePopup.setVisibility(View.GONE);
            }
        });

        werkseinstellungen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                werkPopup.setVisibility(View.VISIBLE);
            }
        });

        nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                werkPopup.setVisibility(View.GONE);
            }
        });

        ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                werkPopup.setVisibility(View.GONE);
                ruecksetzen();
            }
        });

        modi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LightMenu.this, LightModes.class);
                startActivity(i);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                farbWahl.setVisibility(View.GONE);
            }
        });
        farben.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                farbWahl.setVisibility(View.VISIBLE);
            }
        });
        for(int i=0; i<farbButtons.length;i++){
            final int i2 = i;
            farbButtons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    farbWahl.setVisibility(View.GONE);
                    BluetoothAdapterClass.WriteBT("C "+colorArray[i2]);
                }
            });
        }
    }
    public void ruecksetzen(){
        SharedPreferences.Editor editor = getSharedPreferences("saveCodes", 0).edit();
        editor.putBoolean("silvester", false);
        editor.putBoolean("christmas", false);
        editor.commit();
        // TODO: Implement
    }
}
