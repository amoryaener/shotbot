package com.drink.shotbotapp.shot_botapp;

import android.app.Activity;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 25.04.2018.
 */

public class SystemState {
    static boolean offline = true;
    static Activity act;
    static List<GameEntry> gameEntries;
    static int minPlayer;
    static int maxPlayer;
    static boolean[] haerteBools;
    static boolean[] katBools;
    static List<GameEntry> gameEntriesFiltered;


    public static void setOffline(){
        offline = true;
    }

    public static void filter(){
        gameEntriesFiltered = new ArrayList<GameEntry>();
        for(int i=0; i<gameEntries.size();i++){
            //diff-Filter
            if(haerteBools[gameEntries.get(i).schwierigkeit]){
                //Kat-Filter
                if(katBools[gameEntries.get(i).kategorie]){
                    //Spieler-Filter
                    if(gameEntries.get(i).minPlayer<=maxPlayer && gameEntries.get(i).maxPlayer>=minPlayer){
                        gameEntriesFiltered.add(gameEntries.get(i));
                    }
                }
            }
        }
    }

    public static boolean isOffline(){
        return offline;
    }

    public static String[] getNames(){
        String[] ausgabe = new String[gameEntriesFiltered.size()+1];
        ausgabe[0]=null;
        for(int i=0; i<gameEntriesFiltered.size(); i++){
            ausgabe[i+1]=gameEntriesFiltered.get(i).spielname;
        }
        return ausgabe;
    }

    public static String[] getBeschreibungen(){
        String[] ausgabe = new String[gameEntriesFiltered.size()+1];
        ausgabe[0]=null;
        for(int i=0; i<gameEntriesFiltered.size(); i++){
            ausgabe[i+1]=gameEntriesFiltered.get(i).beschreibung;
        }
        return ausgabe;
    }

    public static String[] getPlayers(){
        String[] ausgabe = new String[gameEntriesFiltered.size()+1];
        ausgabe[0]=null;
        for(int i=0; i<gameEntriesFiltered.size(); i++){
            ausgabe[i+1]=gameEntriesFiltered.get(i).minPlayer+"-"+gameEntriesFiltered.get(i).maxPlayer+" Spieler";
        }
        return ausgabe;
    }

    public static int[] getPictures(){
        int[] ausgabe = new int[gameEntriesFiltered.size()+1];
        ausgabe[0]=0;
        for(int i=0; i<gameEntriesFiltered.size(); i++){
            ausgabe[i+1]=gameEntriesFiltered.get(i).image;
        }
        return ausgabe;
    }

    public static int[] getColors(){
        int[] ausgabe = new int[gameEntriesFiltered.size()+1];
        ausgabe[0]=0;
        for(int i=0; i<gameEntriesFiltered.size(); i++){
            ausgabe[i+1]=gameEntriesFiltered.get(i).color;
        }
        return ausgabe;
    }

    public static Intent[] getIntents(){
        Intent[] ausgabe = new Intent[gameEntriesFiltered.size()+1];
        ausgabe[0]=null;
        for(int i=0; i<gameEntriesFiltered.size(); i++){
            ausgabe[i+1]=gameEntriesFiltered.get(i).intent;
        }
        return ausgabe;
    }

    public static String[] getSchwierigkeiten(){
        String[] ausgabe = new String[gameEntriesFiltered.size()+1];
        ausgabe[0]=null;
        for(int i=0; i<gameEntriesFiltered.size(); i++){
            if(gameEntriesFiltered.get(i).schwierigkeit==0){
                ausgabe[i+1]="Easy";
            }else if(gameEntriesFiltered.get(i).schwierigkeit==1){
                ausgabe[i+1]="Medium";
            }else if(gameEntriesFiltered.get(i).schwierigkeit==2){
                ausgabe[i+1]="Hard";
            }
        }
        return ausgabe;
    }

}
