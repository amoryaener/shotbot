package com.drink.shotbotapp.shot_botapp;

import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Hausnummer extends AppCompatActivity implements SensorEventListener {

    int count2 =0;
    boolean begrenzt = false;
    int rounds;
    ImageView ampel;
    String Status;
    Boolean Getrunken = true;
    Button weiter;
    int[] werte;
    int currentPlayer;
    ArrayList<Player> players;
    int ei;
    int ze;
    int hu;
    boolean eSet;
    boolean zSet;
    boolean hSet;
    ImageView playerpic;
    TextView playername;
    TextView hunderter;
    TextView zehner;
    TextView einer;
    ImageView dice;
    boolean hasRolled;
    int roll =5;
    private final int[] diceImages = new int[] { R.drawable.one, R.drawable.two, R.drawable.three, R.drawable.four, R.drawable.five, R.drawable.six };
    private final int rollAnimations = 50;
    private final int delayTime = 15;
    private Resources res;
    private final Random randomGen = new Random();
    private SensorManager sensorMgr;
    private Handler animationHandler;
    private long lastUpdate = -1;
    private float x, y, z;
    private float last_x, last_y, last_z;
    private boolean paused = false;
    private static final int UPDATE_DELAY = 50;
    private static final int SHAKE_THRESHOLD = 700;
    private Drawable dice2[] = new Drawable[6];
    ListView endList;
    LinearLayout startPopup;
    LinearLayout endPopup;
    Button start;
    Button endButton;
    TextView endtext;
    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hausnummer);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });
        GlobalStats.gameStartet("Hausnummer");
        startPopup = (LinearLayout)findViewById(R.id.startpopupHausnummer);
        endPopup = (LinearLayout)findViewById(R.id.endpopupHausnummer);
        endPopup.setVisibility(View.INVISIBLE);
        rounds = getIntent().getIntExtra("Rounds",0);
        if(rounds >0){
            begrenzt=true;
        }
        start = (Button)findViewById(R.id.btn_hausnrStartPopup);
        endButton = (Button)findViewById(R.id.btn_hausnrEndPopup);
        endtext = (TextView)findViewById(R.id.hausnr_endtext);
        players = PlayersClass.getPlayerList();
        werte = new int[players.size()];
        for(int i=0; i<werte.length; i++){
            werte[i]=0;
        }
        ampel = (ImageView)findViewById(R.id.ampel);
        currentPlayer =0;
        BluetoothAdapterClass.setHandler(mHandler);
        weiter = (Button)findViewById(R.id.hausnr_weiter);
        playername = (TextView)findViewById(R.id.hausnr_playername);
        playerpic = (ImageView)findViewById(R.id.hausnr_playerpic);
        hunderter = (TextView)findViewById(R.id.hausnr_hundert);
        zehner = (TextView)findViewById(R.id.hausnr_zehn);
        einer = (TextView)findViewById(R.id.hausnr_eins);
        dice = (ImageView)findViewById(R.id.hausnr_dice);
        setupListview();

        playername.setText(players.get(currentPlayer).getName());
        playerpic.setImageResource(players.get(currentPlayer).getColor());
        if(players.get(currentPlayer).bild!=null){
            playerpic.setImageBitmap(players.get(currentPlayer).bild);
            playerpic.setForeground(null);
        }else{
            playerpic.setForeground(players.get(currentPlayer).playerPic);
        }

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPopup.setVisibility(View.INVISIBLE);
            }
        });
        endButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Getrunken==true){
                    endPopup.setVisibility(View.INVISIBLE);
                    count2++;
                    if(begrenzt && rounds == count2){
                        finish();
                    }
                }
            }
        });
        weiter.setVisibility(View.INVISIBLE);
        hasRolled =false;
        eSet=false;
        hSet=false;
        zSet=false;
        paused = false;
        res = getResources();
        for (int i = 0; i < 6; i++) {
            dice2[i] = res.getDrawable(diceImages[i]);
        }
        animationHandler = new Handler() {
            public void handleMessage(Message msg) {
                dice.setImageDrawable(dice2[roll]);
            }
        };
        sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
        boolean accelSupported = sensorMgr.registerListener( this, sensorMgr.getDefaultSensor(SensorManager.SENSOR_ACCELEROMETER),	SensorManager.SENSOR_DELAY_GAME);
        if (!accelSupported) sensorMgr.unregisterListener( this); //no accelerometer on the device

        dice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!hasRolled){
                    rollDice();
                }
            }
        });

        weiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(eSet&&hSet&&zSet){
                    ei = 0;
                    ze = 0;
                    hu = 0;
                    eSet=false;
                    hSet=false;
                    zSet=false;
                    if(currentPlayer<players.size()-1){
                        currentPlayer ++;
                    }else{
                        calculateLooser();
                        if(!SystemState.isOffline()) {
                            Getrunken = false;
                            ampel.setImageResource(R.drawable.ampel_rot);
                        }
                        endPopup.setVisibility(View.VISIBLE);
                        updatePoiListView();
                        currentPlayer =0;
                    }
                    playername.setText(players.get(currentPlayer).getName());
                    playerpic.setImageResource(players.get(currentPlayer).getColor());
                    if(players.get(currentPlayer).bild!=null){
                        playerpic.setImageBitmap(players.get(currentPlayer).bild);
                        playerpic.setForeground(null);
                    }else{
                        playerpic.setForeground(players.get(currentPlayer).playerPic);
                    }
                    einer.setBackground(null);
                    zehner.setBackground(null);
                    hunderter.setBackground(null);
                    dice.setImageDrawable(dice2[5]);
                    hasRolled =false;
                    weiter.setVisibility(View.INVISIBLE);
                    einer.setClickable(true);
                    zehner.setClickable(true);
                    hunderter.setClickable(true);
                }
            }
        });

        einer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setzen(1);
            }
        });
        zehner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setzen(10);
            }
        });
        hunderter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setzen(100);
            }
        });
    }

    public void calculateLooser(){
        int min = werte[0];
        for(int i=0; i<werte.length; i++){
            if (werte[i]< min){
                min = werte[i];
            }
        }
        ArrayList<Player> looser = new ArrayList<Player>();
        for(int i=0; i<werte.length; i++){
            if (werte[i]== min){
                looser.add(players.get(i));
            }
        }
        if(looser.size()==1){
            looser.get(0).oneShot();
            endtext.setText(looser.get(0).getName()+" hat verloren!");
        }else{
            String names ="";
            String shots ="";
            for(int i=0; i<looser.size(); i++){
                names = names+ players.get(i).getName()+" ";
                shots = shots+"T"+players.get(i).id+"#";
                players.get(i).shotCountPlus();
            }
            shots = shots+"TB";
            BluetoothAdapterClass.WriteBT(shots);
            endtext.setText(names+"haben verloren!");
        }
        looser.clear();
    }

    public void setzen(int a){
        if(hasRolled){
            if(a==1){
                if(!eSet){
                    eSet=true;
                    einer.setBackground(dice2[roll]);
                    ei = roll+1;
                    einer.setClickable(false);
                }
            }else if(a==10){
                if(!zSet){
                    zSet=true;
                    zehner.setBackground(dice2[roll]);
                    ze = roll+1;
                    zehner.setClickable(false);
                }
            }else{
                if(!hSet){
                    hSet=true;
                    hunderter.setBackground(dice2[roll]);
                    hu = roll+1;
                    hunderter.setClickable(false);
                }
            }
            hasRolled=false;
        }
        if(eSet && zSet && hSet){
            werte[currentPlayer] = ei+ze*10+hu*100;
            weiter.setVisibility(View.VISIBLE);
        }
    }

    private void rollDice() {
        hasRolled =true;
        if (paused) return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < rollAnimations; i++) {
                    doRoll();
                }
            }
        }).start();
        MediaPlayer mp = MediaPlayer.create(this, R.raw.roll);
        try {
            mp.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mp.start();
    }

    private void doRoll() { // only does a single roll
        roll = randomGen.nextInt(6);
        synchronized (getLayoutInflater()) {
            animationHandler.sendEmptyMessage(0);
        }
        try { // delay to alloy for smooth animation
            Thread.sleep(delayTime);
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor mySensor = event.sensor;
        if (mySensor.getType() == SensorManager.SENSOR_ACCELEROMETER) {
            long curTime = System.currentTimeMillis();
            if ((curTime - lastUpdate) > UPDATE_DELAY) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;
                x = event.values[SensorManager.DATA_X];
                y = event.values[SensorManager.DATA_Y];
                z = event.values[SensorManager.DATA_Z];
                float speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;
                if (speed > SHAKE_THRESHOLD) { //the screen was shaked

                }
                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        return; //this method isn't used
    }

    public void setupListview(){
        try {
            ArrayAdapter<Player> adapter = new Hausnummer.PlayerListAdapter();
            endList = (ListView) findViewById(R.id.hausnrList);
            endList.setClickable(false);
            endList.setAdapter(adapter);

        } catch (Exception e) {
        }
    }

    public void updatePoiListView() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((Hausnummer.PlayerListAdapter) endList.getAdapter()).notifyDataSetChanged();
            }
        });

    }

    private class PlayerListAdapter extends ArrayAdapter<Player> {
        public PlayerListAdapter() {
            super(Hausnummer.this, R.layout.hausnr_entry, players);


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.hausnr_entry, parent, false);
            }

            //Find Item
            final Player player = players.get(position);

            itemView.setClickable(false);
            TextView name = (TextView)itemView.findViewById(R.id.hausnrEntryName);
            TextView eins = (TextView)itemView.findViewById(R.id.hnrEntryEins);
            TextView zehn = (TextView)itemView.findViewById(R.id.hnrEntryZehn);
            TextView hundert = (TextView)itemView.findViewById(R.id.hnrEntryHundert);
            eins.setClickable(false);
            zehn.setClickable(false);
            hundert.setClickable(false);

            if(werte[position]!=0){
                name.setText(player.getName());
                eins.setBackground(dice2[(werte[position]%10)-1]);
                zehn.setBackground(dice2[((werte[position]/10)%10)-1]);
                hundert.setBackground(dice2[((werte[position]/100)%10)-1]);
            }


            return itemView;
        }
    }
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bedienung.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Status = readMessage;
                    if(Status.contains("1")){
                        Getrunken = true;
                        ampel.setImageResource(R.drawable.ampel_gruen);
                        Status="";
                    }
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }

}
