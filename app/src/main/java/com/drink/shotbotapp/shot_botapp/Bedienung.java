package com.drink.shotbotapp.shot_botapp;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

public class Bedienung extends AppCompatActivity {
    ImageButton btn_M1;
    ImageButton btn_M2;
    ImageButton btn_M3;
    ImageButton btn_M4;
    ImageButton btn_M5;
    ImageButton btn_M6;
    ImageButton btn_M7;
    ImageButton btn_M8;
    ImageButton btn_M9;
    ImageButton btn_M10;
    ImageButton btn_M11;
    ImageButton btn_M12;
    ImageButton btn_M13;
    ImageButton btn_M14;
    ImageButton btn_M15;
    ImageButton btn_M16;
    ImageButton btn_M17;
    ImageButton btn_M18;
    ImageButton btn_M19;
    ImageButton btn_M20;
    ImageButton btn_M21;
    ImageButton btn_M22;
    ImageButton btn_M23;
    ImageButton btn_Allover;
    ImageButton btn_Standart;
    ImageButton btn_Show1;
    ImageButton btn_Show2;
    ImageButton btn_Show3;
    ImageButton btn_Eingabe;

    SeekBar sb_Speed;

    TextView tv_Modi;
    TextView tv_Speed;
    TextView tv_Cheats;
    TextView tv_reply;
    TextView tv_replyText;

    TextInputEditText it_Cheats;

    public static final int MESSAGE_READ = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_steuerung);
        btn_M1 = (ImageButton)findViewById(R.id.btn_M1);
        btn_M2 = (ImageButton)findViewById(R.id.btn_M2);
        btn_M3 = (ImageButton)findViewById(R.id.btn_M3);
        btn_M4 = (ImageButton)findViewById(R.id.btn_M4);
        btn_M5 = (ImageButton)findViewById(R.id.btn_M5);
        btn_M6 = (ImageButton)findViewById(R.id.btn_M6);
        btn_M7 = (ImageButton)findViewById(R.id.btn_M7);
        btn_M8 = (ImageButton)findViewById(R.id.btn_M8);
        btn_M9 = (ImageButton)findViewById(R.id.btn_M9);
        btn_M10 = (ImageButton)findViewById(R.id.btn_M10);
        btn_M11 = (ImageButton)findViewById(R.id.btn_M11);
        btn_M12 = (ImageButton)findViewById(R.id.btn_M12);
        btn_M13 = (ImageButton)findViewById(R.id.btn_M13);
        btn_M14 = (ImageButton)findViewById(R.id.btn_M14);
        btn_M15 = (ImageButton)findViewById(R.id.btn_M15);
        btn_M16 = (ImageButton)findViewById(R.id.btn_M16);
        btn_M17 = (ImageButton)findViewById(R.id.btn_M17);
        btn_M18 = (ImageButton)findViewById(R.id.btn_M18);
        btn_M19 = (ImageButton)findViewById(R.id.btn_M19);
        btn_M20 = (ImageButton)findViewById(R.id.btn_M20);
        btn_M21 = (ImageButton)findViewById(R.id.btn_M21);
        btn_M22 = (ImageButton)findViewById(R.id.btn_M22);
        btn_M23 = (ImageButton)findViewById(R.id.btn_M23);
        btn_Allover = (ImageButton)findViewById(R.id.btn_Allover);
        btn_Standart = (ImageButton)findViewById(R.id.btn_Standart);
        btn_Show1 = (ImageButton)findViewById(R.id.btn_Show1);
        btn_Show2 = (ImageButton)findViewById(R.id.btn_Show2);
        btn_Show3 = (ImageButton)findViewById(R.id.btn_Show3);
        btn_Eingabe = (ImageButton)findViewById(R.id.btn_Eingabe);

        sb_Speed = (SeekBar)findViewById(R.id.sb_Speed);

        tv_Modi = (TextView)findViewById(R.id.tv_Modi);
        tv_Speed = (TextView)findViewById(R.id.tv_Speed);
        tv_Cheats = (TextView)findViewById(R.id.tv_Cheats);
        tv_reply = (TextView)findViewById(R.id.tv_reply);
        tv_replyText = (TextView)findViewById(R.id.tv_replyText);

        it_Cheats = (TextInputEditText)findViewById(R.id.it_Cheats);

        BluetoothAdapterClass.setHandler(mHandler);

        btn_M1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("0 0");
            }
        });

        btn_M2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("0 1");
            }
        });

        btn_M3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("0 2");
            }
        });

        btn_M4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("0 3");
            }
        });

        btn_M5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("0 4");
            }
        });

        btn_M6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("0 5");
            }
        });

        btn_M7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("0 6");
            }
        });

        btn_M8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("0 7");
            }
        });

        btn_M9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("0 8");
            }
        });

        btn_M10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("0 9");
            }
        });

        btn_M11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("0 10");
            }
        });

        btn_M12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("0 11");
            }
        });

        btn_M13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("0 12");
            }
        });

        btn_M14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("0 13");
            }
        });

        btn_M15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("0 14");
            }
        });

        btn_M16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("0 15");
            }
        });

        btn_M17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("0 16");
            }
        });

        btn_M18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("0 17");
            }
        });

        btn_M19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("0 18");
            }
        });

        btn_M20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("0 19");
            }
        });

        btn_M21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("0 20"); }
        });

        btn_M22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("0 21"); }});

        btn_M23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("0 22"); }});

        sb_Speed.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    int progress_value;
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        progress_value = sb_Speed.getProgress()+1;
                        BluetoothAdapterClass.WriteBT("S "+progress_value);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {


                    }
                }
        );

        btn_Allover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("T0#T1#T2#T3#T4#T5#T6#T7#T8#T9#TB");
            }
        });

        btn_Standart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("Z2");
            }
        });

        btn_Show1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("Z2");
            }
        });

        btn_Show2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("Z2");
            }
        });

        btn_Show3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { BluetoothAdapterClass.WriteBT("Z2");
            }
        });

        btn_Eingabe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT(it_Cheats.getText().toString());
                it_Cheats.setText("");
            }
        });
//-----------------------------------------------------------



    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    tv_replyText.setText(readMessage);
                    break;
            }
        }
    };



}
