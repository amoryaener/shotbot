package com.drink.shotbotapp.shot_botapp;

import android.content.ClipData;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class LightModes extends AppCompatActivity {

    String[] cheatComands;
    LinearLayout extraRow;
    ImageView[] fields;
    ImageView[] buttons;
    ImageView[] dragButtons;
    DropZone[] zones;
    RelativeLayout.LayoutParams dragParams;
    ImageView abc;
    int abc2;
    int x;
    int y;
    ImageView firstCheat;
    ImageView secoundCheat;
    ImageView firstCheatDrag;
    ImageView secoundCheatDrag;
    ImageView thirdCheat;
    ImageView thirdCheatDrag;
    boolean firstCheatFree = true;
    boolean secoundCheatFree = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        zones = new DropZone[10];
        setContentView(R.layout.activity_light_modes);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        fields = new ImageView[10];
        buttons = new ImageView[23];
        dragButtons = new ImageView[26];
        cheatComands = new String[3];

        extraRow = findViewById(R.id.extraRow);

        dragButtons[0]= findViewById(R.id.lightDrag0);
        dragButtons[1]= findViewById(R.id.lightDrag1);
        dragButtons[2]= findViewById(R.id.lightDrag2);
        dragButtons[3]= findViewById(R.id.lightDrag3);
        dragButtons[4]= findViewById(R.id.lightDrag4);
        dragButtons[5]= findViewById(R.id.lightDrag5);
        dragButtons[6]= findViewById(R.id.lightDrag6);
        dragButtons[7]= findViewById(R.id.lightDrag7);
        dragButtons[8]= findViewById(R.id.lightDrag8);
        dragButtons[9]= findViewById(R.id.lightDrag9);
        dragButtons[10]= findViewById(R.id.lightDrag10);
        dragButtons[11]= findViewById(R.id.lightDrag11);
        dragButtons[12]= findViewById(R.id.lightDrag12);
        dragButtons[13]= findViewById(R.id.lightDrag13);
        dragButtons[14]= findViewById(R.id.lightDrag14);
        dragButtons[15]= findViewById(R.id.lightDrag15);
        dragButtons[16]= findViewById(R.id.lightDrag16);
        dragButtons[17]= findViewById(R.id.lightDrag17);
        dragButtons[18]= findViewById(R.id.lightDrag18);
        dragButtons[19]= findViewById(R.id.lightDrag19);
        dragButtons[20]= findViewById(R.id.lightDrag20);
        dragButtons[21]= findViewById(R.id.lightDrag21);
        dragButtons[22]= findViewById(R.id.lightDrag22);
        firstCheatDrag = findViewById(R.id.lightDrag23);
        secoundCheatDrag = findViewById(R.id.lightDrag24);
        thirdCheatDrag = findViewById(R.id.lightDrag25);
        dragButtons[23] = firstCheatDrag;
        dragButtons[24] = secoundCheatDrag;
        dragButtons[25] = thirdCheatDrag;


        fields[0]= findViewById(R.id.boardPoint0);
        fields[1]= findViewById(R.id.boardPoint1);
        fields[2]= findViewById(R.id.boardPoint2);
        fields[3]= findViewById(R.id.boardPoint3);
        fields[4]= findViewById(R.id.boardPoint4);
        fields[5]= findViewById(R.id.boardPoint5);
        fields[6]= findViewById(R.id.boardPoint6);
        fields[7]= findViewById(R.id.boardPoint7);
        fields[8]= findViewById(R.id.boardPoint8);
        fields[9]= findViewById(R.id.boardPoint9);
        buttons[0]= findViewById(R.id.lightButton0);
        buttons[1]= findViewById(R.id.lightButton1);
        buttons[2]= findViewById(R.id.lightButton2);
        buttons[3]= findViewById(R.id.lightButton3);
        buttons[4]= findViewById(R.id.lightButton4);
        buttons[5]= findViewById(R.id.lightButton5);
        buttons[6]= findViewById(R.id.lightButton6);
        buttons[7]= findViewById(R.id.lightButton7);
        buttons[8]= findViewById(R.id.lightButton8);
        buttons[9]= findViewById(R.id.lightButton9);
        buttons[10]= findViewById(R.id.lightButton10);
        buttons[11]= findViewById(R.id.lightButton11);
        buttons[12]= findViewById(R.id.lightButton12);
        buttons[13]= findViewById(R.id.lightButton13);
        buttons[14]= findViewById(R.id.lightButton14);
        buttons[15]= findViewById(R.id.lightButton15);
        buttons[16]= findViewById(R.id.lightButton16);
        buttons[17]= findViewById(R.id.lightButton17);
        buttons[18]= findViewById(R.id.lightButton18);
        buttons[19]= findViewById(R.id.lightButton19);
        buttons[20]= findViewById(R.id.lightButton20);
        buttons[21]= findViewById(R.id.lightButton21);
        buttons[22]= findViewById(R.id.lightButton22);
        firstCheat = findViewById(R.id.lightButton23);
        secoundCheat = findViewById(R.id.lightButton24);
        firstCheat.setClickable(false);
        secoundCheat.setClickable(false);
        thirdCheat = findViewById(R.id.lightButton25);
        thirdCheat.setClickable(false);
        for(int i=0; i<23;i++){
            buttons[i].setOnTouchListener(new MyTouchListener(i));
        }
        SharedPreferences sharedPrefs = getSharedPreferences("saveCodes", 0);
        if(sharedPrefs.getBoolean("christmas",false)){
            if(firstCheatFree){
                firstCheat.setClickable(true);
                firstCheat.setImageDrawable(getDrawable(R.drawable.christmas));
                firstCheatDrag.setImageDrawable(getDrawable(R.drawable.christmas));
                firstCheat.setOnTouchListener(new MyTouchListener(23));
                firstCheatFree = false;
                cheatComands[0]="23";
            }else if(secoundCheatFree){
                secoundCheat.setClickable(true);
                secoundCheat.setImageDrawable(getDrawable(R.drawable.christmas));
                secoundCheatDrag.setImageDrawable(getDrawable(R.drawable.christmas));
                secoundCheat.setOnTouchListener(new MyTouchListener(23));
                extraRow.setVisibility(View.VISIBLE);
                secoundCheatFree =false;
                cheatComands[1]="23";
            }else{
                thirdCheat.setClickable(true);
                thirdCheat.setImageDrawable(getDrawable(R.drawable.christmas));
                thirdCheatDrag.setImageDrawable(getDrawable(R.drawable.christmas));
                thirdCheat.setOnTouchListener(new MyTouchListener(25));
                extraRow.setVisibility(View.VISIBLE);
                cheatComands[2]="23";
            }
        }
        if(sharedPrefs.getBoolean("silvester",false)){
            if(firstCheatFree){
                firstCheat.setClickable(true);
                firstCheat.setImageDrawable(getDrawable(R.drawable.silvester));
                firstCheatDrag.setImageDrawable(getDrawable(R.drawable.silvester));
                firstCheat.setOnTouchListener(new MyTouchListener(24));
                firstCheatFree = false;
                cheatComands[0]="24";
            }else if(secoundCheatFree){
                secoundCheat.setClickable(true);
                secoundCheat.setImageDrawable(getDrawable(R.drawable.silvester));
                secoundCheatDrag.setImageDrawable(getDrawable(R.drawable.silvester));
                secoundCheat.setOnTouchListener(new MyTouchListener(24));
                extraRow.setVisibility(View.VISIBLE);
                secoundCheatFree =false;
                cheatComands[1]="24";
            }else{
                thirdCheat.setClickable(true);
                thirdCheat.setImageDrawable(getDrawable(R.drawable.silvester));
                thirdCheatDrag.setImageDrawable(getDrawable(R.drawable.silvester));
                thirdCheat.setOnTouchListener(new MyTouchListener(25));
                extraRow.setVisibility(View.VISIBLE);
                cheatComands[2]="24";
            }
        }
        if(sharedPrefs.getBoolean("halloween",false)){
            if(firstCheatFree){
                firstCheat.setClickable(true);
                firstCheat.setImageDrawable(getDrawable(R.drawable.halloween));
                firstCheatDrag.setImageDrawable(getDrawable(R.drawable.halloween));
                firstCheat.setOnTouchListener(new MyTouchListener(23));
                firstCheatFree = false;
                cheatComands[0]="25";
            }else if(secoundCheatFree){
                secoundCheat.setClickable(true);
                secoundCheat.setImageDrawable(getDrawable(R.drawable.halloween));
                secoundCheatDrag.setImageDrawable(getDrawable(R.drawable.halloween));
                secoundCheat.setOnTouchListener(new MyTouchListener(24));
                extraRow.setVisibility(View.VISIBLE);
                secoundCheatFree =false;
                cheatComands[1]="25";
            }else{
                thirdCheat.setClickable(true);
                thirdCheat.setImageDrawable(getDrawable(R.drawable.halloween));
                thirdCheatDrag.setImageDrawable(getDrawable(R.drawable.halloween));
                thirdCheat.setOnTouchListener(new MyTouchListener(25));
                extraRow.setVisibility(View.VISIBLE);
                cheatComands[2]="25";
            }
        }

        for(int i=0; i<10; i++){
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) fields[i].getLayoutParams();
            zones[i]= new DropZone(params.leftMargin,params.leftMargin+params.width,params.topMargin,params.topMargin+params.height);
            final int j =i;
            fields[i].setOnDragListener(new View.OnDragListener() {
                @Override
                public boolean onDrag(View v, DragEvent event) {
                    switch(event.getAction()) {
                        case DragEvent.ACTION_DRAG_STARTED:
                            // Do nothing
                            dragParams = new RelativeLayout.LayoutParams(70,70);
                            dragParams = (RelativeLayout.LayoutParams) abc.getLayoutParams();
                            abc.setLayoutParams(dragParams);
                            break;

                        case DragEvent.ACTION_DRAG_ENTERED:
                            x = (int) event.getX();
                            y = (int) event.getY();
                            break;

                        case DragEvent.ACTION_DRAG_EXITED :
                            x = (int) event.getX();
                            y = (int) event.getY();
                            dragParams.setMargins(x, y, 0, 0);
                            abc.setLayoutParams(dragParams);
                            break;

                        case DragEvent.ACTION_DRAG_LOCATION  :
                            x = (int) event.getX();
                            y = (int) event.getY();
                            break;

                        case DragEvent.ACTION_DRAG_ENDED   :
                            /*for(int i=0; i<10;i++){
                                fields[i].setImageDrawable(null);
                            }*/
                            // Do nothing
                            break;

                        case DragEvent.ACTION_DROP:
                            /*for(int i=0; i<10;i++){
                                fields[i].setImageDrawable(null);
                            }*/
                            x = (int) event.getX();
                            y = (int) event.getY();
                            x = x-abc.getWidth()/2;
                            y = y-abc.getHeight()/2;
                            dragParams.setMargins(x, y, 0, 0);
                            abc.setLayoutParams(dragParams);
                            if(x >= zones[j].x1-50 && x<= zones[j].x2+50 && y>= zones[j].y1-50 && y <= zones[j].y2+50){
                                fields[j].setImageDrawable(abc.getDrawable());
                                String senden;
                                if(abc2<23){
                                    senden =""+j+" "+abc2;
                                }else if(abc2 ==23){
                                    senden =""+j+" "+cheatComands[0];
                                }else if(abc2 ==24){
                                    senden =""+j+" "+cheatComands[1];
                                }else {
                                    senden =""+j+" "+cheatComands[2];
                                }

                                BluetoothAdapterClass.WriteBT(senden);
                                Log.d("TESTTEST",senden);
                            }
                            break;
                        default: break;
                    }
                    return true;
                }
            });
        }


    }

    private final class MyTouchListener implements View.OnTouchListener {

        int nr;

        public MyTouchListener(int index){
            this.nr = index;
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                abc = dragButtons[nr];
                abc2 = nr;
                abc.setVisibility(View.VISIBLE);
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
                        view);
                abc.startDrag(data, shadowBuilder, abc, 0);
                abc.setVisibility(View.INVISIBLE);
                return true;
            } else {
                return false;
            }
        }
    }

    public class DropZone{
        int x1,x2,y1,y2;

        public DropZone(int x, int xx, int y, int yy){
            this.x1 = x;
            this.x2 = xx;
            this.y1 = y;
            this.y2 = yy;
        }
    }

}
