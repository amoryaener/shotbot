package com.drink.shotbotapp.shot_botapp;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class TimerSpiel extends AppCompatActivity {
    ImageView start;
    LinearLayout layout;
    ImageButton imageButton;
    TextView timer;
    ArrayList<Player> players;
    int currentPlayer;
    int status =0;
    MediaPlayer tick_tack;
    MediaPlayer explode_sound;
    TextView name;
    ImageView ampel;
    String Status;
    Boolean Getrunken = true;
    ImageView einstellungen;
    CrystalRangeSeekbar rangeSeekbar;
    TextView tvMin;
    TextView tvMax;
    boolean einstellungenSichtbar = false;
    LinearLayout range;
    int min =6;
    int max =40;
    int count =0;
    boolean begrenzt = false;
    int rounds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer_spiel);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        players = PlayersClass.getPlayerList();
        rounds = getIntent().getIntExtra("Rounds",0);
        if(rounds >0){
            begrenzt=true;
        }
        name = findViewById(R.id.bombe_name);
        BluetoothAdapterClass.setHandler(mHandler);
        name.setText(players.get(0).getName());
        explode_sound = MediaPlayer.create(this, R.raw.exploded);
        currentPlayer =0;
        range=findViewById(R.id.bombe_range);
        range.setVisibility(View.GONE);
        einstellungen = findViewById(R.id.bombe_einstellungen);
        rangeSeekbar= findViewById(R.id.rangeSeekbar1);
        rangeSeekbar.setMinValue(1);
        rangeSeekbar.setMaxValue(90);
        rangeSeekbar.setMaxStartValue(40);
        tvMin=findViewById(R.id.textMin1);
        tvMax=findViewById(R.id.textMax1);
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tvMin.setText(String.valueOf(minValue));
                tvMax.setText(String.valueOf(maxValue));
                min = minValue.intValue();
                max = maxValue.intValue();
            }
        });

        einstellungen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(einstellungenSichtbar){
                    einstellungenSichtbar=false;
                    range.setVisibility(View.GONE);
                    range.setClickable(false);
                    range.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_hide));
                }else {
                    einstellungenSichtbar=true;
                    range.setVisibility(View.VISIBLE);
                    range.setClickable(true);
                    range.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_show));
                }
            }
        });
        ampel = findViewById(R.id.ampel);
        start = findViewById(R.id.startButton);
        timer = (TextView)findViewById(R.id.timer);
        layout = (LinearLayout)findViewById(R.id.layout_timer);
        imageButton = (ImageButton)findViewById(R.id.imageButton_timer);
        imageButton.setImageResource(players.get(currentPlayer).getColor());
        if(players.get(currentPlayer).bild!=null){
            imageButton.setImageBitmap(players.get(currentPlayer).bild);
            imageButton.setForeground(null);
        }else{
            imageButton.setForeground(players.get(currentPlayer).playerPic);
        }
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status ==0){
                    timerClass tc = new timerClass();
                    tc.start();
                    tick_tack_start();
                    status =1;
                    timer.setText("0");
                    start.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(), "Auf die Bombe klicken zum Weitergeben!", Toast.LENGTH_LONG).show();
                }else if(status ==2){
                    if(Getrunken==true) {
                        if (currentPlayer < players.size() - 1) {
                            currentPlayer++;
                        } else {
                            currentPlayer = 0;
                        }
                        imageButton.setImageResource(players.get(currentPlayer).getColor());
                        name.setText(players.get(currentPlayer).getName());
                        if (players.get(currentPlayer).bild != null) {
                            imageButton.setImageBitmap(players.get(currentPlayer).bild);
                            imageButton.setForeground(null);
                        } else {
                            imageButton.setForeground(players.get(currentPlayer).playerPic);
                        }
                        timerClass tc = new timerClass();
                        tc.start();
                        status = 1;
                        tick_tack_start();
                        timer.setText("0");
                        start.setVisibility(View.INVISIBLE);
                        Toast.makeText(getApplicationContext(), "Auf den Kreis klicken zum Weitergeben!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status ==1){
                    if(currentPlayer<players.size()-1){
                        currentPlayer ++;
                    }else{
                        currentPlayer =0;
                    }
                    imageButton.setImageResource(players.get(currentPlayer).getColor());
                    name.setText(players.get(currentPlayer).getName());
                    if(players.get(currentPlayer).bild!=null){
                        imageButton.setImageBitmap(players.get(currentPlayer).bild);
                        imageButton.setForeground(null);
                    }
                    else{
                        imageButton.setForeground(players.get(currentPlayer).playerPic);
                    }
                }
            }
        });
    }

    private final Handler mHandler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what==0){
                exploded();
            }else if(msg.what==1){
                timer.setText((Integer.parseInt(timer.getText()+"")+1)+"");
            }else if(msg.what==2){
                layout.setBackgroundResource(R.drawable.background1);
            }else if(msg.what==3){
                layout.setBackgroundResource(R.drawable.beach_background);
            }
        }
    };
    public void tick_tack_start(){
        tick_tack = MediaPlayer.create(this, R.raw.tick_tack);
        tick_tack.setLooping(true);
        tick_tack.start();
    }

    public void tick_tack_end(){
        tick_tack.stop();
    }

    public void exploded(){
        count++;
        if(begrenzt && rounds == count){
            finish();
        }
        timerClass2 tc = new timerClass2();
        tc.start();
        start.setVisibility(View.VISIBLE);
        status = 2;
        tick_tack_end();
        explode_sound.start();
        players.get(currentPlayer).oneShot();
        if(!SystemState.isOffline()) {
            Getrunken = false;
            ampel.setImageResource(R.drawable.ampel_rot);
        }
        Toast.makeText(getApplicationContext(), players.get(currentPlayer).getName()+ " Trinkt!", Toast.LENGTH_LONG).show();
    }

    public class timerClass extends Thread{
        public void run(){
            int timer = ThreadLocalRandom.current().nextInt(min, max);
            while(timer >0){
                timer --;
                mHandler2.obtainMessage(1).sendToTarget();
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            mHandler2.obtainMessage(0).sendToTarget();
        }
    }

    public class timerClass2 extends Thread{
        public void run(){
            for(int i=0; i<5; i++){
                mHandler2.obtainMessage(2).sendToTarget();
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mHandler2.obtainMessage(3).sendToTarget();
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            mHandler2.obtainMessage(4).sendToTarget();
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bedienung.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Status = readMessage;
                    if(Status.contains("1")){
                        Getrunken = true;
                        ampel.setImageResource(R.drawable.ampel_gruen);
                        Status="";
                    }
                    break;
            }
        }
    };

}
