package com.drink.shotbotapp.shot_botapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SpielerEinstellungen extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_TAKE_PHOTO = 1;
    String mCurrentPhotoPath;
    EditText playerNameEntry;
    ImageButton btnPlayerImage;
    ImageButton btn_newPlayer;
    Button btn_spielwahl;
    ListView lvPlayer;
    ArrayList<Player> playerList;
    boolean[]belegt;
    int[] colors;
    int currentColor;
    boolean playersfull;
    TextView btnPlayerPic;
    TextView btnPlayerPicDel;
    Bitmap actualBitmap;
    int actualPlayerPic;
    Drawable[] playerPics;
    TextView btnPlayerPicImage;
    SharedPreferences sharedPrefs;
    boolean ownPic;
    String pfad;
    ImageView male_female;
    boolean isMale = true;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spieler_einstellungen2);
        male_female = findViewById(R.id.male_female);
        sharedPrefs = getSharedPreferences("save", 0);
        boolean onBack = false;
        onBack = getIntent().getBooleanExtra("onBack",false);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ownPic = false;
        if(!onBack){
            PlayersClass.clean();
        }
        context = this;
        playerList = PlayersClass.getPlayerList();
        btnPlayerPicImage = (TextView)findViewById(R.id.btnPlayerPicImage);
        playerPics = new Drawable[19];
        playerPics[0] = getResources().getDrawable(R.drawable.playerpic1test);
        playerPics[1] = getResources().getDrawable(R.drawable.playerpic2test);
        playerPics[2] = getResources().getDrawable(R.drawable.playerpic3test);
        playerPics[3] = getResources().getDrawable(R.drawable.playerpic4test);
        playerPics[4] = getResources().getDrawable(R.drawable.playerpic5test);
        playerPics[5] = getResources().getDrawable(R.drawable.playerpic6test);
        playerPics[6] = getResources().getDrawable(R.drawable.playerpic7test);
        playerPics[7] = getResources().getDrawable(R.drawable.playerpic8test);
        playerPics[8] = getResources().getDrawable(R.drawable.playerpic9test);
        playerPics[9] = getResources().getDrawable(R.drawable.playerpic10test);
        playerPics[10] = getResources().getDrawable(R.drawable.playerpic11test);
        playerPics[11] = getResources().getDrawable(R.drawable.playerpic12test);
        playerPics[12] = getResources().getDrawable(R.drawable.playerpic13test);
        playerPics[13] = getResources().getDrawable(R.drawable.playerpic14test);
        playerPics[14] = getResources().getDrawable(R.drawable.playerpic15test);
        playerPics[15] = getResources().getDrawable(R.drawable.playerpic16test);
        playerPics[16] = getResources().getDrawable(R.drawable.playerpic17test);
        playerPics[17] = getResources().getDrawable(R.drawable.playerpic18test);
        playerPics[18] = getResources().getDrawable(R.drawable.playerpic19test);
        actualPlayerPic =0;

        playersfull = false;
        belegt = new boolean[10];
        for(int i=0; i<10; i++){
            belegt[i] = false;
        }

        setupListview();

        if(!onBack){
            if(sharedPrefs.getString("n0",null)!= null){
                PlayersClass.addPlayer(new Player(sharedPrefs.getString("n0",null), sharedPrefs.getInt("c0",0),null,playerPics[sharedPrefs.getInt("pp0",0)],sharedPrefs.getString("mw0",null), sharedPrefs.getInt("pp0",0),context));
                updatePoiListView();
            }
            if(sharedPrefs.getString("n1",null)!= null){
                PlayersClass.addPlayer(new Player(sharedPrefs.getString("n1",null), sharedPrefs.getInt("c1",0),null,playerPics[sharedPrefs.getInt("pp1",0)],sharedPrefs.getString("mw1",null), sharedPrefs.getInt("pp1",0),context));
                updatePoiListView();
            }
            if(sharedPrefs.getString("n2",null)!= null){
                PlayersClass.addPlayer(new Player(sharedPrefs.getString("n2",null), sharedPrefs.getInt("c2",0),null,playerPics[sharedPrefs.getInt("pp2",0)],sharedPrefs.getString("mw2",null), sharedPrefs.getInt("pp2",0),context));
                updatePoiListView();
            }
            if(sharedPrefs.getString("n3",null)!= null){
                PlayersClass.addPlayer(new Player(sharedPrefs.getString("n3",null), sharedPrefs.getInt("c3",0),null,playerPics[sharedPrefs.getInt("pp3",0)],sharedPrefs.getString("mw3",null), sharedPrefs.getInt("pp3",0),context));
                updatePoiListView();
            }
            if(sharedPrefs.getString("n4",null)!= null){
                PlayersClass.addPlayer(new Player(sharedPrefs.getString("n4",null), sharedPrefs.getInt("c4",0),null,playerPics[sharedPrefs.getInt("pp4",0)],sharedPrefs.getString("mw4",null), sharedPrefs.getInt("pp4",0),context));
                updatePoiListView();
            }
            if(sharedPrefs.getString("n5",null)!= null){
                PlayersClass.addPlayer(new Player(sharedPrefs.getString("n5",null), sharedPrefs.getInt("c5",0),null,playerPics[sharedPrefs.getInt("pp5",0)],sharedPrefs.getString("mw5",null), sharedPrefs.getInt("pp5",0),context));
                updatePoiListView();
            }
            if(sharedPrefs.getString("n6",null)!= null){
                PlayersClass.addPlayer(new Player(sharedPrefs.getString("n6",null), sharedPrefs.getInt("c6",0),null,playerPics[sharedPrefs.getInt("pp6",0)],sharedPrefs.getString("mw6",null), sharedPrefs.getInt("pp6",0),context));
                updatePoiListView();
            }
            if(sharedPrefs.getString("n7",null)!= null){
                PlayersClass.addPlayer(new Player(sharedPrefs.getString("n7",null), sharedPrefs.getInt("c7",0),null,playerPics[sharedPrefs.getInt("pp7",0)],sharedPrefs.getString("mw7",null), sharedPrefs.getInt("pp7",0),context));
                updatePoiListView();
            }
            if(sharedPrefs.getString("n8",null)!= null){
                PlayersClass.addPlayer(new Player(sharedPrefs.getString("n8",null), sharedPrefs.getInt("c8",0),null,playerPics[sharedPrefs.getInt("pp8",0)],sharedPrefs.getString("mw8",null), sharedPrefs.getInt("pp8",0),context));
                updatePoiListView();
            }
            if(sharedPrefs.getString("n9",null)!= null){
                PlayersClass.addPlayer(new Player(sharedPrefs.getString("n9",null), sharedPrefs.getInt("c9",0),null,playerPics[sharedPrefs.getInt("pp9",0)],sharedPrefs.getString("mw9",null), sharedPrefs.getInt("pp9",0),context));
                updatePoiListView();
            }
        }

        for(int i=0; i<playerList.size();i++){
            belegt[playerList.get(i).id]=true;
        }

        actualBitmap =null;
        currentColor=0;
        colors = new int[10];
        colors[0]= R.drawable.color_player0;
        colors[1]= R.drawable.color_player1;
        colors[2]= R.drawable.color_player2;
        colors[3]= R.drawable.color_player3;
        colors[4]= R.drawable.color_player4;
        colors[5]= R.drawable.color_player5;
        colors[6]= R.drawable.color_player6;
        colors[7]= R.drawable.color_player7;
        colors[8]= R.drawable.color_player8;
        colors[9]= R.drawable.color_player9;

        btnPlayerPic = (TextView) findViewById(R.id.btnPlayerPic);
        btnPlayerPicDel = (TextView) findViewById(R.id.btnPlayerPicDel);
        btnPlayerPicDel.setVisibility(View.INVISIBLE);
        btn_spielwahl= (Button)findViewById(R.id.btn_spielwahl);
        playerNameEntry = (EditText)findViewById(R.id.playerNameEntry);
        btnPlayerImage = (ImageButton)findViewById(R.id.btnPlayerImage);
        btn_newPlayer = (ImageButton)findViewById(R.id.btn_newPlayer);


        btnPlayerPicImage.setBackground(playerPics[0]);

        if(playerList.size()!=10){
            while(belegt[currentColor]){
                if(currentColor<colors.length-1){
                    currentColor++;
                }else{
                    currentColor = 0;
                }
            }
            btnPlayerImage.setImageResource(colors[currentColor]);
        }

        btnPlayerPicImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(actualPlayerPic<playerPics.length-1){
                   actualPlayerPic++;
               }else{
                   actualPlayerPic= 0;
               }
                btnPlayerPicImage.setBackground(playerPics[actualPlayerPic]);
            }
        });

        male_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isMale){
                    isMale=false;
                    male_female.setImageResource(R.drawable.female);
                }else{
                    isMale=true;
                    male_female.setImageResource(R.drawable.male);
                }
            }
        });

        btnPlayerPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dispatchTakePictureIntent();
            }
        });

        btnPlayerPicDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnPlayerPicDel.setVisibility(View.INVISIBLE);
                String uri = "@android:drawable/ic_menu_camera";
                int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                Drawable draw = getResources().getDrawable(imageResource);
                btnPlayerPic.setBackground(draw);
                ownPic = false;
            }
        });

        btn_spielwahl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PlayersClass.getPlayerList().size()>0){
                    sharedPrefs.edit().clear().commit();
                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    for(int i=0; i<playerList.size();i++){
                        editor.putString("n"+playerList.get(i).id, playerList.get(i).getName());
                        editor.putString("mw"+playerList.get(i).id, playerList.get(i).getMorw());
                        editor.putInt("c"+playerList.get(i).id, playerList.get(i).color);
                        editor.putInt("pp"+playerList.get(i).id, playerList.get(i).picpos);
                    }
                    editor.commit();
                    BluetoothAdapterClass.WriteBT("Ende");
                    //Intent i = new Intent(SpielerEinstellungen.this, Spielauswahl.class);
                    Intent i = new Intent(SpielerEinstellungen.this, SpielauswahlNeu.class);
                    startActivity(i);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Es muss ein Spieler erstellt werden", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnPlayerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentColor<colors.length-1){
                    currentColor++;
                }else{
                    currentColor = 0;
                }if(playerList.size()!=10){
                    while(belegt[currentColor]){
                        if(currentColor<colors.length-1){
                            currentColor++;
                        }else{
                            currentColor = 0;
                        }
                    }
                    btnPlayerImage.setImageResource(colors[currentColor]);
                    BluetoothAdapterClass.WriteBT("P"+currentColor);
                }else{
                    btnPlayerImage.setImageResource(colors[0]);
                    playersfull = true;
                    Toast.makeText(getApplicationContext(), "Bereits 10 Spieler erstellt", Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_newPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!playersfull){
                    String morf = "";
                    if(isMale){
                        morf = "m";
                    }else{
                        morf = "w";
                    }
                    if(ownPic){
                        PlayersClass.addPlayer(new Player(playerNameEntry.getText().toString(), colors[currentColor],actualBitmap,null,morf,actualPlayerPic,context));
                    }else {
                        PlayersClass.addPlayer(new Player(playerNameEntry.getText().toString(), colors[currentColor], actualBitmap, playerPics[actualPlayerPic], morf, actualPlayerPic,context));
                    }
                    updatePoiListView();
                    ownPic = false;
                    belegt[currentColor]=true;
                    resetColor();
                    actualPlayerPic =0;
                    btnPlayerPicImage.setBackground(playerPics[actualPlayerPic]);
                    actualBitmap =null;
                    isMale=true;
                    male_female.setImageResource(R.drawable.male);
                    btnPlayerPicDel.setVisibility(View.INVISIBLE);
                    String uri = "@android:drawable/ic_menu_camera";
                    int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                    Drawable draw = getResources().getDrawable(imageResource);
                    btnPlayerPic.setBackground(draw);
                }else{
                    Toast.makeText(getApplicationContext(), "Bereits 10 Spieler erstellt", Toast.LENGTH_LONG).show();
                }
            }
        });


        Toast.makeText(getApplicationContext(), "Zum Farbe wechseln auf den Kreis klicken", Toast.LENGTH_LONG).show();
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        File root = Environment.getExternalStorageDirectory();
        Bitmap bMap = BitmapFactory.decodeFile(mCurrentPhotoPath);

        Matrix matrix = new Matrix();

        matrix.postRotate(-90);

        bMap = Bitmap.createBitmap(bMap , 0, 0, bMap .getWidth(), bMap .getHeight(), matrix, true);

        if (bMap.getWidth() >= bMap.getHeight()){

            bMap = Bitmap.createBitmap(
                    bMap,
                    bMap.getWidth()/2 - bMap.getHeight()/2,
                    0,
                    bMap.getHeight(),
                    bMap.getHeight()
            );

        }else{

            bMap = Bitmap.createBitmap(
                    bMap,
                    0,
                    bMap.getHeight()/2 - bMap.getWidth()/2,
                    bMap.getWidth(),
                    bMap.getWidth()
            );
        }

        ownPic = true;
        bMap = Bitmap.createScaledBitmap(bMap, 700, 700, false);
        btnPlayerPic.setBackground(new BitmapDrawable(getResources(), bMap));
        actualBitmap = BitmapRound.getCroppedBitmap(bMap);
        btnPlayerPicDel.setVisibility(View.VISIBLE);
    }


    public void resetColor(){
        currentColor = 0;

        if(playerList.size()!=10){
            while(belegt[currentColor]){
                if(currentColor<colors.length-1){
                    currentColor++;
                }else{
                    currentColor = 0;
                }
            }
            btnPlayerImage.setImageResource(colors[currentColor]);
            BluetoothAdapterClass.WriteBT("P"+currentColor);
        }else{
            btnPlayerImage.setImageResource(colors[0]);
            playersfull = true;
        }

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        updatePoiListView();
    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first
        updatePoiListView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(SpielerEinstellungen.this, MainMenu.class);
        startActivity(i);
        finish();
    }

    public void setupListview(){
        try {
            ArrayAdapter<Player> adapter = new PlayerListAdapter();
            lvPlayer = (ListView) findViewById(R.id.lvPlayer);
            lvPlayer.setAdapter(adapter);

        } catch (Exception e) {
        }
    }

    public void updatePoiListView() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((PlayerListAdapter) lvPlayer.getAdapter()).notifyDataSetChanged();
            }
        });

    }

    private class PlayerListAdapter extends ArrayAdapter<Player> {
        public PlayerListAdapter() {
            super(SpielerEinstellungen.this, R.layout.playerentry2, playerList);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            Holder holder;
            //Find Item
            final Player player = playerList.get(position);

            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.playerentry2, parent, false);
                holder = new Holder();
                holder.playerName = (TextView) itemView.findViewById(R.id.PlayerName);
                holder.delButton = (ImageButton) itemView.findViewById(R.id.playerDel);
                holder.playerBild = (TextView) itemView.findViewById(R.id.PlayerBild);
                holder.playerBild2 = (TextView) itemView.findViewById(R.id.PlayerBild2);
                itemView.setTag(holder);
            }else{
                holder = (Holder) itemView.getTag();
            }
            holder.playerName.setText(player.getName());
            holder.playerBild2.setBackgroundResource(player.getColor());
            holder.playerBild.setVisibility(View.INVISIBLE);

            if(player.bild!=null){
                holder.playerBild.setBackground(new BitmapDrawable(getResources(), player.bild));
                holder.playerBild.setVisibility(View.VISIBLE);
            }else{
                holder.playerBild2.setForeground(player.playerPic);
            }

            holder.delButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    belegt[player.id]=false;
                    if(playersfull){
                        PlayersClass.removePlayer(player);
                        updatePoiListView();
                        playersfull = false;
                        resetColor();
                    }else{
                        PlayersClass.removePlayer(player);
                        updatePoiListView();
                    }
                }
            });

            return itemView;
        }
        class Holder {
            TextView playerName, playerBild, playerBild2;
            ImageButton delButton;
        }
    }
}
