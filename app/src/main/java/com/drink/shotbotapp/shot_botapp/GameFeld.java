package com.drink.shotbotapp.shot_botapp;

/**
 * Created by Alexander on 18.02.2018.
 */

public class GameFeld {
    public String name;
    public String text;
    public int action;
    public String buttonText;

    public GameFeld(String n, String t, int a , String b){
        this.name = n;
        this.text = t;
        this.action =  a;
        this.buttonText = b;
    }
}
