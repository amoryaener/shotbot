package com.drink.shotbotapp.shot_botapp;

import android.graphics.Bitmap;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Alexander on 05.04.2018.
 */

public class DrawSave {
    static ArrayList<Bitmap> bitmaps = new ArrayList<Bitmap>();

    public static void setBitmaps (ArrayList<Bitmap> b){
        bitmaps =b;
    }

    public static ArrayList<Bitmap> getBitmaps (){
        return bitmaps;
    }
}
