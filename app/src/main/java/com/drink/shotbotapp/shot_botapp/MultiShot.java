package com.drink.shotbotapp.shot_botapp;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Alexander on 26.04.2018.
 */

public class MultiShot {
    static String start = "ST#";
    static String pin = "X ";
    static String go = "GO#";
    static String fuellhoehe = "Y ";
    static String fuellen = "VM#";
    static ArrayList<Player> players;
    static timerClass tc;
    static int hoehe = 500;

    public void shots(ArrayList<Player> p){
        players = p;
        tc = new timerClass();
        tc.start();
    }

    public class timerClass extends Thread{

        public void run(){
            Player[] pArray = new Player[players.size()];
            for(int i=0; i<players.size(); i++){
                pArray[i]=players.get(i);
            }
            for(int i=0; i<12; i++){
                for(int j=0; j<pArray.length-1; j++){
                    int a = pArray[j].id;
                    int b = pArray[j+1].id;
                    if(a>b){
                        Player tmp = pArray[j];
                        pArray[j]=pArray[j+1];
                        pArray[j+1]=tmp;
                    }
                }
            }
            for(int i=0; i<players.size();i++){
                players.get(i).shotCountPlus();
            }
            String senden = start+pin+pArray[0].id+"#"+go+fuellhoehe+pArray[0].hoehe+"#"+fuellen;
            BluetoothAdapterClass.WriteBT(senden);
            Log.d("Senden",senden);
            try {
                Thread.sleep(pArray[0].hoehe+200);
            }catch (InterruptedException e) {
                e.printStackTrace();
            }
            for(int i=1; i<pArray.length;i++){
                int diff = pArray[i].id-pArray[i-1].id;
                senden = pin+diff+"#"+go+fuellhoehe+pArray[i].hoehe+"#"+fuellen;
                BluetoothAdapterClass.WriteBT(senden);
                Log.d("Senden",senden);
                try {
                    Thread.sleep(pArray[i].hoehe+200+diff*400);
                }catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            senden = pin+"-"+pArray[pArray.length-1].id+"#"+go;
            BluetoothAdapterClass.WriteBT(senden);
            Log.d("Senden",senden);
            try {
                Thread.sleep(300*pArray.length);
            }catch (InterruptedException e) {
                e.printStackTrace();
            }
            senden="";
            for(int i=0; i<pArray.length;i++){
                senden = senden+"P "+pArray[i].id+"#";
            }
            senden = senden+"QUIT";
            BluetoothAdapterClass.WriteBT(senden);
            Log.d("Senden",senden);
        }
    }
}
