package com.drink.shotbotapp.shot_botapp;

import android.content.Intent;

/**
 * Created by Alexander on 02.07.2018.
 */

public class GameEntry {
    public int kategorie;// 0=Karten, 1= Würfel, 2=Wissen, 3=Party
    public int minPlayer;
    public int maxPlayer;
    public int schwierigkeit; //0=easy, 1=medium, 2= hard
    public String spielname;
    public String beschreibung;
    public Intent intent;
    public int image;
    public int color;

    public GameEntry(int kat, int minP, int maxP, int schw, String name, String besch, Intent inten, int ima, int col){
        this.kategorie = kat;
        this.minPlayer = minP;
        this.maxPlayer = maxP;
        this.schwierigkeit = schw;
        this.spielname = name;
        this.beschreibung = besch;
        this.intent = inten;
        this.image = ima;
        this.color = col;
    }
}
