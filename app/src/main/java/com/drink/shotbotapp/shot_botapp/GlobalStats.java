package com.drink.shotbotapp.shot_botapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class GlobalStats {
    public static int shotCount;
    public static Context context;

    public static void save(){
        SharedPreferences.Editor editor = context.getSharedPreferences("saveStats", 0).edit();
        editor.putInt("shotcount", shotCount);
        editor.commit();
    }

    public static void gameStartet(String input){

        SharedPreferences sharedPreferences = context.getSharedPreferences("saveStats", 0);
        int count = sharedPreferences.getInt(input,0);
        count++;
        SharedPreferences.Editor editor = context.getSharedPreferences("saveStats", 0).edit();
        editor.putInt(input,count);
        editor.commit();

        Pair[] counts = new Pair[11];
        counts[0]= new Pair("Quiz",sharedPreferences.getInt("Quiz",0));
        counts[1]= new Pair("Wahr oder Falsch",sharedPreferences.getInt("Wahr oder Falsch",0));
        counts[2]= new Pair("Ring of Fire",sharedPreferences.getInt("Ring of Fire",0));
        counts[3]= new Pair("Höher Tiefer",sharedPreferences.getInt("Höher Tiefer",0));
        counts[4]= new Pair("Bombe",sharedPreferences.getInt("Bombe",0));
        counts[5]= new Pair("Glücksrad",sharedPreferences.getInt("Glücksrad",0));
        counts[6]= new Pair("Balkensaufen",sharedPreferences.getInt("Balkensaufen",0));
        counts[7]= new Pair("Meiern",sharedPreferences.getInt("Meiern",0));
        counts[8]= new Pair("Busfahrer",sharedPreferences.getInt("Busfahrer",0));
        counts[9]= new Pair("Memory",sharedPreferences.getInt("Memory",0));
        counts[10]= new Pair("Hausnummer",sharedPreferences.getInt("Hausnummer",0));

        for(int i=0; i<30; i++){
            for(int j=0; j<counts.length;j++){
                if(j<counts.length-1){
                    if(counts[j].count<counts[j+1].count){
                        Pair tmp = counts[j];
                        counts[j]=counts[j+1];
                        counts[j+1]=tmp;
                    }
                }
            }
        }

        editor = context.getSharedPreferences("saveStats", 0).edit();
        editor.putString("belGame", counts[0].name);
        editor.putString("belGame2", counts[1].name);
        editor.putString("belGame3", counts[2].name);
        editor.commit();

        Log.d("CHECKBEL","BEL: "+counts[0].name+" "+counts[0].count);
        Log.d("CHECKBEL","BEL2: "+counts[1].name+" "+counts[1].count);
        Log.d("CHECKBEL","BEL3: "+counts[2].name+" "+counts[2].count);


    }

}