package com.drink.shotbotapp.shot_botapp;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Alexander on 04.02.2018.
 */

public class MemoryDeck {
    MemoryCard[] cards;

    public MemoryDeck(){
        this.cards = new MemoryCard[16];
        cards[0]= new MemoryCard(0,"@drawable/roulette");
        cards[1]= new MemoryCard(0,"@drawable/roulette");
        cards[2]= new MemoryCard(1,"@drawable/quiz");
        cards[3]= new MemoryCard(1,"@drawable/quiz");
        cards[4]= new MemoryCard(2,"@drawable/gluecksrad");
        cards[5]= new MemoryCard(2,"@drawable/gluecksrad");
        cards[6]= new MemoryCard(3,"@drawable/hoehertiefer");
        cards[7]= new MemoryCard(3,"@drawable/hoehertiefer");
        cards[8]= new MemoryCard(4,"@drawable/bomb");
        cards[9]= new MemoryCard(4,"@drawable/bomb");
        cards[10]= new MemoryCard(5,"@drawable/balkensaufen");
        cards[11]= new MemoryCard(5,"@drawable/balkensaufen");
        cards[12]= new MemoryCard(6,"@drawable/image_1");
        cards[13]= new MemoryCard(6,"@drawable/image_1");
        cards[14]= new MemoryCard(7,"@drawable/image_2");
        cards[15]= new MemoryCard(7,"@drawable/image_2");
    }
    public void shuffle(){
        for(int i=0; i<80; i++){
            for(int j=0; j<16; j++){
                int rand = ThreadLocalRandom.current().nextInt(0, 16);
                MemoryCard tmp = this.cards[rand];
                this.cards[rand] = this.cards[j];
                this.cards[j]= tmp;
            }
        }
    }
}
