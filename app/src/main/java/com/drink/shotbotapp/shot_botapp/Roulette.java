package com.drink.shotbotapp.shot_botapp;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class Roulette extends AppCompatActivity {
    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;
    ListView lvPlayer;
    TextView[] fields;
    TextView[] fields2;
    TextView r_1;
    TextView r_2;
    TextView r_3;
    TextView r_4;
    TextView r_5;
    TextView r_6;
    TextView r_7;
    TextView r_8;
    TextView r_9;
    TextView r_10;
    TextView r_11;
    TextView r_12;
    TextView r_13;
    TextView r_14;
    TextView r_15;
    TextView r_16;
    TextView r_17;
    TextView r_18;
    TextView r_16_1;
    TextView r_16_2;
    TextView r_16_3;
    TextView r_712_1;
    TextView r_712_2;
    TextView r_712_3;
    TextView r_1318_1;
    TextView r_1318_2;
    TextView r_1318_3;
    TextView r_black_1;
    TextView r_black_2;
    TextView r_black_3;
    TextView r_black_4;
    TextView r_black_5;
    TextView r_red_1;
    TextView r_red_2;
    TextView r_red_3;
    TextView r_red_4;
    TextView r_red_5;
    TextView br_1;
    TextView br_2;
    TextView br_3;
    TextView br_4;
    TextView br_5;
    TextView br_6;
    TextView br_7;
    TextView br_8;
    TextView br_9;
    TextView br_10;
    TextView br_11;
    TextView br_12;
    TextView br_13;
    TextView br_14;
    TextView br_15;
    TextView br_16;
    TextView br_17;
    TextView br_18;
    TextView br_16_1;
    TextView br_16_2;
    TextView br_16_3;
    TextView br_712_1;
    TextView br_712_2;
    TextView br_712_3;
    TextView br_1318_1;
    TextView br_1318_2;
    TextView br_1318_3;
    TextView br_black_1;
    TextView br_black_2;
    TextView br_black_3;
    TextView br_black_4;
    TextView br_black_5;
    TextView br_red_1;
    TextView br_red_2;
    TextView br_red_3;
    TextView br_red_4;
    TextView br_red_5;
    boolean[] black;
    boolean[] red;
    boolean[] first;
    boolean[] secound;
    boolean[] third;
    ImageView wheelR;
    ImageView arrowR;
    private static final float FACTOR = 10.00f;
    boolean is_spinning;
    java.util.Random Random;
    int grad = 0, grad_alt = 0;
    //Button buttonRoulette;
    ArrayList<Player> players;
    int currentPlayer;
    int status;
    Drawable[] coins;
    boolean[] coinsetted;
    int last;
    int last2;
    int[]coinPlaces;
    LinearLayout layoutWhite;
    LinearLayout layoutRed;
    LinearLayout layoutMin;
    LinearLayout layoutMid;
    LinearLayout layoutMax;
    ArrayList<Integer> winning;
    Button btn_rouletteStartPopup;
    LinearLayout layoutStartPopup;
    TextView playerNameEnd;
    //TextView win_or_lose_text;
    //Button weiterEnd;
    ArrayList<Player> loosers;
    ImageView endPic;
    PopupWindow mpopup;
    View popUpView;
    PopupWindow mpopup2;
    View popUpView2;
    ImageView setPic;
    ImageView setPlayer;
    PopupWindow mpopup3;
    View popUpView3;
    PopupWindow mpopup4;
    View popUpView4;
    ImageView erklaerung;
    LinearLayout setzen1;
    LinearLayout setzen2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roulette);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setzen1 = findViewById(R.id.roulette_setzen1);
        setzen2 = findViewById(R.id.roulette_setzen2);
        setzen1.setVisibility(View.INVISIBLE);
        setzen2.setVisibility(View.INVISIBLE);
        popUpView = getLayoutInflater().inflate(R.layout.test,
                null); // inflating popup layout
        mpopup = new PopupWindow(popUpView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopup.setAnimationStyle(android.R.style.Animation_Dialog);
        endPic = popUpView.findViewById(R.id.roulette_EndPic);
        popUpView2 = getLayoutInflater().inflate(R.layout.roulettepopup,
                null); // inflating popup layout
        mpopup2 = new PopupWindow(popUpView2, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopup2.setAnimationStyle(android.R.style.Animation_Dialog);
        mpopup2.setOutsideTouchable(true);
        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });
        setPic = popUpView2.findViewById(R.id.roulette_SetPic);
        popUpView3 = getLayoutInflater().inflate(R.layout.roulette_erklaerungspopup,
                null); // inflating popup layout
        mpopup3 = new PopupWindow(popUpView3, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopup3.setAnimationStyle(android.R.style.Animation_Dialog);
        mpopup3.setOutsideTouchable(true);
        popUpView4 = getLayoutInflater().inflate(R.layout.roulette_zahlenbereich,
                null); // inflating popup layout
        mpopup4 = new PopupWindow(popUpView4, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopup4.setAnimationStyle(android.R.style.Animation_Dialog);
        erklaerung = popUpView3.findViewById(R.id.roulette_Erklaerung);

        setPlayer = popUpView2.findViewById(R.id.RouletteSetPlayer);
        endPic = popUpView.findViewById(R.id.roulette_EndPic);
        //endPic.setBackgroundResource(R.drawable.casino);

        winning = new ArrayList<Integer>();
        loosers = new ArrayList<Player>();
        lvPlayer = (ListView)popUpView4.findViewById(R.id.lv_roulette);
        btn_rouletteStartPopup = (Button)findViewById(R.id.btn_rouletteStartPopup);
        layoutStartPopup = (LinearLayout)findViewById(R.id.startpopupRoulette);
        layoutWhite = (LinearLayout)findViewById(R.id.layoutWhite);
        layoutRed = (LinearLayout)findViewById(R.id.layoutRed);
        layoutMin = (LinearLayout)findViewById(R.id.layoutMin);
        layoutMid = (LinearLayout)findViewById(R.id.layoutMid);
        layoutMax = (LinearLayout)findViewById(R.id.layoutMax);
        playerNameEnd = (TextView)popUpView.findViewById(R.id.playerNameEnd);
        //win_or_lose_text = (TextView)findViewById(R.id.win_or_lose_text);
        //weiterEnd = (Button)findViewById(R.id.btn_rouletteNextEnd);
        status =1;
        players = PlayersClass.getPlayerList();
        coins = new Drawable[players.size()];
        coinPlaces = new int[players.size()];
        currentPlayer =0;
        Random = new Random();
        is_spinning = false;
        black = new boolean[5];
        red = new boolean[5];
        first = new boolean[3];
        secound = new boolean[3];
        third = new boolean[3];
        for(int i=0; i<5; i++){
            black[i]=false;
            red[i]=false;
        }
        for(int i=0; i<3; i++){
            first[i]=false;
            secound[i]=false;
            third[i]=false;
        }
        coinsetted = new boolean[players.size()];
        for(int i=0; i<players.size(); i++){
            coinsetted[i]=false;
        }
        wheelR = (ImageView)findViewById(R.id.iv_wheelR);
        arrowR = (ImageView)findViewById(R.id.iv_arrowR);

        fields = new TextView[37];
        fields2 = new TextView[37];
        r_1 = (TextView)popUpView2.findViewById(R.id.r_1);
        r_2 = (TextView)popUpView2.findViewById(R.id.r_2);
        r_3 = (TextView)popUpView2.findViewById(R.id.r_3);
        r_4 = (TextView)popUpView2.findViewById(R.id.r_4);
        r_5 = (TextView)popUpView2.findViewById(R.id.r_5);
        r_6 = (TextView)popUpView2.findViewById(R.id.r_6);
        r_7 = (TextView)popUpView2.findViewById(R.id.r_7);
        r_8 = (TextView)popUpView2.findViewById(R.id.r_8);
        r_9 = (TextView)popUpView2.findViewById(R.id.r_9);
        r_10 = (TextView)popUpView2.findViewById(R.id.r_10);
        r_11 = (TextView)popUpView2.findViewById(R.id.r_11);
        r_12 = (TextView)popUpView2.findViewById(R.id.r_12);
        r_13 = (TextView)popUpView2.findViewById(R.id.r_13);
        r_14 = (TextView)popUpView2.findViewById(R.id.r_14);
        r_15 = (TextView)popUpView2.findViewById(R.id.r_15);
        r_16 = (TextView)popUpView2.findViewById(R.id.r_16);
        r_17 = (TextView)popUpView2.findViewById(R.id.r_17);
        r_18 = (TextView)popUpView2.findViewById(R.id.r_18);
        r_16_1 = (TextView)popUpView2.findViewById(R.id.r_16_1);
        r_16_2 = (TextView)popUpView2.findViewById(R.id.r_16_2);
        r_16_3 = (TextView)popUpView2.findViewById(R.id.r_16_3);
        r_712_1 = (TextView)popUpView2.findViewById(R.id.r_712_1);
        r_712_2 = (TextView)popUpView2.findViewById(R.id.r_712_2);
        r_712_3 = (TextView)popUpView2.findViewById(R.id.r_712_3);
        r_1318_1 = (TextView)popUpView2.findViewById(R.id.r_1318_1);
        r_1318_2 = (TextView)popUpView2.findViewById(R.id.r_1318_2);
        r_1318_3 = (TextView)popUpView2.findViewById(R.id.r_1318_3);
        r_red_1 = (TextView)popUpView2.findViewById(R.id.r_red1);
        r_red_2 = (TextView)popUpView2.findViewById(R.id.r_red2);
        r_red_3 = (TextView)popUpView2.findViewById(R.id.r_red3);
        r_red_4 = (TextView)popUpView2.findViewById(R.id.r_red4);
        r_red_5 = (TextView)popUpView2.findViewById(R.id.r_red5);
        r_black_1 = (TextView)popUpView2.findViewById(R.id.r_black1);
        r_black_2 = (TextView)popUpView2.findViewById(R.id.r_black2);
        r_black_3 = (TextView)popUpView2.findViewById(R.id.r_black3);
        r_black_4= (TextView)popUpView2.findViewById(R.id.r_black4);
        r_black_5 = (TextView)popUpView2.findViewById(R.id.r_black5);
        br_1 = (TextView)findViewById(R.id.r_1);
        br_2 = (TextView)findViewById(R.id.r_2);
        br_3 = (TextView)findViewById(R.id.r_3);
        br_4 = (TextView)findViewById(R.id.r_4);
        br_5 = (TextView)findViewById(R.id.r_5);
        br_6 = (TextView)findViewById(R.id.r_6);
        br_7 = (TextView)findViewById(R.id.r_7);
        br_8 = (TextView)findViewById(R.id.r_8);
        br_9 = (TextView)findViewById(R.id.r_9);
        br_10 = (TextView)findViewById(R.id.r_10);
        br_11 = (TextView)findViewById(R.id.r_11);
        br_12 = (TextView)findViewById(R.id.r_12);
        br_13 = (TextView)findViewById(R.id.r_13);
        br_14 = (TextView)findViewById(R.id.r_14);
        br_15 = (TextView)findViewById(R.id.r_15);
        br_16 = (TextView)findViewById(R.id.r_16);
        br_17 = (TextView)findViewById(R.id.r_17);
        br_18 = (TextView)findViewById(R.id.r_18);
        br_16_1 = (TextView)findViewById(R.id.r_16_1);
        br_16_2 = (TextView)findViewById(R.id.r_16_2);
        br_16_3 = (TextView)findViewById(R.id.r_16_3);
        br_712_1 = (TextView)findViewById(R.id.r_712_1);
        br_712_2 = (TextView)findViewById(R.id.r_712_2);
        br_712_3 = (TextView)findViewById(R.id.r_712_3);
        br_1318_1 = (TextView)findViewById(R.id.r_1318_1);
        br_1318_2 = (TextView)findViewById(R.id.r_1318_2);
        br_1318_3 = (TextView)findViewById(R.id.r_1318_3);
        br_red_1 = (TextView)findViewById(R.id.r_red1);
        br_red_2 = (TextView)findViewById(R.id.r_red2);
        br_red_3 = (TextView)findViewById(R.id.r_red3);
        br_red_4 = (TextView)findViewById(R.id.r_red4);
        br_red_5 = (TextView)findViewById(R.id.r_red5);
        br_black_1 = (TextView)findViewById(R.id.r_black1);
        br_black_2 = (TextView)findViewById(R.id.r_black2);
        br_black_3 = (TextView)findViewById(R.id.r_black3);
        br_black_4= (TextView)findViewById(R.id.r_black4);
        br_black_5 = (TextView)findViewById(R.id.r_black5);
        fields[0]= r_1;
        fields[1]= r_2;
        fields[2]= r_3;
        fields[3]= r_4;
        fields[4]= r_5;
        fields[5]= r_6;
        fields[6]= r_7;
        fields[7]= r_8;
        fields[8]= r_9;
        fields[9]= r_10;
        fields[10]= r_11;
        fields[11]= r_12;
        fields[12]= r_13;
        fields[13]= r_14;
        fields[14]= r_15;
        fields[15]= r_16;
        fields[16]= r_17;
        fields[17]= r_18;
        fields[18]= r_16_1;
        fields[19]= r_16_2;
        fields[20]= r_16_3;
        fields[21]= r_712_1;
        fields[22]= r_712_2;
        fields[23]= r_712_3;
        fields[24]= r_1318_1;
        fields[25]= r_1318_2;
        fields[26]= r_1318_3;
        fields[27]= r_black_1;
        fields[28]= r_black_2;
        fields[29]= r_black_3;
        fields[30]= r_black_4;
        fields[31]= r_black_5;
        fields[32]= r_red_1;
        fields[33]= r_red_2;
        fields[34]= r_red_3;
        fields[35]= r_red_4;
        fields[36]= r_red_5;
        fields2[0]= br_1;
        fields2[1]= br_2;
        fields2[2]= br_3;
        fields2[3]= br_4;
        fields2[4]= br_5;
        fields2[5]= br_6;
        fields2[6]= br_7;
        fields2[7]= br_8;
        fields2[8]= br_9;
        fields2[9]= br_10;
        fields2[10]= br_11;
        fields2[11]= br_12;
        fields2[12]= br_13;
        fields2[13]= br_14;
        fields2[14]= br_15;
        fields2[15]= br_16;
        fields2[16]= br_17;
        fields2[17]= br_18;
        fields2[18]= br_16_1;
        fields2[19]= br_16_2;
        fields2[20]= br_16_3;
        fields2[21]= br_712_1;
        fields2[22]= br_712_2;
        fields2[23]= br_712_3;
        fields2[24]= br_1318_1;
        fields2[25]= br_1318_2;
        fields2[26]= br_1318_3;
        fields2[27]= br_black_1;
        fields2[28]= br_black_2;
        fields2[29]= br_black_3;
        fields2[30]= br_black_4;
        fields2[31]= br_black_5;
        fields2[32]= br_red_1;
        fields2[33]= br_red_2;
        fields2[34]= br_red_3;
        fields2[35]= br_red_4;
        fields2[36]= br_red_5;

        for(int i=0; i<players.size(); i++){
            String uri = players.get(i).draw;
            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
            Drawable coin = getResources().getDrawable(imageResource);
            coins[i]= coin;
        }

        endPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpopup.dismiss();
                next();
            }
        });

        erklaerung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpopup3.dismiss();
                setPlayer.setImageResource(players.get(currentPlayer).getColor());
                if(players.get(currentPlayer).bild!=null){
                    setPlayer.setImageBitmap(players.get(currentPlayer).bild);
                    setPlayer.setForeground(null);
                }else{
                    setPlayer.setForeground(players.get(currentPlayer).playerPic);
                }
                mpopup2.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
            }
        });

        setPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status<20){
                    if(coinsetted[currentPlayer]){
                        mpopup2.dismiss();
                        coinPlaces[currentPlayer]=last;
                        if(status< players.size()){
                            status++;
                            currentPlayer++;
                            setPlayer.setImageResource(players.get(currentPlayer).getColor());
                            if(players.get(currentPlayer).bild!=null){
                                setPlayer.setImageBitmap(players.get(currentPlayer).bild);
                                setPlayer.setForeground(null);
                            }else{
                                setPlayer.setForeground(players.get(currentPlayer).playerPic);
                            }
                            mpopup2.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
                        }else{
                            setzen1.setVisibility(View.VISIBLE);
                            setzen2.setVisibility(View.VISIBLE);
                            status =0;
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Du musst erst deinen Coin setzen", Toast.LENGTH_LONG).show();
                    }
                }else{
                    mpopup2.dismiss();
                    setzen1.setVisibility(View.VISIBLE);
                    setzen2.setVisibility(View.VISIBLE);
                    next();
                }
            }
        });

        btn_rouletteStartPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutStartPopup.setVisibility(View.INVISIBLE);
                setPlayer.setImageResource(players.get(0).getColor());
                if(players.get(0).bild!=null){
                    setPlayer.setImageBitmap(players.get(0).bild);
                    setPlayer.setForeground(null);
                }else{
                    setPlayer.setForeground(players.get(currentPlayer).playerPic);
                }
                mpopup2.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
            }
        });

        for(int i=0; i<37; i++){
            final int a =i;
            fields[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(status>0 && status<11){
                        if(coinsetted[currentPlayer]){
                            reset();
                        }
                        if(a<18){
                            fields[a].setForeground(coins[status-1]);
                            fields2[a].setForeground(coins[status-1]);
                            last = a;
                            coinsetted[currentPlayer]=true;
                        }else if(a<21){
                            for(int j=0; j<3; j++){
                                if(!first[j]){
                                    first[j] = true;
                                    fields[18+j].setForeground(coins[status-1]);
                                    fields2[18+j].setForeground(coins[status-1]);
                                    last = 18+j;
                                    last2 =j;
                                    coinsetted[currentPlayer]=true;
                                    break;
                                }
                            }
                        }else if(a<24){
                            for(int j=0; j<3; j++){
                                if(!secound[j]){
                                    secound[j] = true;
                                    fields[21+j].setForeground(coins[status-1]);
                                    fields2[21+j].setForeground(coins[status-1]);
                                    last = 21+j;
                                    last2 =j;
                                    coinsetted[currentPlayer]=true;
                                    break;
                                }
                            }
                        }else if(a<27){
                            for(int j=0; j<3; j++){
                                if(!third[j]){
                                    third[j] = true;
                                    fields[24+j].setForeground(coins[status-1]);
                                    fields2[24+j].setForeground(coins[status-1]);
                                    last =24+j;
                                    last2 =j;
                                    coinsetted[currentPlayer]=true;
                                    break;
                                }
                            }
                        }else if(a<32){
                            for(int j=0; j<5; j++){
                                if(!black[j]){
                                    black[j] = true;
                                    fields[27+j].setForeground(coins[status-1]);
                                    fields2[27+j].setForeground(coins[status-1]);
                                    last = 27+j;
                                    last2 =j;
                                    coinsetted[currentPlayer]=true;
                                    break;
                                }
                            }
                        }else if(a<37){
                            for(int j=0; j<5; j++){
                                if(!red[j]){
                                    red[j] = true;
                                    fields[32+j].setForeground(coins[status-1]);
                                    fields2[32+j].setForeground(coins[status-1]);
                                    last = 32+j;
                                    last2 =j;
                                    coinsetted[currentPlayer]=true;
                                    break;
                                }
                            }
                        }
                    }

                }
            });
        }
        wheelR.setOnTouchListener(new OnSwipeTouchListener(Roulette.this) {
            public void onSwipeTop() {
                if(!is_spinning && status==0){
                    rotate();
                }
            }
            public void onSwipeRight() {
                if(!is_spinning && status==0){
                    rotate();
                }
            }
            public void onSwipeLeft() {
                if(!is_spinning && status==0){
                    rotate();
                }
            }
            public void onSwipeBottom() {
                if(!is_spinning && status==0){
                    rotate();
                }
            }

        });

        setupListview();
        timerClass tc = new timerClass();
        tc.start();

    }

    public void rotate(){
        is_spinning = true;
        grad = grad_alt % 360;
        grad = Random.nextInt(3600) + 439;
        RotateAnimation rotate= new RotateAnimation(grad_alt, grad,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f );
        rotate.setDuration(4215);
        rotate.setFillAfter(true);
        rotate.setInterpolator(new DecelerateInterpolator());
        rotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                winning.clear();
                int a = CurrentNumber(360 - (grad % 360));
                is_spinning =false;
                fields[a-1].setBackgroundColor(Color.GREEN);
                fields2[a-1].setBackgroundColor(Color.GREEN);
                winning.add(a-1);
                if(a%2 ==0){
                    winning.add(27);
                    winning.add(28);
                    winning.add(29);
                    winning.add(30);
                    winning.add(31);
                    layoutWhite.setBackgroundColor(Color.GREEN);
                }else{
                    winning.add(32);
                    winning.add(33);
                    winning.add(34);
                    winning.add(35);
                    winning.add(36);
                    layoutRed.setBackgroundColor(Color.GREEN);
                }
                if(a<7){
                    winning.add(18);
                    winning.add(19);
                    winning.add(20);
                    layoutMin.setBackgroundColor(Color.GREEN);
                }else if(a<13){
                    winning.add(21);
                    winning.add(22);
                    winning.add(23);
                    layoutMid.setBackgroundColor(Color.GREEN);
                }else{
                    winning.add(24);
                    winning.add(25);
                    winning.add(26);
                    layoutMax.setBackgroundColor(Color.GREEN);
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                status=21;
                currentPlayer=0;
                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 190, getResources().getDisplayMetrics());
                playerNameEnd.setVisibility(View.VISIBLE);
                //lvPlayer.setVisibility(View.INVISIBLE);
                playerNameEnd.setText(players.get(currentPlayer).getName());
                if(winning.contains(coinPlaces[currentPlayer])){
                    if(coinPlaces[currentPlayer]<18){
                        //weiterEnd.setVisibility(View.VISIBLE);
                       // win_or_lose_text.setText("Du hast auf die richtige Zahl gesetzt. Alle anderen bekommen einen Shot!");
                        endPic.setImageResource(R.drawable.casino);
                        mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
                        String shots ="";
                        for(int i=0; i<players.size(); i++){
                            if(i!=currentPlayer){
                                shots = shots+"T"+players.get(i).id+"#";
                                players.get(i).shotCountPlus();
                            }
                        }
                        shots = shots+"TB";
                        if(players.size()>1){
                            BluetoothAdapterClass.WriteBT(shots);
                        }
                    }else if(coinPlaces[currentPlayer]<27){
                        int height3 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 390, getResources().getDisplayMetrics());
                        //lvPlayer.setVisibility(View.VISIBLE);
                        //weiterEnd.setVisibility(View.INVISIBLE);
                        //win_or_lose_text.setText("Du hast auf den richtigen Zahlenbereich gesetzt. Wer bekommt einen Shot?");
                        mpopup4.showAtLocation(popUpView, Gravity.CENTER, 0, 0);
                    }else{
                        //weiterEnd.setVisibility(View.VISIBLE);
                        //win_or_lose_text.setText("Du hast auf die richtige Farbe gesetzt und wirst verschont!");
                        endPic.setImageResource(R.drawable.casino_farbe);
                        mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
                    }
                }else{
                    //weiterEnd.setVisibility(View.VISIBLE);
                    //win_or_lose_text.setText("Du hast verloren und bekommst dafür am Ende der Runde einen Shot");
                    endPic.setImageResource(R.drawable.casino_loose);
                    mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
                    loosers.add(players.get(currentPlayer));
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        wheelR.startAnimation(rotate);
    }

    public void reset(){
        if(last<18){
        }else if(last<21){
            first[last2]=false;
        }else if(last<24){
            secound[last2]=false;
        }else if(last<27){
            third[last2]=false;
        }else if(last<32){
            black[last2]=false;
        }else{
            red[last2]=false;
        }
        fields[last].setForeground(null);
        fields2[last].setForeground(null);

    }

    private int CurrentNumber(int grad){
        int text = 0;

        if(grad >= (FACTOR * 0) && grad <= (FACTOR * 2)){
            text = 18;
        }
        if(grad >= (FACTOR * 2) && grad <= (FACTOR * 4)){
            text = 7;
        }
        if(grad >= (FACTOR * 4) && grad <= (FACTOR * 6)){
            text = 6;
        }
        if(grad >= (FACTOR * 6) && grad <= (FACTOR * 8)){
            text = 17;
        }
        if(grad >= (FACTOR * 8) && grad <= (FACTOR * 10)){
            text = 8;
        }
        if(grad >= (FACTOR * 10) && grad <= (FACTOR * 12)){
            text = 5;
        }
        if(grad >= (FACTOR * 12) && grad <= (FACTOR * 14)){
            text = 12;
        }
        if(grad >= (FACTOR * 14) && grad <= (FACTOR * 16)){
            text = 15;
        }
        if(grad >= (FACTOR * 16) && grad <= (FACTOR * 18)){
            text = 2;
        }
        if(grad >= (FACTOR * 18) && grad <= (FACTOR * 20)){
            text = 11;
        }
        if(grad >= (FACTOR * 20) && grad <= (FACTOR * 22)){
            text = 10;
        }
        if(grad >= (FACTOR * 22) && grad <= (FACTOR * 24)){
            text = 3;
        }
        if(grad >= (FACTOR * 24) && grad <= (FACTOR * 26)){
            text = 16;
        }
        if(grad >= (FACTOR * 26) && grad <= (FACTOR * 28)){
            text = 9;
        }
        if(grad >= (FACTOR * 28) && grad <= (FACTOR * 30)){
            text = 4;
        }
        if(grad >= (FACTOR * 30) && grad <= (FACTOR * 32)){
            text = 13;
        }
        if(grad >= (FACTOR * 32) && grad <= (FACTOR * 34)){
            text = 14;
        }
        if(grad >= (FACTOR * 34) && grad <= (FACTOR * 36)){
            text = 1;
        }


        return text;
    }

    public void resetFields(){
        for(int i=0; i<5; i++){
            black[i]=false;
            red[i]=false;
        }
        for(int i=0; i<3; i++){
            first[i]=false;
            secound[i]=false;
            third[i]=false;
        }
        layoutWhite.setBackgroundResource(R.color.whiteyellow);
        layoutRed.setBackgroundResource(R.color.Rot);
        layoutMin.setBackgroundResource(R.color.whiteyellow);
        layoutMax.setBackgroundResource(R.color.whiteyellow);
        layoutMid.setBackgroundResource(R.color.Rot);
        for(int i=0; i<18; i++){
            if(i%2 ==0){
                fields[i].setBackgroundResource(R.color.Rot);
                fields2[i].setBackgroundResource(R.color.Rot);
            }else{
                fields[i].setBackgroundResource(R.color.whiteyellow);
                fields2[i].setBackgroundResource(R.color.whiteyellow);
            }
        }
        for(int i=0; i<fields.length; i++){
            fields[i].setForeground(null);
            fields2[i].setForeground(null);
        }
    }

    public void setupListview(){
        try {
            ArrayAdapter<Player> adapter = new Roulette.PlayerListAdapter();
            lvPlayer.setAdapter(adapter);

        } catch (Exception e) {
        }
    }

    public void next(){
        if (status<players.size()+20){
            //updateListView();
            //lvPlayer.setVisibility(View.INVISIBLE);
            status++;
            currentPlayer++;
            playerNameEnd.setText(players.get(currentPlayer).getName());
            if(winning.contains(coinPlaces[currentPlayer])){
                if(coinPlaces[currentPlayer]<18){
                    //weiterEnd.setVisibility(View.VISIBLE);
                    //win_or_lose_text.setText("Du hast auf die richtige Zahl gesetzt. Alle anderen bekommen einen Shot!");
                    endPic.setImageResource(R.drawable.casino);
                    mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
                    String shots ="";
                    for(int i=0; i<players.size(); i++){
                        if(i!=currentPlayer){
                            shots = shots+"T"+players.get(i).id+"#";
                            players.get(i).shotCount++;
                        }
                    }
                    shots = shots+"TB";
                    BluetoothAdapterClass.WriteBT(shots);

                }else if(coinPlaces[currentPlayer]<27){
                   // lvPlayer.setVisibility(View.VISIBLE);
                    //weiterEnd.setVisibility(View.INVISIBLE);
                    //win_or_lose_text.setText("Du hast auf den richtigen Zahlenbereich gesetzt. Wer bekommt einen Shot?");
                    mpopup4.showAtLocation(popUpView, Gravity.CENTER, 0, 0);
                }else{
                    //weiterEnd.setVisibility(View.VISIBLE);
                    //win_or_lose_text.setText("Du hast auf die richtige Farbe gesetzt und wirst verschont!");
                    endPic.setImageResource(R.drawable.casino_farbe);
                    mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
                }
            }else{
                //weiterEnd.setVisibility(View.VISIBLE);
                //win_or_lose_text.setText("Du hast verloren und bekommst dafür am Ende der Runde einen Shot");
                endPic.setImageResource(R.drawable.casino_loose);
                mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
                loosers.add(players.get(currentPlayer));
            }
        }else if(status==players.size()+20){
           // weiterEnd.setVisibility(View.VISIBLE);
            //win_or_lose_text.setText("Alle Verlierer dieser Runde trinken!");
            playerNameEnd.setVisibility(View.INVISIBLE);
            endPic.setImageResource(R.drawable.casino_loosershot);
            mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
            String shots2 ="";
            for(int i=0; i<loosers.size(); i++){
                shots2 = shots2+"T"+loosers.get(i).id+"#";
               loosers.get(i).shotCount++;
            }
            shots2 = shots2+"TB";
            if(loosers.size()>0){
                BluetoothAdapterClass.WriteBT(shots2);
            }
            status++;
        }else{
            setzen1.setVisibility(View.INVISIBLE);
            setzen2.setVisibility(View.INVISIBLE);
            mpopup2.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
            currentPlayer=0;
            resetFields();
            loosers.clear();
            status =1;
            for(int i=0; i<coinsetted.length; i++){
                coinsetted[i]=false;
            }
            setPlayer.setImageResource(players.get(currentPlayer).getColor());
            if(players.get(currentPlayer).bild!=null){
                setPlayer.setImageBitmap(players.get(currentPlayer).bild);
                setPlayer.setForeground(null);
            }else{
                setPlayer.setForeground(players.get(currentPlayer).playerPic);
            }
        }
    }

    public void updateListView() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((Roulette.PlayerListAdapter) lvPlayer.getAdapter()).notifyDataSetChanged();
            }
        });

    }

    private class PlayerListAdapter extends ArrayAdapter<Player> {
        public PlayerListAdapter() {
            super(Roulette.this, R.layout.roulettelistentry, players);


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.roulettelistentry, parent, false);
            }

            //Find Item
            final Player player = players.get(position);

            //Insert in View
            TextView b = (TextView) itemView.findViewById(R.id.r_listentry);
            if(b != null){
                b.setText(player.getName());

                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        player.oneShot();
                        next();
                        mpopup4.dismiss();
                    }
                });
            }
            return itemView;
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            mpopup3.showAtLocation(popUpView, Gravity.CENTER, 0, 0); // Displaying popup
        }
    };

    public class timerClass extends Thread{
        public void run(){
            int timer = 3;
            while(timer >0){
                timer --;
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            mHandler.obtainMessage(0).sendToTarget();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }
}
