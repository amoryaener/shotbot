package com.drink.shotbotapp.shot_botapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.MaskFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Random;

public class Draw extends AppCompatActivity implements ColorPickerDialog.OnColorChangedListener{

    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;
    String randomBegriff;
    ImageView drawStart;
    boolean locked = true;
    MyView mv;
    AlertDialog dialog;
    LinearLayout layout;
    ImageView color1;
    ImageView color2;
    ImageView color3;
    ImageView color4;
    ImageView color5;
    ImageView color6;
    ImageView pen1;
    ImageView pen2;
    ImageView pen3;
    ImageView rubber;
    ArrayList<Path> paths = new ArrayList<Path>();
    ArrayList<Paint> paints = new ArrayList<Paint>();
    int width=20;
    int color;
    TextView countdown;
    TextView begriff;
    int count =30;
    int currentPlayer =0;
    ArrayList<Player> players;
    TextView popupname;
    TextView popupbeg;
    PopupWindow mpopupPlayer;
    View popUpViewPlayer;
    PopupWindow mpopupPlayer2;
    View popUpViewPlayer2;
    ArrayList<String> begriffe;
    ImageView popuppic;
    ImageView popuppic2;
    Context context;
    ArrayList<Bitmap> bitmaps;
    Random randomGen = new Random();
    int seks =30;
    ImageView einstellungen;
    CrystalSeekbar rangeSeekbar;
    TextView malzeit;
    boolean einstellungenSichtbar = false;
    LinearLayout ranger;
    boolean running=false;
    boolean begrenzt = false;
    int rounds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        bitmaps = new ArrayList<Bitmap>();
        layout = findViewById(R.id.drawLay2);
        context = this;
        rounds = getIntent().getIntExtra("Rounds",0);
        if(rounds >0){
            begrenzt=true;
        }
        SystemState.act=this;
        mv = new MyView(this);
        mv.setDrawingCacheEnabled(true);
        mv.setBackgroundColor(Color.WHITE);
        layout.addView(mv);
        color = getResources().getColor(R.color.black);
        malzeit = findViewById(R.id.malzeit);
        rangeSeekbar = findViewById(R.id.rangeSeekbar2);
        rangeSeekbar.setLeft(30);
        ranger = findViewById(R.id.ranger);
        ranger.setVisibility(View.GONE);
        malzeit.setVisibility(View.GONE);
        rangeSeekbar.setVisibility(View.GONE);
        drawStart = findViewById(R.id.drawStart);
        einstellungen = findViewById(R.id.draw_einstellungen);
        drawStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawStart.setVisibility(View.GONE);
                mpopupPlayer.showAtLocation(popUpViewPlayer, Gravity.CENTER, 0, 0); // Displaying popup
            }
        });
        /*
        rangeSeekbar.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue) {
                malzeit.setText("Zeit: "+String.valueOf(minValue));
                seks = minValue.intValue();
                if(!running){
                    count =minValue.intValue();
                    countdown.setText(""+count);
                }
            }
        });*/

        einstellungen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(einstellungenSichtbar){
                    einstellungenSichtbar=false;
                    ranger.setVisibility(View.GONE);
                    rangeSeekbar.setVisibility(View.GONE);
                    malzeit.setVisibility(View.GONE);
                    rangeSeekbar.setClickable(false);
                    ranger.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_hide));
                }else {
                    einstellungenSichtbar=true;
                    ranger.setVisibility(View.VISIBLE);
                    rangeSeekbar.setVisibility(View.VISIBLE);
                    malzeit.setVisibility(View.VISIBLE);
                    rangeSeekbar.setClickable(true);
                    ranger.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_show));
                }
            }
        });

        begriffe = new ArrayList<String>();
        begriffe.add("Haus");
        begriffe.add("Baum");
        begriffe.add("Strand");
        begriffe.add("Garten");
        begriffe.add("Auto");
        begriffe.add("DNA");
        begriffe.add("Smartphone");
        begriffe.add("Fernseher");
        begriffe.add("Pferd");
        begriffe.add("Hund");
        begriffe.add("Bier");
        begriffe.add("Nuss");
        begriffe.add("Pizza");
        begriffe.add("Playstation");
        begriffe.add("Batman");
        begriffe.add("Frau");
        begriffe.add("PC");
        begriffe.add("Shot-Bot");
        begriffe.add("Brücke");
        begriffe.add("Tankstelle");
        begriffe.add("Geld");
        begriffe.add("Mount Everest");
        begriffe.add("Kuh");
        begriffe.add("Schwein");
        begriffe.add("Fahrrad");
        begriffe.add("Lutschbonbon");
        begriffe.add("Polenböller");
        begriffe.add("Zungenkuss");
        begriffe.add("Zucker");
        begriffe.add("Hexenkessel");
        begriffe.add("Regenschauer");
        begriffe.add("Kater");
        begriffe.add("Wasser");
        begriffe.add("Hundeleine");
        begriffe.add("Schlagzeug");
        begriffe.add("Sex");
        begriffe.add("Frosch");
        begriffe.add("Fischstäbchen");
        begriffe.add("Kettenraucher");
        begriffe.add("Ameisenbär");
        begriffe.add("Milchstraße");
        begriffe.add("Nudel");
        begriffe.add("einen Halbkreis");
        begriffe.add("Sauerstoff");
        begriffe.add("Schwarzgeld");
        begriffe.add("Kotflügel");
        begriffe.add("Blaskapelle");
        begriffe.add("Kopfsalat");
        begriffe.add("Brechstange");
        begriffe.add("Puffreis");
        begriffe.add("Eierwärmer");
        begriffe.add("Sauerkraut");
        begriffe.add("Rostlaube");
        begriffe.add("Bodensee");
        begriffe.add("Bücherei");
        begriffe.add("Schaumkrone");
        begriffe.add("Medizinball");
        begriffe.add("Marmorkuchen");
        begriffe.add("Quarktasche");
        begriffe.add("Hühnerauge");

        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });

        randomBegriff = begriffe.get(randomGen.nextInt(begriffe.size()));

        popUpViewPlayer = getLayoutInflater().inflate(R.layout.drawplayerpopup,
                null); // inflating popup layout
        mpopupPlayer = new PopupWindow(popUpViewPlayer, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopupPlayer.setAnimationStyle(android.R.style.Animation_Dialog);
        popUpViewPlayer2 = getLayoutInflater().inflate(R.layout.drawlayoutend,
                null); // inflating popup layout
        mpopupPlayer2 = new PopupWindow(popUpViewPlayer2, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopupPlayer2.setAnimationStyle(android.R.style.Animation_Dialog);

        popupname = popUpViewPlayer.findViewById(R.id.drawPopupName);
        popupbeg = popUpViewPlayer.findViewById(R.id.drawPopupBeg);
        popuppic = popUpViewPlayer.findViewById(R.id.drawPopupPic);
        popuppic2 = popUpViewPlayer2.findViewById(R.id.drawPopupPic2);

        players = PlayersClass.getPlayerList();
        begriff = findViewById(R.id.drawBeg);
        countdown = findViewById(R.id.drawCountdown);
        countdown.setText(""+seks);

        popupname.setText(players.get(currentPlayer).getName());
        popupbeg.setText(randomBegriff);
        begriff.setText(randomBegriff);

        color1 = findViewById(R.id.drawColor1);
        color2 = findViewById(R.id.drawColor2);
        color3 = findViewById(R.id.drawColor3);
        color4 = findViewById(R.id.drawColor4);
        color5 = findViewById(R.id.drawColor5);
        color6 = findViewById(R.id.drawColor6);

        pen1 = findViewById(R.id.drawPen1);
        pen2 = findViewById(R.id.drawPen2);
        pen3 = findViewById(R.id.drawPen3);
        rubber = findViewById(R.id.drawPen4);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(getResources().getColor(R.color.black));
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(20);
        mEmboss = new EmbossMaskFilter(new float[] { 1, 1, 1 },
                0.4f, 6, 3.5f);
        mBlur = new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL);

        popuppic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpopupPlayer.dismiss();
                locked = false;
                timerClass tc = new timerClass();
                tc.start();
            }
        });

        popuppic2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count=seks;
                countdown.setText(seks+"");
                Bitmap bitmap = mv.getDrawingCache();
                bitmap = Bitmap.createScaledBitmap(bitmap,400,400,false);
                bitmaps.add(bitmap);
                mpopupPlayer2.dismiss();
                locked=false;
                if(currentPlayer<players.size()-1){
                    layout.removeView(mv);
                    mv = new MyView(context);
                    mv.setDrawingCacheEnabled(true);
                    mv.setBackgroundColor(Color.WHITE);
                    layout.addView(mv);
                    color = getResources().getColor(R.color.black);

                    paths = new ArrayList<Path>();
                    paints = new ArrayList<Paint>();
                    mPaint = new Paint();
                    mPaint.setAntiAlias(true);
                    mPaint.setDither(true);
                    mPaint.setColor(getResources().getColor(R.color.black));
                    mPaint.setStyle(Paint.Style.STROKE);
                    mPaint.setStrokeJoin(Paint.Join.ROUND);
                    mPaint.setStrokeCap(Paint.Cap.ROUND);
                    mPaint.setStrokeWidth(20);
                    currentPlayer++;
                    popupname.setText(players.get(currentPlayer).getName());
                    mpopupPlayer.showAtLocation(popUpViewPlayer, Gravity.CENTER, 0, 0); // Displaying popup
                }else{
                    DrawSave.setBitmaps(bitmaps);
                    Intent i = new Intent(Draw.this, Draw2.class);
                    i.putExtra("beg", 1);
                    startActivity(i);
                }
            }
        });

        color1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mv.colorchange(1);
            }
        });
        color2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mv.colorchange(2);
            }
        });
        color3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mv.colorchange(3);
            }
        });
        color4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mv.colorchange(4);
            }
        });
        color5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mv.colorchange(5);
            }
        });
        color6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mv.colorchange(6);
            }
        });
        pen1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mv.colorchange(7);
            }
        });
        pen2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mv.colorchange(8);
            }
        });
        pen3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mv.colorchange(9);
            }
        });
        rubber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mv.colorchange(10);
            }
        });
    }

    private Paint       mPaint;
    private MaskFilter mEmboss;
    private MaskFilter  mBlur;

    public void colorChanged(int color) {
        mPaint.setColor(color);
    }

    public class MyView extends View {

        private static final float MINP = 0.25f;
        private static final float MAXP = 0.75f;
        private Bitmap  mBitmap;
        private Canvas  mCanvas;
        private Path    mPath;
        private Paint   mBitmapPaint;
        Context context;

        public MyView(Context c) {
            super(c);
            context=c;
            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);

        }

        public void colorchange(int a){
            if(a==1){
                color=getResources().getColor(R.color.black);
            }
            if(a==2){
                color= getResources().getColor(R.color.redDraw);
            }
            if(a==3){
                color=getResources().getColor(R.color.blueDraw);
            }
            if(a==4){
                color=getResources().getColor(R.color.greenDraw);
            }
            if(a==5){
                color=getResources().getColor(R.color.orangeDraw);
            }
            if(a==6){
                color = getResources().getColor(R.color.yellowDraw);
            }
            if(a==7){
                width =20;
            }
            if(a==8){
                width =30;
            }
            if(a==9){
                width =10;
            }
            if(a==10){
                color=getResources().getColor(R.color.white);
            }
            paths.add(mPath);
            paints.add(mPaint);
            mPaint = new Paint();
            mPaint.setColor(color);
            mPaint.setAntiAlias(true);
            mPaint.setDither(true);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeJoin(Paint.Join.ROUND);
            mPaint.setStrokeCap(Paint.Cap.ROUND);
            mPaint.setStrokeWidth(width);
            mPath = new Path();
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);

        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            for (Path p : paths){canvas.drawPath(p, paints.get(paths.indexOf(p)));}
            canvas.drawPath(mPath, mPaint);

            //canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
            //canvas.drawPath(mPath, mPaint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();

            if(!locked){
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mPath.moveTo(event.getX(), event.getY());
                        break;

                    case MotionEvent.ACTION_MOVE:
                        mPath.lineTo(event.getX(), event.getY());
                        invalidate();
                        break;

                    case MotionEvent.ACTION_UP:
                        break;
                }
            }
            return true;
        }
    }

    public void playerchange(){
        locked =true;
        mpopupPlayer2.showAtLocation(popUpViewPlayer, Gravity.CENTER, 0, 0); // Displaying popup
    }


    private static final int COLOR_MENU_ID = Menu.FIRST;
    private static final int EMBOSS_MENU_ID = Menu.FIRST + 1;
    private static final int BLUR_MENU_ID = Menu.FIRST + 2;
    private static final int ERASE_MENU_ID = Menu.FIRST + 3;
    private static final int SRCATOP_MENU_ID = Menu.FIRST + 4;
    private static final int Save = Menu.FIRST + 5;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        menu.add(0, COLOR_MENU_ID, 0, "Color").setShortcut('3', 'c');
        menu.add(0, EMBOSS_MENU_ID, 0, "Emboss").setShortcut('4', 's');
        menu.add(0, BLUR_MENU_ID, 0, "Blur").setShortcut('5', 'z');
        menu.add(0, ERASE_MENU_ID, 0, "Erase").setShortcut('5', 'z');
        menu.add(0, SRCATOP_MENU_ID, 0, "SrcATop").setShortcut('5', 'z');
        menu.add(0, Save, 0, "Save").setShortcut('5', 'z');

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mPaint.setXfermode(null);
        mPaint.setAlpha(0xFF);

        switch (item.getItemId()) {
            case COLOR_MENU_ID:
                new ColorPickerDialog(this, this, mPaint.getColor()).show();
                return true;
            case EMBOSS_MENU_ID:
                if (mPaint.getMaskFilter() != mEmboss) {
                    mPaint.setMaskFilter(mEmboss);
                } else {
                    mPaint.setMaskFilter(null);
                }
                return true;
            case BLUR_MENU_ID:
                if (mPaint.getMaskFilter() != mBlur) {
                    mPaint.setMaskFilter(mBlur);
                } else {
                    mPaint.setMaskFilter(null);
                }
                return true;
            case ERASE_MENU_ID:
                mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                mPaint.setAlpha(0x80);
                return true;
            case SRCATOP_MENU_ID:

                mPaint.setXfermode(new PorterDuffXfermode(
                        PorterDuff.Mode.SRC_ATOP));
                mPaint.setAlpha(0x80);
                return true;
            case Save:
                AlertDialog.Builder editalert = new AlertDialog.Builder(Draw.this);
                editalert.setTitle("Please Enter the name with which you want to Save");
                final EditText input = new EditText(Draw.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.FILL_PARENT,
                        LinearLayout.LayoutParams.FILL_PARENT);
                input.setLayoutParams(lp);
                editalert.setView(input);
                editalert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        String name= input.getText().toString();
                        Bitmap bitmap = mv.getDrawingCache();

                        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
                        File file = new File("/sdcard/"+name+".png");
                        try
                        {
                            if(!file.exists())
                            {
                                file.createNewFile();
                            }
                            FileOutputStream ostream = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 10, ostream);
                            ostream.close();
                            mv.invalidate();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }finally
                        {

                            mv.setDrawingCacheEnabled(false);
                        }
                    }
                });

                editalert.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what==0){
                countdown.setText(""+count--);
            }else if(msg.what==1){
                playerchange();
            }
        }
    };

    public class timerClass extends Thread{

        public void run(){
            running=true;
            int zeit = seks+1;
            for(int i=0; i<zeit; i++){
                try {
                    Thread.sleep(1000);
                    mHandler.obtainMessage(0).sendToTarget();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            running=false;
            mHandler.obtainMessage(1).sendToTarget();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }

}
