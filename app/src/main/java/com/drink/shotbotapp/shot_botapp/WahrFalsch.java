package com.drink.shotbotapp.shot_botapp;

import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.wajahatkarim3.easyflipview.EasyFlipView;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class WahrFalsch extends AppCompatActivity {

    int zufall;
    int progressDiff;
    String Status;
    Boolean Getrunken = true;
    int roundCounter=0;
    String[] zwischenAktionen;
    boolean einstellungenSichtbar = false;
    boolean timeStopper=false;
    ImageView einstellungen;
    Switch switch1;
    Switch switch2;
    Switch switch3;
    ListView lvList;
    ProgressBar progressBar;
    ImageView ampel;
    ArrayList<Player> players;
    int currentPlayer;
    ImageView playerpic;
    TextView playername;
    TextView frage;
    TextView frage2;
    EasyFlipView flip;
    TextView wahr;
    TextView falsch;
    boolean[] randomQ;
    ArrayList<PlayersWithCounts> listCount;
    int actualEntry;
    int status;
    ArrayList<WahrFalschEntry> entries;
    boolean randomAktion = false;
    Random randomGen = new Random();
    int count =0;
    boolean begrenzt = false;
    int rounds;
    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wahr_falsch);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        rounds = getIntent().getIntExtra("Rounds",0);
        if(rounds >0){
            begrenzt=true;
        }
        GlobalStats.gameStartet("Wahr oder Falsch");
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        players = PlayersClass.getPlayerList();
        Typeface KreideSchrift = Typeface.createFromAsset(getAssets(), "KreideSchrift.ttf");
        ampel = findViewById(R.id.ampel);
        progressBar = findViewById(R.id.progressBar_wahr);
        flip = findViewById(R.id.flip_wahr);
        frage2 = findViewById(R.id.question2_wahr);
        lvList = findViewById(R.id.quizlv_wahr);
        einstellungen = findViewById(R.id.quiz_einstellungen_wahr);
        switch1 = findViewById(R.id.quiz_switch1_wahr);
        switch2 = findViewById(R.id.quiz_switch2_wahr);
        switch3 = findViewById(R.id.quiz_switch3_wahr);
        flip.setFlipOnTouch(true);
        flip.setFlipEnabled(true);
        flip.setFlipDuration(1000);
        zwischenAktionen = new String[2];
        zwischenAktionen[0]="Der Dümmste trinkt!";
        zwischenAktionen[1]="Der Schlauste trinkt!";
        progressBar.setMax(12000);
        progressBar.setProgress(progressBar.getMax());
        progressDiff = progressBar.getMax()/120;
        switch1.setVisibility(View.GONE);
        switch2.setVisibility(View.GONE);
        switch3.setVisibility(View.GONE);
        einstellungen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(einstellungenSichtbar){
                    einstellungenSichtbar=false;
                    switch1.setVisibility(View.GONE);
                    switch2.setVisibility(View.GONE);
                    switch3.setVisibility(View.GONE);
                    switch1.setClickable(false);
                    switch2.setClickable(false);
                    switch3.setClickable(false);
                    switch1.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_hide));
                    switch2.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab3_hide));
                    switch3.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab4_hide));
                }else {
                    einstellungenSichtbar=true;
                    switch1.setVisibility(View.GONE);
                    switch2.setVisibility(View.VISIBLE);
                    switch3.setVisibility(View.VISIBLE);
                    switch1.setClickable(true);
                    switch2.setClickable(true);
                    switch3.setClickable(true);
                    switch1.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_show));
                    switch2.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab3_show));
                    switch3.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab4_show));
                }
            }
        });
        listCount = new ArrayList<PlayersWithCounts>();
        for(int i=0; i<players.size(); i++){
            listCount.add(new PlayersWithCounts(players.get(i)));
        }
        BluetoothAdapterClass.setHandler(mHandler);
        setupListview();
        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });
        switch3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePoiListView();
            }
        });
        entries = new ArrayList<WahrFalschEntry>();
        entries.add(new WahrFalschEntry("Bud Spencer hält ein Patent auf einen Spazierstock mit eingebauter Sitzgelegenheit.",true));
        entries.add(new WahrFalschEntry("Buzz Aldrin war der erste Mensch, der auf der Erde Stuhlgang hatte.",true));
        entries.add(new WahrFalschEntry("Ein Pferd kann ungefähr 24PS leisten.",true));
        entries.add(new WahrFalschEntry("Die upsprüngliche Bezeichnung für Fernseher war Weitwegguckapparat.",false));
        entries.add(new WahrFalschEntry("Kängururs furzen hochkonzentreiertes Methan.",false));
        entries.add(new WahrFalschEntry("Fabian Völker heißt mit zweitem Vornamen Florian.",true));
        entries.add(new WahrFalschEntry("Alle US-Präsidenten waren Einzelkinder.",false));
        entries.add(new WahrFalschEntry("Ameisen schlafen nie.",true));
        entries.add(new WahrFalschEntry("Fische können ertrinken.",true));
        entries.add(new WahrFalschEntry("Kakerlaken können bis zu eine Woche ohne Kopf überleben.",true));
        entries.add(new WahrFalschEntry("Die Zunge eines Blauwahls wiegt so viel wie drei Elefanten.",false));
        entries.add(new WahrFalschEntry("Die Inka waren ein Indianervolk in Nordeuropa.",false));
        entries.add(new WahrFalschEntry("Ein Schaltjahr hat 366 Tage.",true));
        entries.add(new WahrFalschEntry("Die Kontinente nehmen über 50% der Erdfläche ein.",false));
        entries.add(new WahrFalschEntry("Ein Monat hat bei 30 Tagen 744 Stunden.",false));
        entries.add(new WahrFalschEntry("Der ehemalige Tennisstar Boris Becker heißt mit zweitem Vornamen 'Karl'.",false));
        entries.add(new WahrFalschEntry("Die Antarktis wird mitlerweile als eigener Kontinent angesehen.",true));
        entries.add(new WahrFalschEntry("Laut Einstein bewegen sich Photonen stets mit Lichtgeschwindigkeit.",true));
        entries.add(new WahrFalschEntry("Eine Fabel ist eine kurze Erzählung, in sich zumeist Menschen wie Tiere verhalten.",false));
        entries.add(new WahrFalschEntry("Das Kartenspiel Uno wurde 1969 in Mailand entwickelt.",false));
        entries.add(new WahrFalschEntry("Stubenfliegen können bis zu 20 Tage alt werden.",false));
        entries.add(new WahrFalschEntry("Stephen King's Durchbruch gelang ihm mit dem Roman 'Carrie'.",true));
        entries.add(new WahrFalschEntry("Global Positioning System wird 'GPS' abgekürzt.",true));
        entries.add(new WahrFalschEntry("Eisbären fressen Pinguine.",false));
        entries.add(new WahrFalschEntry("Die Nachbarplaneten der Erde sind Venus und Mars.",true));
        entries.add(new WahrFalschEntry("Krim ist eine Halbinsel an der nördlichen Küste des Schwarzen Meeres.",false));
        entries.add(new WahrFalschEntry("Der Landgeschwindigkeitsrekord liegt bei über 1.227 km/h.",true));
        entries.add(new WahrFalschEntry("Die Erde ist kugelförmig.",true));
        entries.add(new WahrFalschEntry("Wale können Sonnenbrand bekommen.",true));
        entries.add(new WahrFalschEntry("In Deutschland sind 38% der Hausärzte übergewichtig.",true));
        entries.add(new WahrFalschEntry("'Scream' sollte ursprünglich 'Scary Movie' heißen.",true));
        entries.add(new WahrFalschEntry("Der FC Bayern München war Gründungsmitglied der Bundesliga.",false));
        entries.add(new WahrFalschEntry("Die Abkürzung Ka im Periodensystem steht für das Element Kalium.",false));
        entries.add(new WahrFalschEntry("Silizium ist ein Halbmetall.", true));
        entries.add(new WahrFalschEntry("Frankfurt am Main ist die Landeshauptstadt Hessens.", false));
        entries.add(new WahrFalschEntry("Frankreich ist ein Mitglied des Sicherheitsrats der Vereinten Nationen.", true));
        entries.add(new WahrFalschEntry("Kolumbien ist der größte Kaffeeproduzent der Welt.", false));
        entries.add(new WahrFalschEntry("Austern sind zweigeschlechtlich.", true));
        entries.add(new WahrFalschEntry("Seesterne ernähren sich nur von Algen.", false));
        entries.add(new WahrFalschEntry("Die Erdbeere ist keine Beere sondern eine Sammelnussfrucht.", true));
        entries.add(new WahrFalschEntry("Der Geburtsort von Franz Kafka ist Berlin.", false));
        entries.add(new WahrFalschEntry("Stee Lee ist der Schöpfer von Spiderman.", true));
        entries.add(new WahrFalschEntry("'Robinson Crusoe' ist ein Roman von Jonathan Swift.", false));
        entries.add(new WahrFalschEntry("Das Tal der Könige befindet sich in Ägypten.", true));
        entries.add(new WahrFalschEntry("Die Inkas haben in Nordamerika gelebt.", false));
        entries.add(new WahrFalschEntry("Christoph Kolumbus war ein spanischer Seefahrer.", false));
        entries.add(new WahrFalschEntry("Luanda ist die Hauptstadt und größte Stadt von Angola.", true));
        entries.add(new WahrFalschEntry("Der 'Tokyo Tower' ist eine Nachbildung des Empire State Building.", false));
        entries.add(new WahrFalschEntry("Der Mount St. Helens ist der höchste Berg in Nordamerika.", false));
        entries.add(new WahrFalschEntry("'Coq au vin' ist ein Fischgericht.", false));
        entries.add(new WahrFalschEntry("Das Schnabeltier ist ein eierlegendes Säugetier aus Australien.", true));
        entries.add(new WahrFalschEntry("Gisele Bündchen ist ein brasilianisches Model.", true));
        entries.add(new WahrFalschEntry("Natalie Portman hatte ihr Filmdebüt in 'Leon - Der Profi'.", true));
        entries.add(new WahrFalschEntry("Mumbai ist die Hauptstadt von Indien.", false));
        entries.add(new WahrFalschEntry("Mark Zuckerberg ist der Gründer von Twitter.", false));
        entries.add(new WahrFalschEntry("'Call me Maybe' ist ein Song der Sängerin Loreen.", false));
        entries.add(new WahrFalschEntry("Jupiter ist der größte Planet im Sonnensystem.", true));
        entries.add(new WahrFalschEntry("Der Vulkan Ätna liegt auf der italienischen Insel Sizilien.", true));
        entries.add(new WahrFalschEntry("Monaco grenzt an Frankreich und Italien.", false));
        entries.add(new WahrFalschEntry("Die meisten Meschen sind linkshänder.", false));
        entries.add(new WahrFalschEntry("Commander Spock ist ein Erdling.", false));
        entries.add(new WahrFalschEntry("Himengway hieß mit Vornamen Ernest Miller.", true));
        entries.add(new WahrFalschEntry("Ein misstrausischer Mensch ist ein Choleriker.", false));
        entries.add(new WahrFalschEntry("DIe Aufgabe der Vereinten Nationen ist unter Anderem der Schutz der Menschenrechte.", true));
        entries.add(new WahrFalschEntry("Der Präriehund ist ein Nagetier.", true));
        entries.add(new WahrFalschEntry("Die Frucht der Rose heißt Quitte.", false));
        entries.add(new WahrFalschEntry("San Franzisco liegt an der Ostküste der USA.", false));
        entries.add(new WahrFalschEntry("Der Erdkern besteht aus reinem Gold.", false));
        entries.add(new WahrFalschEntry("Die meisten Senioren fühlen sich jünger als sie tatsächlich sind.", true));
        entries.add(new WahrFalschEntry("Die Rolling Stones schrieben 'Knocking On Haevens Door'.", false));
        entries.add(new WahrFalschEntry("Pinguine sind unter den Vögeln die schnellsten Schwimmer.", true));
        entries.add(new WahrFalschEntry("Ein Thermostat reguliert die Fahrgeschwindigkeit von Autos.", false));
        entries.add(new WahrFalschEntry("Endivie ist eine bekannte Shampoomarke.", false));
        entries.add(new WahrFalschEntry("In Athen wurden die ersten Olympischen Spiele der Neuzeit ausgetragen.", true));
        entries.add(new WahrFalschEntry("Kaffeebohnen wachsen an Reben.", false));
        entries.add(new WahrFalschEntry("Die Brasilianer Ronaldinho und Ronaldo sind Brüder.", false));
        entries.add(new WahrFalschEntry("Der Macintosh-Computer wurde von Apple erstellt.", true));
        entries.add(new WahrFalschEntry("Die errsten Cheerleeder-Teams kamen aus Neuseeland.", false));
        entries.add(new WahrFalschEntry("Yeezus ist der Titel eines Albums von Kanye West.", true));
        entries.add(new WahrFalschEntry("Handicap ist die Bezeichnung für einen Golfschläger.", false));
        entries.add(new WahrFalschEntry("Streptokokken sind spezielle Weinverschlüsse.", false));
        entries.add(new WahrFalschEntry("Ein bekannter Filmklassiker mit James Dean heißt 'Denn sie Zun nur, was sie wollen'.", false));
        entries.add(new WahrFalschEntry("Barbra Streisand war ein berühmter Broadway-Star, bevor sie zum Film wechselte.", true));
        entries.add(new WahrFalschEntry("Das Thermometer wurde vor dem Radio erfunden.", true));
        entries.add(new WahrFalschEntry("Hyänen sind Pflanzenfresser.", false));
        entries.add(new WahrFalschEntry("Ein CD-ROM-Laufwerk ist ein Gerät zum Lesen von CD´s.", true));
        entries.add(new WahrFalschEntry("Kolumbus entdeckte Amerika im Jahre 1789.", true));
        entries.add(new WahrFalschEntry("Die italienische Bezeichnung für den Wolf heißt Canis Lupus.", true));
        entries.add(new WahrFalschEntry("Das Louvre-Museum ist in Madrid.", false));
        entries.add(new WahrFalschEntry("Ameisen können das 100-fache ihres eigenen Körpergewischts tragen.", true));
        entries.add(new WahrFalschEntry("Wer an Weihnachten geboren wird ist Sternzeichen Löwe.", false));
        entries.add(new WahrFalschEntry("Die Sonne besteht hauptsächlich aus Wasserstoff und Helium.", true));
        entries.add(new WahrFalschEntry("Das Krümelmonster spielt in der Muppet Show mit.", false));
        entries.add(new WahrFalschEntry("Hyänen sind Raubtiere.", true));
        entries.add(new WahrFalschEntry("Ein buddhistischer Geistlicher wird Lama genannt.", true));
        entries.add(new WahrFalschEntry("Moskau liegt nördlicher als Paris", true));
        entries.add(new WahrFalschEntry("Steven Tyler ist der Sänger der Band Aerosmith.", true));
        entries.add(new WahrFalschEntry("Nur Männer können schnarchen.", false));
        entries.add(new WahrFalschEntry("Der Karneval in Rio de Janeiro findet alle 3 Jahre statt", false));
        entries.add(new WahrFalschEntry("Die Alt-Taste auf einer Tastatur steht für -alternate-.", true));
        entries.add(new WahrFalschEntry("Karibu ist ein anderer Name für ein Renntier.", true));
        entries.add(new WahrFalschEntry("Die Hauptstatt der Türkei ist Moskau.", false));
        entries.add(new WahrFalschEntry("Das Logo des Nike-Herstellers wird Swoosh genannt.", true));
        entries.add(new WahrFalschEntry("Kakerlaken können eine Zeit lang ohne Kopf überleben.", true));
        entries.add(new WahrFalschEntry("Rauke ist besser bekannt als Rucolasalt", true));
        entries.add(new WahrFalschEntry("Die jährlichen Tennisturniere in Wimbelten finden in Australien statt.", false));
        entries.add(new WahrFalschEntry("Wasser besteht aus Sauerstoff und Wasserstoff.", true));
        entries.add(new WahrFalschEntry("'Gangnam Style' erreichte als erster Youtube-Clip 1 Milliarde Clicks.", true));
        entries.add(new WahrFalschEntry("Die Spitzen Teile einer Gabel nennt man Zinken.", true));
        entries.add(new WahrFalschEntry("Italien ist wie eine Handtasche geformt.", false));
        entries.add(new WahrFalschEntry("Alaska und Hawai sind US-Bundesstaaten.", true));
        entries.add(new WahrFalschEntry("Flamenco ist ein argentinisches Nationalgericht.", false));
        entries.add(new WahrFalschEntry("Norah Jones begann ihre Karriere mit dem Song 'Don´t Know Why'.", false));
        entries.add(new WahrFalschEntry("Die Automarke Ford brachte den New Beetle auf den Markt.", false));
        entries.add(new WahrFalschEntry("Peter der Große regierte einst Russland.", true));
        entries.add(new WahrFalschEntry("Ein Granny Smith ist ein Knoten.", false));
        entries.add(new WahrFalschEntry("Sudoku ist eine japanische Gedichtform.", false));
        entries.add(new WahrFalschEntry("Die Titanic sank auf ihrer Jungfernfahrt.", true));
        entries.add(new WahrFalschEntry("The Eagles machten den Song 'Hotel California'", true));
        entries.add(new WahrFalschEntry("Das Kap Hoorn findet man an der Nordspitze Afrikas.", false));
        entries.add(new WahrFalschEntry("Timon ist ein Erdmännchen", true));
        entries.add(new WahrFalschEntry("Pancetta ist ein italienischer Schinken", true));
        entries.add(new WahrFalschEntry("Ein -Zappelphilipp- ist jemand, der sehr neugierig ist.", false));
        entries.add(new WahrFalschEntry("Ein bekanntes Sprichtwort heit -Zu viele Köche verderben den Kuchen-.", false));
        entries.add(new WahrFalschEntry("Die Trickfilmfigur Road Runner ist ein Bär.", false));
        entries.add(new WahrFalschEntry("Eine Nachricht auf Twitter nennt man 'Tweety'.", false));
        entries.add(new WahrFalschEntry("Ein Würfel hat 4 Ecken.", false));
        entries.add(new WahrFalschEntry("40% der Thailändischen Prostituierten haben Aids",true));
        entries.add(new WahrFalschEntry("Ein Piercing am Penis heißt 'Prince Albert'",true));
        entries.add(new WahrFalschEntry("Auch Männer haben einen G-Punkt",true));
        entries.add(new WahrFalschEntry("79% aller Brustvergrößerungen werden an dunkelhäutigen Frauen durchgeführt",false));

        currentPlayer =0;
        status =0;
        playerpic = (ImageView) findViewById(R.id.wahr_falsch_playerpic);
        playername = (TextView) findViewById(R.id.wahr_falsch_playername);
        frage = (TextView)findViewById(R.id.question_wahr);
        frage.setTypeface(KreideSchrift);
        frage2.setTypeface(KreideSchrift);
        wahr = (TextView)findViewById(R.id.wahr);
        falsch = (TextView)findViewById(R.id.falsch);

        randomQ = new boolean[entries.size()];
        for(int i=0; i<randomQ.length; i++){ // Keine Frage war bereits dran
            randomQ[i]=false;
        }
        actualEntry = ThreadLocalRandom.current().nextInt(0, randomQ.length); // random erste Frage
        randomQ[actualEntry] = true; // erste Frage markieren (war schon dran)

        playerpic.setImageResource(players.get(0).getColor());
        if(players.get(0).bild!=null){
            playerpic.setImageBitmap(players.get(0).bild);
            playerpic.setForeground(null);
        }else{
            playerpic.setForeground(players.get(currentPlayer).playerPic);
        }
        playername.setText(players.get(0).getName());
        frage2.setText(entries.get(actualEntry).getQuestion());

        frage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Getrunken==true) {
                    if (status == 1) {
                        if(randomAktion){
                            randomAktion=false;
                            if(zufall==0){
                                Player looser = duemmster();
                                looser.oneShot();
                                if(!SystemState.isOffline()) {
                                    Getrunken = false;
                                    ampel.setImageResource(R.drawable.ampel_rot);
                                }
                            }else if(zufall==1){
                                Player looser = schlauster();
                                looser.oneShot();
                                if(!SystemState.isOffline()) {
                                    Getrunken = false;
                                    ampel.setImageResource(R.drawable.ampel_rot);
                                }
                            }
                        }
                        flip.flipTheView();
                        status = 0;
                        newQuestion();
                    }
                }
            }
        });
        frage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Getrunken==true) {
                    if (status == 1) {
                        if(randomAktion){
                            randomAktion=false;
                            if(zufall==0){
                                players.get(0).oneShot();
                                if(!SystemState.isOffline()) {
                                    Getrunken = false;
                                    ampel.setImageResource(R.drawable.ampel_rot);
                                }
                            }
                        }
                        flip.flipTheView();
                        status = 0;
                        newQuestion();
                    }
                }
            }
        });

        wahr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status ==0){checkAnswer(0);}
            }
        });
        falsch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status ==0){checkAnswer(1);}
            }
        });
    }

    public void checkAnswer(int antwort){
        if(randomAktion){

        }else {
            timeStopper=true;
            if (antwort == 0 && entries.get(actualEntry).getCorrect()) {
                wahr.setBackground(getResources().getDrawable(R.drawable.paper_right));
                MediaPlayer quiz_right = MediaPlayer.create(this, R.raw.richtige_antwort);
                quiz_right.start();
                for(int i=0; i<listCount.size();i++){
                    if(listCount.get(i).player.id == players.get(currentPlayer).id){
                        listCount.get(i).count++;
                    }
                }
            } else if (antwort != 0 && !entries.get(actualEntry).getCorrect()) {
                falsch.setBackground(getResources().getDrawable(R.drawable.paper_wrong));
                MediaPlayer quiz_right = MediaPlayer.create(this, R.raw.richtige_antwort);
                quiz_right.start();
                for(int i=0; i<listCount.size();i++){
                    if(listCount.get(i).player.id == players.get(currentPlayer).id){
                        listCount.get(i).count++;
                    }
                }
            } else {
                if (antwort == 5) {
                    wahr.setBackground(getResources().getDrawable(R.drawable.paper_wrong));
                    falsch.setBackground(getResources().getDrawable(R.drawable.paper_wrong));
                    if (entries.get(actualEntry).getCorrect()) {
                        wahr.setBackground(getResources().getDrawable(R.drawable.paper_right));
                    } else {
                        falsch.setBackground(getResources().getDrawable(R.drawable.paper_right));
                    }
                }
                for(int i=0; i<listCount.size();i++){
                    if(listCount.get(i).player.id == players.get(currentPlayer).id){
                        listCount.get(i).count2++;
                    }
                }
                players.get(currentPlayer).oneShot();
                updatePoiListView();
                if (!SystemState.isOffline()) {
                    Getrunken = false;
                    ampel.setImageResource(R.drawable.ampel_rot);
                }
                if (antwort == 0) {
                    wahr.setBackground(getResources().getDrawable(R.drawable.paper_wrong));
                    falsch.setBackground(getResources().getDrawable(R.drawable.paper_right));
                    MediaPlayer quiz_false = MediaPlayer.create(this, R.raw.falsche_antwort);
                    quiz_false.start();
                } else {
                    falsch.setBackground(getResources().getDrawable(R.drawable.paper_wrong));
                    wahr.setBackground(getResources().getDrawable(R.drawable.paper_right));
                    MediaPlayer quiz_false = MediaPlayer.create(this, R.raw.falsche_antwort);
                    quiz_false.start();
                }
            }
            Toast.makeText(getApplicationContext(), "Auf Frage tippen zum Fortfahren", Toast.LENGTH_LONG).show();
        }
            updatePoiListView();
            status = 1;
    }

    public Player duemmster(){
        PlayersWithCounts[] array = new PlayersWithCounts[listCount.size()];
        for(int i=0; i<array.length; i++){
            array[i] = listCount.get(i);
        }
        for(int i=0; i<12; i++){
            for(int j=0; j<array.length-1; j++){
                PlayersWithCounts tmp = array[j];
                if(tmp.count>array[j+1].count){
                    array[j]=array[j+1];
                    array[j+1]=tmp;
                }
            }
        }
        for(int i=0; i<12; i++){
            for(int j=0; j<array.length-1; j++){
                PlayersWithCounts tmp = array[j];
                if(tmp.count2<array[j+1].count2){
                    array[j]=array[j+1];
                    array[j+1]=tmp;
                }
            }
        }
        listCount.clear();
        for(int i=0; i<array.length; i++){
            listCount.add(array[i]);
        }
        return listCount.get(0).player;
    }

    public Player schlauster(){
        PlayersWithCounts[] array = new PlayersWithCounts[listCount.size()];
        for(int i=0; i<array.length; i++){
            array[i] = listCount.get(i);
        }
        for(int i=0; i<12; i++){
            for(int j=0; j<array.length-1; j++){
                PlayersWithCounts tmp = array[j];
                if(tmp.count<array[j+1].count){
                    array[j]=array[j+1];
                    array[j+1]=tmp;
                }
            }
        }
        for(int i=0; i<12; i++){
            for(int j=0; j<array.length-1; j++){
                PlayersWithCounts tmp = array[j];
                if(tmp.count2>array[j+1].count2){
                    array[j]=array[j+1];
                    array[j+1]=tmp;
                }
            }
        }
        listCount.clear();
        for(int i=0; i<array.length; i++){
            listCount.add(array[i]);
        }
        return listCount.get(0).player;
    }

    public void newQuestion(){
        if(begrenzt && rounds == count){
            finish();
        }
        if(roundCounter%(players.size()*5)==0 && switch1.isChecked()){
            status=1;
            randomAktion = true;
            zufall = randomGen.nextInt(zwischenAktionen.length);
            if(count %2 ==0){
                frage.setText(zwischenAktionen[zufall]);
            }else{
                frage2.setText(zwischenAktionen[zufall]);
            }
            count++;
        }else{
            if(currentPlayer<players.size()-1){
                currentPlayer ++;
            }else{
                currentPlayer =0;
            }
            playername.setText(players.get(currentPlayer).getName());
            playerpic.setImageResource(players.get(currentPlayer).getColor());
            if(players.get(currentPlayer).bild!=null){
                playerpic.setImageBitmap(players.get(currentPlayer).bild);
                playerpic.setForeground(null);
            }else{
                playerpic.setForeground(players.get(currentPlayer).playerPic);
            }
            boolean allQ = true; // Waren alle schon dran?
            for(int i=0; i<randomQ.length; i++){
                if(!randomQ[i]){
                    allQ = false; // Es gibt min. eine Frage, die noch nicht dran war
                }
            }
            if(!allQ){ // es gibt noch Fragen, die noch nicht dran waren
                while(randomQ[actualEntry]){ // wenn random gewählte Frage schon dran war, bestimme die nächste random
                    actualEntry = ThreadLocalRandom.current().nextInt(0, randomQ.length);
                }
                randomQ[actualEntry] = true; // neue Frage markieren
            }else{ // Alle Fragen waren schon dran, daher alle Markierungen aufheben
                for(int i=0; i<randomQ.length; i++){
                    randomQ[i]=false;
                }
                actualEntry = ThreadLocalRandom.current().nextInt(0, randomQ.length); // neue erste Frage
                randomQ[actualEntry]=true; // erste Frage markieren
            }
            progressBar.setProgress(progressBar.getMax());
            if(switch2.isChecked()){
                progressBar.setProgress(progressBar.getMax());
                timeStopper=false;
                timerClass tc = new timerClass();
                tc.start();
            }
            if(count %2 ==0){
                frage.setText(entries.get(actualEntry).getQuestion());
            }else{
                frage2.setText(entries.get(actualEntry).getQuestion());
            }
            count++;
            wahr.setBackground(getResources().getDrawable(R.drawable.paper));
            falsch.setBackground(getResources().getDrawable(R.drawable.paper));
        }
        roundCounter++;
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bedienung.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Status = readMessage;
                    if(Status.contains("1")){
                        Getrunken = true;
                        ampel.setImageResource(R.drawable.ampel_gruen);
                        Status="";
                    }
                    break;
            }
        }
    };
    public void setupListview(){
        try {
            ArrayAdapter<PlayersWithCounts> adapter = new PlayerListAdapter();
            lvList = (ListView) findViewById(R.id.quizlv_wahr);
            lvList.setClickable(false);
            lvList.setAdapter(adapter);

        } catch (Exception e) {
        }
    }
    public void updatePoiListView() {
        PlayersWithCounts[] array = new PlayersWithCounts[listCount.size()];
        for(int i=0; i<array.length; i++){
            array[i] = listCount.get(i);
        }
        if(switch3.isChecked()){
            for(int i=0; i<12; i++){
                for(int j=0; j<array.length-1; j++){
                    PlayersWithCounts tmp = array[j];
                    if(tmp.count<array[j+1].count){
                        array[j]=array[j+1];
                        array[j+1]=tmp;
                    }
                }
            }
            for(int i=0; i<12; i++){
                for(int j=0; j<array.length-1; j++){
                    PlayersWithCounts tmp = array[j];
                    if(tmp.count2>array[j+1].count2){
                        array[j]=array[j+1];
                        array[j+1]=tmp;
                    }
                }
            }
        }else{
            for(int i=0; i<12; i++){
                for(int j=0; j<array.length-1; j++){
                    PlayersWithCounts tmp = array[j];
                    if(tmp.count>array[j+1].count){
                        array[j]=array[j+1];
                        array[j+1]=tmp;
                    }
                }
            }
            for(int i=0; i<12; i++){
                for(int j=0; j<array.length-1; j++){
                    PlayersWithCounts tmp = array[j];
                    if(tmp.count2<array[j+1].count2){
                        array[j]=array[j+1];
                        array[j+1]=tmp;
                    }
                }
            }
        }
        listCount.clear();
        for(int i=0; i<array.length; i++){
            listCount.add(array[i]);
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((WahrFalsch.PlayerListAdapter) lvList.getAdapter()).notifyDataSetChanged();
            }
        });

    }
    private class PlayerListAdapter extends ArrayAdapter<PlayersWithCounts> {
        public PlayerListAdapter() {
            super(WahrFalsch.this, R.layout.quizentry, listCount);


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.quizentry, parent, false);
            }

            //Find Item
            final PlayersWithCounts player = listCount.get(position);

            itemView.setClickable(false);
            TextView name = (TextView)itemView.findViewById(R.id.quizentryName);
            TextView right = (TextView)itemView.findViewById(R.id.quizRight);
            TextView wrong = (TextView)itemView.findViewById(R.id.quizWrong);

            name.setText(player.player.getName());
            right.setText(""+player.count);
            wrong.setText(""+player.count2);

            return itemView;
        }
    }

    private final Handler mHandler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what==0){
                progressBar.setProgress(progressBar.getProgress()-progressDiff);
            }else if(msg.what==1){
                checkAnswer(5);
            }
        }
    };

    public class timerClass extends Thread{
        public void run(){
            for(int i=0; i<120;i++){
                if(timeStopper){
                    break;
                }
                mHandler2.obtainMessage(0).sendToTarget();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if(!timeStopper){
                mHandler2.obtainMessage(1).sendToTarget();
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }

}