package com.drink.shotbotapp.shot_botapp;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class Draw2 extends AppCompatActivity {

    ImageView ergebnisPic;
    ListView ergebnisList;
    ImageView[] pics;
    RelativeLayout elem1;
    RelativeLayout elem2;
    RelativeLayout elem3;
    RelativeLayout elem4;
    RelativeLayout elem5;
    RelativeLayout elem6;
    RelativeLayout elem7;
    RelativeLayout elem8;
    RelativeLayout elem9;
    RelativeLayout elem10;
    RelativeLayout[] elems;
    ImageView bitmapPic;
    ImageView bitmapPic2;
    ImageView bitmapPic3;
    ImageView bitmapPic4;
    ImageView bitmapPic5;
    ImageView bitmapPic6;
    ImageView bitmapPic7;
    ImageView bitmapPic8;
    ImageView bitmapPic9;
    ImageView bitmapPic10;
    ImageView star1_1;
    ImageView star1_2;
    ImageView star1_3;
    ImageView star1_4;
    ImageView star2_1;
    ImageView star2_2;
    ImageView star2_3;
    ImageView star2_4;
    ImageView star3_1;
    ImageView star3_2;
    ImageView star3_3;
    ImageView star3_4;
    ImageView star4_1;
    ImageView star4_2;
    ImageView star4_3;
    ImageView star4_4;
    ImageView star5_1;
    ImageView star5_2;
    ImageView star5_3;
    ImageView star5_4;
    ImageView star6_1;
    ImageView star6_2;
    ImageView star6_3;
    ImageView star6_4;
    ImageView star7_1;
    ImageView star7_2;
    ImageView star7_3;
    ImageView star7_4;
    ImageView star8_1;
    ImageView star8_2;
    ImageView star8_3;
    ImageView star8_4;
    ImageView star9_1;
    ImageView star9_2;
    ImageView star9_3;
    ImageView star9_4;
    ImageView star10_1;
    ImageView star10_2;
    ImageView star10_3;
    ImageView star10_4;
    boolean[] bewertet;
    int[]wertung;
    int[]gesWertung;
    PopupWindow mpopupPlayer;
    View popUpViewPlayer;
    PopupWindow mpopupPlayer2;
    View popUpViewPlayer2;
    TextView popupName;
    ImageView popupPic;
    TextView popupBeg;
    int currentPlayer =0;
    ArrayList<Player> players;
    ArrayList<Player> players2;
    ImageView[] stars;
    boolean begrenzt;
    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw2);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        int a = getIntent().getIntExtra("beg",0);
        if(a!=0){
            begrenzt=true;
        }else{
            begrenzt=false;
        }
        players = PlayersClass.getPlayerList();
        bewertet = new boolean[players.size()];
        wertung = new int[players.size()];
        gesWertung = new int[players.size()];
        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });
        popUpViewPlayer2 = getLayoutInflater().inflate(R.layout.drawergebnis,
                null); // inflating popup layout
        mpopupPlayer2 = new PopupWindow(popUpViewPlayer2, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopupPlayer2.setAnimationStyle(android.R.style.Animation_Dialog);
        popUpViewPlayer = getLayoutInflater().inflate(R.layout.drawplayerpopup,
                null); // inflating popup layout
        mpopupPlayer = new PopupWindow(popUpViewPlayer, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopupPlayer.setAnimationStyle(android.R.style.Animation_Dialog);
        popupName = popUpViewPlayer.findViewById(R.id.drawPopupName);
        popupBeg = popUpViewPlayer.findViewById(R.id.drawPopupBeg);
        popupPic = popUpViewPlayer.findViewById(R.id.drawPopupPic);

        ergebnisPic = popUpViewPlayer2.findViewById(R.id.drawPopupPicErgebnis);
        ergebnisList = popUpViewPlayer2.findViewById(R.id.drawErgebnisList);
        popupPic.setImageResource(R.drawable.drawpopup3);
        popupBeg.setVisibility(View.INVISIBLE);
        popupName.setText(players.get(currentPlayer).getName());

        ergebnisPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(begrenzt){
                    SystemState.act.finish();
                    finish();
                }else{
                    Intent i = new Intent(Draw2.this, Draw.class);
                    startActivity(i);
                }
            }
        });
        popupPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpopupPlayer.dismiss();
            }
        });
        elems = new RelativeLayout[10];
        elem1 = findViewById(R.id.drawElem1);
        elem2 = findViewById(R.id.drawElem2);
        elem3 = findViewById(R.id.drawElem3);
        elem4 = findViewById(R.id.drawElem4);
        elem5 = findViewById(R.id.drawElem5);
        elem6 = findViewById(R.id.drawElem6);
        elem7 = findViewById(R.id.drawElem7);
        elem8 = findViewById(R.id.drawElem8);
        elem9 = findViewById(R.id.drawElem9);
        elem10 = findViewById(R.id.drawElem10);
        elems[0]=elem1;
        elems[1]=elem2;
        elems[2]=elem3;
        elems[3]=elem4;
        elems[4]=elem5;
        elems[5]=elem6;
        elems[6]=elem7;
        elems[7]=elem8;
        elems[8]=elem9;
        elems[9]=elem10;
        pics = new ImageView[10];
        bitmapPic = findViewById(R.id.drawElemBitmap);
        bitmapPic2 = findViewById(R.id.drawElemBitmap2);
        bitmapPic3 = findViewById(R.id.drawElemBitmap3);
        bitmapPic4 = findViewById(R.id.drawElemBitmap4);
        bitmapPic5 = findViewById(R.id.drawElemBitmap5);
        bitmapPic6 = findViewById(R.id.drawElemBitmap6);
        bitmapPic7 = findViewById(R.id.drawElemBitmap7);
        bitmapPic8 = findViewById(R.id.drawElemBitmap8);
        bitmapPic9 = findViewById(R.id.drawElemBitmap9);
        bitmapPic10 = findViewById(R.id.drawElemBitmap10);
        pics[0]=bitmapPic;
        pics[1]=bitmapPic2;
        pics[2]=bitmapPic3;
        pics[3]=bitmapPic4;
        pics[4]=bitmapPic5;
        pics[5]=bitmapPic6;
        pics[6]=bitmapPic7;
        pics[7]=bitmapPic8;
        pics[8]=bitmapPic9;
        pics[9]=bitmapPic10;

        for(int i=0; i<10; i++){
            if(players.size()-1<i){
                elems[i].setVisibility(View.GONE);
            }else{
                pics[i].setImageBitmap(DrawSave.getBitmaps().get(i));
            }
        }

        elems[currentPlayer].setVisibility(View.INVISIBLE);
        bewertet[currentPlayer]=true;

        stars = new ImageView[40];
        star1_1 = findViewById(R.id.drawStar1);
        star1_2 = findViewById(R.id.drawStar2);
        star1_3 = findViewById(R.id.drawStar3);
        star1_4 = findViewById(R.id.drawStar4);
        star2_1 = findViewById(R.id.drawStar5);
        star2_2 = findViewById(R.id.drawStar6);
        star2_3 = findViewById(R.id.drawStar7);
        star2_4 = findViewById(R.id.drawStar8);
        star3_1 = findViewById(R.id.drawStar9);
        star3_2 = findViewById(R.id.drawStar10);
        star3_3 = findViewById(R.id.drawStar11);
        star3_4 = findViewById(R.id.drawStar12);
        star4_1 = findViewById(R.id.drawStar13);
        star4_2 = findViewById(R.id.drawStar14);
        star4_3 = findViewById(R.id.drawStar15);
        star4_4 = findViewById(R.id.drawStar16);
        star5_1 = findViewById(R.id.drawStar17);
        star5_2 = findViewById(R.id.drawStar18);
        star5_3 = findViewById(R.id.drawStar19);
        star5_4 = findViewById(R.id.drawStar20);
        star6_1 = findViewById(R.id.drawStar21);
        star6_2 = findViewById(R.id.drawStar22);
        star6_3 = findViewById(R.id.drawStar23);
        star6_4 = findViewById(R.id.drawStar24);
        star7_1 = findViewById(R.id.drawStar25);
        star7_2 = findViewById(R.id.drawStar26);
        star7_3 = findViewById(R.id.drawStar27);
        star7_4 = findViewById(R.id.drawStar28);
        star8_1 = findViewById(R.id.drawStar29);
        star8_2 = findViewById(R.id.drawStar30);
        star8_3 = findViewById(R.id.drawStar31);
        star8_4 = findViewById(R.id.drawStar32);
        star9_1 = findViewById(R.id.drawStar33);
        star9_2 = findViewById(R.id.drawStar34);
        star9_3 = findViewById(R.id.drawStar35);
        star9_4 = findViewById(R.id.drawStar36);
        star10_1 = findViewById(R.id.drawStar37);
        star10_2 = findViewById(R.id.drawStar38);
        star10_3 = findViewById(R.id.drawStar39);
        star10_4 = findViewById(R.id.drawStar40);


        stars[0]= star1_1;
        stars[1]= star1_2;
        stars[2]= star1_3;
        stars[3]= star1_4;
        stars[4]= star2_1;
        stars[5]= star2_2;
        stars[6]= star2_3;
        stars[7]= star2_4;
        stars[8]= star3_1;
        stars[9]= star3_2;
        stars[10]= star3_3;
        stars[11]= star3_4;
        stars[12]= star4_1;
        stars[13]= star4_2;
        stars[14]= star4_3;
        stars[15]= star4_4;

        star1_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[0]=true;
                wertung[0]=1;
                star1_1.setImageResource(R.drawable.drawstar2);
                star1_2.setImageResource(R.drawable.drawstar);
                star1_3.setImageResource(R.drawable.drawstar);
                star1_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star1_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[0]=true;
                wertung[0]=2;
                star1_1.setImageResource(R.drawable.drawstar2);
                star1_2.setImageResource(R.drawable.drawstar2);
                star1_3.setImageResource(R.drawable.drawstar);
                star1_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star1_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[0]=true;
                wertung[0]=3;
                star1_1.setImageResource(R.drawable.drawstar2);
                star1_2.setImageResource(R.drawable.drawstar2);
                star1_3.setImageResource(R.drawable.drawstar2);
                star1_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star1_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[0]=true;
                wertung[0]=4;
                star1_1.setImageResource(R.drawable.drawstar2);
                star1_2.setImageResource(R.drawable.drawstar2);
                star1_3.setImageResource(R.drawable.drawstar2);
                star1_4.setImageResource(R.drawable.drawstar2);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star2_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[1]=true;
                wertung[1]=1;
                star2_1.setImageResource(R.drawable.drawstar2);
                star2_2.setImageResource(R.drawable.drawstar);
                star2_3.setImageResource(R.drawable.drawstar);
                star2_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star2_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[1]=true;
                wertung[1]=2;
                star2_1.setImageResource(R.drawable.drawstar2);
                star2_2.setImageResource(R.drawable.drawstar2);
                star2_3.setImageResource(R.drawable.drawstar);
                star2_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star2_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[1]=true;
                wertung[1]=3;
                star2_1.setImageResource(R.drawable.drawstar2);
                star2_2.setImageResource(R.drawable.drawstar2);
                star2_3.setImageResource(R.drawable.drawstar2);
                star2_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star2_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[1]=true;
                wertung[1]=4;
                star2_1.setImageResource(R.drawable.drawstar2);
                star2_2.setImageResource(R.drawable.drawstar2);
                star2_3.setImageResource(R.drawable.drawstar2);
                star2_4.setImageResource(R.drawable.drawstar2);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star3_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[2]=true;
                wertung[2]=1;
                star3_1.setImageResource(R.drawable.drawstar2);
                star3_2.setImageResource(R.drawable.drawstar);
                star3_3.setImageResource(R.drawable.drawstar);
                star3_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star3_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[2]=true;
                wertung[2]=2;
                star3_1.setImageResource(R.drawable.drawstar2);
                star3_2.setImageResource(R.drawable.drawstar2);
                star3_3.setImageResource(R.drawable.drawstar);
                star3_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star3_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[2]=true;
                wertung[2]=3;
                star3_1.setImageResource(R.drawable.drawstar2);
                star3_2.setImageResource(R.drawable.drawstar2);
                star3_3.setImageResource(R.drawable.drawstar2);
                star3_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star3_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[2]=true;
                wertung[2]=4;
                star3_1.setImageResource(R.drawable.drawstar2);
                star3_2.setImageResource(R.drawable.drawstar2);
                star3_3.setImageResource(R.drawable.drawstar2);
                star3_4.setImageResource(R.drawable.drawstar2);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star4_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[3]=true;
                wertung[3]=1;
                star4_1.setImageResource(R.drawable.drawstar2);
                star4_2.setImageResource(R.drawable.drawstar);
                star4_3.setImageResource(R.drawable.drawstar);
                star4_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star4_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[3]=true;
                wertung[3]=2;
                star4_1.setImageResource(R.drawable.drawstar2);
                star4_2.setImageResource(R.drawable.drawstar2);
                star4_3.setImageResource(R.drawable.drawstar);
                star4_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star4_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[3]=true;
                wertung[3]=3;
                star4_1.setImageResource(R.drawable.drawstar2);
                star4_2.setImageResource(R.drawable.drawstar2);
                star4_3.setImageResource(R.drawable.drawstar2);
                star4_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star4_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[3]=true;
                wertung[3]=4;
                star4_1.setImageResource(R.drawable.drawstar2);
                star4_2.setImageResource(R.drawable.drawstar2);
                star4_3.setImageResource(R.drawable.drawstar2);
                star4_4.setImageResource(R.drawable.drawstar2);
                if(alleBewertet()){
                    next();
                }
            }
        });

        star5_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[4]=true;
                wertung[4]=1;
                star5_1.setImageResource(R.drawable.drawstar2);
                star5_2.setImageResource(R.drawable.drawstar);
                star5_3.setImageResource(R.drawable.drawstar);
                star5_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star5_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[4]=true;
                wertung[4]=2;
                star5_1.setImageResource(R.drawable.drawstar2);
                star5_2.setImageResource(R.drawable.drawstar2);
                star5_3.setImageResource(R.drawable.drawstar);
                star5_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star5_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[4]=true;
                wertung[4]=3;
                star5_1.setImageResource(R.drawable.drawstar2);
                star5_2.setImageResource(R.drawable.drawstar2);
                star5_3.setImageResource(R.drawable.drawstar2);
                star5_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star5_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[4]=true;
                wertung[4]=4;
                star5_1.setImageResource(R.drawable.drawstar2);
                star5_2.setImageResource(R.drawable.drawstar2);
                star5_3.setImageResource(R.drawable.drawstar2);
                star5_4.setImageResource(R.drawable.drawstar2);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star6_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[5]=true;
                wertung[5]=1;
                star6_1.setImageResource(R.drawable.drawstar2);
                star6_2.setImageResource(R.drawable.drawstar);
                star6_3.setImageResource(R.drawable.drawstar);
                star6_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star6_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[5]=true;
                wertung[5]=2;
                star6_1.setImageResource(R.drawable.drawstar2);
                star6_2.setImageResource(R.drawable.drawstar2);
                star6_3.setImageResource(R.drawable.drawstar);
                star6_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star6_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[5]=true;
                wertung[5]=3;
                star6_1.setImageResource(R.drawable.drawstar2);
                star6_2.setImageResource(R.drawable.drawstar2);
                star6_3.setImageResource(R.drawable.drawstar2);
                star6_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star6_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[5]=true;
                wertung[5]=4;
                star6_1.setImageResource(R.drawable.drawstar2);
                star6_2.setImageResource(R.drawable.drawstar2);
                star6_3.setImageResource(R.drawable.drawstar2);
                star6_4.setImageResource(R.drawable.drawstar2);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star7_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[6]=true;
                wertung[6]=1;
                star7_1.setImageResource(R.drawable.drawstar2);
                star7_2.setImageResource(R.drawable.drawstar);
                star7_3.setImageResource(R.drawable.drawstar);
                star7_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star7_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[6]=true;
                wertung[6]=2;
                star7_1.setImageResource(R.drawable.drawstar2);
                star7_2.setImageResource(R.drawable.drawstar2);
                star7_3.setImageResource(R.drawable.drawstar);
                star7_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star7_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[6]=true;
                wertung[6]=3;
                star7_1.setImageResource(R.drawable.drawstar2);
                star7_2.setImageResource(R.drawable.drawstar2);
                star7_3.setImageResource(R.drawable.drawstar2);
                star7_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star7_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[6]=true;
                wertung[6]=4;
                star7_1.setImageResource(R.drawable.drawstar2);
                star7_2.setImageResource(R.drawable.drawstar2);
                star7_3.setImageResource(R.drawable.drawstar2);
                star7_4.setImageResource(R.drawable.drawstar2);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star8_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[7]=true;
                wertung[7]=1;
                star8_1.setImageResource(R.drawable.drawstar2);
                star8_2.setImageResource(R.drawable.drawstar);
                star8_3.setImageResource(R.drawable.drawstar);
                star8_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star8_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[7]=true;
                wertung[7]=2;
                star8_1.setImageResource(R.drawable.drawstar2);
                star8_2.setImageResource(R.drawable.drawstar2);
                star8_3.setImageResource(R.drawable.drawstar);
                star8_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star8_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[7]=true;
                wertung[7]=3;
                star8_1.setImageResource(R.drawable.drawstar2);
                star8_2.setImageResource(R.drawable.drawstar2);
                star8_3.setImageResource(R.drawable.drawstar2);
                star8_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star8_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[7]=true;
                wertung[7]=4;
                star8_1.setImageResource(R.drawable.drawstar2);
                star8_2.setImageResource(R.drawable.drawstar2);
                star8_3.setImageResource(R.drawable.drawstar2);
                star8_4.setImageResource(R.drawable.drawstar2);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star9_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[8]=true;
                wertung[8]=1;
                star9_1.setImageResource(R.drawable.drawstar2);
                star9_2.setImageResource(R.drawable.drawstar);
                star9_3.setImageResource(R.drawable.drawstar);
                star9_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star9_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[8]=true;
                wertung[8]=2;
                star9_1.setImageResource(R.drawable.drawstar2);
                star9_2.setImageResource(R.drawable.drawstar2);
                star9_3.setImageResource(R.drawable.drawstar);
                star9_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star9_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[8]=true;
                wertung[8]=3;
                star9_1.setImageResource(R.drawable.drawstar2);
                star9_2.setImageResource(R.drawable.drawstar2);
                star9_3.setImageResource(R.drawable.drawstar2);
                star9_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star9_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[8]=true;
                wertung[8]=4;
                star9_1.setImageResource(R.drawable.drawstar2);
                star9_2.setImageResource(R.drawable.drawstar2);
                star9_3.setImageResource(R.drawable.drawstar2);
                star9_4.setImageResource(R.drawable.drawstar2);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star10_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[9]=true;
                wertung[9]=1;
                star10_1.setImageResource(R.drawable.drawstar2);
                star10_2.setImageResource(R.drawable.drawstar);
                star10_3.setImageResource(R.drawable.drawstar);
                star10_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star10_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[9]=true;
                wertung[9]=2;
                star10_1.setImageResource(R.drawable.drawstar2);
                star10_2.setImageResource(R.drawable.drawstar2);
                star10_3.setImageResource(R.drawable.drawstar);
                star10_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star10_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[9]=true;
                wertung[9]=3;
                star10_1.setImageResource(R.drawable.drawstar2);
                star10_2.setImageResource(R.drawable.drawstar2);
                star10_3.setImageResource(R.drawable.drawstar2);
                star10_4.setImageResource(R.drawable.drawstar);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star10_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bewertet[9]=true;
                wertung[9]=4;
                star10_1.setImageResource(R.drawable.drawstar2);
                star10_2.setImageResource(R.drawable.drawstar2);
                star10_3.setImageResource(R.drawable.drawstar2);
                star10_4.setImageResource(R.drawable.drawstar2);
                if(alleBewertet()){
                    next();
                }
            }
        });
        star10_4.post(new Runnable() {
            public void run() {
                mpopupPlayer.showAtLocation(popUpViewPlayer, Gravity.CENTER, 0, 0); // Displaying popup
            }
        });
    }

    public boolean alleBewertet(){
        boolean a = true;
        for(int i=0; i<bewertet.length; i++){
            if(!bewertet[i]){
                a = false;
            }
        }
        return a;
    }

    public void next(){
        for(int i=0; i<wertung.length; i++){
            gesWertung[i] = gesWertung[i]+wertung[i];
        }
        if(currentPlayer<players.size()-1){
            for(int i=0; i<bewertet.length; i++){
                bewertet[i]=false;
                wertung[i]=0;
            }
            for(int i=0; i<players.size()*4; i++){
                stars[i].setImageResource(R.drawable.drawstar);
            }
            currentPlayer++;
            bewertet[currentPlayer]=true;
            elems[currentPlayer].setVisibility(View.INVISIBLE);
            elems[currentPlayer-1].setVisibility(View.VISIBLE);
            popupName.setText(players.get(currentPlayer).getName());
            mpopupPlayer.showAtLocation(popUpViewPlayer, Gravity.CENTER, 0, 0); // Displaying popup
        }else{
            int minSterne = 100;
            for(int i=0; i<gesWertung.length; i++){
                if(gesWertung[i]<minSterne){
                    minSterne = gesWertung[i];
                }
            }
            ArrayList<Player> loosers = new ArrayList<Player>();
            for(int i=0; i<players.size(); i++){
                if(gesWertung[i] == minSterne){
                    loosers.add(players.get(i));
                }
            }
            if(loosers.size()==1){
                loosers.get(0).oneShot();
            }else{
                String shots2 ="";
                for(int i=0; i<loosers.size(); i++){
                    shots2 = shots2+"T"+loosers.get(i).id+"#";
                    loosers.get(i).shotCount++;
                }
                BluetoothAdapterClass.WriteBT(shots2);
            }
            setupListview();
            mpopupPlayer2.showAtLocation(popUpViewPlayer, Gravity.CENTER, 0, 0); // Displaying popup
        }
    }

    public void setupListview(){
        try {
            ArrayAdapter<Player> adapter = new Draw2.PlayerListAdapter();
            ergebnisList.setAdapter(adapter);

        } catch (Exception e) {
        }
    }

    private class PlayerListAdapter extends ArrayAdapter<Player> {
        public PlayerListAdapter() {
            super(Draw2.this, R.layout.drawergebisentry, players);


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.drawergebisentry, parent, false);
            }

            //Find Item
            final Player player = players.get(position);

            //Insert in View
            TextView b = (TextView) itemView.findViewById(R.id.drawEntryName);
            b.setText(player.getName());
            TextView starnumber = (TextView) itemView.findViewById(R.id.drawEntryStars);
            starnumber.setText(gesWertung[position]+"");

            return itemView;
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }
}
