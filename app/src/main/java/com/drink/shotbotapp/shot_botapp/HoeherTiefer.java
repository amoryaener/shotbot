package com.drink.shotbotapp.shot_botapp;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.wajahatkarim3.easyflipview.EasyFlipView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class HoeherTiefer extends AppCompatActivity {

    boolean zwischenaktionaktiv = false;
    LinearLayout zwischenPopup;
    int[] zwischenaktionen;
    //PopupWindow mpopup;
    //View popUpView;
    boolean begrenzt = false;
    int rounds;
    boolean einstellungenSichtbar = false;
    Switch switchAnstieg;
    Switch switchZwischenaktionen;
    ImageView einstellungen;
    TextView old_card;
    TextView new_card;
    TextView new_card2;
    ImageView playerPic;
    ImageView ampel;
    ImageButton hoeher;
    ImageButton gleich;
    ImageButton tiefer;
    TextView playerName;
    ArrayList<Player> players;
    Card old;
    Card n;
    int currentPlayer;
    int cardNumber;
    CardDeck deck;
    int status;
    int count;
    EasyFlipView efv;
    int counter=0;
    String Status;
    Boolean Getrunken = true;
    ImageView glas;
    int fuellhoehe= 1000;
    ImageView zwischenaktionpic;
    LinearLayout einsLay;
    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hoeher_tiefer);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        rounds = getIntent().getIntExtra("Rounds",0);
        if(rounds >0){
            begrenzt=true;
        }

        zwischenPopup = findViewById(R.id.htZwischenPopup);
        zwischenPopup.setVisibility(View.GONE);
        einsLay = findViewById(R.id.hoeherTieferEins);
        zwischenaktionen = new int[4];
        zwischenaktionen[0]=R.drawable.zwischenaktion8;
        zwischenaktionen[1]=R.drawable.zwischenaktion2;
        zwischenaktionen[2]=R.drawable.zwischenaktion5;
        zwischenaktionen[3]=R.drawable.zwischenaktion6;
        //zwischenaktionen[4]=R.drawable.zwischenaktion7;

        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        GlobalStats.gameStartet("Höher Tiefer");
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });

        zwischenaktionpic = findViewById(R.id.htZwischen);

        efv = findViewById(R.id.flip2);
        efv.setFlipOnTouch(true);
        efv.setFlipEnabled(true);
        efv.setFlipDuration(1000);

        switchAnstieg = findViewById(R.id.hoeher_switch1);
        switchAnstieg.setVisibility(View.GONE);
        switchZwischenaktionen = findViewById(R.id.hoeher_switch2);
        switchZwischenaktionen.setVisibility(View.GONE);
        einstellungen = findViewById(R.id.hoeher_einstellungen);
        glas = findViewById(R.id.hoeher_glas);
        glas.setVisibility(View.GONE);

        switchAnstieg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(switchAnstieg.isChecked()){
                   glas.setVisibility(View.VISIBLE);
                   glas.setImageResource(R.drawable.balkenglas1);
                   fuellhoehe = 1000;
               }else{
                   glas.setVisibility(View.GONE);
               }
            }
        });

        old_card = (TextView) findViewById(R.id.oldCard);
        new_card = (TextView) findViewById(R.id.newCard);
        new_card2 = (TextView) findViewById(R.id.newCard2);
        hoeher = (ImageButton)findViewById(R.id.hoeher);
        gleich = (ImageButton)findViewById(R.id.gleich);
        tiefer = (ImageButton)findViewById(R.id.tiefer);
        playerPic = (ImageView)findViewById(R.id.playerPic);
        ampel = (ImageView)findViewById(R.id.ampel);
        playerName =(TextView)findViewById(R.id.hoeherTieferPlayerName);
        deck = new CardDeck();
        deck.shuffle();

        players = PlayersClass.getPlayerList();
        currentPlayer =0;
        cardNumber=1;
        old = deck.getCard(0);
        n = deck.getCard(1);
        status =0;
        count =0;
        BluetoothAdapterClass.setHandler(mHandler);

        String uri = old.getPic();
        int imageResource = getResources().getIdentifier(uri, null, getPackageName());
        Drawable draw = getResources().getDrawable(imageResource);
        old_card.setBackground(draw);
        playerName.setText(players.get(currentPlayer).getName());
        playerPic.setImageResource(players.get(0).getColor());
        if(players.get(0).bild!=null){
            playerPic.setImageBitmap(players.get(0).bild);
            playerPic.setForeground(null);
        }else{
            playerPic.setForeground(players.get(currentPlayer).playerPic);
        }

        zwischenaktionpic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Getrunken){
                    einsLay.setVisibility(View.VISIBLE);
                    zwischenPopup.setVisibility(View.GONE);
                    zwischenaktionaktiv=false;
                }
            }
        });

        einstellungen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(einstellungenSichtbar){
                    einstellungenSichtbar=false;
                    switchAnstieg.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_hide));
                    switchAnstieg.setVisibility(View.GONE);
                    switchAnstieg.setClickable(false);
                    switchZwischenaktionen.setVisibility(View.GONE);
                    switchZwischenaktionen.setClickable(false);
                    switchZwischenaktionen.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab3_hide));
                }else {
                    einstellungenSichtbar=true;
                    //switchAnstieg.setVisibility(View.VISIBLE);
                    switchAnstieg.setClickable(true);
                    switchAnstieg.setAlpha((float)0.8);
                    switchAnstieg.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_show));
                    switchZwischenaktionen.setVisibility(View.VISIBLE);
                    switchZwischenaktionen.setClickable(true);
                    switchZwischenaktionen.setAlpha((float)0.8);
                    switchZwischenaktionen.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab3_show));
                }
            }
        });

        new_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!zwischenaktionaktiv){
                    if(Getrunken==true) {
                        if (status == 1) {
                            status = 0;
                            hoeher.setBackground(getResources().getDrawable(R.drawable.hoehertiefer_hoeher));
                            gleich.setBackground(getResources().getDrawable(R.drawable.hoehertiefer_gleich));
                            tiefer.setBackground(getResources().getDrawable(R.drawable.hoehertiefer_tiefer));
                            String uri2 = "@drawable/cardback";
                            int imageResource2 = getResources().getIdentifier(uri2, null, getPackageName());
                            Drawable draw2 = getResources().getDrawable(imageResource2);
                            new_card.setBackground(draw2);
                            new_card2.setBackground(draw2);
                            if(begrenzt && rounds == count){
                                finish();
                            }
                            if(switchZwischenaktionen.isChecked() && count%5==0){
                                Zufallsaktionen za = new Zufallsaktionen(players);
                                Random rand = new Random();
                                int randint = rand.nextInt(4);
                                zwischenaktionpic.setImageResource(za.getPic(randint));
                                einsLay.setVisibility(View.GONE);
                                if(!einstellungenSichtbar){
                                    switchAnstieg.setAlpha(0);switchZwischenaktionen.setAlpha(0);
                                }
                                zwischenPopup.setVisibility(View.VISIBLE);
                                zwischenaktionaktiv=true;
                                boolean ampelchange =za.aktion(randint);
                                if(ampelchange){
                                    Getrunken = false;
                                    ampel.setImageResource(R.drawable.ampel_rot);
                                }
                            }
                            if (currentPlayer < players.size() - 1 && cardNumber < 31) {
                                currentPlayer++;
                                cardNumber++;
                                old = n;
                                n = deck.getCard(cardNumber);
                                String uri = old.getPic();
                                int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                                Drawable draw = getResources().getDrawable(imageResource);
                                old_card.setBackground(draw);
                                playerName.setText(players.get(currentPlayer).getName());
                                playerPic.setImageResource(players.get(currentPlayer).getColor());
                                if (players.get(currentPlayer).bild != null) {
                                    playerPic.setImageBitmap(players.get(currentPlayer).bild);
                                    playerPic.setForeground(null);
                                } else {
                                    playerPic.setForeground(players.get(currentPlayer).playerPic);
                                }
                            } else {
                                if (cardNumber < 31) {
                                    currentPlayer = 0;
                                    cardNumber++;
                                    old = n;
                                    n = deck.getCard(cardNumber);
                                    String uri = old.getPic();
                                    int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                                    Drawable draw = getResources().getDrawable(imageResource);
                                    old_card.setBackground(draw);
                                    playerName.setText(players.get(currentPlayer).getName());
                                    playerPic.setImageResource(players.get(currentPlayer).getColor());
                                    if (players.get(currentPlayer).bild != null) {
                                        playerPic.setImageBitmap(players.get(currentPlayer).bild);
                                        playerPic.setForeground(null);
                                    } else {
                                        playerPic.setForeground(players.get(currentPlayer).playerPic);
                                    }
                                } else if (currentPlayer < players.size() - 1) {
                                    currentPlayer++;
                                    deck.shuffle();
                                    cardNumber = 0;
                                    old = n;
                                    n = deck.getCard(cardNumber);
                                    String uri = old.getPic();
                                    int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                                    Drawable draw = getResources().getDrawable(imageResource);
                                    old_card.setBackground(draw);
                                    playerName.setText(players.get(currentPlayer).getName());
                                    playerPic.setImageResource(players.get(currentPlayer).getColor());
                                    if (players.get(currentPlayer).bild != null) {
                                        playerPic.setImageBitmap(players.get(currentPlayer).bild);
                                        playerPic.setForeground(null);
                                    } else {
                                        playerPic.setForeground(players.get(currentPlayer).playerPic);
                                    }
                                    Toast.makeText(getApplicationContext(), "Deck wurde neu gemischt", Toast.LENGTH_LONG).show();
                                } else {
                                    currentPlayer = 0;
                                    cardNumber = 0;
                                    deck.shuffle();
                                    old = n;
                                    n = deck.getCard(cardNumber);
                                    String uri = old.getPic();
                                    int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                                    Drawable draw = getResources().getDrawable(imageResource);
                                    old_card.setBackground(draw);
                                    playerName.setText(players.get(currentPlayer).getName());
                                    playerPic.setImageResource(players.get(currentPlayer).getColor());
                                    if (players.get(currentPlayer).bild != null) {
                                        playerPic.setImageBitmap(players.get(currentPlayer).bild);
                                        playerPic.setForeground(null);
                                    } else {
                                        playerPic.setForeground(players.get(currentPlayer).playerPic);
                                    }
                                    Toast.makeText(getApplicationContext(), "Deck wurde neu gemischt", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }
                }

            }
        });

        new_card2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!zwischenaktionaktiv){
                    if(Getrunken==true) {
                        if (status == 1) {
                            status = 0;
                            hoeher.setBackground(getResources().getDrawable(R.drawable.hoehertiefer_hoeher));
                            gleich.setBackground(getResources().getDrawable(R.drawable.hoehertiefer_gleich));
                            tiefer.setBackground(getResources().getDrawable(R.drawable.hoehertiefer_tiefer));
                            String uri2 = "@drawable/cardback";
                            int imageResource2 = getResources().getIdentifier(uri2, null, getPackageName());
                            Drawable draw2 = getResources().getDrawable(imageResource2);
                            new_card.setBackground(draw2);
                            new_card2.setBackground(draw2);
                            if(begrenzt && rounds == count){
                                finish();
                            }
                            if(switchZwischenaktionen.isChecked() && count%5==0){
                                Zufallsaktionen za = new Zufallsaktionen(players);
                                Random rand = new Random();
                                int randint = rand.nextInt(4);
                                zwischenaktionpic.setImageResource(za.getPic(randint));
                                einsLay.setVisibility(View.GONE);
                                if(!einstellungenSichtbar){
                                    switchAnstieg.setAlpha(0);switchZwischenaktionen.setAlpha(0);
                                }
                                zwischenPopup.setVisibility(View.VISIBLE);
                                zwischenaktionaktiv=true;
                                boolean ampelchange =za.aktion(randint);
                                if(ampelchange){
                                    Getrunken = false;
                                    ampel.setImageResource(R.drawable.ampel_rot);
                                }
                            }
                            if (currentPlayer < players.size() - 1 && cardNumber < 31) {
                                currentPlayer++;
                                cardNumber++;
                                old = n;
                                n = deck.getCard(cardNumber);
                                String uri = old.getPic();
                                int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                                Drawable draw = getResources().getDrawable(imageResource);
                                old_card.setBackground(draw);
                                playerName.setText(players.get(currentPlayer).getName());
                                playerPic.setImageResource(players.get(currentPlayer).getColor());
                                if (players.get(currentPlayer).bild != null) {
                                    playerPic.setImageBitmap(players.get(currentPlayer).bild);
                                    playerPic.setForeground(null);
                                } else {
                                    playerPic.setForeground(players.get(currentPlayer).playerPic);
                                }
                            } else {
                                if (cardNumber < 31) {
                                    currentPlayer = 0;
                                    cardNumber++;
                                    old = n;
                                    n = deck.getCard(cardNumber);
                                    String uri = old.getPic();
                                    int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                                    Drawable draw = getResources().getDrawable(imageResource);
                                    old_card.setBackground(draw);
                                    playerName.setText(players.get(currentPlayer).getName());
                                    playerPic.setImageResource(players.get(currentPlayer).getColor());
                                    if (players.get(currentPlayer).bild != null) {
                                        playerPic.setImageBitmap(players.get(currentPlayer).bild);
                                        playerPic.setForeground(null);
                                    } else {
                                        playerPic.setForeground(players.get(currentPlayer).playerPic);
                                    }
                                } else if (currentPlayer < players.size() - 1) {
                                    currentPlayer++;
                                    deck.shuffle();
                                    cardNumber = 0;
                                    old = n;
                                    n = deck.getCard(cardNumber);
                                    String uri = old.getPic();
                                    int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                                    Drawable draw = getResources().getDrawable(imageResource);
                                    old_card.setBackground(draw);
                                    playerName.setText(players.get(currentPlayer).getName());
                                    playerPic.setImageResource(players.get(currentPlayer).getColor());
                                    if (players.get(currentPlayer).bild != null) {
                                        playerPic.setImageBitmap(players.get(currentPlayer).bild);
                                        playerPic.setForeground(null);
                                    } else {
                                        playerPic.setForeground(players.get(currentPlayer).playerPic);
                                    }
                                    Toast.makeText(getApplicationContext(), "Deck wurde neu gemischt", Toast.LENGTH_LONG).show();
                                } else {
                                    currentPlayer = 0;
                                    cardNumber = 0;
                                    deck.shuffle();
                                    old = n;
                                    n = deck.getCard(cardNumber);
                                    String uri = old.getPic();
                                    int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                                    Drawable draw = getResources().getDrawable(imageResource);
                                    old_card.setBackground(draw);
                                    playerName.setText(players.get(currentPlayer).getName());
                                    playerPic.setImageResource(players.get(currentPlayer).getColor());
                                    if (players.get(currentPlayer).bild != null) {
                                        playerPic.setImageBitmap(players.get(currentPlayer).bild);
                                        playerPic.setForeground(null);
                                    } else {
                                        playerPic.setForeground(players.get(currentPlayer).playerPic);
                                    }
                                    Toast.makeText(getApplicationContext(), "Deck wurde neu gemischt", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }
                }
            }
        });

        hoeher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status ==0){
                    step(2);
                }
            }
        });
        gleich.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status ==0){
                    step(1);
                }
            }
        });
        tiefer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status ==0){
                    step(0);
                }
            }
        });
    }

    public void step(int a){
        count++;
        status =1;
        if(a ==2){
            if(old.value<n.value){
                gewonnen(hoeher);
            }else{
                loose(hoeher);
            }
        }else if(a ==1){
            if(old.value==n.value){
                gewonnen(gleich);
            }else{
                loose(gleich);
            }
        }else if(a ==0){
            if(old.value>n.value){
                gewonnen(tiefer);
            }else{
                loose(tiefer);
            }
        }
        String uri = n.getPic();
        int imageResource = getResources().getIdentifier(uri, null, getPackageName());
        Drawable draw = getResources().getDrawable(imageResource);
        if(counter%2==1){
            new_card.setBackground(draw);
        }else{
            new_card2.setBackground(draw);
        }
        counter++;
        efv.flipTheView();
        if(count <5){
            Toast.makeText(getApplicationContext(), "Zum Fortfahren auf aufgedeckte Karte tippen", Toast.LENGTH_LONG).show();
        }
    }

    public void gewonnen(ImageButton a){
        MediaPlayer quiz_right = MediaPlayer.create(this, R.raw.richtige_antwort);
        try {
            quiz_right.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        quiz_right.start();
        if(a==hoeher) {
            a.setBackground(getResources().getDrawable(R.drawable.hoehertiefer_hoeher_richtig));
        }
        if(a==tiefer) {
            a.setBackground(getResources().getDrawable(R.drawable.hoehertiefer_tiefer_richtig));
        }
        if(a==gleich) {
            String shots = "";
            for (int i = 0; i < players.size(); i++) {
                if (i != currentPlayer) {
                    shots = shots + "T" + players.get(i).id + "#";
                    players.get(i).shotCount++;
                }
            }
            shots = shots + "TB";
            BluetoothAdapterClass.WriteBT(shots);
            if (!SystemState.isOffline()) {

                Getrunken = false;
                ampel.setImageResource(R.drawable.ampel_rot);
            }
            a.setBackground(getResources().getDrawable(R.drawable.hoehertiefer_gleich_richtig));
        }
        fuellhoehe =1000;
        glas.setImageResource(R.drawable.balkenglas1);
    }

    public void loose(ImageButton a){
        MediaPlayer quiz_false = MediaPlayer.create(this, R.raw.falsche_antwort);
        if(!SystemState.isOffline()){
            ampel.setImageResource(R.drawable.ampel_rot);
            Getrunken=false;
        }
        try {
            quiz_false.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        quiz_false.start();
        if(a==hoeher) {
            a.setBackground(getResources().getDrawable(R.drawable.hoehertiefer_hoeher_falsch));
        }
        if(a==tiefer) {
            a.setBackground(getResources().getDrawable(R.drawable.hoehertiefer_tiefer_falsch));
        }
        if(a==gleich) {
            a.setBackground(getResources().getDrawable(R.drawable.hoehertiefer_gleich_falsch));
        }
        if(switchAnstieg.isChecked()){
            BluetoothAdapterClass.WriteBT("ST#X "+players.get(currentPlayer).id+"#GO#Y "+fuellhoehe+"#VM#X "+"-"+players.get(currentPlayer).id+"#GO#P "+players.get(currentPlayer).id+"#QUIT");
            Log.d("Test", "ST#X "+players.get(currentPlayer).id+"#GO#Y "+fuellhoehe+"#VM#X "+"-"+players.get(currentPlayer).id+"#GO#P "+players.get(currentPlayer).id+"#QUIT");
            if(fuellhoehe<3100){
                if(fuellhoehe==1000){
                    glas.setImageResource(R.drawable.balkenglas2);
                }else if(fuellhoehe==1725){
                    glas.setImageResource(R.drawable.balkenglas3);
                }else if(fuellhoehe==2450){
                    glas.setImageResource(R.drawable.balkenglas4);
                }
                fuellhoehe=fuellhoehe+725;
            }
        }else{
            players.get(currentPlayer).oneShot();
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bedienung.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Status = readMessage;
                    if(Status.contains("1")){
                        Getrunken = true;
                        ampel.setImageResource(R.drawable.ampel_gruen);
                        Status="";
                    }
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }
}
