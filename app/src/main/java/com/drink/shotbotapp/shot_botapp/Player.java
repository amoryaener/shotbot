package com.drink.shotbotapp.shot_botapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;

/**
 * Created by Alexander on 19.01.2018.
 */

public class Player {
    SharedPreferences sharedPrefs;
    Context context;
    String morw;
    String name;
    int color;
    String shotCommand;
    int shotCount;
    int id;
    int ml;
    String draw;
    int draw2;
    Bitmap bild;
    int color2;
    ArrayList<Integer> x;
    Drawable playerPic;
    String color3;
    int picpos;
    int hoehe;

    public String getName() {
        return name;
    }

    public int getColor() {
        return color;
    }

    public String getMorw(){ return morw;}

    public void oneShot(){
        //BluetoothAdapterClass.WriteBT(this.shotCommand);
        String senden ="T"+this.id+"#TB";
        BluetoothAdapterClass.WriteBT(senden);
        this.shotCountPlus();
    }

    public void shotCountPlus(){
        this.shotCount++;
        GlobalStats.shotCount ++;
        GlobalStats.save();
        this.ml = this.ml + (int)(this.hoehe/133);
        ArrayList<Player>playerlist = PlayersClass.getPlayerList();
        for(int i=0; i<playerlist.size(); i++){
            playerlist.get(i).x.add(playerlist.get(i).getShotCount());
        }

        sharedPrefs = this.context.getSharedPreferences("saveStats", 0);
        int topPlayer = sharedPrefs.getInt("playerShotsMax",0);

        if(this.shotCount>topPlayer){
            SharedPreferences.Editor editor = context.getSharedPreferences("saveStats", 0).edit();
            editor.putInt("playerShotsMax", this.shotCount);
            editor.putString("playerShotsName", this.name);
            editor.putInt("imgRes", this.color);
            editor.putInt("imgDrawable", this.picpos);
            editor.commit();
        }

    }

    public int getShotCount(){
        return this.shotCount;
    }

    public int getShotCountMl(){
        return this.shotCount*20;
    }

    public Player(String n, int c, Bitmap b, Drawable d, String mw, int pp, Context con){
        this.context = con;
        this.ml =0;
        this.hoehe = 3000;
        this.picpos = pp;
        this.morw = mw;
        this.playerPic = d;
        this.bild = b;
        this.shotCount = 0;
        this.name =n;
        this.color = c;
        this.x = new ArrayList<Integer>();
        if(c==R.drawable.color_player0)     {this.shotCommand = "T0#TB";this.id=0;this.draw ="@drawable/player0";this.color2=R.color.Player0;this.color3="#0134fd";this.draw2 = R.drawable.player0;}
        else if(c==R.drawable.color_player1){this.shotCommand = "T1#TB";this.id=1;this.draw ="@drawable/player1";this.color2=R.color.Player1;this.color3="#04e9bb";this.draw2 = R.drawable.player1;}
        else if(c==R.drawable.color_player2){this.shotCommand = "T2#TB";this.id=2;this.draw ="@drawable/player2";this.color2=R.color.Player2;this.color3="#9af400";this.draw2 = R.drawable.player2;}
        else if(c==R.drawable.color_player3){this.shotCommand = "T3#TB";this.id=3;this.draw ="@drawable/player3";this.color2=R.color.Player3;this.color3="#00bf06";this.draw2 = R.drawable.player3;}
        else if(c==R.drawable.color_player4){this.shotCommand = "T4#TB";this.id=4;this.draw ="@drawable/player4";this.color2=R.color.Player4;this.color3="#e4d100";this.draw2 = R.drawable.player4;}
        else if(c==R.drawable.color_player5){this.shotCommand = "T5#TB";this.id=5;this.draw ="@drawable/player5";this.color2=R.color.Player5;this.color3="#ff8000";this.draw2 = R.drawable.player5;}
        else if(c==R.drawable.color_player6){this.shotCommand = "T6#TB";this.id=6;this.draw ="@drawable/player6";this.color2=R.color.Player6;this.color3="#ff0900";this.draw2 = R.drawable.player6;}
        else if(c==R.drawable.color_player7){this.shotCommand = "T7#TB";this.id=7;this.draw ="@drawable/player7";this.color2=R.color.Player7;this.color3="#6600ff";this.draw2 = R.drawable.player7;}
        else if(c==R.drawable.color_player8){this.shotCommand = "T8#TB";this.id=8;this.draw ="@drawable/player8";this.color2=R.color.Player8;this.color3="#ff02a6";this.draw2 = R.drawable.player8;}
        else if(c==R.drawable.color_player9){this.shotCommand = "T9#TB";this.id=9;this.draw ="@drawable/player9";this.color2=R.color.Player9;this.color3="#ecffb8";this.draw2 = R.drawable.player9;}
    }
}
