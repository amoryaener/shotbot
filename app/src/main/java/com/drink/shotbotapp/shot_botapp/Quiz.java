package com.drink.shotbotapp.shot_botapp;

import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.wajahatkarim3.easyflipview.EasyFlipView;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Quiz extends AppCompatActivity {

    boolean timeStopper=false;
    ProgressBar progressBar;
    int roundCounter=0;
    String[] zwischenAktionen;
    boolean einstellungenSichtbar = false;
    Switch switchAktion;
    Switch switchTime;
    Switch switchSort;
    ImageView einstellungen;
    ArrayList<PlayersWithCounts> listCount;
    ListView lv;
    ImageView playerPic;
    ImageView ampel;
    TextView playerName;
    TextView question;
    TextView question2;
    TextView ans1;
    TextView ans2;
    TextView ans3;
    TextView ans4;
    int actualEntry;
    TextView [] answers;
    ArrayList<Player> players;
    int actualPlayer;
    int status;
    int playerNumber;
    ArrayList<QuizEntry> entries;
    EasyFlipView efv;
    String Status;
    Boolean Getrunken = true;
    int count =0;
    //QuizEntry[] entries;
    boolean[]randomQ; // true, wenn Frage schon dran war (in dem Durchlauf)
    Random randomGen = new Random();
    boolean randomAktion = false;
    int zufall;
    int progressDiff;
    int rounds;
    boolean begrenzt =false;
    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        rounds = getIntent().getIntExtra("Rounds",0);
        if(rounds >0){
            begrenzt=true;
        }
        GlobalStats.gameStartet("Quiz");
        efv = findViewById(R.id.flip);
        efv.setFlipOnTouch(true);
        efv.setFlipEnabled(true);
        efv.setFlipDuration(1000);
        Typeface KreideSchrift = Typeface.createFromAsset(getAssets(), "KreideSchrift.ttf");
        Typeface Bleistift = Typeface.createFromAsset(getAssets(), "Bleistift.ttf");
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //playerNumber = 4;
        players = PlayersClass.getPlayerList();
        //players[0] = "Player1";
        //players[1] = "Player2";
        //players[2] = "Player3";
        //players[3] = "Player4";
        playerNumber = players.size();
        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });
        zwischenAktionen = new String[9];
        zwischenAktionen[0] ="Dummheit wird bestraft.. Der Dümmste trinkt!";
        zwischenAktionen[1] ="Streber mag keiner.. Der Schlauste trinkt!";
        zwischenAktionen[2] ="Mann voran.. Alle Männer trinken!";
        zwischenAktionen[3] ="Emanzipation! Alle Frauen trinken!";
        zwischenAktionen[4] ="Rohrbruch! Alle trinken einen";
        //zwischenAktionen[5] ="Die Flut kommt! Der Füllstand steigt";
        //zwischenAktionen[6] ="Ebbe in Sicht! Der Füllstand sinkt";
        //zwischenAktionen[7] ="Treppensaufen! Es geht abwärts..";
        //zwischenAktionen[8] ="Treppensaufen! Es geht aufwärts..";
        zwischenAktionen[5] ="Siegerehrung! Gold, Silber und Bronze stoßen an";
        zwischenAktionen[6]="Dabei sein ist alles! Die drei letzten Plätze dürfen auch mal ran";
        zwischenAktionen[7]="Die goldene Mitte darf einen trinken!";
        zwischenAktionen[8]="Zufall! Wer darf trinken?";


        progressBar = findViewById(R.id.progressBar);
        progressBar.setMax(12000);
        progressBar.setProgress(progressBar.getMax());
        progressDiff = progressBar.getMax()/120;

        einstellungen = findViewById(R.id.quiz_einstellungen);
        switchSort = findViewById(R.id.quiz_switch3);
        switchTime = findViewById(R.id.quiz_switch2);
        switchAktion = findViewById(R.id.quiz_switch1);
        switchSort.setVisibility(View.GONE);
        switchTime.setVisibility(View.GONE);
        switchAktion.setVisibility(View.GONE);

        einstellungen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(einstellungenSichtbar){
                    einstellungenSichtbar=false;
                    switchAktion.setVisibility(View.GONE);
                    switchTime.setVisibility(View.GONE);
                    switchSort.setVisibility(View.GONE);
                    switchAktion.setClickable(false);
                    switchTime.setClickable(false);
                    switchSort.setClickable(false);
                    switchAktion.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_hide));
                    switchTime.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab3_hide));
                    switchSort.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab4_hide));
                }else {
                    einstellungenSichtbar=true;
                    switchAktion.setVisibility(View.GONE);
                    switchTime.setVisibility(View.VISIBLE);
                    switchSort.setVisibility(View.VISIBLE);
                    switchAktion.setClickable(true);
                    switchTime.setClickable(true);
                    switchSort.setClickable(true);
                    switchAktion.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_show));
                    switchTime.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab3_show));
                    switchSort.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab4_show));
                }
            }
        });

        listCount = new ArrayList<PlayersWithCounts>();
        for(int i=0; i<players.size(); i++){
            listCount.add(new PlayersWithCounts(players.get(i)));
        }

        actualPlayer = 0;
        actualEntry = 0;
        status =0;
        BluetoothAdapterClass.setHandler(mHandler);

        setupListview();

        switchSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePoiListView();
            }
        });

        //Sex und Liebe:
        QuizEntry qSL0  = new QuizEntry("Wie groß war der kleinste erigierte Penis, der je vermessen wurde?","2cm","1cm","4,2cm","0,5cm", 1);
        QuizEntry qSL1  = new QuizEntry("Welche Tiere haben neben den Menschen nur aus Spaß an der Freude Sex?","Nashörner","Delfine","Frösche","Waschbären", 1);
        QuizEntry qSL2  = new QuizEntry("Wie lange dauern nächtliche Errektion durchschnittlich?","50 Min.","2 Std.","30 Min.","45 Sek. ", 0);
        QuizEntry qSL3  = new QuizEntry("Wie viel Energie verbrennt eine halbe Stunde Sex?","150 kcal","900 kcal","200 kcal","30 kcal", 2);
        QuizEntry qSL4  = new QuizEntry("Wie lange haben Shimpansen durchschnittlich Sex?","10 Min.","2 Std.","30 Sek.","7 Sek.", 3);
        QuizEntry qSL5  = new QuizEntry("Welchen Dienst nutzen ca 1.500.000 Männer pro Tag?","Lieferservice","Netflix","Prostitution","Reinigung", 2);
        QuizEntry qSL6  = new QuizEntry("Wie viel % aller erzählten Witze handeln von Sexualität?","79%","31%","98%","54%", 0);
        QuizEntry qSL7  = new QuizEntry("","","","","", 1);
        QuizEntry qSL8  = new QuizEntry("","","","","", 1);
        QuizEntry qSL9  = new QuizEntry("","","","","", 1);
        QuizEntry qSL10 = new QuizEntry("","","","","", 1);
        QuizEntry qSL11 = new QuizEntry("","","","","", 1);
        QuizEntry qSL12 = new QuizEntry("","","","","", 1);
        QuizEntry qSL13 = new QuizEntry("","","","","", 1);

        //Recht und Ordnung:
        QuizEntry qRO0 = new QuizEntry("In welchem Land muss man auf der linken Straßenseite fahren?","Kanada", "Australien", "Brasilien", "Mongolei", 1);
        QuizEntry qRO1 = new QuizEntry("Welcher Staat besteht aus 16 Bundesländer?","Spanien", "Italien", "USA", "Deutschland", 3);
        QuizEntry qRO2 = new QuizEntry("Was ist der Nationalvogel von Neuseeland","Moa", "Steinadler", "Kiwi", "Pinguin", 2);
        QuizEntry qRO3 = new QuizEntry("Welche Tiere sind in England grundsätzlich Eigentum der Queen?","Schwäne", "Affen", "Corgis", "Pinguine", 0);
        QuizEntry qRO4 = new QuizEntry("Welche Bärenart tötet am meisten Kinder?","Braunbär", "Koalabär", "Teddybär", "Eisbär", 2);
        QuizEntry qRO5 = new QuizEntry("Was ist in einigen Städten in Frankreich verboten??","Rauchen", "Sterben", "Weinen", "Lachen", 2);
        QuizEntry qRO6 = new QuizEntry("In Washington D.C. ist ausschließlich welche Sexstellung erlaubt?","Missionar", "Wackelpeter", "Schnecke", "Schaukelstuhl", 0);
        QuizEntry qRO7 = new QuizEntry("Wie viele Vornamen darf ein Kind in Deutschland haben?","2", "5", "7", "10", 1);
        QuizEntry qRO8 = new QuizEntry("Bis wann war der Verkauf von Kondomen in Irland verboten?","2005", "1980", "2000", "1990", 3);
        QuizEntry qRO9 = new QuizEntry("Welches Delikt ist Teil des Strafrechts?","Unterlassene Nötigung", "Unterlassene Veruntreeung", "Unterlassene Hilfeleistung", "Unterlassener Beischlaf", 2);
        QuizEntry qRO10 = new QuizEntry("Welche sind KEINE sozialen Insekten?","Bienen", "Ameisen", "Termiten", "Motten", 3);

        //Geschichte:
        QuizEntry qG0  = new QuizEntry("Was ist die Quadratwurzel aus 81?","20.25", "9", "81", "18", 1);
        QuizEntry qG1 = new QuizEntry("Welches Messer wird an den Lauf eines Gewehres gesteckt?","Bajonett", "Berdysch", "Dolch", "Claymore", 0);
        QuizEntry qG2 = new QuizEntry("Wessen Spitzname war ''Der Wüstenfuchs''?","Joseph Stalin", "Douglas MacArthur", "Erwin Rommel", "Adolf Hitler", 2);
        QuizEntry qG3 = new QuizEntry("Aus welchem Land stammt die Freiheitsstatue?","Spanien", "Frankreich", "Portugal", "Vereinigtes Königreich", 1);
        QuizEntry qG4 = new QuizEntry("Wann wurden die Atombomben Nagasaki und Hiroshima abgeworfen?","1946", "1948", "1945", "1947", 2);
        QuizEntry qG5 = new QuizEntry("Wo starb Napoleon?","Insel St. Helena", "Rom", "Insel Elba", "Paris", 0);
        QuizEntry qG6 = new QuizEntry("Wer war der Führer der Spartaner in der Schlacht von Thermopylae","Perikles", "Herodot", "Solon", "Leonidas", 3);
        QuizEntry qG7 = new QuizEntry("In welchem Land wurde Adolf Hitler geboren?","Deutschland", "Österreich", "Frankreich", "Spanien", 1);
        QuizEntry qG8 = new QuizEntry("In welchem Land begann die Demokratie?","Irak", "Ägypten", "China", "Griechenland", 3);
        QuizEntry qG9  = new QuizEntry("Wo ist Albert Einstein geboren?","Frankreich", "Österreich", "Deutschland", "Niederlande", 2);
        QuizEntry qG10 = new QuizEntry("Romeo und...?","Sabine","Barbara","Julia","Helga", 2);
        QuizEntry qG11 = new QuizEntry("Was nutzen die alten Römer zur Zahnreinigung?","Urin","Olivenöl","Essig","Kalk", 0);
        QuizEntry qG12 = new QuizEntry("Welches Land hat zwei Weltkriege verloren?","Niederlande","Deutschland","England","Russland", 1);
        QuizEntry qG13 = new QuizEntry("Wie viel wog die schwerste lebende Frau?","212kg","374kg","456kg","544kg", 3);
        QuizEntry qG14 = new QuizEntry("Wer erfand den Glöckner von Notre-Dame?","Émile Zola","Alexandre Dumas","Victor Hugo","Jules Verne", 2);
        QuizEntry qG15 = new QuizEntry("Was sind die Ureinwohner Australiens?","Indianer","Berber","Aborigines","Buschmänner", 2);
        QuizEntry qG16 = new QuizEntry("Welche Novelle wurde von Volette verfasst?","Atala","Heartburn","Passing","Gigi", 3);
        QuizEntry qG17 = new QuizEntry("Woraus entwickelte sich das Rokoko?","Barock","Romantik","Gotik","Neoklassizismus", 0);
        QuizEntry qG18 = new QuizEntry("Wie viele Parteien stellten die Regierung der Sowjetunion?","1","3","4","2", 0);
        QuizEntry qG19 = new QuizEntry("Was existierte bereits in dern Küchen des Mittelalters?","Seiher","Remoska","Tauchsieder","Mikrowellenherd", 0);
        QuizEntry qG20 = new QuizEntry("Wie starb Adolf Hitler?","Nierenversagen","Hinrichtung","Selbstmord","Bombe", 2);
        QuizEntry qG21 = new QuizEntry("Wer war der erste römische Kaiser?","Augustus","Tiberius","Caesar","Nero", 0);
        QuizEntry qG22 = new QuizEntry("Wer war Marie Curie?","Physikerin", "Prophetin", "Kaiserin", "Hexe", 0);
        QuizEntry qG23 = new QuizEntry("Die Entente Cordiale von 1904 war ein Abkommen zwischen Frankreich und...?", "Spanien", "Deutschland", "Italien", " England", 3);
        QuizEntry qG24 = new QuizEntry("Auf welchem Schiff gewann Charles Darwin Erkenntnisse für seine Evolutionstheorie", "Bismark", "Mayflower", "HMS Beagle", "Santa Maria", 2);
        QuizEntry qG25 = new QuizEntry("Wann starteten die USA eine Militärkampagne in Afghanistan?", "2001", "2005", "1998", "2003", 0);
        QuizEntry qG26 = new QuizEntry("Unter welchem Kurfürsten begann 1640 der Aufstieg Brandenburg-Preußens?", "Maximilian III. Joseph", "Karl Theodor", "Friedrich Wilhelm", "Ludwig von Meißen", 2);
        QuizEntry qG27 = new QuizEntry("Wofür ist der Broadway berühmt?", "Börse", "Woodstock", "Theater", "Eishockey", 2);
        QuizEntry qG28 = new QuizEntry("Welche Präsidenten kommunizierten als Erste über das Rote Telefon (Moskau-Washington)?", "Nixon - Andropow", "Truman - Breschnew", "Kennedy - Chruschtschow", "Eisenhower - Malenkow", 2);
        QuizEntry qG29 = new QuizEntry("Welche Zivilisation brach im 9. JH. n. Chr. aus inbekannten Gründen zusammen?", "Hethiter", "Mykene", "Khmer", "Maya", 3);

        //Erdkunde:
        QuizEntry qE0  = new QuizEntry("Wer gilt der Legende nach als Gründer von Rom?","Ceasar", "Romulus", "Romanow", "Augustus", 1);
        QuizEntry qE1  = new QuizEntry("Wo lebt das Volk der Samen?","Skandinavien", "Nepal", "Südafrika", "Nordamerika", 0);
        QuizEntry qE2  = new QuizEntry("Wie heißt die Hauptinsel Japans?","Okinawa", "Honshu", "Hokkaido", "Shikoku", 1);
        QuizEntry qE3  = new QuizEntry("Wie heißt die Hauptstadt Griechenlands?","Patra", "Nepal", "Rom", "Athen", 3);
        QuizEntry qE4  = new QuizEntry("Wie viele Streifen hat die Flagge der USA?","15", "11", "13", "9", 2);
        QuizEntry qE5  = new QuizEntry("Zu welchen Gebirge gehört der Mount Everest?","Tian Shan", "Anden", "Ural", "Himalaya", 3);
        QuizEntry qE6  = new QuizEntry("Was war die russische Hauptstadt vor Moskau?","Jekaterinburg", "Sankt Petersburg", "Novosibirsk", "Samara", 1);
        QuizEntry qE7  = new QuizEntry("Welcher Buchstabe dient einem französischem Ort als Name?","A", "U", "E", "Y", 3);
        QuizEntry qE8  = new QuizEntry("Welche türkische Stadt gibt es wirklich?","Batman", "Superman", "Aquaman", "Sandman", 0);
        QuizEntry qE9  = new QuizEntry("Welcher ist der größte Binnenstaat?","Schweiz", "Mongolei", "Bolivien", "Mali", 1);
        QuizEntry qE10  = new QuizEntry("Welches Land hat keine rechteckige Flagge?","Belgien", "Kanada", "Nepal", "Kuba", 2);
        QuizEntry qE11  = new QuizEntry("Was für ein Tier ist der Tasmanische Teufel?","Rüsselspringer", "Kloakentier", "Beuteltier", "Schliefer", 2);
        QuizEntry qE12  = new QuizEntry("Wo leben die Jakuten?","Südafrika", "Nordeuropa", "Südamerika", "Nordasien", 3);
        QuizEntry qE13  = new QuizEntry("Wovon ernährt sich ein Flusspferd hauptsächlich","Gräser", "Weichtiere", "Früchte", "Fische", 0);
        QuizEntry qE14  = new QuizEntry("Wo entspringt die Elbe?","Polen", "Deutschland", "Tschechien", "Ungarn", 2);
        QuizEntry qE15  = new QuizEntry("Phoenix ist die Hauptstadt von...?","Maine", "Arizona", "Iowa", "Delaware", 1);
        QuizEntry qE16  = new QuizEntry("Mit welchem anderen Land teilt sich die Dominikanische Republik eine Insel?","Haiti", "Puerto Rico", "Jamaika", "Kuba", 0);
        QuizEntry qE17  = new QuizEntry("Was ist die Hauptstadt von Mecklenburg-Vorpommern?","Rostock", "Schwerin", "Magdeburg", "Hamburg", 1);
        QuizEntry qE18  = new QuizEntry("Was ist die Zweitgrößte Stadt Russlands?","Nowosibirsk", "Sankt Petersburg", "Wladiwostok", "Kasan", 1);
        QuizEntry qE19  = new QuizEntry("Was ist die Hauptstatt der Demokratischen Republik Kongo?","Kinshasa", "Durban", "Nairobi", "Lagos", 0);
        QuizEntry qE20  = new QuizEntry("Von wo aus können wir die Mitternachtssonne beobachten?","Wladivostok", "Omsk", "Tromsø", "Innsbruck", 2);
        QuizEntry qE21  = new QuizEntry("In welchem Land können wir eine alte Stadt namens Samarkand finden?","Syrien", "Usbekistan", "Iran", "Kuwait", 1);
        QuizEntry qE22  = new QuizEntry("Monrovia ist die Hauptstadt von ...?","Togo", "Liberia", "Burkina Faso", "Gambia", 1);
        QuizEntry qE23  = new QuizEntry("Welche Stadt liegt am nächsten an Wolfsburg?","Magdeburg", "Hamburg", "Würzburg", "Karlsruhe", 0);
        QuizEntry qE24  = new QuizEntry("Welches Land ist dem Great Barrier Reef am nächsten?","Indonesien", "Costa Rica", "Madagaskar", "Australien", 3);
        QuizEntry qE25  = new QuizEntry("Welche Regierung ist für ihre umfassende Zensur von Webseiten bekannt?","USA", "China", "Japan", "Frankreich", 1);
        QuizEntry qE26  = new QuizEntry("In welches Meer mündet die Weichsel?","Ostsee", "Schwarzes Meer", "Nordsee", "Atlantik", 0);
        QuizEntry qE27  = new QuizEntry("Was ist die Hauptstadt von Ungarn?","Bukarest", "Prag", "Budapest", "Bratislava", 2);
        QuizEntry qE28  = new QuizEntry("Welche dieser Regionen verfügt über die größten natürlichen Erdgasreserven?","Himalaya", "Sibirien", "Südafrika", "Gran Chaco", 1);
        QuizEntry qE29  = new QuizEntry("In welchem Bundesland befindet sich das Schloss Neuschwanstein?","Saarland", "Sachsen-Anhalt", "Hessen", "Bayern", 3);
        QuizEntry qE30  = new QuizEntry("In welchem Land fand der Boxeraufstand statt?","Indien", "Saudi Arabien", "China", "USA", 2);
        QuizEntry qE31  = new QuizEntry("Wo fand der Zweite Burenkrieg (1899 - 1902) statt?","Südafrika", "Kolumbien", "Ägypten", "Niederlande", 0);
        QuizEntry qE32  = new QuizEntry("Wie nennen die Tschechen ihre Hauptstadt?","Prago", "Pradu", "Praha", "Pragia", 2);
        QuizEntry qE33  = new QuizEntry("Wo liegt die Republik Dagestan?","Ural", "Sibirien", "Jablonowy", "Kaukasus", 3);
        QuizEntry qE34  = new QuizEntry("Wo befindet sich der Alexanderplatz?","Bern", "Berlin", "München", "Wien", 1);
        QuizEntry qE35  = new QuizEntry("Wie heißt die Hauptstadt Australiens?","Sydney","Canberra", "Melbourne", "Brisbane", 1);
        QuizEntry qE36  = new QuizEntry("Wie heißt die Hauptstadt Kanadas?", "Vancouver","Montreal","Toronto","Ottawa", 3);
        QuizEntry qE37  = new QuizEntry("Welcher Ozean liegt zwischen Europa und Amerika?","Nordmeer","Indischer Ozean","Atlantik","Pazifik", 2);
        QuizEntry qE38  = new QuizEntry("In welchem Land liegt der Suezkanal?","Ägypten","Lybien","Libanon","Israel",0);
        QuizEntry qE39  = new QuizEntry("Niamey ist die Hauptstadt welches Landes?","Gabun","Niger","Sudan","Tschad",1);
        QuizEntry qE40  = new QuizEntry("Quito ist die Hauptstadt welches Landes?","Panama","Venezuela","Kolumbien","Ecuador",3);
        QuizEntry qE41  = new QuizEntry("Ungarn grenzt nicht an?","Österreich","Serbien","Tschechien","Kroatien",2);
        QuizEntry qE42  = new QuizEntry("Litauen grenzt nicht an?","Estland","Lettland","Polen","Weißrussland",0);
        QuizEntry qE43  = new QuizEntry("Die Türkei grenzt nicht an?","Irak","Aserbaidschan","Syrien","Bulgarien",1);


        //Essen & Trinken:
        QuizEntry qET0  = new QuizEntry("In welcher Farbe gibt es keine Gummibärchen von Haribo?","Gelb", "Weiss", "Rot", "Blau", 3);
        QuizEntry qET1  = new QuizEntry("Was für eine Speise ist Gazpacho?","Omelette", "Salat", "Suppe", "Sandwich", 2);
        QuizEntry qET2  = new QuizEntry("Wo kommt das Gericht Souvlaki ursprünglich her?","Griechenland", "Italien", "Spanien", "Frankreich", 0);
        QuizEntry qET3  = new QuizEntry("Von welchem Tier kommen Buffalo Wings?","Schwein", "Büffel", "Huhn", "Ente", 2);
        QuizEntry qET4  = new QuizEntry("Biryani ist ein Reisgericht aus...?","Ägypten", "Marokko", "Indien", "China", 2);
        QuizEntry qET5  = new QuizEntry("Ein traditionelles schwedisches Milchprodukt heißt?","Filmjölk", "Aarts", "Uunijuusto", "Sergem", 0);
        QuizEntry qET6  = new QuizEntry("Von welcher Pflanze sind nur die Blattstile und nicht die Blätter essbar?","Endivie", "Radicchio", "Rhabarber", "Fleischkraut", 2);
        QuizEntry qET7  = new QuizEntry("Wonach ist der Tee namens Earls Grey benannt?","Pferderennen", "Roman von Agatha Christie", "Premierminister", "Hunderasse", 2);


        //Alkohol:
        QuizEntry qA0  = new QuizEntry("Was darf nach dem Reinheitsgebot nicht für die Herstellung von Bier verwendet werden?","Malz", "Hopfen", "Wasser", "Mais", 3);
        QuizEntry qA1  = new QuizEntry("Aus welchem Jahr stammt das Reinheitsgebot des Bieres?","1677", "1516", "1765", "1415", 1);
        QuizEntry qA2  = new QuizEntry("Was ist für die Herbe des Bieres ausschlaggebend?","Zucker", "Trinktemperatur", "Hopfengabe", "Alkoholgehalt", 2);
        QuizEntry qA3  = new QuizEntry("Welche Hefesorte gibt es nicht?","Obergärige Hefe", "Sprossenhefe", "Untergärige Hefe", "Spalthefe", 1);
        QuizEntry qA4  = new QuizEntry("Welches Bier wird mit untergäriger Hefe vergoren?","Weizen", "Alt", "Kölsch", "Pilsener", 3);
        QuizEntry qA5  = new QuizEntry("Welches berühmte Getränk wird mit Zitrusfrucht und Salz serviert?","Wodka", "Tequila", "Whiskey", "Ouzo", 1);
        QuizEntry qA6  = new QuizEntry("Was hat den höchsten Alkoholgehalt?","Bier", "Wodka", "Strohrum", "Korn", 2);
        QuizEntry qA7  = new QuizEntry("Was kann das Wachstum von Krebszellen hemmen?","Rotwein", "Bier", "Korn", "Rum", 0);
        QuizEntry qA8  = new QuizEntry("Wie viel Bier trinken Männer durchschnittlich im Jahr?","50,7 Liter", "92,3 Liter", "134,5 Liter", "79,1 Liter", 1);

        //Technik
        QuizEntry qT0  = new QuizEntry("Welcher Panzer wurde am häufigsten produziert?","M48 Patton", "Panzer IV", "t-55", "M4 Sherman", 2);
        QuizEntry qT1  = new QuizEntry("Wofür steht die Abkürzung DB","Driver Boost", "Data Boot", "Drivers Base", "Database", 3);
        QuizEntry qT2  = new QuizEntry("Was ist der schnellste Speicher in einem Computer?","Festplatte", "USB-Stick", "Cache", "RAM", 2);
        QuizEntry qT3  = new QuizEntry("Wofür steht die Abkürzung AI?","Artificial Intelligence", "Automated Icon", "Advanced Icon", "Active Interface", 0);
        QuizEntry qT4  = new QuizEntry("Welche Automarke hat ein Pferd im Logo?","Audi", "Ferrari", "Bentley", "Alfa Romeo", 1);
        QuizEntry qT5  = new QuizEntry("Was braucht am wenigsten Strom?","Elektroherd", "elektrischer Boiler", "Klimaanlage", "Fernseher", 3);
        QuizEntry qT6  = new QuizEntry("Welches Unternehmen produzierte als erstes ein Handy?","Sony Ericsson", "Samsung", "Motorola", "Nokia", 2);
        QuizEntry qT7  = new QuizEntry("In welchem Ton hupen Amerikanische Autos?","F", "G", "C", "b", 0);
        QuizEntry qT8  = new QuizEntry("Welches Gas ist leicht entzündlich?","Wasserstoff", "Kohlendioxid", "Helium", "Ammoniak", 0);
        QuizEntry qT9  = new QuizEntry("145/5=","25", "29", "35", "17", 1);
        QuizEntry qT10  = new QuizEntry("Woraus werden Leinenstoffe gemacht?","Flachs", "Baumwolle", "Jute", "Hanf", 0);
        QuizEntry qT11  = new QuizEntry("Welche Software ermöglicht dem Betriebssystem bestimmte Hardware zu kontrollieren?","Webber", "Stepper", "Filter", "Treiber", 3);
        QuizEntry qT12  = new QuizEntry("Was ist der Übergang von fest zu gasförmig?","Kondensation", "Ionisation", "Rekombination", "Sublimation", 3);
        QuizEntry qT13  = new QuizEntry("Wie viele Seiten gleicher Länge hat ein Quadrat?","0", "4", "1", "2", 1);
        QuizEntry qT14  = new QuizEntry("Auf welcher Technologie basiert seit den 1960ern die überwiegende Mehrzahl aller Computer?","PA-RISC", "Kohlenstoff-Nanorörchen", "CMOS", "FinFET", 2);
        QuizEntry qT15  = new QuizEntry("Welches Material wird bei der Vulkanisation verwendet?","Kautschuk", "Stahl", "Kupfer", "Kohle", 0);
        QuizEntry qT16  = new QuizEntry("Welches ist KEIN guter elektrischer Isolator?","Plastik", "Holz", "Silber", "Glas", 2);
        QuizEntry qT17  = new QuizEntry("Was sollte man beim Straßenbau nicht vergessen?","Venturi-Schlauch", "Entwässerung", "Fliegengitter", "HVAC", 1);
        QuizEntry qT18  = new QuizEntry("Welche Mauer verhindert Überflutungen?","Vangard", "Moor", "Kurk", "Deich", 3);
        QuizEntry qT19  = new QuizEntry("Wlche Brüder erfanden die Filmkamera","Wright", "Lumière", "Marx", "Warner", 1);
        QuizEntry qT20  = new QuizEntry("Wobei erweißt sich ein Fingerhut als nützlich?","Weben", "Klöppeln", "Häkeln", "Nähen", 3);
        QuizEntry qT21  = new QuizEntry("Welches Autoteil kann aus einer Membranfeder bestehen?","Kupplung", "Servolenkung", "Kolben", "Lichtmaschine", 0);
        QuizEntry qT22  = new QuizEntry("Wie nennt man ein Dreieck, das KEINEN Winkel hat, der größer als 90° ist?","Gleichschenklich", "Spitzwinklig", "Stumpfwinklig", "Mesoskalar", 1);
        QuizEntry qT23  = new QuizEntry("Was für eine Technik ist Makramee?","Weben", "Knüpfen", "Wabensteppen", "Stricken", 1);


        //Wissenschaft
        QuizEntry qW0  = new QuizEntry("Was ist die SI-Einheit für Elektrischen Strom?","Meter", "Ampere", "Volt", "Ohm", 1);
        QuizEntry qW1  = new QuizEntry("Welche Farbe haben Rubine?","Rot", "Blau", "Grün", "Weiß", 0);
        QuizEntry qW2  = new QuizEntry("Ein Chronometer misst:","Frequenz", "Lichtintensität", "Zeit", "Temperatur", 2);
        QuizEntry qW3  = new QuizEntry("Wie viele Sekunden hat eine Stunde?","60", "1800", "6000", "3600", 3);
        QuizEntry qW4  = new QuizEntry("Welches Zahlensystem basiert auf den Ziffern 1 und 0?","Dezimalsystem", "Binärsystem", "Ternärsystem", "Quartärsystem", 1);
        QuizEntry qW5  = new QuizEntry("Was ist die Einheit zum Messen der Amplitude eines Klanges?","Ampere", "Hertz", "Dezibel", "Siemens", 2);
        QuizEntry qW6  = new QuizEntry("Woraus bestehen Diamanten hauptsächlich?","Silizium", "Glas", "Titanium", "Kohlenstoff", 3);
        QuizEntry qW7 = new QuizEntry("Was kommt heraus wenn man Gelb und Rot mischt?","Blau", "Violett", "Grün", "Orange", 3);
        QuizEntry qW8 = new QuizEntry("Wann erleiden die meisten Menschen Herzinfarkte?","Sonntags", "Montags", "Freitags", "Mittwochs", 1);
        QuizEntry qW9 = new QuizEntry("Welche Abkürzung hat das Element Zinn im Periodensystem?","Si", "Zn", "Zi", "Sn", 3);
        QuizEntry qW10  = new QuizEntry("Wie nennt man eine Lösung von Salzen in Wasser","Sole", "Borax", "Beryll", "Barrerit", 0);
        QuizEntry qW11  = new QuizEntry("In welchem Winkel schneiden sich perpendikulare Linien?","90°", "360°", "180°", "270°", 0);
        QuizEntry qW12  = new QuizEntry("Was ist die nach oben gerichtete Kraft eines eingetauchten Körpers?","Viskosität", "Abscherkaft", "Auftrieb", "Reynold-Kraft", 2);
        QuizEntry qW13  = new QuizEntry("Welches Gelenk liegt zwischen Humerus und Ulna?","Handgelenk", "Schulter", "Ellbogen", "Knie", 2);
        QuizEntry qW14  = new QuizEntry("Was ist die Einheit der Radioaktivität?","Henry", "Siemens", "Becquerel", "Weber", 2);
        QuizEntry qW15  = new QuizEntry("Welche Isotope werden für die Radiokarbon-Datierung verwendet?","Kohlenstoff-13", "Kohlenstoff-14", "Kohlenstoff-12", "Kohlenstoff-6", 1);
        QuizEntry qW16  = new QuizEntry("Was stammt von Equus ferus ab?","Hund", "Pferd", "Katze", "Schwein", 1);
        QuizEntry qW17  = new QuizEntry("Welche dieser Krankheiten ist heilbar?","Alzheimer", "Muskeldystrophie", "Multiple Sklerose", "Keine von diesen", 3);
        QuizEntry qW18  = new QuizEntry("Welches grüne Pigment findet man in Pflanzen?","Phtalocyanin", "Indanthron", "Chlorophyll", "Vermillion", 2);
        QuizEntry qW19  = new QuizEntry("Wlche wissenstschaftliche Fachzeitschrift hat einen der höchsten Impact Factors?","Icarus", "Nautilus", "Phonetica", "Nature", 3);
        QuizEntry qW20  = new QuizEntry("Was ist der natürliche Lebensraum einer Tier- oder Pflanzenart?","Quadrant", "Habitat", "Exosphere", "Flachsäule", 1);
        QuizEntry qW21  = new QuizEntry("Welche Art von Blitz ist unmöglich?","Wolke -> Wolke", "Wolke -> Erde", "Wolke -> Mond", "Perlschnurblitz", 2);
        QuizEntry qW22  = new QuizEntry("Was ist ein schnell rotierender Neutronenstern, der periodisch Strahlung emittiert?","Supernova", "Blauer Riese", "Pulsar", "Weißer Zwerg", 2);
        QuizEntry qW23  = new QuizEntry("Welches hat weder definiertes Volumen, noch definitive Form?","Flüssiger Stickstoff", "Milch", "Heliumgas", "Stahl", 2);
        QuizEntry qW24  = new QuizEntry("Was befindet sich im Zentrum der Milchstraße?","Supernova", "Schwarzes Loch", "Supervoid", "Quasar", 1);
        QuizEntry qW25  = new QuizEntry("Wmit können schwere Depressionen oder Schizophrenien behandelt werden?","Immunsuppressiva", "Osmotherapie", "Kobalt-Therapie", "Elektrokrampftherapie", 3);
        QuizEntry qW26  = new QuizEntry("Wo finden wir p-n Übergange?","Sterne", "Dioden", "Atomkern", "Enzyme", 1);
        QuizEntry qW27  = new QuizEntry("Was verstand Charles Darwin als Treiber der Evolution?","Schicksal", "Künstliche Auslese", "Unabsehbarkeit", "Natürliche Selektion", 3);

        //Religion
        QuizEntry qR0  = new QuizEntry("Welche hindische Gottheit hat den Kopf eines Elefanten?","Bhagavati", "Krishna", "Ganesha", "Brahma", 2);
        QuizEntry qR1 = new QuizEntry("Welche Leute glauben, dass Christus zur Erde zurückkommt?","Anabaptisten", "Assyrer", "Artisten", "Adventisten", 3);
        QuizEntry qR2 = new QuizEntry("Wie groß ist ein durchschnittlicher Klopapierstreifen in Deutschland?","13cm x 9,9cm", "15cm x 12cm", "9,1cm x 7cm", "12cm x 10cm", 0);
        QuizEntry qR3 = new QuizEntry("Wer ist laut Altem Testament der Stammvater Israels?","Matthias", "Moses", "Jesus", "Abraham", 3);

        //Nerd-Wissen:
        QuizEntry qNW0 = new QuizEntry("Welcher Superheld kommt aus dem DC Universum?","Deadpool", "Sandman", "Iron Man", "Vision", 1);
        QuizEntry qNW1 = new QuizEntry("Welcher Schauspieler ist nicht Teil der Handlung und bleibt im Hintergrund?","Komfort-Schauspieler", "Method-Actor", "Paraszän", "Statist", 3);
        QuizEntry qNW2 = new QuizEntry("In welcher Reihenfolge kamen die ersten 6 Teile der Star Wars Saga in die Kinos?","456123", "123456", "142536", "654321", 0);


        //Sport:
        QuizEntry qS0 = new QuizEntry("Wie viele Perioden hat ein National Hockey League-Spiel?","3", "5", "2", "4", 0);
        QuizEntry qS1 = new QuizEntry("Welches Land gewann die Fußball WM im Jahr 2002?","Italien", "Deutschland", "Brasilien", "Spanien", 2);
        QuizEntry qS2 = new QuizEntry("In Welcher Stadt fanden die ersten Olympischen Spiele statt?","Athen", "Berlin", "Rom", "Amsterdam", 0);
        QuizEntry qS3 = new QuizEntry("Welchen Preis kriegt der Spitzentorschütze in einer Fußball-WM?","den goldenen Schuh", "den goldenen Handschuh", "den goldenen Helm", "die goldene Socke", 0);
        QuizEntry qS4 = new QuizEntry("In welcher Sportart benutzt der Athlet ein Degen?","Karate", "Fechten", "Baseball", "Skifahren", 1);
        QuizEntry qS5 = new QuizEntry("Woher stammt der berühmte Fußballspieler Christiano Ronaldo?","Argentinien", "Italien", "Portugal", "Spanien", 2);
        QuizEntry qS6 = new QuizEntry("Wer hat Basketball erfunden?","A. Rupp", "J. Naismith", "D. Smith", "E. Miller", 1);
        QuizEntry qS7 = new QuizEntry("Welcher Fußballverein spielte immer in der 1. Bundesliga?","FC Bayern München", "FC Schalke 04", "Hamburger SV", "Werder Bremen", 2);
        QuizEntry qS8 = new QuizEntry("Welcher Verein ist Rekordmeister der Handball-Bundesliga?","THW Kiel", "Flensburg", "Rhein-Neckar Löwen", "HSG Wetzlar", 0);
        QuizEntry qS9 = new QuizEntry("Welche Fußballmannschaft trägt ihre Heimspiele im Tivoli aus?","1. FC Kaiserslautern", "Alemanina Aachen", "Borussia Mönchengladbach", "RW Essen", 1);
        QuizEntry qS10 = new QuizEntry("Welches ist das erste Grand Slam Turnier im Jahr?","US Open", "French Open", "Wimbledon", "Australian Open", 3);
        QuizEntry qS11 = new QuizEntry("Welche dieser 4 Skiflugschanzen ist die Größte?","Vikersundbakken", "Kulm", "Oberstdorf", "Bad Mitterndorf", 0);
        QuizEntry qS12 = new QuizEntry("Wo ruht der Golfball zu Beginn des Spiels?","Tee", "Stall", "Marquee", "Puk", 0);
        QuizEntry qS13 = new QuizEntry("Wie oft darf ein Volleyballteam den Ball berühren?","2x", "5x", "4x", "3x", 3);
        QuizEntry qS14 = new QuizEntry("Welche dieser Sportarten wird mit den wenigsten Spielern pro Mannschaft gespielt?","Cricket", "Baseball", "Fußball", "Basketball", 3);
        QuizEntry qS15 = new QuizEntry("Für welchen Sport brauchen wir einen Ball?","Karate", "Speerwurf", "Cricket", "Taekwondo", 2);
        QuizEntry qS16 = new QuizEntry("Wie heißt das NFL Team der Stadt Baltimore","Falcons", "Cardinals", "Hawks", "Ravens", 3);


        //Musik:
        QuizEntry qM0 = new QuizEntry("Welcher Musiker wurde als King of Pop bekannt?","Justin Bieber", "Michael Jackson", "Justin Timberlake", "Sido", 1);
        QuizEntry qM1 = new QuizEntry("Welche Sängerin hatte ihren Durchbruch mit 'Baby One More Time'?","Rihanna", "Britney Spears", "Katy Perry", "Jenifer Lopez", 1);
        QuizEntry qM2 = new QuizEntry("Welches Land gewann 2015 den Eurovision Song Contest?","Griechenland", "Österreich", "Schweden", "Deutschland", 2);
        QuizEntry qM3 = new QuizEntry("Welche Band brachte das Album 'The Dark Side of the Moon' heraus?","Nirvana", "Led Zeppelin", "Deep Purple", "Pink Floyd", 3);
        QuizEntry qM4 = new QuizEntry("Wer war der Lead-Sänger von 'The Supremes'?","Diana Ross", "Darlene Love", "Martha Reeves", "Bushido", 0);
        QuizEntry qM5 = new QuizEntry("Wie viele Saiten hat eine klassische Gitarre?","4", "6", "12", "8", 1);
        QuizEntry qM6 = new QuizEntry("Wer war ein einflussreicher Komponist elektronischer Musik?","Karlheinz Stockhausen", "Alphons Czibulka", "Emmanuel Boyer", "Max Erdmannsdörfer", 0);
        QuizEntry qM7 = new QuizEntry("Welcher Komponist gehörte NICHT zur Ära der Wiener Klassik?","Beethofen", "Mozart", "J.S. Bach", "Haydn", 2);

        //entries = new QuizEntry[204];
        entries = new ArrayList<QuizEntry>();

        //Sex und Liebe
        entries.add(qSL0);
        entries.add(qSL1);
        entries.add(qSL2);
        entries.add(qSL3);
        entries.add(qSL4);
        entries.add(qSL5);
        entries.add(qSL6);

        //Recht und Ordnung
        entries.add(qRO0);
        entries.add(qRO1);
        entries.add(qRO2);
        entries.add(qRO3);
        entries.add(qRO4);
        entries.add(qRO5);
        entries.add(qRO6);
        entries.add(qRO7);
        entries.add(qRO8);
        entries.add(qRO9);
        entries.add(qRO10);

        //Geschichte
        entries.add(qG0);
        entries.add(qG1);
        entries.add(qG2);
        entries.add(qG3);
        entries.add(qG4);
        entries.add(qG5);
        entries.add(qG6);
        entries.add(qG7);
        entries.add(qG8);
        entries.add(qG9);
        entries.add(qG10);
        entries.add(qG11);
        entries.add(qG12);
        entries.add(qG13);
        entries.add(qG14);
        entries.add(qG15);
        entries.add(qG16);
        entries.add(qG17);
        entries.add(qG18);
        entries.add(qG19);
        entries.add(qG20);
        entries.add(qG21);
        entries.add(qG22);
        entries.add(qG23);
        entries.add(qG24);
        entries.add(qG25);
        entries.add(qG26);
        entries.add(qG27);
        entries.add(qG28);
        entries.add(qG29);

        //Erdkunde:
        entries.add(qE0);
        entries.add(qE1);
        entries.add(qE2);
        entries.add(qE3);
        entries.add(qE4);
        entries.add(qE5);
        entries.add(qE6);
        entries.add(qE7);
        entries.add(qE8);
        entries.add(qE9);
        entries.add(qE10);
        entries.add(qE11);
        entries.add(qE12);
        entries.add(qE13);
        entries.add(qE14);
        entries.add(qE15);
        entries.add(qE16);
        entries.add(qE17);
        entries.add(qE18);
        entries.add(qE19);
        entries.add(qE20);
        entries.add(qE21);
        entries.add(qE22);
        entries.add(qE23);
        entries.add(qE24);
        entries.add(qE25);
        entries.add(qE26);
        entries.add(qE27);
        entries.add(qE28);
        entries.add(qE29);
        entries.add(qE30);
        entries.add(qE31);
        entries.add(qE32);
        entries.add(qE33);
        entries.add(qE34);
        entries.add(qE35);
        entries.add(qE36);
        entries.add(qE37);
        entries.add(qE38);
        entries.add(qE39);
        entries.add(qE40);
        entries.add(qE41);
        entries.add(qE42);
        entries.add(qE43);

        //Essen & Trinken:
        entries.add(qET0);
        entries.add(qET1);
        entries.add(qET2);
        entries.add(qET3);
        entries.add(qET4);
        entries.add(qET5);
        entries.add(qET6);
        entries.add(qET7);

        //Alkohol
        entries.add(qA0);
        entries.add(qA1);
        entries.add(qA2);
        entries.add(qA3);
        entries.add(qA4);
        entries.add(qA5);
        entries.add(qA6);
        entries.add(qA7);
        entries.add(qA8);

        //Technik
        entries.add(qT0);
        entries.add(qT1);
        entries.add(qT2);
        entries.add(qT3);
        entries.add(qT4);
        entries.add(qT5);
        entries.add(qT6);
        entries.add(qT7);
        entries.add(qT8);
        entries.add(qT9);
        entries.add(qT10);
        entries.add(qT11);
        entries.add(qT12);
        entries.add(qT13);
        entries.add(qT14);
        entries.add(qT15);
        entries.add(qT16);
        entries.add(qT17);
        entries.add(qT18);
        entries.add(qT19);
        entries.add(qT20);
        entries.add(qT21);
        entries.add(qT22);
        entries.add(qT23);

        //Wissenschaft
        entries.add(qW0);
        entries.add(qW1);
        entries.add(qW2);
        entries.add(qW3);
        entries.add(qW4);
        entries.add(qW5);
        entries.add(qW6);
        entries.add(qW7);
        entries.add(qW8);
        entries.add(qW9);
        entries.add(qW10);
        entries.add(qW11);
        entries.add(qW12);
        entries.add(qW13);
        entries.add(qW14);
        entries.add(qW15);
        entries.add(qW16);
        entries.add(qW17);
        entries.add(qW18);
        entries.add(qW19);
        entries.add(qW20);
        entries.add(qW21);
        entries.add(qW22);
        entries.add(qW23);
        entries.add(qW24);
        entries.add(qW25);
        entries.add(qW26);
        entries.add(qW27);

        //Religion
        entries.add(qR0);
        entries.add(qR1);
        entries.add(qR2);
        entries.add(qR3);

        //Nerd-Wissen
        entries.add(qNW0);
        entries.add(qNW1);
        entries.add(qNW2);

        //Sport
        entries.add(qS0);
        entries.add(qS1);
        entries.add(qS2);
        entries.add(qS3);
        entries.add(qS4);
        entries.add(qS5);
        entries.add(qS6);
        entries.add(qS7);
        entries.add(qS8);
        entries.add(qS9);
        entries.add(qS10);
        entries.add(qS11);
        entries.add(qS12);
        entries.add(qS13);
        entries.add(qS14);
        entries.add(qS15);
        entries.add(qS16);

        //Musik
        entries.add(qM0);
        entries.add(qM1);
        entries.add(qM2);
        entries.add(qM3);
        entries.add(qM4);
        entries.add(qM5);
        entries.add(qM6);
        entries.add(qM7);

        //Noch nicht sortierte:
        entries.add(new QuizEntry("Welches Gebirge befindet sich in Afghanistan?","Kunlun","Altai","Tian Shian","Hindukusch",3));
        entries.add(new QuizEntry("Der Verfasser der 'Odyssee', schrieb auch..?","Aeneid","Heroides","Candide","Ilias",3));
        entries.add(new QuizEntry("Wo befindet sich Nova Scotia?","Australien","Grönland","USA","Kanada",3));
        entries.add(new QuizEntry("Welche dieser Städte liegt am nähsten an Brüssel?","Paris","Frankfurt","Köln","Hamburg",2));
        entries.add(new QuizEntry("Wofür ist die Tokajer-Region bekannt?","Jesusstatuen","Wein","Fjörde","Schinken",1));
        entries.add(new QuizEntry("Wer war ein russischer Schriftsteller?","Makarow","Molotow","Pawlowich","Gorky",3));
        entries.add(new QuizEntry("Welche Zahl steht oben im Bruch?","Nenner","Quotient","Divisor","Zähler",3));
        entries.add(new QuizEntry("Welche Verbindung sind hochgiftig?","Palmitate","Oxidane","Dioxine","Carboxylsäuren",2));
        entries.add(new QuizEntry("Was ist das langweiligste Fach im ETS Studium?","Physik","E-Technik","Organisation","Informatik",2));
        entries.add(new QuizEntry("Welches Enzym ermöglicht es Erwachsenen, Milch zu trinken?","Lactalbumin","Lactase","Lactobacillus","Lactat",1));
        entries.add(new QuizEntry("Was ist der durchschnittliche Alkoholgehalt der meisten Biere?","4,8%","5%","25%","5,8%",1));
        entries.add(new QuizEntry("Welcher Stein schwimmt im Wasser?","Granit","Bims","Basalt","Obsidian",1));
        entries.add(new QuizEntry("Giacomo Puccini ist bekannt für seine...?","Skulpturen", "Opern", "Romane", "Gemälde", 1));
        entries.add(new QuizEntry("Wie nennt man den Bereich um der Brustwarze","Ileum", "Areola", "Cornea", "Cauda equina", 1));
        entries.add(new QuizEntry("Wer war ein Pharao der 18. ägyptischen Dynastie","Sneferu", "Tutenchamun", "Imhetep", "Hammurabi", 1));
        entries.add(new QuizEntry("Welche Sehenswürdigkeit liegt am nächsten am Time Square","Bronx Zoo", "Freiheitsstatue", "Chinatown", "Rockefeller Center", 3));
        entries.add(new QuizEntry("Welche Pflanze ist nach dem griechischen Gott der Heilkunst benannt?","Achillea", "Asclepias", "Clematis", "Ambrosia", 1));
        entries.add(new QuizEntry("Welche Hunderasse ist am größten?","Rottweiler", "Deutscher Schäferhund", "Bullterrier", "Deutsche Dogge", 3));
        entries.add(new QuizEntry("Wie nennt man die Larve eines Schmetterlings","Kokon", "Raupe", "Rankenfuß", "Made", 1));
        entries.add(new QuizEntry("Welches Wort hat 2 Silben?","Familie", "Jupiter", "Frühling", "Piano", 2));
        entries.add(new QuizEntry("Wie hoch ist der Mount Everest","9.318m", "8.588m", "9.851m", "8.848m", 3));
        entries.add(new QuizEntry("Was ist ein Jeschiwa","Jüdische Akademie", "Christliche Religionsstätte", "Islamischer Staat", "Buddhistischer Tempel", 0));
        entries.add(new QuizEntry("Welcher Muskel ist für die Atmung zuständig?","Kehlkopfmuskel", "Deltoid", "Zwerkfell", "Herz", 2));
        entries.add(new QuizEntry("Wer war Mitbegründer der Firma 'Apple'","Thomas Watsen", "Bill Gates", "Steve Jobs", "Larry Ellison", 2));
        entries.add(new QuizEntry("Honig besteht hauptsächlich aus...?","Galactose, Trehalose", "Glucose, Fructose", "Fructose, Lactose", "Lactose, Sucrose", 1));
        entries.add(new QuizEntry("Welche Pflanze enthält Tetrahydrocannabinol","Akazie", "Hanf", "Aronstab", "Kartoffel", 1));
        entries.add(new QuizEntry("Welcher Teil der Blüte ist klebrig um den Pollen einzufangen","Kelchblatt", "Narbe", "Fruchtknoten", "Staubblatt", 1));
        entries.add(new QuizEntry("Was produziert Perlen","Würfelqualle", "Perlenspinne", "Hamster", "Auster", 3));
        entries.add(new QuizEntry("Wie viele Ringe hat die Olympische Flagge","5", "4", "6", "7", 0));
        entries.add(new QuizEntry("Welche Vögel können rückwärts fliegen?","Kiwis", "Störche", "Papageien", "Kolibris", 3));
        entries.add(new QuizEntry("Pflanzen sind...?","Prokaryoten", "autotroph", "Wiederkäuer", "ovipar", 1));
        entries.add(new QuizEntry("Für welche Art von Malerie werden nur Grautöne verwendet?","Grisaile", "Impasto", "Repoussé", "Filigree", 0));
        entries.add(new QuizEntry("Wer handelt an der Wall Street?","Booker", "Broocher", "Broker", "Stoker", 2));
        entries.add(new QuizEntry("Welche Farbe wird von den Gasen der Erdatmosphäre am stärksten gestreut?","Blau", "Grün", "Rot", "Gelb", 0));
        entries.add(new QuizEntry("Welcher Filmemacher gehörte zur Nouvelle Vague?","Luc Besson", "Christophe Gans", "Francois Truffaut", "Michel Ocelot", 2));
        entries.add(new QuizEntry("Welche Erkrankung führt zur Entzündung und Schwellung des Gehirns?","Enzephalitis", "Hepatitis B", "Chickenpox", "Gonorrhoe", 0));
        entries.add(new QuizEntry("Was ist ein Schlangenträger?","Nordische Gottheit", "Säugetier", "Transportschiff", "Sternbild", 3));
        entries.add(new QuizEntry("Welche Erkrankung tritt häufiger bei Frauen auf?","Depression", "Klumpfuß", "Prostatakrebs", "Autismus", 0));
        entries.add(new QuizEntry("Welcher Hafen ist für kleinere Boote und Yachten vorgeshen?","Cantina", "Semolina", "Fontina", "Marina", 3));
        entries.add(new QuizEntry("Welches Tier hat asymetrische Ohren um besser Hören zu können?","Hase", "Känguru", "Bär", "Eule", 3));
        entries.add(new QuizEntry("Welche Sportart war erstmals seit 1920 wieder in München 1972 olympische Disziplin?","Segeln","Bogenschießen","Tennis","Tanzen",1));
        entries.add(new QuizEntry("In welchem Jahr rief der Papst zum ersten Kreuzzug auf ?","50 v. Chr.","1095 n. Chr.","886 n. Chr.","1408 n. Chr.",1));
        entries.add(new QuizEntry("Aus welcher Sprache stammt Safari?","Hindi","Mongolisch","Zulu","Arabisch",3));
        entries.add(new QuizEntry("Wie viel Prozent liegt die Lichtgeschwindigkeit in der Luft unter der Vakuumlichtgeschwindigkeit?","10%","0,3%","1,3%","0,03%",3));
        entries.add(new QuizEntry("Wann begann in Frankreich das Erste Kaiserreich?","1815","1804","1796","1799",1));
        entries.add(new QuizEntry("Wer schrieb `Generation X`?","Anne Rice","Stephen King","John Grisham","Douglas Coupland",3));
        entries.add(new QuizEntry("Welchem Verteidigungsbündnis gehört Deutschland an?","OSCE","NATO","Warschauer Pakt","EU",1));
        entries.add(new QuizEntry("Wer schuf die Benzoltheorie mit dem berühmten Benzolring?","Pauling","Liebig","Kekule","Pasteur",2));
        entries.add(new QuizEntry("Wie nennt man Wasser speichernde trockenresistente Pflanzen?","Destruenten","Eukaryonten","Transpiranten","Sukkulenten",3));
        entries.add(new QuizEntry("Was gibt man in Lumen an?","Lichtstrom","Elektrische Feldstärke","Lichtstärke","Beleuchtungsstärke",0));
        entries.add(new QuizEntry("Wie viele Argentinier sind Katholiken?","ca. 90 %","ca. 10 %","ca. 50 %","ca. 2 %",0));
        entries.add(new QuizEntry("Wer war Vorsitzender des Organisationskomitees der Fußball-WM 2006?","Franz Beckenbauer","Paul Breitner","Uli Hoeneß","Günter Netzer",0));
        entries.add(new QuizEntry("In welches Land reiste Erich Honecker im Jahre 1993 aus?","Russland","Argentinien","Kuba","Chile",3));
        entries.add(new QuizEntry("Wann meldete der Amerikaner Donald M. Mitchell das Mobiltelefon zum Patent an?","1940","1948","1942","1950",2));
        entries.add(new QuizEntry("Welche Firma brachte als erste einen Walkman heraus?","Canon","Sony","Xerox","Philips",1));
        entries.add(new QuizEntry("Wie heißt die Hauptstadt von Sri Lanka?","Mumbai","Goa","Colombo","Dehli",2));
        entries.add(new QuizEntry("1000 Kilogramm nennt man ...","einen Zentner","einen Pfund","einen Barrel","eine Tonne",3));
        entries.add(new QuizEntry("Qibla (Kibla) bezeichnet die Gebetsrichtung nach","Mekka","Osten","Medina","Jerusalem",0));
        entries.add(new QuizEntry("In welchem Land regierte König Baudouin?","Niederlande","Belgien","Schweden","Dänemark",1));
        entries.add(new QuizEntry("Wann erzielte erstmals bei einem Fußball-Länderspiel ein Torwart einen Treffer?","1872","1882","1892","1902",1));
        entries.add(new QuizEntry("Teilt man Arbeit durch Zeit. ergibt sich die ...","Flussdichte","Geschwindigkeit","Leistung","Energie",2));
        entries.add(new QuizEntry("Für welchen Film erhielt Hattie McDaniel einen Oscar?","Casablanca","Glory","West Side Story","Vom Winde verweht",3));
        entries.add(new QuizEntry("Wie heißt das nördliche große Nachbarland Nigerias?","Niger","Benin","Tschad","Togo",0));
        entries.add(new QuizEntry("Wie viele Bilder braucht man für eine Sekunde Zeichentrickfilm?","18","24","36","20",1));
        entries.add(new QuizEntry("Welche Stadt ist die Hauptstadt von Schweden?","Oslo","Stockholm","Helsinki","Kopenhagen",1));
        entries.add(new QuizEntry("Bei wie viel Grad Fahrenheit gefriert Wasser?","32","100","212","-32",0));
        entries.add(new QuizEntry("Titograd heißt heute ...","Rijeka","Wolgograd","Podgorica","Srebrenica",2));
        entries.add(new QuizEntry("Wer stellte 1867 das Massewirkungsgesetz auf?","Guldberg und Waage","Einstein","Compton","Mach",0));
        entries.add(new QuizEntry("Wie heißt ein Nadelbaum?","Lerche","Lärche","Ulme","Eberesche",1));
        entries.add(new QuizEntry("Wie wird der Dompfaff auch genannt?","Meise","Grünschnabel","Gimpel","Rotkehlchen",2));
        entries.add(new QuizEntry("Die veraltete Bezeichnung für Telegramm ist?","Droschke","Kartusche","Depesche","Retusche",2));
        entries.add(new QuizEntry("Welcher Fluss fließt durch den Grand Canyon?","Mississippi","Colorado","Sacramento","Missouri",1));
        entries.add(new QuizEntry("Wo entstand der Vertrag , der die Beitrittskriterien zur Währungsunion für EU Staaten regelt?","Wien","Madrid","Maastricht","Brüssel",2));
        entries.add(new QuizEntry("Was ist dreidimensional?","Kugel","Strecke","Punkt","Kreis",0));
        entries.add(new QuizEntry("In welchem Land wurde 1968 Celine Dion geboren?","Frankreich","England","Kanada","Belgien",2));
        entries.add(new QuizEntry("Wie heißt das größte Meer der Erde?","Pazifik","Indischer Ozean","Karibisches Meer","Atlantik",0));
        entries.add(new QuizEntry("Zu welchem Land gehört Sansibar?","Kenia","Großbritannien","Tansania","Frankreich",2));
        entries.add(new QuizEntry("Was ist keine deutsche Nachrichtenagentur?","sid","dpa","afp","ddp",2));
        entries.add(new QuizEntry("Was ist kein Brettspiel?","Dame","Halma","Kniffel","Mühle",2));
        entries.add(new QuizEntry("Welche beiden Farben hat San Marinos Flagge?","Rot und Grün","Weiß und Blau","Gelb und Weiß","Rot und Weiß",1));
        entries.add(new QuizEntry("Wo steht die größte Universitätsklinik Europas?","München","Berlin","Madrid","London",1));
        entries.add(new QuizEntry("Addiert man zwei Vektoren mit gleichem Betrag, aber entgegengesetzter Richtung, entsteht ein(e) ....","Nullstelle","Kreis","Nullvektor","Sektor",2));
        entries.add(new QuizEntry("In welchem Land liegen Braga und Coimbra?","Portugal","Spanien","Italien","Frankreich",0));
        entries.add(new QuizEntry("Wie heißt Albaniens Währung?","Lek","Lew","Leu","Lar",0));
        entries.add(new QuizEntry("Welche Stadt liegt nicht in Niedersachsen?","Gifhorn","Wolfenbüttel","Hildesheim","Kleve",3));
        entries.add(new QuizEntry("Welcher Käse ist nicht nach einem Ort benannt?","Gouda","Camembert","Gorgonzola","Feta",3));
        entries.add(new QuizEntry("Welche Republik startete in den 90ern einen Aufstand gegen Russland?","Tschuwaschien","Murmansk","Tschetschenien","Karelien",2));
        entries.add(new QuizEntry("Was ist der Name des Handelsabkommens zwischen USA, Kanada und Mexiko?","TTIP","CETA","CAFICOM","NAFTA",3));
        entries.add(new QuizEntry("Welche Bühnenfigur kommt in der Pantomime vor?","Zumurrud","Habakkuk","Cassim","Pierrot",3));
        entries.add(new QuizEntry("Was ist eine lebensgefährliche Entzündungsreaktion auf eine Infektion?","Progerie","Silikose","SARS","Sepsis",3));
        entries.add(new QuizEntry("Welche geschützte Blume filt als Symbol für die Alpen?","Edelweiß","Hahnenfuß","Ringelblume","Veilchen",0));
        entries.add(new QuizEntry("Wer erstellte die erste Version des Periodensystems?","Otto Hahn","Dmitiri Mendelejew","Antoine Lavosier","Dudley R. Herschbach",1));
        entries.add(new QuizEntry("Wie viele Kondome werden in Deutschland pro Minute verbraucht?","1022","9051","342","79",2));
        entries.add(new QuizEntry("Wie viele Kondome werden in Deutschland pro Minute verbraucht?","1022","9051","342","79",2));
        entries.add(new QuizEntry("Was verwenden Radaranlagen, um fliegende Objekte zu detektieren?","Röntgenstrahlen","660nm Licht","Radiowellen","Infrarotes Licht",2));
        entries.add(new QuizEntry("Welches Tier gehört zu den Crustacea?","Heuschrecke","Garnele","Ameisenbär","Zecke",1));
        entries.add(new QuizEntry("Welche Fische können bei Gefahr ihren Körper mit Wasser aufpumpen?","Kaluga-Hausen","Grundeln","Papageifische","Kugelfische",3));
        entries.add(new QuizEntry("Welcher Fluss fließt nicht in Frankreich?","Maas","Ebro","Mosel","Loire",1));
        entries.add(new QuizEntry("Welcher Schadstoff wird beim Schweißen von Stahl freigesetzt?","Propan","Bromchlorid","Zinkoxid","Diboran",2));
        entries.add(new QuizEntry("Was wird am 17. März gefeiert?","Weltfrauentag","Hanukkah","St. Patricks Day","Neujahr",2));
        entries.add(new QuizEntry("Die Hauptwörter welcher Sprache haben KEIN Geschlecht?","Russisch","Hindi","Deutsch","Chinesisch",3));
        entries.add(new QuizEntry("Was verwendeten die alten Ägypter als Schrift?","Braille","Aramäisch","Zapotek","Hieroglyphen",3));
        entries.add(new QuizEntry("Welches Mineralölunternehmen stammt aus Frankreich?","BP","Sinopec","Eni","Total",3));
        entries.add(new QuizEntry("Wie nennt man eine abrupte Grenze zwischen Luftmassen?","Flocke","Halm","Front","Kante",2));
        entries.add(new QuizEntry("Wie nennt man einen islamischen Rechtsbegutachter?","Mufti","Hafiz","Hakim","Ashraf",0));
        entries.add(new QuizEntry("Welche Region grenzt an Nordrhein-Westfalen?","Limburg","Hennegau","Limousin","Thüringen",0));
        entries.add(new QuizEntry("Wie nennt man die Larven der Blatthornkäfer?","Engerlinge","Puppen","Raupen","Bärenspinner",0));
        entries.add(new QuizEntry("Wie nannte man das Heer der Sowjetunion?","Comecon","Gubernija","Rote Arme","UDSSR",2));
        entries.add(new QuizEntry("Welches ist ein russischer Tanz?","Hakken","Kasatschok","Flamenco","Walzer",1));
        entries.add(new QuizEntry("Was ist der Übergang von Gasförmig zu flüssig?","Sieden","Gefrieren","Schmelzen","Kondensieren",3));
        entries.add(new QuizEntry("Für welche Fähigkeit sind Tauben bekannt?","Lebendgeburt","Tauchverhalten","Heimkehr","Giftabwehr",2));
        entries.add(new QuizEntry("Welche Konstante entspricht ~2,718?","c","e","?","G",1));
        entries.add(new QuizEntry("Casablanca ist eine Stadt in ..?","Algerien","Marokko","Belize","Portugal",1));
        entries.add(new QuizEntry("Was dient dem Zahnarzt als Füllmaterial für die Zähne?","Amalgan","Bronze","Lötzinn","Wolfram",0));
        entries.add(new QuizEntry("Joule ist die Einheit von ...?","Temperatur","Drehmoment","Energie","Spannung",2));
        entries.add(new QuizEntry("Welches Land produziert jedes Jahr am meisten Kohle?","Südafrika","USA","China","Indien",2));
        entries.add(new QuizEntry("Wie nennt man einen heiligen tantrischen Spruch?","Kalyptra","Mantra","Mandela","Kabbala",1));
        entries.add(new QuizEntry("Was ist die Amtssprache in Brasilien?","Französisch","Portugiesisch","Englisch","Spanisch",1));
        entries.add(new QuizEntry("Wo steht die Stammburg der Hohenzollern?","Sachsen","Brandenburg","Bayern","Württenberg",3));
        entries.add(new QuizEntry("Zu welcher Familie gehört der Wolf?","Felidae","Equidae","Lemuridae","Canidae",3));
        entries.add(new QuizEntry("Welche Organismen sind immun gegen Viren?","Pilze","Keine von diesen","Pflanzen","Bakterien",1));
        entries.add(new QuizEntry("Welches ist eine Funktion der Milz?","Fettzellenproduktion","Sauerstoffspeicher","Blutzellenabbau","Hormonaustoß",2));
        entries.add(new QuizEntry("Wie viele Zentimeter sind ein Meter?","1.000m","100cm","10.000cm","10cm",1));
        entries.add(new QuizEntry("Eine starke Säure hat einen pH-Wert in der Nähe von..?","1","7","21","14",0));
        entries.add(new QuizEntry("Welches ist ein Säugetier?","Zaunkönig","Rochen","Gelbbauchunke","Feldhase",3));
        entries.add(new QuizEntry("Welcher von diesen Körperteilen wächst am schnellsten?","Kopfhaar","Fingernagel","Schneidezahn","Zehennagel",0));
        entries.add(new QuizEntry("Was wird zu Beton hinzugefügt, um das Erhärten zu beschleunigen?","Natriumhydrid","Calciumnitrat","Chlorsäure","Zinksulfat",1));
        entries.add(new QuizEntry("Was ist der Name eines Strohhuts sowie einer Wasserstraße?","St. Lawrence","Panama","Suez","Boater",1));
        entries.add(new QuizEntry("Männliche Säugetiere ejakulieren Samen durch ...?","Henle-Schleife","die Harnröhre","den Coccyx","den Fallopio",1));
        entries.add(new QuizEntry("Was wurde von Jacques Cousteau erkundet?","Weltraum","Ozeane","Regenwald","Troja",1));
        entries.add(new QuizEntry("Welche dieser Firmen wurde zuerst gegründet?","Intel","Google","Dell","IBM",3));
        entries.add(new QuizEntry("Welches Bauwerk verhindert Überflutungen?","Deich","Moor","Kurk","Vangard",0));
        entries.add(new QuizEntry("Was ist 'Umami'?","Stadt","Reiswein","Fisch","Geschmack",3));
        entries.add(new QuizEntry("Was wirkt als Gegengift bei einer Methanolvergiftung?","Paraffin","Dextrose","Bienenwachs","Ethanol",3));
        entries.add(new QuizEntry("Welches dieser Unternehmen bietet nicht Finanzdienstleistungen an?","Linde","Citigroup","KfW","BNP Paribas",0));
        entries.add(new QuizEntry("Was ist die größte russische Fluggesellschaft?","Aeroflot","Rossija","Donavia","Transneft",0));
        entries.add(new QuizEntry("Womit werden die meisten Lastwagen angetrieben?","Dampf","Uran","Benzin","Diesel",3));
        entries.add(new QuizEntry("Was ist ein Teil des endokrinen Systems?","Lymphozyten","Muskeln","Drüsen","Sehnen",2));
        entries.add(new QuizEntry("Welche dieser Städte liegt an der Küste?","Tours","Marseille","Rennes","Reims",1));
        entries.add(new QuizEntry("Welche Kunstrichtung wurde durch Jackson Pollock bekannt?","Action Painting","Momo Paint","Wet-On-Wet","Paint by Number",0));
        entries.add(new QuizEntry("Wofür wird ein Radar verwendet?","Flussmessung","Ortung","Schallverstärkung","Dichtemessung",1));
        entries.add(new QuizEntry("Was ist das hawaiianische Wort für Liebe, Freundschaft und Mitgefühl?","Holoholo","Moana","Aloha","Kahuna",2));
        entries.add(new QuizEntry("Kalabrien und Ligurien sind Regionen in ..?","Italien","Spanien","Frankreich","Portugal",3));
        entries.add(new QuizEntry("Welcher Fluss wird vom Hoover Damm gestaut?","Missouri","Colorado","Rio Grande","Yukon",1));
        entries.add(new QuizEntry("Auf welchem Prinzip basiert ein Fahrzeugkatalysator?","Grignard-Reaktion","Photosynthese","Methanisierung","Redoxreaktion",3));
        entries.add(new QuizEntry("Wie nennt man den Teil eines Experiments, der sich ändert?","Konstante","Axiom","Dogma","Variable",3));





        randomQ = new boolean[entries.size()];
        for(int i=0; i<randomQ.length; i++){ // Keine Frage war bereits dran
            randomQ[i]=false;
        }
        actualEntry = ThreadLocalRandom.current().nextInt(0, randomQ.length); // random erste Frage
        randomQ[actualEntry] = true; // erste Frage markieren (war schon dran)

        playerPic = (ImageView)findViewById(R.id.playerPic);
        ampel = (ImageView)findViewById(R.id.ampel);
        playerName = (TextView)findViewById(R.id.playerName);
        question = (TextView)findViewById(R.id.question);
        question2 = (TextView)findViewById(R.id.question2);
        question.setTypeface(KreideSchrift);
        question2.setTypeface(KreideSchrift);

        ans1 = (TextView)findViewById(R.id.answer1);
        //ans1.setTypeface(Bleistift);
        ans2 = (TextView)findViewById(R.id.answer2);
        //ans2.setTypeface(Bleistift);
        ans3 = (TextView)findViewById(R.id.answer3);
        //ans3.setTypeface(Bleistift);
        ans4 = (TextView)findViewById(R.id.answer4);
        //ans4.setTypeface(Bleistift);


        playerPic.setImageResource(players.get(0).getColor());
        if(players.get(0).bild!=null){
            playerPic.setImageBitmap(players.get(0).bild);
            playerPic.setForeground(null);
        }else{
            playerPic.setForeground(players.get(actualPlayer).playerPic);
        }
        playerName.setText(players.get(0).getName());
        question2.setText(entries.get(actualEntry).getQuestion());
        ans1.setText(entries.get(actualEntry).getAns1());
        ans2.setText(entries.get(actualEntry).getAns2());
        ans3.setText(entries.get(actualEntry).getAns3());
        ans4.setText(entries.get(actualEntry).getAns4());

        question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(Getrunken==true) {
                        if (status == 1) {
                            if(randomAktion){
                                randomAktion=false;
                                zufallsaktion();
                            }
                            efv.flipTheView();
                            status = 0;
                            newQuestion();
                        }
                    }
            }
        });

        question2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Getrunken==true) {
                    if (status == 1) {
                        if(randomAktion){
                            randomAktion=false;
                            zufallsaktion();
                        }
                        efv.flipTheView();
                        status = 0;
                        newQuestion();
                    }
                }
            }
        });

        ans1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status ==0){checkAnswer(0);}
            }
        });
        ans2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status ==0){checkAnswer(1);}
            }
        });
        ans3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(status ==0){checkAnswer(2);}
            }
        });
        ans4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status ==0){checkAnswer(3);}
            }
        });

        answers = new TextView[4];
        answers[0] = ans1;
        answers[1] = ans2;
        answers[2] = ans3;
        answers[3] = ans4;
    }

    public void checkAnswer(int antwort){
        if(randomAktion){

        }else{
            timeStopper=true;
            if(antwort == entries.get(actualEntry).getCorrectAns()){
                answers[antwort].setBackground(getResources().getDrawable(R.drawable.paper_right));
                MediaPlayer quiz_right = MediaPlayer.create(this, R.raw.richtige_antwort);
                quiz_right.start();
                Toast.makeText(getApplicationContext(), "Auf Frage tippen zum Fortfahren", Toast.LENGTH_LONG).show();
                for(int i=0; i<listCount.size();i++){
                    if(listCount.get(i).player.id == players.get(actualPlayer).id){
                        listCount.get(i).count++;
                    }
                }
                updatePoiListView();
            }else{
                if(antwort==5){
                    answers[0].setBackground(getResources().getDrawable(R.drawable.paper_wrong));
                    answers[1].setBackground(getResources().getDrawable(R.drawable.paper_wrong));
                    answers[2].setBackground(getResources().getDrawable(R.drawable.paper_wrong));
                    answers[3].setBackground(getResources().getDrawable(R.drawable.paper_wrong));
                }else{
                    answers[antwort].setBackground(getResources().getDrawable(R.drawable.paper_wrong));
                }
                answers[entries.get(actualEntry).getCorrectAns()].setBackground(getResources().getDrawable(R.drawable.paper_right));
                MediaPlayer quiz_false = MediaPlayer.create(this, R.raw.falsche_antwort);
                quiz_false.start();
                for(int i=0; i<listCount.size();i++){
                    if(listCount.get(i).player.id == players.get(actualPlayer).id){
                        listCount.get(i).count2++;
                    }
                }
                updatePoiListView();
                if(!SystemState.isOffline()) {
                    Getrunken = false;
                    ampel.setImageResource(R.drawable.ampel_rot);
                }
                players.get(actualPlayer).oneShot();
                //Toast.makeText(getApplicationContext(), players.get(actualPlayer).getName()+ " Trinkt!", Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), "Auf Frage tippen zum Fortfahren", Toast.LENGTH_LONG).show();
            }
        }
        status =1;
    }

    public void zufallsaktion(){
        if(zufall==0){
            Player looser = duemmster();
            looser.oneShot();
            if(!SystemState.isOffline()) {
                Getrunken = false;
                ampel.setImageResource(R.drawable.ampel_rot);
            }
        }else if(zufall==1){
            Player looser = schlauster();
            looser.oneShot();
            if(!SystemState.isOffline()) {
                Getrunken = false;
                ampel.setImageResource(R.drawable.ampel_rot);
            }
        }else if(zufall==2){
            ArrayList<Player> loosers = new ArrayList<Player>();
            for(int i=0; i<players.size(); i++){
                if(players.get(i).morw.equals("m")){
                    loosers.add(players.get(i));
                }
            }
            if(loosers.size()>0){
                if(loosers.size()==1){
                    loosers.get(0).oneShot();
                }else{
                    MultiLight ml = new MultiLight();
                    ml.shots(loosers);
                   // MultiShot ms = new MultiShot();
                    //ms.shots(loosers);
                }
                if(!SystemState.isOffline()) {
                    Getrunken = false;
                    ampel.setImageResource(R.drawable.ampel_rot);
                }
            }
        }else if(zufall==3){
            ArrayList<Player> loosers = new ArrayList<Player>();
            for(int i=0; i<players.size(); i++){
                if(players.get(i).morw.equals("w")){
                    loosers.add(players.get(i));
                }
            }
            if(loosers.size()>0){
                if(loosers.size()==1){
                    loosers.get(0).oneShot();
                }else{
                    MultiLight ml = new MultiLight();
                    ml.shots(loosers);
                    //MultiShot ms = new MultiShot();
                    //ms.shots(loosers);
                }
                if(!SystemState.isOffline()) {
                    Getrunken = false;
                    ampel.setImageResource(R.drawable.ampel_rot);
                }
            }
        }else if(zufall==4){
            ArrayList<Player> loosers = new ArrayList<Player>();
            for(int i=0; i<players.size(); i++){
                loosers.add(players.get(i));
            }
            MultiLight ml = new MultiLight();
            ml.shots(loosers);
           // MultiShot ms = new MultiShot();
            //ms.shots(loosers);
            if(!SystemState.isOffline()) {
                Getrunken = false;
                ampel.setImageResource(R.drawable.ampel_rot);
            }
        }else if(zufall==5){
            ArrayList<Player> loosers = new ArrayList<Player>();
            int[] tmp2 = new int[3];
            PlayersWithCounts[] array = new PlayersWithCounts[listCount.size()];
            for(int i=0; i<array.length; i++){
                array[i] = listCount.get(i);
            }
            for(int i=0; i<12; i++){
                for(int j=0; j<array.length-1; j++){
                    PlayersWithCounts tmp = array[j];
                    if(tmp.count<array[j+1].count){
                        array[j]=array[j+1];
                        array[j+1]=tmp;
                    }
                }
            }
            for(int i=0; i<12; i++){
                for(int j=0; j<array.length-1; j++){
                    PlayersWithCounts tmp = array[j];
                    if(tmp.count2>array[j+1].count2){
                        array[j]=array[j+1];
                        array[j+1]=tmp;
                    }
                }
            }
            listCount.clear();
            for(int i=0; i<array.length; i++){
                listCount.add(array[i]);
            }
            for(int i=0; i<3;i++){
                tmp2[i]=listCount.get(i).player.hoehe;
                loosers.add(listCount.get(i).player);
            }
            loosers.get(0).hoehe=4000;
            loosers.get(1).hoehe=2800;
            loosers.get(2).hoehe=1600;
            MultiLight ml = new MultiLight();
            ml.shots(loosers);
            //MultiShot ms = new MultiShot();
            //ms.shots(loosers);
            if(!SystemState.isOffline()) {
                Getrunken = false;
                ampel.setImageResource(R.drawable.ampel_rot);
            }
            for(int i=0; i<3; i++){
                listCount.get(i).player.hoehe = tmp2[i];
            }
        }else if(zufall==6){
            ArrayList<Player> loosers = new ArrayList<Player>();
            int[] tmp2 = new int[3];
            PlayersWithCounts[] array = new PlayersWithCounts[listCount.size()];
            for(int i=0; i<array.length; i++){
                array[i] = listCount.get(i);
            }
            for(int i=0; i<12; i++){
                for(int j=0; j<array.length-1; j++){
                    PlayersWithCounts tmp = array[j];
                    if(tmp.count>array[j+1].count){
                        array[j]=array[j+1];
                        array[j+1]=tmp;
                    }
                }
            }
            for(int i=0; i<12; i++){
                for(int j=0; j<array.length-1; j++){
                    PlayersWithCounts tmp = array[j];
                    if(tmp.count2<array[j+1].count2){
                        array[j]=array[j+1];
                        array[j+1]=tmp;
                    }
                }
            }
            listCount.clear();
            for(int i=0; i<array.length; i++){
                listCount.add(array[i]);
            }
            for(int i=0; i<3;i++){
                tmp2[i]=listCount.get(i).player.hoehe;
                loosers.add(listCount.get(i).player);
            }
            loosers.get(0).hoehe=4000;
            loosers.get(1).hoehe=2800;
            loosers.get(2).hoehe=1600;
            MultiLight ml = new MultiLight();
            ml.shots(loosers);
            //MultiShot ms = new MultiShot();
            //ms.shots(loosers);
            if(!SystemState.isOffline()) {
                Getrunken = false;
                ampel.setImageResource(R.drawable.ampel_rot);
            }
            for(int i=0; i<3; i++){
                listCount.get(i).player.hoehe = tmp2[i];
            }
        }else if(zufall==7){
            PlayersWithCounts[] array = new PlayersWithCounts[listCount.size()];
            for(int i=0; i<array.length; i++){
                array[i] = listCount.get(i);
            }
            for(int i=0; i<12; i++){
                for(int j=0; j<array.length-1; j++){
                    PlayersWithCounts tmp = array[j];
                    if(tmp.count>array[j+1].count){
                        array[j]=array[j+1];
                        array[j+1]=tmp;
                    }
                }
            }
            for(int i=0; i<12; i++){
                for(int j=0; j<array.length-1; j++){
                    PlayersWithCounts tmp = array[j];
                    if(tmp.count2<array[j+1].count2){
                        array[j]=array[j+1];
                        array[j+1]=tmp;
                    }
                }
            }
            listCount.clear();
            for(int i=0; i<array.length; i++){
                listCount.add(array[i]);
            }
            if(players.size()%2==0){
                ArrayList<Player> loosers = new ArrayList<Player>();
                loosers.add(listCount.get((listCount.size()/2)-1).player);
                loosers.add(listCount.get((listCount.size()/2)).player);
                MultiLight ml = new MultiLight();
                ml.shots(loosers);
               // MultiShot ms = new MultiShot();
               // ms.shots(loosers);
            }else{
                listCount.get((listCount.size()-1)/2).player.oneShot();
            }
            if(!SystemState.isOffline()) {
                Getrunken = false;
                ampel.setImageResource(R.drawable.ampel_rot);
            }
        }else if(zufall==8){
            players.get(randomGen.nextInt(players.size())).oneShot();
            if(!SystemState.isOffline()) {
                Getrunken = false;
                ampel.setImageResource(R.drawable.ampel_rot);
            }
        }
    }

    public Player duemmster(){
        PlayersWithCounts[] array = new PlayersWithCounts[listCount.size()];
        for(int i=0; i<array.length; i++){
            array[i] = listCount.get(i);
        }
        for(int i=0; i<12; i++){
            for(int j=0; j<array.length-1; j++){
                PlayersWithCounts tmp = array[j];
                if(tmp.count>array[j+1].count){
                    array[j]=array[j+1];
                    array[j+1]=tmp;
                }
            }
        }
        for(int i=0; i<12; i++){
            for(int j=0; j<array.length-1; j++){
                PlayersWithCounts tmp = array[j];
                if(tmp.count2<array[j+1].count2){
                    array[j]=array[j+1];
                    array[j+1]=tmp;
                }
            }
        }
        listCount.clear();
        for(int i=0; i<array.length; i++){
            listCount.add(array[i]);
        }
        return listCount.get(0).player;
    }

    public Player schlauster(){
        PlayersWithCounts[] array = new PlayersWithCounts[listCount.size()];
        for(int i=0; i<array.length; i++){
            array[i] = listCount.get(i);
        }
        for(int i=0; i<12; i++){
            for(int j=0; j<array.length-1; j++){
                PlayersWithCounts tmp = array[j];
                if(tmp.count<array[j+1].count){
                    array[j]=array[j+1];
                    array[j+1]=tmp;
                }
            }
        }
        for(int i=0; i<12; i++){
            for(int j=0; j<array.length-1; j++){
                PlayersWithCounts tmp = array[j];
                if(tmp.count2>array[j+1].count2){
                    array[j]=array[j+1];
                    array[j+1]=tmp;
                }
            }
        }
        listCount.clear();
        for(int i=0; i<array.length; i++){
            listCount.add(array[i]);
        }
        return listCount.get(0).player;
    }

    public void newQuestion(){
        if(begrenzt && rounds == count){
            finish();
        }
        if(roundCounter%(players.size()*5)==0 && switchAktion.isChecked()){
            status=1;
            randomAktion = true;
            zufall = randomGen.nextInt(zwischenAktionen.length);
            if(players.size()<3 && zufall>8){
                zufall = randomGen.nextInt(9);
            }
            if(count %2 ==0){
                question.setText(zwischenAktionen[zufall]);
            }else{
                question2.setText(zwischenAktionen[zufall]);
            }
            count++;
        }else{
            if(actualPlayer<playerNumber-1){
                actualPlayer ++;
            }else{
                actualPlayer =0;
            }
            playerName.setText(players.get(actualPlayer).getName());
            playerPic.setImageResource(players.get(actualPlayer).getColor());
            if(players.get(actualPlayer).bild!=null){
                playerPic.setImageBitmap(players.get(actualPlayer).bild);
                playerPic.setForeground(null);
            }else{
                playerPic.setForeground(players.get(actualPlayer).playerPic);
            }
            boolean allQ = true; // Waren alle schon dran?
            for(int i=0; i<randomQ.length; i++){
                if(!randomQ[i]){
                    allQ = false; // Es gibt min. eine Frage, die noch nicht dran war
                }
            }
            if(!allQ){ // es gibt noch Fragen, die noch nicht dran waren
                while(randomQ[actualEntry]){ // wenn random gewählte Frage schon dran war, bestimme die nächste random
                    actualEntry = ThreadLocalRandom.current().nextInt(0, randomQ.length);
                }
                randomQ[actualEntry] = true; // neue Frage markieren
            }else{ // Alle Fragen waren schon dran, daher alle Markierungen aufheben
                for(int i=0; i<randomQ.length; i++){
                    randomQ[i]=false;
                }
                actualEntry = ThreadLocalRandom.current().nextInt(0, randomQ.length); // neue erste Frage
                randomQ[actualEntry]=true; // erste Frage markieren
            }
            if(count %2 ==0){
                question.setText(entries.get(actualEntry).getQuestion());
            }else{
                question2.setText(entries.get(actualEntry).getQuestion());
            }
            ans1.setText(entries.get(actualEntry).getAns1());
            ans2.setText(entries.get(actualEntry).getAns2());
            ans3.setText(entries.get(actualEntry).getAns3());
            ans4.setText(entries.get(actualEntry).getAns4());
            for(int i=0; i<4; i++){
                answers[i].setBackground(getResources().getDrawable(R.drawable.paper));
            }
            count++;
            if(switchTime.isChecked()){
                progressBar.setProgress(progressBar.getMax());
                timeStopper=false;
                timerClass tc = new timerClass();
                tc.start();
            }
        }
        roundCounter++;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bedienung.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Status = readMessage;
                    if(Status.contains("1")){
                        Getrunken = true;
                        ampel.setImageResource(R.drawable.ampel_gruen);
                        Status="";
                    }
                    break;
            }
        }
    };
    public void setupListview(){
        try {
            ArrayAdapter<PlayersWithCounts> adapter = new Quiz.PlayerListAdapter();
            lv = (ListView) findViewById(R.id.quizlv);
            lv.setClickable(false);
            lv.setAdapter(adapter);

        } catch (Exception e) {
        }
    }

    public void updatePoiListView() {
        PlayersWithCounts[] array = new PlayersWithCounts[listCount.size()];
        for(int i=0; i<array.length; i++){
            array[i] = listCount.get(i);
        }
        if(switchSort.isChecked()){
            for(int i=0; i<12; i++){
                for(int j=0; j<array.length-1; j++){
                    PlayersWithCounts tmp = array[j];
                    if(tmp.count<array[j+1].count){
                        array[j]=array[j+1];
                        array[j+1]=tmp;
                    }
                }
            }
            for(int i=0; i<12; i++){
                for(int j=0; j<array.length-1; j++){
                    PlayersWithCounts tmp = array[j];
                    if(tmp.count2>array[j+1].count2){
                        array[j]=array[j+1];
                        array[j+1]=tmp;
                    }
                }
            }
        }else{
            for(int i=0; i<12; i++){
                for(int j=0; j<array.length-1; j++){
                    PlayersWithCounts tmp = array[j];
                    if(tmp.count>array[j+1].count){
                        array[j]=array[j+1];
                        array[j+1]=tmp;
                    }
                }
            }
            for(int i=0; i<12; i++){
                for(int j=0; j<array.length-1; j++){
                    PlayersWithCounts tmp = array[j];
                    if(tmp.count2<array[j+1].count2){
                        array[j]=array[j+1];
                        array[j+1]=tmp;
                    }
                }
            }
        }
        listCount.clear();
        for(int i=0; i<array.length; i++){
            listCount.add(array[i]);
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((Quiz.PlayerListAdapter) lv.getAdapter()).notifyDataSetChanged();
            }
        });

    }

    private class PlayerListAdapter extends ArrayAdapter<PlayersWithCounts> {
        public PlayerListAdapter() {
            super(Quiz.this, R.layout.quizentry, listCount);


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.quizentry, parent, false);
            }

            //Find Item
            final PlayersWithCounts player = listCount.get(position);

            itemView.setClickable(false);
            TextView name = (TextView)itemView.findViewById(R.id.quizentryName);
            TextView right = (TextView)itemView.findViewById(R.id.quizRight);
            TextView wrong = (TextView)itemView.findViewById(R.id.quizWrong);

            name.setText(player.player.getName());
            right.setText(""+player.count);
            wrong.setText(""+player.count2);

            return itemView;
        }
    }

    private final Handler mHandler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what==0){
                progressBar.setProgress(progressBar.getProgress()-progressDiff);
            }else if(msg.what==1){
                checkAnswer(5);
            }
        }
    };

    public class timerClass extends Thread{
        public void run(){
            for(int i=0; i<120;i++){
                if(timeStopper){
                    break;
                }
                mHandler2.obtainMessage(0).sendToTarget();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if(!timeStopper){
                mHandler2.obtainMessage(1).sendToTarget();
            }
        }
    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }
}
