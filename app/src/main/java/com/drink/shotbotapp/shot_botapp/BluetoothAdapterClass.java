package com.drink.shotbotapp.shot_botapp;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;


/**
 * Created by Fabian on 12.01.2018.
 */

public class BluetoothAdapterClass {
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    static BluetoothSocket btSocket = null;
    private static boolean isBtConnected = false;
    private static ProgressDialog progress;
    static android.bluetooth.BluetoothAdapter myBluetooth = null;
    static String address_bt = null;
    private static Handler mHandler;

    private ConnectedThread mConnectedThread;

    public static TextView active =null;

    public static void setHandler(Handler handler){
        mHandler =handler;
    }

    public static void setActiveClass(TextView t){
        active =t;
    }

    public static void start(){
        try {
            myBluetooth = android.bluetooth.BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
            BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address_bt);//connects to the device's address and checks if it's available
            btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
            android.bluetooth.BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
            btSocket.connect();//start connection
            //mHandler.obtainMessage(StartSeite_Connected.MESSAGE_BT,"Connected").sendToTarget();
            SystemState.offline = false;
        } catch (IOException e) {
            e.printStackTrace();
            //mHandler.obtainMessage(StartSeite_Connected.MESSAGE_BT, "not Connected").sendToTarget();
        }
        Log.d("UUUUUUU",myUUID+"");

        ConnectBT c = new ConnectBT();
        c.execute();
        ConnectedThread ct = new ConnectedThread(btSocket);
        ct.start();
    }

    public static void setAddress_bt(String Adresse){
        address_bt = Adresse;
    }

    public static void WriteBT(String s) {
        if (btSocket!=null) {
            try{btSocket.getOutputStream().write(s.toString().getBytes());}
            catch (IOException e) {}}}



    public static class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute()
        {
            //progress = ProgressDialog.show(StartSeite_Connected., "Connecting...", "Please wait!!!");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try
            {
                if (btSocket == null || !isBtConnected)
                {/*
                    myBluetooth = android.bluetooth.BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address_bt);//connects to the device's address and checks if it's available
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    android.bluetooth.BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection*/
                }
            }
            catch (Exception e)
            {
                ConnectSuccess = false;//if the try failed, you can check the exception here
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess)
            {
                //finish();
            }
            else
            {
                isBtConnected = true;
            }
            //progress.dismiss();
        }
    }

    private static class ConnectedThread extends Thread {
        private static  BluetoothSocket mmSocket;
        private static  InputStream mmInStream;

        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = btSocket.getInputStream();
            } catch (IOException e) {
            }
            mmInStream = tmpIn;
        }

        public void run() {
            byte[] buffer = new byte[1024];
            int bytes;
            // Keep listening to the InputStream while connected
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);
                    mHandler.obtainMessage(Bedienung.MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
            }
        }
    }
}
