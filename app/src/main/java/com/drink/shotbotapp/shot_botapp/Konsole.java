package com.drink.shotbotapp.shot_botapp;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Konsole extends AppCompatActivity {

    Button senden;
    TextView log;
    TextView eingabe;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konsole);
        BluetoothAdapterClass.setHandler(mHandler);
        senden = (Button)findViewById(R.id.btn_konsole_send);
        log = (TextView)findViewById(R.id.konsole_log);
        eingabe = (TextView)findViewById(R.id.konsole_eingabe);

        senden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(eingabe.getText().length()>0){
                    log.setText(log.getText()+"\n"+">"+eingabe.getText());
                    String s = ""+eingabe.getText();
                    BluetoothAdapterClass.WriteBT(s);
                }
            }
        });
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            log.setText(log.getText()+"\n"+"<"+msg);
        }
    };
}
