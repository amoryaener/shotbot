package com.drink.shotbotapp.shot_botapp;

import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class INever extends AppCompatActivity {

    int count =0;
    boolean zwischenaktionaktiv = false;
    LinearLayout zwischenPopup;
    int[] zwischenaktionen;
    ImageView zwischenaktionpic;
    boolean einstellungenSichtbar = false;
    Switch switchZwischenaktionen;
    ImageView einstellungen;
    TextView iNeverText;
    TextView p1T;
    TextView p2T;
    TextView p3T;
    TextView p4T;
    TextView p5T;
    TextView p6T;
    TextView p7T;
    TextView p8T;
    TextView p9T;
    TextView p10T;
    ImageView p1P;
    ImageView p2P;
    ImageView p3P;
    ImageView p4P;
    ImageView p5P;
    ImageView p6P;
    ImageView p7P;
    ImageView p8P;
    ImageView p9P;
    ImageView p10P;
    ImageView[] pics;
    TextView[] names;
    ArrayList<Player> players;
    ArrayList<String> entries;
    boolean[] shots;
    LinearLayout p1l;
    LinearLayout p2l;
    LinearLayout p3l;
    LinearLayout p4l;
    LinearLayout p5l;
    LinearLayout p6l;
    LinearLayout p7l;
    LinearLayout p8l;
    LinearLayout p9l;
    LinearLayout p10l;
    LinearLayout[] layouts;
    int actualEntry;
    Random random;
    ImageView ampel;
    String Status;
    Boolean Getrunken = true;
    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inever);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        players = PlayersClass.getPlayerList();
        actualEntry=0;
        ampel = findViewById(R.id.ampel);
        BluetoothAdapterClass.setHandler(mHandler);
        zwischenPopup = findViewById(R.id.iNZwischenPopup);
        zwischenPopup.setVisibility(View.GONE);
        zwischenaktionen = new int[4];
        zwischenaktionen[0]=R.drawable.zwischenaktion8;
        zwischenaktionen[1]=R.drawable.zwischenaktion2;
        zwischenaktionen[2]=R.drawable.zwischenaktion5;
        zwischenaktionen[3]=R.drawable.zwischenaktion7;

        zwischenaktionpic = findViewById(R.id.iNZwischen);
        switchZwischenaktionen = findViewById(R.id.inever_switch1);
        switchZwischenaktionen.setVisibility(View.GONE);
        einstellungen = findViewById(R.id.inever_einstellungen);
        einstellungen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(einstellungenSichtbar){
                    einstellungenSichtbar=false;
                    switchZwischenaktionen.setVisibility(View.GONE);
                    switchZwischenaktionen.setClickable(false);
                    switchZwischenaktionen.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_hide));
                }else {
                    einstellungenSichtbar=true;
                    switchZwischenaktionen.setVisibility(View.VISIBLE);
                    switchZwischenaktionen.setClickable(true);
                    switchZwischenaktionen.setAlpha((float)0.8);
                    switchZwischenaktionen.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_show));
                }
            }
        });

        zwischenaktionpic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Getrunken){
                    zwischenPopup.setVisibility(View.GONE);
                    zwischenaktionaktiv=false;
                }
            }
        });

        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });

        entries = new ArrayList<String>();
        entries.add("Ich wurde noch nie beim Sex gestört");
        entries.add("Ich habe noch nie innerhalb der letzten Woche mit jemanden rumgemacht");
        entries.add("Ich war noch nie bei einer Party wo die Polizei kam");
        entries.add("Mir wurde noch nie Unterwäsche geklaut");
        entries.add("Ich habe noch nie innerhalb der letzten 7 Tage eine Zigarette geraucht");
        entries.add("Ich habe noch nie Shots aus dem Bauchnabel von jemanden getrunken");
        entries.add("Ich habe mir noch nie in der Öffentlichkeit zwischen die Beine gefasst");
        entries.add("Ich habe noch nie gedacht das es ekelig ist wenn zwei Männer sich öffentlich küssen");
        entries.add("Ich habe noch nie die Nachrichten meines Partners gelesen (Handy/Facebook/Email)");
        entries.add("Ich hatte noch nie Sex mit jemanden von der Arbeit");
        entries.add("Ich habe noch nie erzählt, ich wäre in einer Beziehung, so das ich nicht angebaggert werde");
        entries.add("Ich habe noch nie meine Brustwarze gekitzelt");
        entries.add("Ich habe noch nie Betrunken Auto gefahren");
        entries.add("Ich wollte momentan nie gerne einen festen Freund/Freundin haben");
        entries.add("Ich bin noch nie vor der Polizei geflohen");
        entries.add("Ich habe noch nie jemanden mit einem Zungenpiercing geküsst");
        entries.add("Ich war noch nie unter der Woche sehr betrunken");
        entries.add("Noch nie hat mir jemand gesagt das ich ein schlechter Küsser bin");
        entries.add("Ich habe noch nie gepinkelt während ich geduscht hab");
        entries.add("Ich habe noch nie Alkohol getrunken als ich noch nicht alt genug war");
        entries.add("Ich habe noch nie gedacht das Single sein besser ist als in einer Beziehung zu sein");
        entries.add("Ich habe noch nie in ein Schwimmbad gepinkelt");
        entries.add("Ich habe noch nie mit jemandem rumgemacht, der verheiratet war");
        entries.add("Ich habe noch nie Gras geraucht");
        entries.add("Ich habe noch nie Ecstasy probiert");
        entries.add("Ich war noch nie Stripper in einem Stripclub");
        entries.add("Ich hatte noch nie ein Blind Date");
        entries.add("Ich habe noch nie für jemandem gestrippt");
        entries.add("Ich habe noch nie meinen Beziehungspartner betrogen");
        entries.add("Ich bin noch nie auf dem Badezimmerboden eingeschlafen");
        entries.add("Ich habe noch nie gedacht das jemand in Polizeiuniform heiß aussieht");
        entries.add("Ich habe noch nie heimlich bei Freunden die Schränke durchsucht");
        entries.add("Ich habe noch nie in der Öffentlichkeit gefurzt");
        entries.add("Ich bin noch nie irgendwo aufgewacht und wusste nicht wo ich bin");
        entries.add("Ich habe noch nie zu einem Partner gesagt das er schlecht im Bett sei");
        entries.add("Ich hatte noch nie eine Alkoholvergiftung");
        entries.add("Ich habe noch nie vorgetäuscht das ich ein Kind habe");
        entries.add("Ich habe noch nie einen Orgasmus vorgetäuscht");
        entries.add("Ich habe noch nie eine Zigarette auf der falschen Seite angezündet");
        entries.add("Ich habe noch nie etwas Illegales getan");
        entries.add("Ich war noch nie für das Recht auf Abtreibung");
        entries.add("Ich war noch nie Nackt aus meiner Wohnung ausgeschlossen");
        entries.add("Ich habe noch nie gesagt, dass ich nie wieder Alkohol trinke");
        entries.add("Ich hatte noch nie mehr One Night Stands als feste Beziehungen");
        entries.add("Ich wurde noch nie bei einem Date verletzt");
        entries.add("Ich habe mich noch nie in der Öffentlichkeit übergeben");
        entries.add("Ich habe mich noch nie in dem Bett eines Freundes übergeben");
        entries.add("Ich war noch nie in den letzten sieben Tagen betrunken");
        entries.add("Ich war noch nie in diesem Monat betrunken");
        entries.add("Ich habe noch nie gedacht das es Sexy ist wenn zwei Mädchen sich küssen");
        entries.add("Ich habe noch nie jemanden vom gleichen Geschlecht geküsst");
        entries.add("Ich habe mich noch nie alleine Betrunken");
        entries.add("Ich habe noch nie gedacht das ich gut im Bett bin");
        entries.add("Ich habe noch nie vergessen das ich mit jemanden rumgemacht habe");
        entries.add("Ich habe mich noch nie an eine/n Barkeeper/in rangemacht");
        entries.add("Ich habe noch nie einen Porno geschaut");
        entries.add("Ich habe mich noch nie Selbstbefriedigt");
        entries.add("Ich habe noch nie einen feuchten Traum gehabt");
        entries.add("Ich hatte noch nie mit einer Person Sex, während ich in eine andere Person verliebt war");
        entries.add("Ich wurde noch nie von einem Partner betrogen");
        entries.add("Ich hatte noch nie ein Date und habe am selben Tag mit jemand anderem rumgemacht");
        entries.add("Ich hatte noch nie sexuelle Fantasien mit dickeren Frauen");
        entries.add("Ich hatte noch nie Sex im Dunkeln");
        entries.add("Ich habe mich noch nie unter den Achseln rasiert");
        entries.add("Ich hatte noch nie kein Tampon, als ich einen brauchte");
        entries.add("Ich wollte noch nie gerne eine Pistole besitzen");
        entries.add("Ich habe mir noch nie ein Buch über Sexpraktiken gekauft");
        entries.add("Ich habe noch nie etwas Illegales über die Grenze geschmuggelt");
        entries.add("Ich habe noch nie in einem Garten gekotzt");
        entries.add("Ich habe noch 'Ich liebe dich' gesagt, ohne es zu meinen");
        entries.add("Ich habe noch nie ein Kondom falsch herum aufgesetzt");
        entries.add("Ich war noch nie verlobt");
        entries.add("Ich habe noch nie etwas Unanständiges bei einer Kirchenveranstaltung gemacht");
        entries.add("Ich wollte noch nie Jemanden, wo ich wusste das ich keine Chance hab");
        entries.add("Ich hatte noch nie Angst, das mein Partner schwanger ist");
        entries.add("Ich habe noch nie jemanden beim Sex/Masturbieren erwischt");
        entries.add("Ich habe noch nie einen der Mitspieler nackt gesehen");
        entries.add("Ich habe zurzeit noch nie gedacht, dass Küssen betrügen ist");
        entries.add("Ich habe noch nie Alkohol von meinen Eltern geklaut");
        entries.add("Ich habe noch nie mit jemanden in diesem Zimmer rumgemacht");
        entries.add("Ich habe noch nie bei diesem Spiel etwas herausgefunden, was ich lieber nicht gewusst hätte");
        entries.add("Ich habe noch nie in einer Ausnüchterungszelle geschlafen");
        entries.add("Ich wurde noch nie innerhalb der letzten Woche angebaggert");
        entries.add("Ich saß noch nie nackt in einer Sauna");
        entries.add("Ich hatte noch nie einen Dirty Talk bei Whatsapp");
        entries.add("Ich wurde noch nie von der Polizei festgenommen");
        entries.add("Ich hatte noch nie einen Filmriss");
        entries.add("Ich war noch nie momentan angetrunken");
        entries.add("Ich musste während des Sex noch nie laut loslachen");
        entries.add("Ich hatte noch nie Sex auf einem Trampolin");
        entries.add("Ich habe noch nie mit jemanden rumgemacht, der kein Deutsch sprach");
        entries.add("Ich habe noch nie eine Dating App benutzt");
        entries.add("Ich habe meine Eltern noch nie beim Sex gehört");
        entries.add("Ich habe noch nie eine Beschwerde von einem Nachbar/Mitbewohner darüber bekommen,dass ich beim Sex zu laut bin");
        entries.add("Ich wurde noch nie von einer Biene gestochen");
        entries.add("Ich habe noch nie Alkohol von meinen Eltern geklaut");
        entries.add("Ich hatte noch nie Sex vor einem Publikum");
        entries.add("Ich hatte noch nie Sex mit einer Sekretärin");
        entries.add("Ich hatte noch nie Sex in einem Zimmer, in dem zur gleichen Zeit ein Tier war");
        entries.add("Ich wurde noch nie mit jemanden vom gleichen Geschlecht intim");
        entries.add("Ich hatte noch nie Sex mit jemandem, der über zehn Jahre älter als ich war");
        entries.add("Ich habe noch nie verkatert an einem Sporttunier teilgenommen");
        entries.add("Ich habe noch nie die Person links von mit betrunken erlebt");
        entries.add("Ich habe noch nie mit jemanden rumgemacht, der verheiratet war");
        entries.add("Ich hatte noch nie ein Blind Date");
        entries.add("Ich hatte noch nie Sex mit jemandem, dessen Namen ich nicht kannte");
        entries.add("Ich habe noch nie Tierfutter probiert");
        entries.add("Ich habe noch nie in den letzten sechs Monaten ein Kondom benutzt");
        entries.add("Ich habe noch nie einen Geldbeutel gefunden und das Geld behalten");
        entries.add("Ich war noch nie in der Klapse");
        entries.add("Ich habe noch nie Sex auf dem Schulklo gehabt");
        entries.add("Ich habe noch nie einen Mann auf den Mund geküsst");
        entries.add("Ich habe mir noch nie jemanden/etwas schön getrunken");
        entries.add("ich wurde noch nie von einem Hund gebissen");
        entries.add("Ich habe noch nie meine Finger bei einfachen Rechnungen zur Hilfe genommen");
        entries.add("Ich habe noch nie einem Fremden den Mittelfinger gezeigt");
        entries.add("Ich stand noch nie auf einen meiner Lehrer");
        entries.add("Ich hatte noch nie eine Affäre die über zwei Jahre ging");
        entries.add("Ich hatte noch nie betrunken Sex mit jemandem, mit dem ich nüchtern nicht geschlafen hätte");
        entries.add("Ich war noch nie Klettern");
        entries.add("Ich habe mir noch nie in der Öffentlichkeit zwischen die Beine gefasst");
        entries.add("Ich habe mich noch nie an einer Tablette verschluckt");
        entries.add("Ich war noch nie nacht aus meiner Wohnung/meinem Haus ausgeschlossen");
        entries.add("Ich hatte noch nie Sex mit einer Plastikpuppe");
        entries.add("Ich hatte noch nie Schwule/lesbische Gedanken gehabt");
        entries.add("Ich war noch nie blond");
        entries.add("Ich habe noch nie einen operativen Eingriff gehabt");
        entries.add("Ich habe noch nie gedacht, dass Sex ohne Liebe genauso gut ist, wie Sex mit Liebe");
        entries.add("Ich hatte noch nie Sex in einem Hochbett");
        entries.add("Ich war noch nie in einem Hubschrauber");
        entries.add("Ich wurde noch nie innerhalb der letzten woche angebaggert");
        entries.add("Ich habe noch nie die füße von jemand anderem geleckt");
        entries.add("Ich habe noch nie meinem Genitalbereich im Spiegel betrachtet");
        entries.add("Ich habe noch nie Flugangst gehabt");
        entries.add("Ich habe noch nie in ein Schwimmbad gepinkelt");
        entries.add("Ich hatte noch nie einen Orgasmus");
        entries.add("Ich hatte noch nie pinke Haare");
        entries.add("Ich hatte noch nie Sex beim ersten Date");
        entries.add("Ich hatte noch nie Sex auf einer Wiese/im Park");
        entries.add("Ich hatte noch nie mehr als vier feste Freunde/Freundinnen");
        entries.add("Ich habe dieses Spiel noch nie mit jemandem gespielt,auf den ich stand");
        entries.add("Ich habe noch nie Schokolade beim Sex mit einbezogen");
        entries.add("Ich habe noch nie mein Passwort für etwas vergessen");
        entries.add("Ich habe noch nie den Shotbot geliebt");
        entries.add("Ich habe noch nie eine Challenge bei Facebook mitgemacht");
        entries.add("Ich habe noch nie mit einem Lehrer rumgemacht");
        entries.add("Ich habe noch nie Sex mit meiner/meinem Ex nach der Beziehung gehabt");
        entries.add("Ich habe noch nie auf einem Tisch getanzt");
        entries.add("Ich habe mir noch jemanden schön getrunken");
        entries.add("Ich hatte noch nie einen Knutschfleck");
        entries.add("Ich habe noch nie Shisha geraucht");
        entries.add("Ich war noch nie drei Tage hintereinander betrunken");
        entries.add("Ich habe noch nie einem Mann auf dem Mund geküsst");
        entries.add("Ich dachte noch nie ich hätte den Partner fürs Leben gefunden");
        entries.add("Ich stand noch nie auf einen der Mitspieler");
        entries.add("Ich habe noch nie beim Sex gefurzt");
        entries.add("Ich habe noch nie die Person links von mir betrunken erlebt");
        entries.add("Ich bin noch nie absichtlich durch einen Test gefallen");
        entries.add("Ich hatte noch nie mehr als 3 feste Freunde/Freundinnen");
        entries.add("Ich besaß noch nie Sexspielzeug");
        entries.add("Ich habe noch nie meinen momentanen Job gehasst");
        entries.add("Ich habe noch nie Kuscheln dem Sex vorgezogen");
        entries.add("Ich habe noch nie vor einem guten Freund geweint");
        entries.add("Ich habe mich noch nie im Auto übergeben");
        entries.add("Ich hatte noch nie Sex in einer Tiefgarage");
        entries.add("Ich habe noch nie meinen Mitbewohner beim Sex gehört");
        entries.add("Ich habe noch nie Nackt gebadet");
        entries.add("Ich habe noch nie einen Fremden auf der Straße ausversehen gegrüßt");
        entries.add("Ich habe noch nie einen nackten Hintern geküsst");
        entries.add("Ich hatte noch nie Telefonsex");
        entries.add("Ich habe noch nie etwas geklaut");
        entries.add("Ich bin noch nie während dem Sex eingeschlafen");
        entries.add("Ich war noch nie in einem Hubschrauber");
        entries.add("Ich habe noch nie einen Lap Dance bekommen");
        entries.add("Ich habe noch nie aus einer Bierrutsche getrunken");
        entries.add("Ich habe noch nie Bong geraucht");
        entries.add("Ich habe noch nie per SMS/Whatsapp schluss gemacht");
        entries.add("Ich habe noch nie Nacktfotos von mir gemacht");
        entries.add("Ich habe noch nie einen Test von meinen Lehrern gestohlen");
        entries.add("Ich habe mir noch nie vorgestellt, wie einer von meinen Lehrern nackt aussieht");
        entries.add("Ich habe mich noch nie vor Anderen übergeben");
        entries.add("Ich habe noch nie vorgetäuscht krank zu sein");
        entries.add("Ich habe noch nie einen Drink spendiert bekommen");
        entries.add("Ich habe als Minderjähriger noch nie getrunken");
        entries.add("Ich habe noch nie wild gepinkelt");
        entries.add("Ich habe noch nie gesagt, ich würde nie mehr trinken");
        entries.add("Ich bin noch nie verkatert zur Arbeit/Schule gegangen");
        entries.add("Ich habe noch nie keine Unterwäsche drunter getragen");
        entries.add("Ich habe noch nie ein Ungenügend als Note bekommen");
        entries.add("Ich habe noch nie an Sex mit einem Mitspieler gedacht");
        entries.add("Ich habe noch nie einen Strafzettel bekommen");
        entries.add("Ich habe mich noch nie geprügelt");
        entries.add("Ich habe mich noch nie als älter/jünger ausgegeben");
        entries.add("Ich habe noch nie Kleidung gekauft, die teurer als 200€ war");
        entries.add("Ich habe noch nie Nacktfotos von mir gemacht");


        shots = new boolean[players.size()];
        for(int i=0; i<shots.length;i++){
            shots[i]=false;
        }
        Collections.shuffle(entries);
        names = new TextView[10];
        pics = new ImageView[10];
        layouts = new LinearLayout[10];
        p1l= (LinearLayout)findViewById(R.id.iNeverP1l);
        p2l= (LinearLayout)findViewById(R.id.iNeverP2l);
        p3l= (LinearLayout)findViewById(R.id.iNeverP3l);
        p4l= (LinearLayout)findViewById(R.id.iNeverP4l);
        p5l= (LinearLayout)findViewById(R.id.iNeverP5l);
        p6l= (LinearLayout)findViewById(R.id.iNeverP6l);
        p7l= (LinearLayout)findViewById(R.id.iNeverP7l);
        p8l= (LinearLayout)findViewById(R.id.iNeverP8l);
        p9l= (LinearLayout)findViewById(R.id.iNeverP9l);
        p10l= (LinearLayout)findViewById(R.id.iNeverP10l);
        iNeverText = (TextView)findViewById(R.id.iNeverText);
        p1T = (TextView)findViewById(R.id.iNeverP1n);
        p2T = (TextView)findViewById(R.id.iNeverP2t);
        p3T = (TextView)findViewById(R.id.iNeverP3t);
        p4T = (TextView)findViewById(R.id.iNeverP4t);
        p5T = (TextView)findViewById(R.id.iNeverP5t);
        p6T = (TextView)findViewById(R.id.iNeverP6t);
        p7T = (TextView)findViewById(R.id.iNeverP7t);
        p8T = (TextView)findViewById(R.id.iNeverP8t);
        p9T = (TextView)findViewById(R.id.iNeverP9t);
        p10T = (TextView)findViewById(R.id.iNeverP10t);
        p1P = (ImageView)findViewById(R.id.iNeverP1p);
        p2P = (ImageView)findViewById(R.id.iNeverP2p);
        p3P = (ImageView)findViewById(R.id.iNeverP3p);
        p4P = (ImageView)findViewById(R.id.iNeverP4p);
        p5P = (ImageView)findViewById(R.id.iNeverP5p);
        p6P = (ImageView)findViewById(R.id.iNeverP6p);
        p7P = (ImageView)findViewById(R.id.iNeverP7p);
        p8P = (ImageView)findViewById(R.id.iNeverP8p);
        p9P = (ImageView)findViewById(R.id.iNeverP9p);
        p10P = (ImageView)findViewById(R.id.iNeverP10p);
        names[0]=p1T;
        names[1]=p2T;
        names[2]=p3T;
        names[3]=p4T;
        names[4]=p5T;
        names[5]=p6T;
        names[6]=p7T;
        names[7]=p8T;
        names[8]=p9T;
        names[9]=p10T;
        pics[0]=p1P;
        pics[1]=p2P;
        pics[2]=p3P;
        pics[3]=p4P;
        pics[4]=p5P;
        pics[5]=p6P;
        pics[6]=p7P;
        pics[7]=p8P;
        pics[8]=p9P;
        pics[9]=p10P;
        layouts[0]=p1l;
        layouts[1]=p2l;
        layouts[2]=p3l;
        layouts[3]=p4l;
        layouts[4]=p5l;
        layouts[5]=p6l;
        layouts[6]=p7l;
        layouts[7]=p8l;
        layouts[8]=p9l;
        layouts[9]=p10l;
        for(int i=0; i<10; i++){
            names[i].setVisibility(View.INVISIBLE);
            pics[i].setVisibility(View.INVISIBLE);
        }
        for(int i=0; i<players.size(); i++){
            names[i].setText(players.get(i).getName());
            pics[i].setImageResource(players.get(i).getColor());
            if(players.get(i).bild!=null){
                pics[i].setImageBitmap(players.get(i).bild);
                pics[i].setForeground(null);
            }else{
                pics[i].setForeground(players.get(i).playerPic);
            }
            names[i].setVisibility(View.VISIBLE);
            pics[i].setVisibility(View.VISIBLE);
        }
        for(int i=0; i<players.size(); i++){
            final int a =i;
            pics[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!shots[a]){
                        shots[a]=true;
                        layouts[a].setForeground(getResources().getDrawable(R.drawable.kreuz_transparent));
                    }else{
                        shots[a]=false;
                        layouts[a].setForeground(null);
                    }
                }
            });
        }
        iNeverText.setText(entries.get(actualEntry));
        iNeverText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shottext ="";
                boolean minEinPlayer = false;
                for(int i=0; i<players.size(); i++){
                    if(shots[i]){
                        minEinPlayer = true;
                        shottext = shottext+"T"+players.get(i).id+"#";
                        players.get(i).shotCountPlus();
                    }
                }
                shottext = shottext+"TB";
                if(minEinPlayer){
                    BluetoothAdapterClass.WriteBT(shottext);
                    if(!SystemState.isOffline()) {
                        Getrunken = false;
                        ampel.setImageResource(R.drawable.ampel_rot);
                    }
                }
                reset();
            }
        });
    }

    public void zwischenaktion(int random){
        if(random==2){ // Männer
            ArrayList<Player> loosers = new ArrayList<Player>();
            for(int i=0; i<players.size(); i++){
                if(players.get(i).morw.equals("m")){
                    loosers.add(players.get(i));
                }
            }
            if(loosers.size()>0){
                if(loosers.size()==1){
                    loosers.get(0).oneShot();
                }else{
                    MultiLight ml = new MultiLight();
                    ml.shots(loosers);
                    //MultiShot ms = new MultiShot();
                    //ms.shots(loosers);
                }
                if(!SystemState.isOffline()) {
                    Getrunken = false;
                    ampel.setImageResource(R.drawable.ampel_rot);
                }
            }
        }else if(random==3){ // Frauen
            ArrayList<Player> loosers = new ArrayList<Player>();
            for(int i=0; i<players.size(); i++){
                if(players.get(i).morw.equals("w")){
                    loosers.add(players.get(i));
                }
            }
            if(loosers.size()>0){
                if(loosers.size()==1){
                    loosers.get(0).oneShot();
                }else{
                    MultiLight ml = new MultiLight();
                    ml.shots(loosers);
                    //MultiShot ms = new MultiShot();
                    //ms.shots(loosers);
                }
                if(!SystemState.isOffline()) {
                    Getrunken = false;
                    ampel.setImageResource(R.drawable.ampel_rot);
                }
            }
        }else if(random==0){ // Alle
            ArrayList<Player> loosers = new ArrayList<Player>();
            for(int i=0; i<players.size(); i++){
                loosers.add(players.get(i));
            }
            MultiLight ml = new MultiLight();
            ml.shots(loosers);
            //MultiShot ms = new MultiShot();
            //ms.shots(loosers);
            if(!SystemState.isOffline()) {
                Getrunken = false;
                ampel.setImageResource(R.drawable.ampel_rot);
            }
        }else if(random==1){ // Zufall
            Random ran = new Random();
            players.get(ran.nextInt(players.size())).oneShot();
            if(!SystemState.isOffline()) {
                Getrunken = false;
                ampel.setImageResource(R.drawable.ampel_rot);
            }
        }
    }

    public void reset(){
        if(switchZwischenaktionen.isChecked() && count%5==0){
            Random rand = new Random();
            int randint = rand.nextInt(4);
            zwischenaktionpic.setImageResource(zwischenaktionen[randint]);
            if(!einstellungenSichtbar){
                switchZwischenaktionen.setAlpha(0);
            }
            zwischenPopup.setVisibility(View.VISIBLE);
            zwischenaktionaktiv=true;
            zwischenaktion(randint);
        }
        for(int i=0; i<players.size(); i++){
            shots[i]=false;
            layouts[i].setForeground(null);
        }
        if(actualEntry<entries.size()-1){
            actualEntry++;
        }else{
            actualEntry=0;
        }
        iNeverText.setText(entries.get(actualEntry));
        count++;
    }
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bedienung.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Status = readMessage;
                    if(Status.contains("1")){
                        Getrunken = true;
                        ampel.setImageResource(R.drawable.ampel_gruen);
                        Status="";
                    }
                    break;
            }
        }
    };
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }
}
