package com.drink.shotbotapp.shot_botapp;

import java.util.Random;

/**
 * Created by Alexander on 02.02.2018.
 */

public class CardDeck {
    Card[] deck;
    Random randomGen = new Random();

    public CardDeck(){
        this.deck = new Card[32];
        this.deck[0]= new Card(7,"Herz","@drawable/cardh7", R.drawable.cardh7);
        this.deck[1]= new Card(8,"Herz","@drawable/cardh8", R.drawable.cardh8);
        this.deck[2]= new Card(9,"Herz","@drawable/cardh9", R.drawable.cardh9);
        this.deck[3]= new Card(10,"Herz","@drawable/cardh10", R.drawable.cardh10);
        this.deck[4]= new Card(11,"Herz","@drawable/cardhb", R.drawable.cardhb);
        this.deck[5]= new Card(12,"Herz","@drawable/cardhd", R.drawable.cardhd);
        this.deck[6]= new Card(13,"Herz","@drawable/cardhk", R.drawable.cardhk);
        this.deck[7]= new Card(14,"Herz","@drawable/cardha", R.drawable.cardha);
        this.deck[8]= new Card(7,"Karo","@drawable/cardc7", R.drawable.cardc7);
        this.deck[9]= new Card(8,"Karo","@drawable/cardc8", R.drawable.cardc8);
        this.deck[10]= new Card(9,"Karo","@drawable/cardc9", R.drawable.cardc9);
        this.deck[11]= new Card(10,"Karo","@drawable/cardc10", R.drawable.cardc10);
        this.deck[12]= new Card(11,"Karo","@drawable/cardcb", R.drawable.cardcb);
        this.deck[13]= new Card(12,"Karo","@drawable/cardcd", R.drawable.cardcd);
        this.deck[14]= new Card(13,"Karo","@drawable/cardck", R.drawable.cardck);
        this.deck[15]= new Card(14,"Karo","@drawable/cardca", R.drawable.cardca);
        this.deck[16]= new Card(7,"Kreuz","@drawable/cardk7", R.drawable.cardk7);
        this.deck[17]= new Card(8,"Kreuz","@drawable/cardk8", R.drawable.cardk8);
        this.deck[18]= new Card(9,"Kreuz","@drawable/cardk9", R.drawable.cardk9);
        this.deck[19]= new Card(10,"Kreuz","@drawable/cardk10", R.drawable.cardk10);
        this.deck[20]= new Card(11,"Kreuz","@drawable/cardkb", R.drawable.cardkb);
        this.deck[21]= new Card(12,"Kreuz","@drawable/cardkd", R.drawable.cardkd);
        this.deck[22]= new Card(13,"Kreuz","@drawable/cardkk", R.drawable.cardkk);
        this.deck[23]= new Card(14,"Kreuz","@drawable/cardka", R.drawable.cardka);
        this.deck[24]= new Card(7,"Pik","@drawable/cardp7", R.drawable.cardp7);
        this.deck[25]= new Card(8,"Pik","@drawable/cardp8", R.drawable.cardp8);
        this.deck[26]= new Card(9,"Pik","@drawable/cardp9", R.drawable.cardp9);
        this.deck[27]= new Card(10,"Pik","@drawable/cardp10", R.drawable.cardp10);
        this.deck[28]= new Card(11,"Pik","@drawable/cardpb", R.drawable.cardpb);
        this.deck[29]= new Card(12,"Pik","@drawable/cardpd", R.drawable.cardpd);
        this.deck[30]= new Card(13,"Pik","@drawable/cardpk", R.drawable.cardpk);
        this.deck[31]= new Card(14,"Pik","@drawable/cardpa", R.drawable.cardpa);
    }

    public void shuffle(){
        for(int i=0; i<80; i++){
            for(int j=0; j<32; j++){
                int rand = randomGen.nextInt(32);
                Card tmp = this.deck[rand];
                this.deck[rand] = this.deck[j];
                this.deck[j]= tmp;
            }
        }
    }

    public Card getCard(int i){
        return this.deck[i];
    }
}
