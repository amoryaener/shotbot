package com.drink.shotbotapp.shot_botapp;

import android.graphics.drawable.Drawable;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainMenu extends AppCompatActivity {

    Drawable[] playerPics;
    public static final String CHANNEL_1_ID = "channel1";
    public static final String CHANNEL_2_ID = "channel2";
    private NotificationManagerCompat notificationManager;
    ListView lv;
    PopupWindow mpopup;
    View popUpView;
    ImageView exit;
    Context context;
    LinearLayout info;
    LinearLayout einstellungen;
    LinearLayout licht;
    LinearLayout minispiele;
    LinearLayout stats;
    ImageView bluet;
    ImageView check;
    ImageView mitte;
    WebView webView;
    public static final int MESSAGE_BT = 2;
    int state=0;
    SharedPreferences sharedPrefs;
    TextView statsCountText;
    TextView statsDrunkName;
    TextView statsDrunkCount;
    TextView statsGame;
    ImageView statsGameImg;
    ImageView statsDrunkPic;
    ImageView statsRueck;
    TextView statsGame2;
    ImageView statsGameImg2;
    TextView statsGame3;
    ImageView statsGameImg3;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            BluetoothAdapterClass.setHandler(mHandler);
            if (getIntent().getBooleanExtra("bluetooth", false)) {
                BluetoothAdapterClass.start();
            }
            playerPics = new Drawable[19];
            playerPics[0] = getResources().getDrawable(R.drawable.playerpic1test);
            playerPics[1] = getResources().getDrawable(R.drawable.playerpic2test);
            playerPics[2] = getResources().getDrawable(R.drawable.playerpic3test);
            playerPics[3] = getResources().getDrawable(R.drawable.playerpic4test);
            playerPics[4] = getResources().getDrawable(R.drawable.playerpic5test);
            playerPics[5] = getResources().getDrawable(R.drawable.playerpic6test);
            playerPics[6] = getResources().getDrawable(R.drawable.playerpic7test);
            playerPics[7] = getResources().getDrawable(R.drawable.playerpic8test);
            playerPics[8] = getResources().getDrawable(R.drawable.playerpic9test);
            playerPics[9] = getResources().getDrawable(R.drawable.playerpic10test);
            playerPics[10] = getResources().getDrawable(R.drawable.playerpic11test);
            playerPics[11] = getResources().getDrawable(R.drawable.playerpic12test);
            playerPics[12] = getResources().getDrawable(R.drawable.playerpic13test);
            playerPics[13] = getResources().getDrawable(R.drawable.playerpic14test);
            playerPics[14] = getResources().getDrawable(R.drawable.playerpic15test);
            playerPics[15] = getResources().getDrawable(R.drawable.playerpic16test);
            playerPics[16] = getResources().getDrawable(R.drawable.playerpic17test);
            playerPics[17] = getResources().getDrawable(R.drawable.playerpic18test);
            playerPics[18] = getResources().getDrawable(R.drawable.playerpic19test);
            GlobalStats.context = this;
            sharedPrefs = getSharedPreferences("saveStats", 0);
            GlobalStats.shotCount = sharedPrefs.getInt("shotcount", 0);
            context = this;
            popUpView = getLayoutInflater().inflate(R.layout.mainstats,
                    null); // inflating popup layout
            mpopup = new PopupWindow(popUpView, ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
            exit = popUpView.findViewById(R.id.statsExit);
            exit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mpopup.dismiss();
                }
            });

            statsCountText = popUpView.findViewById(R.id.statsShotsGes);
            statsDrunkName = popUpView.findViewById(R.id.statsDrunkName);
            statsDrunkCount = popUpView.findViewById(R.id.statsDrunkCount);
            statsGame = popUpView.findViewById(R.id.statsBelText);
            statsGameImg = popUpView.findViewById(R.id.statsBelImg);
            statsGame2 = popUpView.findViewById(R.id.statsBelText2);
            statsGameImg2 = popUpView.findViewById(R.id.statsBelImg2);
            statsGame3 = popUpView.findViewById(R.id.statsBelText3);
            statsGameImg3 = popUpView.findViewById(R.id.statsBelImg3);
            statsDrunkPic = popUpView.findViewById(R.id.statsDrunkPic);
            statsRueck= popUpView.findViewById(R.id.statsRueck);

            statsRueck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        GlobalStats.shotCount=0;
                        SharedPreferences.Editor editor = context.getSharedPreferences("saveStats", 0).edit();
                        editor.putInt("playerShotsMax", 0);
                        editor.putString("playerShotsName", "");
                        editor.putInt("imgRes", 0);
                        editor.putInt("imgDrawable", 0);
                        editor.putInt("shotcount", 0);
                        editor.putString("belGame", "");
                        editor.putString("belGame2", "");
                        editor.putString("belGame3", "");
                        editor.putInt("Quiz",0);
                        editor.putInt("Wahr oder Falsch",0);
                        editor.putInt("Ring of Fire",0);
                        editor.putInt("Höher Tiefer",0);
                        editor.putInt("Bombe",0);
                        editor.putInt("Glücksrad",0);
                        editor.putInt("Balkensaufen",0);
                        editor.putInt("Meiern",0);
                        editor.putInt("Busfahrer",0);
                        editor.putInt("Memory",0);
                        editor.putInt("Hausnummer",0);
                        editor.commit();
                    statsCountText.setText("Shots gesamt: "+GlobalStats.shotCount);
                    statsDrunkCount.setText(""+sharedPrefs.getInt("playerShotsMax", 0)+" Shots");
                    statsDrunkName.setText(sharedPrefs.getString("playerShotsName", ""));
                    statsGame.setText(sharedPrefs.getString("belGame", "")+" ("+sharedPrefs.getInt(sharedPrefs.getString("belGame", ""), 0)+")");
                    statsGame2.setText(sharedPrefs.getString("belGame2", "")+" ("+sharedPrefs.getInt(sharedPrefs.getString("belGame2", ""), 0)+")");
                    statsGame3.setText(sharedPrefs.getString("belGame3", "")+" ("+sharedPrefs.getInt(sharedPrefs.getString("belGame3", ""), 0)+")");

                    statsDrunkPic.setImageResource(sharedPrefs.getInt("imgRes", 0));
                    statsDrunkPic.setForeground(playerPics[sharedPrefs.getInt("imgDrawable", 0)]);

                    if(sharedPrefs.getString("belGame", "").equals("Quiz")){
                        statsGameImg.setImageDrawable(getDrawable(R.drawable.quiz));
                    }else if(sharedPrefs.getString("belGame", "").equals("Wahr oder Falsch")){
                        statsGameImg.setImageDrawable(getDrawable(R.drawable.wahrfalsch));
                    }else if(sharedPrefs.getString("belGame", "").equals("Ring of Fire")){
                        statsGameImg.setImageDrawable(getDrawable(R.drawable.ringoffire));
                    } else if(sharedPrefs.getString("belGame", "").equals("Höher Tiefer")){
                        statsGameImg.setImageDrawable(getDrawable(R.drawable.hoehertiefer));
                    }else if(sharedPrefs.getString("belGame", "").equals("Bombe")){
                        statsGameImg.setImageDrawable(getDrawable(R.drawable.bomb));
                    }else if(sharedPrefs.getString("belGame", "").equals("Glücksrad")){
                        statsGameImg.setImageDrawable(getDrawable(R.drawable.gluecksrad));
                    }else if(sharedPrefs.getString("belGame", "").equals("Balkensaufen")){
                        statsGameImg.setImageDrawable(getDrawable(R.drawable.balkensaufen));
                    }else if(sharedPrefs.getString("belGame", "").equals("Meiern")){
                        statsGameImg.setImageDrawable(getDrawable(R.drawable.meiern));
                    }else if(sharedPrefs.getString("belGame", "").equals("Busfahrer")){
                        statsGameImg.setImageDrawable(getDrawable(R.drawable.bus));
                    }else if(sharedPrefs.getString("belGame", "").equals("Memory")){
                        statsGameImg.setImageDrawable(getDrawable(R.drawable.memory));
                    }else if(sharedPrefs.getString("belGame", "").equals("Hausnummer")){
                        statsGameImg.setImageDrawable(getDrawable(R.drawable.hausnummer));
                    }
                    if(sharedPrefs.getString("belGame2", "").equals("Quiz")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.quiz));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Wahr oder Falsch")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.wahrfalsch));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Ring of Fire")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.ringoffire));
                    } else if(sharedPrefs.getString("belGame2", "").equals("Höher Tiefer")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.hoehertiefer));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Bombe")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.bomb));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Glücksrad")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.gluecksrad));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Balkensaufen")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.balkensaufen));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Meiern")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.meiern));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Busfahrer")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.bus));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Memory")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.memory));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Hausnummer")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.hausnummer));
                    }

                    if(sharedPrefs.getString("belGame3", "").equals("Quiz")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.quiz));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Wahr oder Falsch")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.wahrfalsch));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Ring of Fire")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.ringoffire));
                    } else if(sharedPrefs.getString("belGame3", "").equals("Höher Tiefer")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.hoehertiefer));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Bombe")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.bomb));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Glücksrad")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.gluecksrad));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Balkensaufen")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.balkensaufen));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Meiern")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.meiern));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Busfahrer")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.bus));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Memory")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.memory));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Hausnummer")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.hausnummer));
                    }
                    }
            });

            setContentView(R.layout.main_menu);
            info = findViewById(R.id.mainEinstellungen);
            einstellungen = findViewById(R.id.mainShots);
            licht = findViewById(R.id.mainLicht);
            minispiele = findViewById(R.id.mainMini);
            stats = findViewById(R.id.mainStats);
            bluet = findViewById(R.id.mainBlue);
            check = findViewById(R.id.mainCheck);
            mitte = findViewById(R.id.mainMitte);
            check.setVisibility(View.GONE);
            licht.setAlpha((float) 0.2);
            einstellungen.setAlpha((float) 0.2);
            webView = findViewById(R.id.webview);
            webView.setVisibility(View.GONE);
            notificationManager = NotificationManagerCompat.from(this);
            //lv = (ListView)popUpView.findViewById(R.id.lvPlayerStats);

            Log.d("BLUETOOTHLOG2", "#" + BluetoothAdapterClass.address_bt + "#");
            if (!SystemState.isOffline()) {
                check.setVisibility(View.VISIBLE);
                licht.setAlpha(1);
            }

            bluet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SystemState.isOffline()) {
                        Intent i = new Intent(MainMenu.this, BluetoothConnect.class);
                        startActivity(i);
                        finish();
                    }
                }
            });
            minispiele.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(MainMenu.this, SpielerEinstellungen.class);
                    startActivity(i);
                }
            });
            licht.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                if(SystemState.isOffline()){
                    mitte.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.mainmenurotate));
                }else{
                    Intent i = new Intent(MainMenu.this, LightMenu.class);
                    startActivity(i);
                }
                }
            });
            einstellungen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SystemState.isOffline()) {
                        mitte.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.mainmenurotate));
                    } else {
                        Intent i = new Intent(MainMenu.this, Bedienung.class);
                        startActivity(i);
                    }
                }
            });

            stats.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0);
                        statsCountText.setText("Shots gesamt: "+GlobalStats.shotCount);
                        statsDrunkCount.setText(""+sharedPrefs.getInt("playerShotsMax", 0)+" Shots");
                        statsDrunkName.setText(sharedPrefs.getString("playerShotsName", ""));
                    statsGame.setText(sharedPrefs.getString("belGame", "")+" ("+sharedPrefs.getInt(sharedPrefs.getString("belGame", ""), 0)+")");
                    statsGame2.setText(sharedPrefs.getString("belGame2", "")+" ("+sharedPrefs.getInt(sharedPrefs.getString("belGame2", ""), 0)+")");
                    statsGame3.setText(sharedPrefs.getString("belGame3", "")+" ("+sharedPrefs.getInt(sharedPrefs.getString("belGame3", ""), 0)+")");

                        statsDrunkPic.setImageResource(sharedPrefs.getInt("imgRes", 0));
                        statsDrunkPic.setForeground(playerPics[sharedPrefs.getInt("imgDrawable", 0)]);

                        if(sharedPrefs.getString("belGame", "").equals("Quiz")){
                            statsGameImg.setImageDrawable(getDrawable(R.drawable.quiz));
                        }else if(sharedPrefs.getString("belGame", "").equals("Wahr oder Falsch")){
                            statsGameImg.setImageDrawable(getDrawable(R.drawable.wahrfalsch));
                        }else if(sharedPrefs.getString("belGame", "").equals("Ring of Fire")){
                            statsGameImg.setImageDrawable(getDrawable(R.drawable.ringoffire));
                        } else if(sharedPrefs.getString("belGame", "").equals("Höher Tiefer")){
                            statsGameImg.setImageDrawable(getDrawable(R.drawable.hoehertiefer));
                        }else if(sharedPrefs.getString("belGame", "").equals("Bombe")){
                            statsGameImg.setImageDrawable(getDrawable(R.drawable.bomb));
                        }else if(sharedPrefs.getString("belGame", "").equals("Glücksrad")){
                            statsGameImg.setImageDrawable(getDrawable(R.drawable.gluecksrad));
                        }else if(sharedPrefs.getString("belGame", "").equals("Balkensaufen")){
                            statsGameImg.setImageDrawable(getDrawable(R.drawable.balkensaufen));
                        }else if(sharedPrefs.getString("belGame", "").equals("Meiern")){
                            statsGameImg.setImageDrawable(getDrawable(R.drawable.meiern));
                        }else if(sharedPrefs.getString("belGame", "").equals("Busfahrer")){
                            statsGameImg.setImageDrawable(getDrawable(R.drawable.bus));
                        }else if(sharedPrefs.getString("belGame", "").equals("Memory")){
                            statsGameImg.setImageDrawable(getDrawable(R.drawable.memory));
                        }else if(sharedPrefs.getString("belGame", "").equals("Hausnummer")){
                            statsGameImg.setImageDrawable(getDrawable(R.drawable.hausnummer));
                        }

                    if(sharedPrefs.getString("belGame2", "").equals("Quiz")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.quiz));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Wahr oder Falsch")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.wahrfalsch));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Ring of Fire")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.ringoffire));
                    } else if(sharedPrefs.getString("belGame2", "").equals("Höher Tiefer")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.hoehertiefer));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Bombe")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.bomb));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Glücksrad")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.gluecksrad));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Balkensaufen")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.balkensaufen));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Meiern")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.meiern));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Busfahrer")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.bus));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Memory")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.memory));
                    }else if(sharedPrefs.getString("belGame2", "").equals("Hausnummer")){
                        statsGameImg2.setImageDrawable(getDrawable(R.drawable.hausnummer));
                    }

                    if(sharedPrefs.getString("belGame3", "").equals("Quiz")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.quiz));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Wahr oder Falsch")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.wahrfalsch));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Ring of Fire")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.ringoffire));
                    } else if(sharedPrefs.getString("belGame3", "").equals("Höher Tiefer")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.hoehertiefer));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Bombe")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.bomb));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Glücksrad")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.gluecksrad));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Balkensaufen")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.balkensaufen));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Meiern")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.meiern));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Busfahrer")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.bus));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Memory")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.memory));
                    }else if(sharedPrefs.getString("belGame3", "").equals("Hausnummer")){
                        statsGameImg3.setImageDrawable(getDrawable(R.drawable.hausnummer));
                    }
                    }
            });

            info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainMenu.this, InfoActivity.class);
                    startActivity(intent);
                }
            });

            mitte.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {/*
                    state = 1;
                    webView.setVisibility(View.VISIBLE);
                    webView.getSettings().setJavaScriptEnabled(true);
                    webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                    webView.setWebViewClient(new WebViewClient());
                    webView.loadUrl("http://www.shot-bot.de");
                    WebChromeClient wbc = new WebChromeClient() {

                        public void onCloseWindow(WebView wv) {
                            wv.setVisibility(View.GONE);
                        }
                    };
                    webView.setWebChromeClient(wbc);*/
                }
            });

        }


        public void setupListview() {
            try {
                ArrayAdapter<Player> adapter = new PlayerListAdapter();
                lv.setAdapter(adapter);

            } catch (Exception e) {
            }
        }

        private class PlayerListAdapter extends ArrayAdapter<Player> {
            public PlayerListAdapter() {
                super(MainMenu.this, R.layout.balkendia, PlayersClass.getPlayerList());


            }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                View itemView = convertView;
                if (itemView == null) {
                    itemView = getLayoutInflater().inflate(R.layout.balkendia, parent, false);
                }

                //Find Item
                final Player player = PlayersClass.getPlayerList().get(position);

                //Insert in View

                int maxW = 250;
                int maxShots = PlayersClass.maxShots();
                double verhaeltnis;
                if (maxShots != 0) {
                    verhaeltnis = (double) player.getShotCount() / (double) maxShots;
                } else {
                    verhaeltnis = 0;
                }
                int width = (int) (maxW * verhaeltnis);
                final float scale = getContext().getResources().getDisplayMetrics().density;
                int pixels = (int) (width * scale + 0.5f);

                TextView balken = (TextView) itemView.findViewById(R.id.diaBalken);
                ImageView diaPic = itemView.findViewById(R.id.diaBild);
                TextView shots = itemView.findViewById(R.id.diaShots);
                TextView name = itemView.findViewById(R.id.diaName);

                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(pixels, (int) (25 * scale + 0.5f));
                ViewGroup.LayoutParams params2 = balken.getLayoutParams();
                params2.height = (int) (25 * scale + 0.5f);
                params2.width = pixels;
                balken.setLayoutParams(params2);

                diaPic.setImageResource(player.getColor());
                if (player.bild != null) {
                    diaPic.setImageBitmap(player.bild);
                    diaPic.setForeground(null);
                } else {
                    diaPic.setForeground(player.playerPic);
                }
                balken.setBackgroundResource(player.color2);
                String sh = " Shots // ";
                if (player.getShotCount() == 1) {
                    sh = " Shot // ";
                }
                shots.setText(player.getShotCount() + sh + player.ml + "ml");
                name.setText("" + player.getName());
                return itemView;
            }
        }


        @Override
        public void onBackPressed() {
            //super.onBackPressed();
            if (state == 1) {
                webView.setVisibility(View.GONE);
                state = 0;
            } else {
                super.onBackPressed();
            }
        }

        private final Handler mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case MESSAGE_BT:
                        String s = (String) msg.obj;
                        Toast.makeText(context, s, Toast.LENGTH_LONG).show();
                        break;
                }
            }
        };
    }

