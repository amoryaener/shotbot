package com.drink.shotbotapp.shot_botapp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Alexander on 13.04.2018.
 */

public class CardDeckDrag {
    ArrayList<Card> deck;
    Random randomGen = new Random();

    public CardDeckDrag(int[] c){
        this.deck = new ArrayList<Card>();
        for(int i =0; i<c[0];i++){
            this.deck.add(new Card(5,"Random","@drawable/cardrandom", R.drawable.cardrandom));
        }
        for(int i =0; i<c[1];i++){
            this.deck.add(new Card(1,"Shot","@drawable/cardshot", R.drawable.cardshot));
        }
        for(int i =0; i<c[2];i++){
            this.deck.add(new Card(6,"Regel","@drawable/cardr", R.drawable.cardr));
        }
        for(int i =0; i<c[3];i++){
            this.deck.add(new Card(4,"Alle","@drawable/cardalle", R.drawable.cardalle));
        }
        for(int i =0; i<c[4];i++){
            this.deck.add(new Card(3,"Frauen","@drawable/cardf", R.drawable.cardf));
        }
        for(int i =0; i<c[5];i++){
            this.deck.add(new Card(0,"MiniSpiel","@drawable/cardmini", R.drawable.cardmini));
        }
        for(int i =0; i<c[6];i++){
            this.deck.add(new Card(2,"Maenner","@drawable/cardm", R.drawable.cardm));
        }
        for(int i =0; i<c[7];i++){
            this.deck.add(new Card(7,"Voll","@drawable/cardvoll", R.drawable.cardvoll));
        }
        for(int i =0; i<c[8];i++){
            this.deck.add(new Card(8,"Nuechtern","@drawable/cardnuechtern", R.drawable.cardnuechtern));
        }
        for(int i =0; i<c[9];i++){
            this.deck.add(new Card(9,"Buddy","@drawable/cardbuddy", R.drawable.cardbuddy));
        }
    }

    public void shuffle(){
        Collections.shuffle(this.deck);
    }

    public Card getCard(int i){
        return this.deck.get(i);
    }
}
