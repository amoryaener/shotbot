package com.drink.shotbotapp.shot_botapp;

import android.content.ClipData;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ogaclejapan.arclayout.ArcLayout;

import java.util.ArrayList;
import java.util.Random;

public class WheelBuilder extends AppCompatActivity {

    ListView lvWheel;
    LinearLayout choosePlayer;
    LinearLayout infoLay;
    LinearLayout playerInfo;
    ImageView ampel;
    String Status;
    Boolean Getrunken = true;
    int[]actionNr;
    Button fertig;
    /*
    state:
    0 = Wahl Neu oder vorgefetigt
    1 = Eigenes Design wählen
    2 = Fetiges wählen
    3 = Erstellen
    4 = Spielen
    */
    int state =0;
    boolean isVorgegeben;
    LinearLayout vorgegebenOderNeu;
    LinearLayout designChoose;
    DropZone[] zones;
    ImageView[] fields;
    ImageView[] fields2;
    ArcLayout arcLayout;
    ImageView field0;
    ImageView field1;
    ImageView field2;
    ImageView field3;
    ImageView field4;
    ImageView field5;
    ImageView field6;
    ImageView field7;
    ImageView field8;
    ImageView field9;
    ImageView field02;
    ImageView field12;
    ImageView field22;
    ImageView field32;
    ImageView field42;
    ImageView field52;
    ImageView field62;
    ImageView field72;
    ImageView field82;
    ImageView field92;
    ImageView choose0;
    ImageView choose1;
    ImageView choose2;
    ImageView choose3;
    ImageView choose4;
    ImageView abc;
    ImageView choose0drag;
    ImageView choose1drag;
    ImageView choose2drag;
    ImageView choose3drag;
    ImageView choose4drag;
    ImageView choose5;
    ImageView choose6;
    ImageView choose7;
    ImageView choose8;
    ImageView choose9;
    ImageView choose5drag;
    ImageView choose6drag;
    ImageView choose7drag;
    ImageView choose8drag;
    ImageView choose9drag;
    int x;
    int y;
    RelativeLayout relLay;
    RelativeLayout.LayoutParams dragParams;
    ImageView vorgegeben;
    ImageView eigenes;
    ImageView design0;
    ImageView design1;
    ImageView design2;
    ImageView design3;
    ImageView design4;
    ImageView arrow;
    boolean is_spinning;
    int count =0;
    boolean begrenzt = false;
    int rounds;
    boolean ready = false;
    java.util.Random Random;
    int grad = 0, grad_alt = 0;
    int abc2;
    ImageView playerPic;
    TextView playerName;
    ArrayList<Player> players;
    int currentPlayer;
    ImageView info;
    ImageView closeInfo;

    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;


    private static final float FACTOR = 10.00f;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wheel_builder);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        players = PlayersClass.getPlayerList();
        currentPlayer = 0;
        playerName = findViewById(R.id.playerNameWheel);
        playerPic = findViewById(R.id.playerPicWheel);
        playerPic.setImageResource(players.get(currentPlayer).getColor());
        if(players.get(currentPlayer).bild!=null){
            playerPic.setImageBitmap(players.get(currentPlayer).bild);
            playerPic.setForeground(null);
        }else{
            playerPic.setForeground(players.get(currentPlayer).playerPic);
        }
        GlobalStats.gameStartet("Glücksrad");
        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });

        playerName.setText(players.get(currentPlayer).getName());
        vorgegebenOderNeu = findViewById(R.id.wheelVorOderEigen);
        lvWheel = findViewById(R.id.lvWheel);
        choosePlayer = findViewById(R.id.choosePlayerLay);
        choosePlayer.setVisibility(View.GONE);
        info = findViewById(R.id.info);
        closeInfo = findViewById(R.id.closeInfo);
        infoLay = findViewById(R.id.infoLay);
        infoLay.setVisibility(View.GONE);
        designChoose = findViewById(R.id.designChooseLay);
        designChoose.setVisibility(View.GONE);
        arrow = findViewById(R.id.iv_arrow);
        arrow.setVisibility(View.GONE);
        rounds = getIntent().getIntExtra("Rounds",0);
        if(rounds >0){
            begrenzt=true;
        }
        actionNr= new int[10];
        for(int i=0; i<10; i++){
            actionNr[i]=99;
        }
        playerInfo = findViewById(R.id.playerInfoLay);
        playerInfo.setVisibility(View.GONE);
        is_spinning =false;
        fertig = findViewById(R.id.wheelFertig);
        ampel = findViewById(R.id.ampel);
        BluetoothAdapterClass.setHandler(mHandler);
        ampel.setVisibility(View.GONE);
        fertig.setVisibility(View.GONE);
        Random = new Random();
        vorgegebenOderNeu.setVisibility(View.VISIBLE);
        eigenes = findViewById(R.id.wheelEigen);
        vorgegeben = findViewById(R.id.wheelVorgegeben);
        design0 = findViewById(R.id.designChoose0);
        design1 = findViewById(R.id.designChoose1);
        design2 = findViewById(R.id.designChoose2);
        design3 = findViewById(R.id.designChoose3);
        design4 = findViewById(R.id.designChoose4);
        arcLayout = findViewById(R.id.arcLayWheel);
        relLay = findViewById(R.id.layoutWheelCreator);
        field0 = findViewById(R.id.circlePic0Wheel);
        field1 = findViewById(R.id.circlePic1Wheel);
        field2 = findViewById(R.id.circlePic2Wheel);
        field3 = findViewById(R.id.circlePic3Wheel);
        field4 = findViewById(R.id.circlePic4Wheel);
        field5 = findViewById(R.id.circlePic5Wheel);
        field6 = findViewById(R.id.circlePic6Wheel);
        field7 = findViewById(R.id.circlePic7Wheel);
        field8 = findViewById(R.id.circlePic8Wheel);
        field9 = findViewById(R.id.circlePic9Wheel);
        fields = new ImageView[10];
        fields[0]=field0;
        fields[1]=field1;
        fields[2]=field2;
        fields[3]=field3;
        fields[4]=field4;
        fields[5]=field5;
        fields[6]=field6;
        fields[7]=field7;
        fields[8]=field8;
        fields[9]=field9;
        field02 = findViewById(R.id.circlePic0Wheel2);
        field12 = findViewById(R.id.circlePic1Wheel2);
        field22 = findViewById(R.id.circlePic2Wheel2);
        field32 = findViewById(R.id.circlePic3Wheel2);
        field42 = findViewById(R.id.circlePic4Wheel2);
        field52 = findViewById(R.id.circlePic5Wheel2);
        field62 = findViewById(R.id.circlePic6Wheel2);
        field72 = findViewById(R.id.circlePic7Wheel2);
        field82 = findViewById(R.id.circlePic8Wheel2);
        field92 = findViewById(R.id.circlePic9Wheel2);
        fields2 = new ImageView[10];
        fields2[0]=field02;
        fields2[1]=field12;
        fields2[2]=field22;
        fields2[3]=field32;
        fields2[4]=field42;
        fields2[5]=field52;
        fields2[6]=field62;
        fields2[7]=field72;
        fields2[8]=field82;
        fields2[9]=field92;
        for(int i=0; i<10;i++){
            fields2[i].setImageDrawable(null);
            fields[i].setImageDrawable(null);
        }
        choose0 = findViewById(R.id.wheelChoose0);
        choose1 = findViewById(R.id.wheelChoose1);
        choose2 = findViewById(R.id.wheelChoose2);
        choose3 = findViewById(R.id.wheelChoose3);
        choose4 = findViewById(R.id.wheelChoose4);
        choose0drag = findViewById(R.id.choose0drag);
        choose1drag = findViewById(R.id.choose1drag);
        choose2drag = findViewById(R.id.choose2drag);
        choose3drag = findViewById(R.id.choose3drag);
        choose4drag = findViewById(R.id.choose4drag);
        choose5 = findViewById(R.id.wheelChoose5);
        choose6 = findViewById(R.id.wheelChoose6);
        choose7 = findViewById(R.id.wheelChoose7);
        choose8 = findViewById(R.id.wheelChoose8);
        choose9 = findViewById(R.id.wheelChoose9);
        choose5drag = findViewById(R.id.choose5drag);
        choose6drag = findViewById(R.id.choose6drag);
        choose7drag = findViewById(R.id.choose7drag);
        choose8drag = findViewById(R.id.choose8drag);
        choose9drag = findViewById(R.id.choose9drag);
        choose0.setClickable(false);
        choose1.setClickable(false);
        choose2.setClickable(false);
        choose3.setClickable(false);
        choose4.setClickable(false);
        choose5.setClickable(false);
        choose6.setClickable(false);
        choose7.setClickable(false);
        choose8.setClickable(false);
        choose9.setClickable(false);
        zones = new DropZone[10];
        for(int i=0; i<10; i++){
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) fields[i].getLayoutParams();
            zones[i]= new DropZone(params.leftMargin,params.leftMargin+params.width,params.topMargin,params.topMargin+params.height);
            final int j =i;
            fields[i].setOnDragListener(new View.OnDragListener() {
                @Override
                public boolean onDrag(View v, DragEvent event) {
                    switch(event.getAction()) {
                        case DragEvent.ACTION_DRAG_STARTED:
                            // Do nothing
                            dragParams = new RelativeLayout.LayoutParams(70,70);
                            dragParams = (RelativeLayout.LayoutParams) abc.getLayoutParams();
                            abc.setLayoutParams(dragParams);
                            break;

                        case DragEvent.ACTION_DRAG_ENTERED:
                            x = (int) event.getX();
                            y = (int) event.getY();
                            break;

                        case DragEvent.ACTION_DRAG_EXITED :
                            x = (int) event.getX();
                            y = (int) event.getY();
                            dragParams.setMargins(x, y, 0, 0);
                            abc.setLayoutParams(dragParams);
                            break;

                        case DragEvent.ACTION_DRAG_LOCATION  :
                            x = (int) event.getX();
                            y = (int) event.getY();
                            break;

                        case DragEvent.ACTION_DRAG_ENDED   :
                            for(int i=0; i<10;i++){
                                fields[i].setImageDrawable(null);
                            }
                            // Do nothing
                            break;

                        case DragEvent.ACTION_DROP:
                            for(int i=0; i<10;i++){
                                fields[i].setImageDrawable(null);
                            }
                            x = (int) event.getX();
                            y = (int) event.getY();
                            x = x-abc.getWidth()/2;
                            y = y-abc.getHeight()/2;
                            dragParams.setMargins(x, y, 0, 0);
                            abc.setLayoutParams(dragParams);
                            if(x >= zones[j].x1-50 && x<= zones[j].x2+50 && y>= zones[j].y1-50 && y <= zones[j].y2+50){
                                fields2[j].setImageDrawable(abc.getDrawable());
                                actionNr[j]=abc2;
                                if(alleBelegt()){
                                    fertig.setVisibility(View.VISIBLE);
                                }
                            }
                            // Do nothing
                            break;
                        default: break;
                    }
                    return true;
                }
            });
            vorgegeben.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    state=2;
                    vorgegebenOderNeu.setVisibility(View.GONE);
                    designChoose.setVisibility(View.VISIBLE);
                    design0.setImageDrawable(getResources().getDrawable(R.drawable.wheelfertig0));
                    design1.setImageDrawable(getResources().getDrawable(R.drawable.wheelfertig1));
                    design2.setImageDrawable(getResources().getDrawable(R.drawable.wheelfertig2));
                    design3.setImageDrawable(getResources().getDrawable(R.drawable.wheelfertig3));
                    design4.setImageDrawable(getResources().getDrawable(R.drawable.wheelfertig4));
                    isVorgegeben =true;
                }
            });
            eigenes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    state=1;
                    vorgegebenOderNeu.setVisibility(View.GONE);
                    designChoose.setVisibility(View.VISIBLE);
                    design0.setImageDrawable(getResources().getDrawable(R.drawable.wheel10er));
                    design1.setImageDrawable(getResources().getDrawable(R.drawable.wheel10er1));
                    design2.setImageDrawable(getResources().getDrawable(R.drawable.wheel10er2));
                    design3.setImageDrawable(getResources().getDrawable(R.drawable.wheel10er3));
                    design4.setImageDrawable(getResources().getDrawable(R.drawable.wheel10er4));
                    isVorgegeben =false;
                }
            });

            info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    infoLay.setVisibility(View.VISIBLE);
                }
            });

            closeInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    infoLay.setVisibility(View.GONE);
                }
            });

            design0.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    designChoosed(0);
                }
            });
            design1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    designChoosed(1);
                }
            });
            design2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    designChoosed(2);
                }
            });
            design3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    designChoosed(3);
                }
            });
            design4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    designChoosed(4);
                }
            });
            arcLayout.setOnTouchListener(new OnSwipeTouchListener(this) {
                public void onSwipeTop() {
                    if(!is_spinning && ready && Getrunken){
                        rotate();
                    }
                }
                public void onSwipeRight() {
                    if(!is_spinning && ready && Getrunken){
                        rotate();
                    }
                }
                public void onSwipeLeft() {
                    if(!is_spinning && ready && Getrunken){
                        rotate();
                    }
                }
                public void onSwipeBottom() {
                    if(!is_spinning && ready && Getrunken){
                        rotate();
                    }
                }

            });
            fertig.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    state = 4;
                    playerInfo.setVisibility(View.VISIBLE);
                    fertig.setVisibility(View.GONE);
                    ampel.setVisibility(View.VISIBLE);
                    arrow.setVisibility(View.VISIBLE);
                    choose0.setVisibility(View.GONE);
                    choose1.setVisibility(View.GONE);
                    choose2.setVisibility(View.GONE);
                    choose3.setVisibility(View.GONE);
                    choose4.setVisibility(View.GONE);
                    choose5.setVisibility(View.GONE);
                    choose6.setVisibility(View.GONE);
                    choose7.setVisibility(View.GONE);
                    choose8.setVisibility(View.GONE);
                    choose9.setVisibility(View.GONE);
                    choose0.setClickable(false);
                    choose1.setClickable(false);
                    choose2.setClickable(false);
                    choose3.setClickable(false);
                    choose4.setClickable(false);
                    choose5.setClickable(false);
                    choose6.setClickable(false);
                    choose7.setClickable(false);
                    choose8.setClickable(false);
                    choose9.setClickable(false);
                    ready = true;
                }
            });
        }
    }

    public void nextPlayer(){
        if(currentPlayer<players.size()-1){
            currentPlayer ++;
        }else{
            currentPlayer =0;
        }
        playerName.setText(players.get(currentPlayer).getName());
        playerPic.setImageResource(players.get(currentPlayer).getColor());
        if(players.get(currentPlayer).bild!=null){
            playerPic.setImageBitmap(players.get(currentPlayer).bild);
            playerPic.setForeground(null);
        }else{
            playerPic.setForeground(players.get(currentPlayer).playerPic);
        }
    }

    public void designChoosed(int index){
        designChoose.setVisibility(View.GONE);
        if(isVorgegeben){
            state=4;
            playerInfo.setVisibility(View.VISIBLE);
            ampel.setVisibility(View.VISIBLE);
            arrow.setVisibility(View.VISIBLE);
            choose0.setVisibility(View.GONE);
            choose1.setVisibility(View.GONE);
            choose2.setVisibility(View.GONE);
            choose3.setVisibility(View.GONE);
            choose4.setVisibility(View.GONE);
            choose5.setVisibility(View.GONE);
            choose6.setVisibility(View.GONE);
            choose7.setVisibility(View.GONE);
            choose8.setVisibility(View.GONE);
            choose9.setVisibility(View.GONE);
            choose0.setClickable(false);
            choose1.setClickable(false);
            choose2.setClickable(false);
            choose3.setClickable(false);
            choose4.setClickable(false);
            choose5.setClickable(false);
            choose6.setClickable(false);
            choose7.setClickable(false);
            choose8.setClickable(false);
            choose9.setClickable(false);
            ready = true;
            if(index==0){
                arcLayout.setBackground(getResources().getDrawable(R.drawable.wheelfertig0));
                actionNr[0]=0;actionNr[1]=6;actionNr[2]=7;actionNr[3]=8;actionNr[4]=1;actionNr[5]=2;actionNr[6]=9;actionNr[7]=4;actionNr[8]=5;actionNr[9]=3;
            }else if(index==1){
                arcLayout.setBackground(getResources().getDrawable(R.drawable.wheelfertig1));
                actionNr[0]=4;actionNr[1]=2;actionNr[2]=4;actionNr[3]=2;actionNr[4]=4;actionNr[5]=2;actionNr[6]=4;actionNr[7]=2;actionNr[8]=4;actionNr[9]=2;
            }else if(index==2){
                arcLayout.setBackground(getResources().getDrawable(R.drawable.wheelfertig2));
                actionNr[0]=8;actionNr[1]=7;actionNr[2]=2;actionNr[3]=9;actionNr[4]=3;actionNr[5]=8;actionNr[6]=7;actionNr[7]=2;actionNr[8]=9;actionNr[9]=3;
            }else if(index==3){
                arcLayout.setBackground(getResources().getDrawable(R.drawable.wheelfertig3));
                actionNr[0]=0;actionNr[1]=4;actionNr[2]=6;actionNr[3]=1;actionNr[4]=7;actionNr[5]=4;actionNr[6]=7;actionNr[7]=5;actionNr[8]=2;actionNr[9]=3;
            }else if(index==4){
                arcLayout.setBackground(getResources().getDrawable(R.drawable.wheelfertig4));
                actionNr[0]=4;actionNr[1]=2;actionNr[2]=4;actionNr[3]=2;actionNr[4]=3;actionNr[5]=2;actionNr[6]=4;actionNr[7]=2;actionNr[8]=4;actionNr[9]=3;
            }
        }else{
            state = 3;
            choose0.setClickable(true);
            choose1.setClickable(true);
            choose2.setClickable(true);
            choose3.setClickable(true);
            choose4.setClickable(true);
            choose5.setClickable(true);
            choose6.setClickable(true);
            choose7.setClickable(true);
            choose8.setClickable(true);
            choose9.setClickable(true);
            choose0.setOnTouchListener(new MyTouchListener(0));
            choose1.setOnTouchListener(new MyTouchListener(1));
            choose2.setOnTouchListener(new MyTouchListener(2));
            choose3.setOnTouchListener(new MyTouchListener(3));
            choose4.setOnTouchListener(new MyTouchListener(4));
            choose5.setOnTouchListener(new MyTouchListener(5));
            choose6.setOnTouchListener(new MyTouchListener(6));
            choose7.setOnTouchListener(new MyTouchListener(7));
            choose8.setOnTouchListener(new MyTouchListener(8));
            choose9.setOnTouchListener(new MyTouchListener(9));
            if(index==0){
                arcLayout.setBackground(getResources().getDrawable(R.drawable.wheel10er));
            }else if(index==1){
                arcLayout.setBackground(getResources().getDrawable(R.drawable.wheel10er1));
            }else if(index==2){
                arcLayout.setBackground(getResources().getDrawable(R.drawable.wheel10er2));
            }else if(index==3){
                arcLayout.setBackground(getResources().getDrawable(R.drawable.wheel10er3));
            }else if(index==4){
                arcLayout.setBackground(getResources().getDrawable(R.drawable.wheel10er4));
            }
        }
    }
    private final class MyTouchListener implements View.OnTouchListener {

        int nr;

        public MyTouchListener(int index){
            this.nr = index;
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                for(int i=0; i<10;i++){
                    fields[i].setImageDrawable(getResources().getDrawable(R.drawable.whiteround));
                }
                if(nr==0){
                    abc = choose0drag;
                    abc2 = 0;
                }
                if(nr==1){
                    abc = choose1drag;
                    abc2 = 1;
                }
                if(nr==2){
                    abc = choose2drag;
                    abc2 = 2;
                }
                if(nr==3){
                    abc = choose3drag;
                    abc2 = 3;
                }
                if(nr==4){
                    abc = choose4drag;
                    abc2 = 4;
                }
                if(nr==5){
                    abc = choose5drag;
                    abc2 = 5;
                }
                if(nr==6){
                    abc = choose6drag;
                    abc2 = 6;
                }
                if(nr==7){
                    abc = choose7drag;
                    abc2 = 7;
                }
                if(nr==8){
                    abc = choose8drag;
                    abc2 = 8;
                }
                if(nr==9){
                    abc = choose9drag;
                    abc2 = 9;
                }
                abc.setVisibility(View.VISIBLE);
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
                        view);
                abc.startDrag(data, shadowBuilder, abc, 0);
                abc.setVisibility(View.INVISIBLE);
                return true;
            } else {
                return false;
            }
        }
    }

    public class DropZone{
        int x1,x2,y1,y2;

        public DropZone(int x, int xx, int y, int yy){
            this.x1 = x;
            this.x2 = xx;
            this.y1 = y;
            this.y2 = yy;
        }
    }

    public boolean alleBelegt(){
        boolean ausgabe = true;
        for(int i=0; i<10; i++){
            if(actionNr[i]==99){
                ausgabe = false;
            }
        }
        return ausgabe;
    }

    public void rotate(){
        is_spinning = true;
        grad = grad_alt % 360;
        grad = Random.nextInt(3655) + 939;
        RotateAnimation rotate= new RotateAnimation(grad_alt, grad,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f );
        rotate.setDuration(2215);
        rotate.setFillAfter(true);
        rotate.setInterpolator(new DecelerateInterpolator());
        rotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                aktion(360 - (grad % 360));
                is_spinning =false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        arcLayout.startAnimation(rotate);
    }

    public void aktion(int wert){
        if(wert >= 342 ||wert <=18 ){
            aktionAusfuehren(actionNr[9]);
        }
        if(wert >= 18 && wert <=54 ){
            aktionAusfuehren(actionNr[0]);
        }
        if(wert >= 54 && wert <= 90){
            aktionAusfuehren(actionNr[1]);
        }
        if(wert >= 90 && wert <= 126){
            aktionAusfuehren(actionNr[2]);
        }
        if(wert >= 126 && wert <= 162){
            aktionAusfuehren(actionNr[3]);
        }
        if(wert >= 162 && wert <= 198){
            aktionAusfuehren(actionNr[4]);
        }
        if(wert >= 198 && wert <= 234){
            aktionAusfuehren(actionNr[5]);
        }
        if(wert >= 234 && wert <= 270){
            aktionAusfuehren(actionNr[6]);
        }
        if(wert >= 270 && wert <= 306){
            aktionAusfuehren(actionNr[7]);
        }
        if(wert >= 306 && wert <= 342){
            aktionAusfuehren(actionNr[8]);
        }
    }
    public void aktionAusfuehren(int nr){
        if(nr==0){
            //Männer
            ArrayList<Player> loosers = new ArrayList<Player>();
            for(int i=0; i<players.size(); i++){
                if(players.get(i).morw.equals("m")){
                    loosers.add(players.get(i));
                }
            }
            if(loosers.size()>0){
                if(loosers.size()==1){
                    loosers.get(0).oneShot();
                }else{
                    MultiLight ms = new MultiLight();
                    ms.shots(loosers);
                }
                if(!SystemState.isOffline()) {
                    Getrunken = false;
                    ampel.setImageResource(R.drawable.ampel_rot);
                }
            }
            //Toast.makeText(this,"Männer",Toast.LENGTH_LONG).show();
            nextPlayer();
        }else if(nr==1){
            //Frauen
            ArrayList<Player> loosers = new ArrayList<Player>();
            for(int i=0; i<players.size(); i++){
                if(players.get(i).morw.equals("w")){
                    loosers.add(players.get(i));
                }
            }
            if(loosers.size()>0){
                if(loosers.size()==1){
                    loosers.get(0).oneShot();
                }else{
                    MultiLight ms = new MultiLight();
                    ms.shots(loosers);
                }
                if(!SystemState.isOffline()) {
                    Getrunken = false;
                    ampel.setImageResource(R.drawable.ampel_rot);
                }
            }
            //Toast.makeText(this,"Frauen",Toast.LENGTH_LONG).show();
            nextPlayer();
        }else if(nr==2){
            //Einzel
            players.get(currentPlayer).oneShot();
            if(!SystemState.isOffline()) {
                Getrunken = false;
                ampel.setImageResource(R.drawable.ampel_rot);
            }
            //Toast.makeText(this,"Einzel",Toast.LENGTH_LONG).show();
            nextPlayer();
        }else if(nr==3){
            //Alle
            ArrayList<Player> loosers = new ArrayList<Player>();
            for(int i=0; i<players.size(); i++){
                loosers.add(players.get(i));
            }
            MultiLight ms = new MultiLight();
            ms.shots(loosers);
            if(!SystemState.isOffline()) {
                Getrunken = false;
                ampel.setImageResource(R.drawable.ampel_rot);
            }
            //Toast.makeText(this,"Alle",Toast.LENGTH_LONG).show();
            nextPlayer();
        }else if(nr==4){
            //Nichts
            //Toast.makeText(this,"Nichts",Toast.LENGTH_LONG).show();
            nextPlayer();
        }else if(nr==5){
            //Links
            if(currentPlayer==players.size()-1){
                players.get(0).oneShot();
            }else{
                players.get(currentPlayer+1).oneShot();
            }
            if(!SystemState.isOffline()) {
                Getrunken = false;
                ampel.setImageResource(R.drawable.ampel_rot);
            }
            //Toast.makeText(this,"Links",Toast.LENGTH_LONG).show();
            nextPlayer();
        }else if(nr==6){
            //Rechts
            if(currentPlayer==0){
                players.get(players.size()-1).oneShot();
            }else{
                players.get(currentPlayer-1).oneShot();
            }
            if(!SystemState.isOffline()) {
                Getrunken = false;
                ampel.setImageResource(R.drawable.ampel_rot);
            }
            //Toast.makeText(this,"Rechts",Toast.LENGTH_LONG).show();
            nextPlayer();
        }else if(nr==7){
            //Aussuchen
            setupListview();
            timerClass tc = new timerClass();
            tc.start();
            //Toast.makeText(this,"Aussuchen",Toast.LENGTH_LONG).show();
            nextPlayer();
        }else if(nr==8){
            //nochmal
            //Toast.makeText(this,"Nochmal",Toast.LENGTH_LONG).show();
        }else if(nr==9){
            //Alle außer aktueller Spieler
            ArrayList<Player> loosers = new ArrayList<Player>();
            for(int i=0; i<players.size(); i++){
                if(i!=currentPlayer){
                    loosers.add(players.get(i));
                }
            }
            if(loosers.size()>0){
                if(loosers.size()==1){
                    loosers.get(0).oneShot();
                }else{
                    MultiLight ms = new MultiLight();
                    ms.shots(loosers);
                }
                if(!SystemState.isOffline()) {
                    Getrunken = false;
                    ampel.setImageResource(R.drawable.ampel_rot);
                }
            }
            //Toast.makeText(this,"Alle anderen",Toast.LENGTH_LONG).show();
            nextPlayer();
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bedienung.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Status = readMessage;
                    if(Status.contains("1")){
                        Getrunken = true;
                        ampel.setImageResource(R.drawable.ampel_gruen);
                        Status="";
                    }
                    break;
            }
        }
    };

    public void setupListview(){
        try {
            ArrayAdapter<Player> adapter = new PlayerListAdapter();
            lvWheel.setAdapter(adapter);

        } catch (Exception e) {
        }
    }

    private class PlayerListAdapter extends ArrayAdapter<Player> {
        public PlayerListAdapter() {
            super(WheelBuilder.this, R.layout.wheelentry, players);


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.wheelentry, parent, false);
            }

            //Find Item
            final Player player = players.get(position);

            //Insert in View
            TextView b = (TextView) itemView.findViewById(R.id.wheelEntryName);
            b.setText(player.getName());

            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    choosePlayer.setVisibility(View.GONE);
                    player.oneShot();
                    if(!SystemState.isOffline()) {
                        Getrunken = false;
                        ampel.setImageResource(R.drawable.ampel_rot);
                    }
                }
            });

            return itemView;
        }
    }

    public class timerClass extends Thread{
        public void run(){
            try {
                sleep(600);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mHandler2.obtainMessage(0).sendToTarget();
        }
    }

    private final Handler mHandler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            choosePlayer.setVisibility(View.VISIBLE);
        }
    };

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(state ==0){
            beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
        }else if(state ==1){
            state=0;
            vorgegebenOderNeu.setVisibility(View.VISIBLE);
            designChoose.setVisibility(View.GONE);
        }else if (state == 2){
            state=0;
            vorgegebenOderNeu.setVisibility(View.VISIBLE);
            designChoose.setVisibility(View.GONE);
        }else if (state ==3){
            designChoose.setVisibility(View.VISIBLE);
            state = 1;
            choose0.setClickable(false);
            choose1.setClickable(false);
            choose2.setClickable(false);
            choose3.setClickable(false);
            choose4.setClickable(false);
            choose5.setClickable(false);
            choose6.setClickable(false);
            choose7.setClickable(false);
            choose8.setClickable(false);
            choose9.setClickable(false);
            choose0.setOnTouchListener(null);
            choose1.setOnTouchListener(null);
            choose2.setOnTouchListener(null);
            choose3.setOnTouchListener(null);
            choose4.setOnTouchListener(null);
            choose5.setOnTouchListener(null);
            choose6.setOnTouchListener(null);
            choose7.setOnTouchListener(null);
            choose8.setOnTouchListener(null);
            choose9.setOnTouchListener(null);
        }else if(state ==4){
            beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
        }
    }
}
