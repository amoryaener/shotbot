package com.drink.shotbotapp.shot_botapp;


import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class slideAdapter extends PagerAdapter{
    Context context;
    LayoutInflater inflater;

    //List of Images
    public int[] lst_images = {
            R.drawable.image_1,
            R.drawable.image_2,
            R.drawable.image_3,
            R.drawable.image_4
    };
    //List of Titles
    public String[] lst_titles = {
            "EINEN TRINKEN",
            "MINISPIELE",
            "DAWN OF THE GAG",
            "QUIZ"
    };
    //List of Beschreibungen
    public String[] lst_describtions = {
            "Wenns Schnell gehen muss",
            "Der kleine Partyspass",
            "Wer lacht wird flüssig bestraft!",
            "Faaaaalsch! Einen trinken."
    };
    //List of Colors
    public int[] lst_colors = {
            Color.rgb(239, 85,85),
            Color.rgb(200, 35,43),
            Color.rgb(110, 49,89),
            Color.rgb(1, 188,212)
    };


    public slideAdapter(Context context){
        this.context = context;
    }


    @Override
    public int getCount() {
        return lst_titles.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==(LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.slider, container, false);
        LinearLayout layoutslide = (LinearLayout)view.findViewById(R.id.sliderLinearLayout);
        ImageView imgslide = (ImageView)view.findViewById(R.id.sliderImg);
        TextView texttitle = (TextView)view.findViewById(R.id.txttitle);
        TextView beschreibung = (TextView)view.findViewById(R.id.TxtBeschreibung);
        layoutslide.setBackgroundColor(lst_colors[position]);
        imgslide.setImageResource(lst_images[position]);
        texttitle.setText(lst_titles[position]);
        beschreibung.setText(lst_describtions[position]);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
