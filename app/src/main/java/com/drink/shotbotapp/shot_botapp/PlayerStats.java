package com.drink.shotbotapp.shot_botapp;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class PlayerStats extends AppCompatActivity {

    ArrayList<Player> players;
    ListView lv;
    ImageButton graphStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_stats);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        players = PlayersClass.getPlayerList();
        lv = (ListView)findViewById(R.id.lvPlayerStats);
        setupListview();
    }

    public void setupListview(){
        try {
            ArrayAdapter<Player> adapter = new PlayerStats.PlayerListAdapter();
            lv.setAdapter(adapter);

        } catch (Exception e) {
        }
    }

    private class PlayerListAdapter extends ArrayAdapter<Player> {
        public PlayerListAdapter() {
            super(PlayerStats.this, R.layout.player_statistic_entry, players);


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.player_statistic_entry, parent, false);
            }

            //Find Item
            final Player player = players.get(position);

            //Insert in View

            int maxW = 320;
            int maxShots = PlayersClass.maxShots();
            double verhaeltnis;
            if(maxShots!=0){
                verhaeltnis = (double)player.getShotCount()/(double)maxShots;
            }else{
                verhaeltnis = 0;
            }
            int width = (int)(maxW*verhaeltnis);
            final float scale = getContext().getResources().getDisplayMetrics().density;
            int pixels = (int) (width * scale + 0.5f);

            TextView balken = (TextView) itemView.findViewById(R.id.statsBalken);
            TextView kreis = (TextView) itemView.findViewById(R.id.statsKreis);
            //TextView name = (TextView) itemView.findViewById(R.id.statsName);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(pixels,(int) (20 * scale + 0.5f));
            balken.setLayoutParams(params);

            if(player.id==9){
                kreis.setTextColor(Color.BLACK);
            }

            kreis.setBackgroundResource(player.getColor());
            balken.setBackgroundResource(player.color2);
            kreis.setText(""+player.getShotCount());
            //name.setText(""+player.getName());
            return itemView;
        }
    }
}
