package com.drink.shotbotapp.shot_botapp;

/**
 * Created by Alexander on 06.02.2018.
 */

public class WahrFalschEntry {
    String question;
    boolean correct;

    public WahrFalschEntry(String q,boolean c){
        this.question = q;
        this.correct = c;
    }

    public String getQuestion() {
        return question;
    }

    public boolean getCorrect() {
        return correct;
    }
}
