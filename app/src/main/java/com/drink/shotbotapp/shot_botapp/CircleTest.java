package com.drink.shotbotapp.shot_botapp;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.ogaclejapan.arclayout.ArcLayout;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;


public class CircleTest extends AppCompatActivity {

    boolean activ = true;
    boolean vis=true;
    ArcLayout arcLayout;
    ImageView playerPic0;
    ImageView playerPic1;
    ImageView playerPic2;
    ImageView playerPic3;
    ImageView playerPic4;
    ImageView playerPic5;
    ImageView playerPic6;
    ImageView playerPic7;
    ImageView playerPic8;
    ImageView playerPic9;
    TextView playerName0;
    TextView playerName1;
    TextView playerName2;
    TextView playerName3;
    TextView playerName4;
    TextView playerName5;
    TextView playerName6;
    TextView playerName7;
    TextView playerName8;
    TextView playerName9;
    TextView[] playerNames;
    ImageView[] playerPics;
    ArrayList<Player> players;
    RelativeLayout layout;
    int currentPlayer;
    int status =0;
    MediaPlayer tick_tack;
    MediaPlayer explode_sound;
    ImageView ampel;
    String Status;
    Boolean Getrunken = true;
    ImageView einstellungen;
    CrystalRangeSeekbar rangeSeekbar;
    TextView tvMin;
    TextView tvMax;
    boolean einstellungenSichtbar = false;
    LinearLayout range;
    int min =6;
    int max =40;
    int count =0;
    boolean begrenzt = false;
    int rounds;
    ImageView start;
    ImageView bomb;
    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circle_test);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        players = PlayersClass.getPlayerList();
        rounds = getIntent().getIntExtra("Rounds",0);
        if(rounds >0){
            begrenzt=true;
        }
        GlobalStats.gameStartet("Bombe");
        tick_tack = MediaPlayer.create(this, R.raw.tick_tack);
        BluetoothAdapterClass.setHandler(mHandler);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        arcLayout = findViewById(R.id.arcLay);
        players = PlayersClass.getPlayerList();
        bomb = findViewById(R.id.timerBomb);
        start = findViewById(R.id.startButton);
        playerNames = new TextView[10];
        playerPics = new ImageView[10];
        playerName0=findViewById(R.id.circleName0);
        playerName1=findViewById(R.id.circleName1);
        playerName2=findViewById(R.id.circleName2);
        playerName3=findViewById(R.id.circleName3);
        playerName4=findViewById(R.id.circleName4);
        playerName5=findViewById(R.id.circleName5);
        playerName6=findViewById(R.id.circleName6);
        playerName7=findViewById(R.id.circleName7);
        playerName8=findViewById(R.id.circleName8);
        playerName9=findViewById(R.id.circleName9);
        playerPic0=findViewById(R.id.circlePic0);
        playerPic1=findViewById(R.id.circlePic1);
        playerPic2=findViewById(R.id.circlePic2);
        playerPic3=findViewById(R.id.circlePic3);
        playerPic4=findViewById(R.id.circlePic4);
        playerPic5=findViewById(R.id.circlePic5);
        playerPic6=findViewById(R.id.circlePic6);
        playerPic7=findViewById(R.id.circlePic7);
        playerPic8=findViewById(R.id.circlePic8);
        playerPic9=findViewById(R.id.circlePic9);
        playerNames[0]=playerName0;
        playerPics[0]=playerPic0;
        playerNames[1]=playerName1;
        playerPics[1]=playerPic1;
        playerNames[2]=playerName2;
        playerPics[2]=playerPic2;
        playerNames[3]=playerName3;
        playerPics[3]=playerPic3;
        playerNames[4]=playerName4;
        playerPics[4]=playerPic4;
        playerNames[5]=playerName5;
        playerPics[5]=playerPic5;
        playerNames[6]=playerName6;
        playerPics[6]=playerPic6;
        playerNames[7]=playerName7;
        playerPics[7]=playerPic7;
        playerNames[8]=playerName8;
        playerPics[8]=playerPic8;
        playerNames[9]=playerName9;
        playerPics[9]=playerPic9;

        for(int i=0; i<(10-PlayersClass.getPlayerList().size());i++){
            arcLayout.removeViewAt(9-i);
        }
        for(int i=0; i<PlayersClass.getPlayerList().size();i++){
            playerNames[i].setText(players.get(i).getName());
            if(players.get(i).bild!=null){
                playerPics[i].setImageBitmap(players.get(i).bild);
                playerPics[i].setForeground(null);
            }else{
                playerPics[i].setImageDrawable(players.get(i).playerPic);
            }
        }
        playerPics[0].setForeground(getResources().getDrawable(R.drawable.skull));
        bomb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status ==1){
                    playerPics[currentPlayer].setForeground(null);
                    if(currentPlayer<players.size()-1){
                        currentPlayer ++;
                    }else{
                        currentPlayer =0;
                    }
                    //playerPics[currentPlayer].setForeground(getResources().getDrawable(R.drawable.skull));
                }
            }
        });

        explode_sound = MediaPlayer.create(this, R.raw.exploded);
        currentPlayer =0;
        range=findViewById(R.id.bombe_range);
        range.setVisibility(View.GONE);
        einstellungen = findViewById(R.id.bombe_einstellungen);
        rangeSeekbar= findViewById(R.id.rangeSeekbar1);
        rangeSeekbar.setMinValue(1);
        rangeSeekbar.setMaxValue(90);
        rangeSeekbar.setMaxStartValue(40);
        tvMin=findViewById(R.id.textMin1);
        tvMax=findViewById(R.id.textMax1);
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tvMin.setText(String.valueOf(minValue));
                tvMax.setText(String.valueOf(maxValue));
                min = minValue.intValue();
                max = maxValue.intValue();
            }
        });

        einstellungen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(einstellungenSichtbar){
                    einstellungenSichtbar=false;
                    range.setVisibility(View.GONE);
                    range.setClickable(false);
                    range.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_hide));
                }else {
                    einstellungenSichtbar=true;
                    range.setVisibility(View.VISIBLE);
                    range.setClickable(true);
                    range.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_show));
                }
            }
        });
        ampel = findViewById(R.id.ampel);
        layout = findViewById(R.id.layoutTimer);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status ==0){
                    timerClass tc = new timerClass();
                    tc.start();
                    tick_tack_start();
                    status =1;
                    start.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(), "Auf den Kreis klicken zum Weitergeben!", Toast.LENGTH_LONG).show();
                }else if(status ==2){
                    if(Getrunken==true) {
                        playerPics[currentPlayer].setForeground(null);
                        if (currentPlayer < players.size() - 1) {
                            currentPlayer++;
                        } else {
                            currentPlayer = 0;
                        }
                        playerPics[currentPlayer].setForeground(getResources().getDrawable(R.drawable.skull));
                        timerClass tc = new timerClass();
                        tc.start();
                        status = 1;
                        tick_tack_start();
                        start.setVisibility(View.INVISIBLE);
                        Toast.makeText(getApplicationContext(), "Auf die Bombe klicken zum Weitergeben!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tick_tack.stop();
                activ=false;
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });
    }
    private final Handler mHandler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what==0){
                if(activ){
                    exploded();
                }
            }else if(msg.what==1){
                if(vis){
                    vis = false;
                    playerPics[currentPlayer].setForeground(null);
                }else{
                    vis = true;
                    playerPics[currentPlayer].setForeground(getResources().getDrawable(R.drawable.skull));
                }
            }else if(msg.what==2){
                layout.setBackgroundResource(R.drawable.background1);
            }else if(msg.what==3){
                layout.setBackgroundResource(R.drawable.beach_background);
            }
        }
    };
    public void tick_tack_start(){
        tick_tack.setLooping(true);
        tick_tack.start();
    }

    public void tick_tack_end(){
        tick_tack.stop();
    }

    public void exploded(){
        playerPics[currentPlayer].setForeground(getResources().getDrawable(R.drawable.skull));
        count++;
        if(begrenzt && rounds == count){
            finish();
        }
        timerClass2 tc = new timerClass2();
        tc.start();
        start.setVisibility(View.VISIBLE);
        status = 2;
        tick_tack_end();
        explode_sound.start();
        players.get(currentPlayer).oneShot();
        if(!SystemState.isOffline()) {
            Getrunken = false;
            ampel.setImageResource(R.drawable.ampel_rot);
        }
        Toast.makeText(getApplicationContext(), players.get(currentPlayer).getName()+ " Trinkt!", Toast.LENGTH_LONG).show();
    }

    public class timerClass extends Thread{
        public void run(){
            int timer = ThreadLocalRandom.current().nextInt(min, max);
            while(timer >0){
                timer --;
                mHandler2.obtainMessage(1).sendToTarget();
                try {
                    sleep(333);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mHandler2.obtainMessage(1).sendToTarget();
                try {
                    sleep(333);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mHandler2.obtainMessage(1).sendToTarget();
                try {
                    sleep(333);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            mHandler2.obtainMessage(0).sendToTarget();
        }
    }

    public class timerClass2 extends Thread{
        public void run(){
            for(int i=0; i<5; i++){
                mHandler2.obtainMessage(2).sendToTarget();
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mHandler2.obtainMessage(3).sendToTarget();
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            mHandler2.obtainMessage(4).sendToTarget();
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bedienung.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Status = readMessage;
                    if(Status.contains("1")){
                        Getrunken = true;
                        ampel.setImageResource(R.drawable.ampel_gruen);
                        Status="";
                    }
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }
}
