package com.drink.shotbotapp.shot_botapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {
    TextView lizenz_copyright;
    TextView datenschutz;
    TextView impressum;
    TextView lc1;
    TextView lc2;
    TextView lc3;
    TextView lc4;
    TextView ds1;
    TextView imp1;
    TextView version;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        lizenz_copyright = (TextView)findViewById(R.id.lizenz_copyright);
        datenschutz = (TextView)findViewById(R.id.datenschutz);
        impressum = (TextView)findViewById(R.id.impressum);
        lc1 = (TextView)findViewById(R.id.lc1);
        lc2 = (TextView)findViewById(R.id.lc2);
        ds1 = (TextView)findViewById(R.id.ds1);
        imp1 = (TextView)findViewById(R.id.imp1);
        version = (TextView)findViewById(R.id.version);

        ImageView infoback = findViewById(R.id.infoback);
        infoback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
