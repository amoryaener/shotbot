package com.drink.shotbotapp.shot_botapp;

/**
 * Created by Alexander on 02.02.2018.
 */

public class Card {
    int value;
    String color;
    String pic;
    int pic2;

    public Card(int v, String c, String p, int p2){
        this.value = v;
        this.color = c;
        this.pic = p;
        this.pic2 = p2;
    }

    public int getValue() {
        return value;
    }

    public String getColor() {
        return color;
    }

    public String getPic() {
        return pic;
    }
    public int getPic2() {
        return pic2;
    }
}
