package com.drink.shotbotapp.shot_botapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.ogaclejapan.arclayout.ArcLayout;

import java.util.ArrayList;

public class SpielauswahlNeu extends AppCompatActivity {

    ViewPager viewPager;
    slideAdapter2 myadapter;
    LinearLayout pfeil;
    Animation anim2;
    ListView lv;
    PopupWindow mpopup;
    View popUpView;
    ImageView stats;
    ImageView exit;
    int spieleranzahl=0;
    int spieleranzahl2=10;
    Context context;
    boolean notestate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spielauswahl_neu);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        SystemState.gameEntries = new ArrayList<GameEntry>();
        SystemState.gameEntries.add(new GameEntry(2,1,10,1,"Quiz","Teste dein Wissen",new Intent(SpielauswahlNeu.this, Quiz.class),R.drawable.quiz,Color.rgb(239, 85,85)));
        SystemState.gameEntries.add(new GameEntry(2,1,10,1,"Wahr oder Falsch","50/50 Change. Wer liegt richtig?",new Intent(SpielauswahlNeu.this, WahrFalsch.class),R.drawable.wahrfalsch,Color.rgb(1, 188,212)));
        SystemState.gameEntries.add(new GameEntry(3,1,10,2,"Ring of Fire","Zieh eine Karte!",new Intent(SpielauswahlNeu.this, DragDrop.class),R.drawable.ringoffire,Color.rgb(110, 49,89)));
        SystemState.gameEntries.add(new GameEntry(0,1,10,2,"Höher Tiefer","Wähle ob die nächste Karte höher, tiefer oder gleich ist",new Intent(SpielauswahlNeu.this, HoeherTiefer.class),R.drawable.hoehertiefer,Color.rgb(255, 139,4)));
        SystemState.gameEntries.add(new GameEntry(3,1,10,1,"Bombe","Gib die Bombe weiter aber pass auf ... tick ... tack",new Intent(SpielauswahlNeu.this, CircleTest.class),R.drawable.bomb,Color.rgb(200, 35,43)));
        SystemState.gameEntries.add(new GameEntry(3,1,10,2,"Glücksrad","Drehe und lass das Glück entscheiden wer trinken muss.",new Intent(SpielauswahlNeu.this, WheelBuilder.class),R.drawable.gluecksrad,Color.rgb(140, 26,91)));
        SystemState.gameEntries.add(new GameEntry(3,1,10,2,"Balkensaufen","Versuch den Pointer in der Mitte anzuhalten, ansonsten wird getrunken.",new Intent(SpielauswahlNeu.this, BalkenSaufen.class),R.drawable.balkensaufen,Color.rgb(74, 193,39)));
        SystemState.gameEntries.add(new GameEntry(1,1,10,1,"Meiern","Klassisch",new Intent(SpielauswahlNeu.this, Meiern.class),R.drawable.meiern,Color.rgb(255, 174,4)));
        SystemState.gameEntries.add(new GameEntry(0,1,1,2,"Busfahrer","Wer mit seiner Entscheidung falsch liegt, der muss trinken. Wer schafft es bis zum Ende?",new Intent(SpielauswahlNeu.this, Busfahrer.class),R.drawable.bus,Color.rgb(255, 64,45)));
        SystemState.gameEntries.add(new GameEntry(0,1,10,0,"Memory","Deckt die Paare auf. Wer jedoch falsch liegt trinkt.",new Intent(SpielauswahlNeu.this, Memory.class),R.drawable.memory,Color.rgb(15, 85,130)));
        SystemState.gameEntries.add(new GameEntry(1,1,10,0,"Hausnummer","Derjenige mit der höhsten Hausnummer gewinnt.",new Intent(SpielauswahlNeu.this, Hausnummer.class),R.drawable.hausnummer,Color.rgb(37, 160,155)));
        SystemState.gameEntries.add(new GameEntry(3,1,10,0,"Draw","Coming Soon",new Intent(SpielauswahlNeu.this, Draw.class),R.drawable.draw,Color.rgb(214, 25,104)));
        SystemState.gameEntries.add(new GameEntry(3,1,10,1,"Roulette","Coming Soon",new Intent(SpielauswahlNeu.this, Roulette.class),R.drawable.roulette,Color.rgb(1, 124,38)));
        SystemState.gameEntries.add(new GameEntry(3,1,10,2,"I Never","Coming Soon",new Intent(SpielauswahlNeu.this, INever.class),R.drawable.inever,Color.rgb(37, 224,79)));
        pfeil = findViewById(R.id.pfeil);
        context = this;
        Toast.makeText(getApplicationContext(), "nach links wischen zur Spielauswahl", Toast.LENGTH_LONG).show();
        SystemState.haerteBools = new boolean[3];
        SystemState.haerteBools[0]=true;
        SystemState.haerteBools[1]=true;
        SystemState.haerteBools[2]=true;
        SystemState.katBools = new boolean[4];
        SystemState.katBools[0]=true;
        SystemState.katBools[1]=true;
        SystemState.katBools[2]=true;
        SystemState.katBools[3]=true;
        SystemState.minPlayer=1;
        SystemState.maxPlayer=10;
        SystemState.filter();
        notestate=true;
        Animation anim = AnimationUtils.loadAnimation(getApplication(), R.anim.pfeil_ein);
        anim2 = AnimationUtils.loadAnimation(getApplication(), R.anim.pfeil_aus);
        pfeil.startAnimation(anim);
        stats = findViewById(R.id.statsButton);
        popUpView = getLayoutInflater().inflate(R.layout.activity_player_stats,
                null); // inflating popup layout
        mpopup = new PopupWindow(popUpView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        lv = (ListView)popUpView.findViewById(R.id.lvPlayerStats);
        anim.setAnimationListener(new Animation.AnimationListener(){
            @Override
            public void onAnimationStart(Animation arg0) {
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {
            }
            @Override
            public void onAnimationEnd(Animation arg0) {
                pfeil.startAnimation(anim2);
            }
        });
        exit = popUpView.findViewById(R.id.statsExit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpopup.dismiss();
            }
        });
        anim2.setAnimationListener(new Animation.AnimationListener(){
            @Override
            public void onAnimationStart(Animation arg0) {
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {
            }
            @Override
            public void onAnimationEnd(Animation arg0) {
                pfeil.setVisibility(View.GONE);
            }
        });
        viewPager= (ViewPager) findViewById(R.id.ViewPager2);
        myadapter = new slideAdapter2(this);
        viewPager.setAdapter(myadapter);
        stats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0);
                setupListview();
            }
        });
    }

    public void update(){
        SystemState.filter();
        Log.d("UPDATE","!!!");
        for(int i=0; i<SystemState.getNames().length;i++){
            Log.d("::","::"+SystemState.getNames()[i]);
        }
        //myadapter.notifyDataSetChanged();
        //viewPager.destroyDrawingCache();
        myadapter = new slideAdapter2(this);
        viewPager.setAdapter(myadapter);
    }

    public class slideAdapter2 extends PagerAdapter {
        Context context;
        LayoutInflater inflater;




        public slideAdapter2(Context context){
            this.context = context;
        }


        @Override
        public int getCount() {
            return SystemState.getNames().length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view==(LinearLayout)object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.slider2, container, false);
            LinearLayout layoutslide = (LinearLayout)view.findViewById(R.id.slider2LinearLayout);
            ImageView imgslide = (ImageView)view.findViewById(R.id.slider2Img);
            TextView texttitle = (TextView)view.findViewById(R.id.slider2title);
            TextView beschreibung = (TextView)view.findViewById(R.id.slider2beschreibung);
            final TextView haerte = view.findViewById(R.id.slider2_haerte);
            TextView anzahl = view.findViewById(R.id.slider2_anzahl);
            final TextView spieleranzahlview = view.findViewById(R.id.filerSpieler);
            CrystalRangeSeekbar rangeSeekbar = view.findViewById(R.id.rangeSeekbar1);
            final LinearLayout soon = view.findViewById(R.id.soon);
            rangeSeekbar.setMinValue(1);
            rangeSeekbar.setMaxValue(10);
            rangeSeekbar.setMaxStartValue(10);
            rangeSeekbar.setMinStartValue(1);
            rangeSeekbar.setMinStartValue(SystemState.minPlayer);
            rangeSeekbar.setMaxStartValue(SystemState.maxPlayer);
            rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
                @Override
                public void valueChanged(Number minValue, Number maxValue) {
                    spieleranzahl=minValue.intValue();
                    spieleranzahl2=maxValue.intValue();
                    SystemState.maxPlayer=maxValue.intValue();
                    SystemState.minPlayer=minValue.intValue();
                    spieleranzahlview.setText("Spieleranzahl: "+spieleranzahl+" - "+spieleranzahl2);
                    if(spieleranzahl==spieleranzahl2){
                        spieleranzahlview.setText("Spieleranzahl: "+spieleranzahl);
                    }
                    //update();
                }
            });
            final ArcLayout arc = view.findViewById(R.id.arcLay8);
            LinearLayout filterLay = view.findViewById(R.id.filterLay);
            if(position==0){
                filterLay.setVisibility(View.VISIBLE);
                arc.setVisibility(View.VISIBLE);
            }else{
                filterLay.setVisibility(View.GONE);
                arc.setVisibility(View.GONE);
            }

            if(position>11){
                soon.setVisibility(View.VISIBLE);
            }else{
                soon.setVisibility(View.GONE);
            }
            final ImageView easy = view.findViewById(R.id.sliderEasy);
            final ImageView medium = view.findViewById(R.id.sliderMedium);
            final ImageView hard = view.findViewById(R.id.sliderHard);
            final ImageView karten = view.findViewById(R.id.kartens);
            final ImageView wuerfel = view.findViewById(R.id.wuerfels);
            final ImageView wissen = view.findViewById(R.id.wissens);
            final ImageView party = view.findViewById(R.id.partys);

            karten.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(SystemState.katBools[0]){
                        if(SystemState.katBools[1]||SystemState.katBools[2]||SystemState.katBools[3]){
                            SystemState.katBools[0]=false;
                            karten.setImageResource(R.drawable.kartenspiele2);
                        }
                    }else{
                        SystemState.katBools[0]=true;
                        karten.setImageResource(R.drawable.kartenspiele);
                    }
                    update();
                }
            });
            wuerfel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(SystemState.katBools[1]){
                        if(SystemState.katBools[0]||SystemState.katBools[2]||SystemState.katBools[3]){
                            SystemState.katBools[1]=false;
                            wuerfel.setImageResource(R.drawable.wuerfelspiele2);
                        }
                    }else{
                        SystemState.katBools[1]=true;
                        wuerfel.setImageResource(R.drawable.wuerfelspiele);
                    }
                    update();
                }
            });
            wissen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(SystemState.katBools[2]){
                        if(SystemState.katBools[1]||SystemState.katBools[0]||SystemState.katBools[3]){
                            SystemState.katBools[2]=false;
                            wissen.setImageResource(R.drawable.wissensspiele2);
                        }
                    }else{
                        SystemState.katBools[2]=true;
                        wissen.setImageResource(R.drawable.wissensspiele);
                    }
                    update();
                }
            });
            party.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(SystemState.katBools[3]){
                        if(SystemState.katBools[1]||SystemState.katBools[2]||SystemState.katBools[0]){
                            SystemState.katBools[3]=false;
                            party.setImageResource(R.drawable.partyspiele2);
                        }
                    }else{
                        SystemState.katBools[3]=true;
                        party.setImageResource(R.drawable.partyspiele);
                    }
                    update();
                }
            });
            easy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(SystemState.haerteBools[0]){
                        if(SystemState.haerteBools[1]||SystemState.haerteBools[2]){
                            SystemState.haerteBools[0]=false;
                            easy.setImageResource(R.drawable.easy_round2);
                        }
                    }else{
                        SystemState.haerteBools[0]=true;
                        easy.setImageResource(R.drawable.easy_round);
                    }
                    update();
                }
            });
            medium.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(SystemState.haerteBools[1]){
                        if(SystemState.haerteBools[0]||SystemState.haerteBools[2]){
                            SystemState.haerteBools[1]=false;
                            medium.setImageResource(R.drawable.medium_round2);
                        }
                    }else{
                        SystemState.haerteBools[1]=true;
                        medium.setImageResource(R.drawable.medium_round);
                    }
                    update();
                }
            });
            hard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(SystemState.haerteBools[2]){
                        if(SystemState.haerteBools[0]||SystemState.haerteBools[1]){
                            SystemState.haerteBools[2]=false;
                            hard.setImageResource(R.drawable.hard_round2);
                        }
                    }else{
                        SystemState.haerteBools[2]=true;
                        hard.setImageResource(R.drawable.hard_round);
                    }
                    update();
                }
            });

            if(!SystemState.haerteBools[0]){
                easy.setImageResource(R.drawable.easy_round2);
            }
            if(!SystemState.haerteBools[1]){
                medium.setImageResource(R.drawable.medium_round2);
            }
            if(!SystemState.haerteBools[2]){
                hard.setImageResource(R.drawable.hard_round2);
            }
            if(!SystemState.katBools[0]){
                karten.setImageResource(R.drawable.kartenspiele2);
            }
            if(!SystemState.katBools[1]){
                wuerfel.setImageResource(R.drawable.wuerfelspiele2);
            }
            if(!SystemState.katBools[2]){
                wissen.setImageResource(R.drawable.wissensspiele2);
            }
            if(!SystemState.katBools[3]){
                party.setImageResource(R.drawable.partyspiele2);
            }
            if(position==2 && notestate==true){
                Toast.makeText(getApplicationContext(), "Auf das Bild klicken zum Starten", Toast.LENGTH_LONG).show();
                notestate=false;
            }

            layoutslide.setBackgroundColor(SystemState.getColors()[position]);
            imgslide.setImageResource(SystemState.getPictures()[position]);
            texttitle.setText(SystemState.getNames()[position]);
            beschreibung.setText(SystemState.getBeschreibungen()[position]);
            anzahl.setText(SystemState.getPlayers()[position]);
            haerte.setText(SystemState.getSchwierigkeiten()[position]);
            imgslide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(position==7){
                        GlobalStats.gameStartet("Balkensaufen");
                    }
                    if(position==9){
                        GlobalStats.gameStartet("Busfahrer");
                    }
                    if(position<12)
                    startActivity(SystemState.getIntents()[position]);
                }
            });
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout)object);
        }
    }
    public void setupListview(){
        try {
            ArrayAdapter<Player> adapter = new PlayerListAdapter();
            lv.setAdapter(adapter);

        } catch (Exception e) {
        }
    }

    private class PlayerListAdapter extends ArrayAdapter<Player> {
        public PlayerListAdapter() {
            super(SpielauswahlNeu.this, R.layout.balkendia, PlayersClass.getPlayerList());


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.balkendia, parent, false);
            }

            //Find Item
            final Player player = PlayersClass.getPlayerList().get(position);

            //Insert in View

            int maxW = 250;
            int maxShots = PlayersClass.maxShots();
            double verhaeltnis;
            if(maxShots!=0){
                verhaeltnis = (double)player.getShotCount()/(double)maxShots;
            }else{
                verhaeltnis = 0;
            }
            int width = (int)(maxW*verhaeltnis);
            final float scale = getContext().getResources().getDisplayMetrics().density;
            int pixels = (int) (width * scale + 0.5f);

            TextView balken = (TextView) itemView.findViewById(R.id.diaBalken);
            ImageView diaPic = itemView.findViewById(R.id.diaBild);
            TextView shots = itemView.findViewById(R.id.diaShots);
            TextView name = itemView.findViewById(R.id.diaName);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(pixels,(int) (25 * scale + 0.5f));
            ViewGroup.LayoutParams params2 = balken.getLayoutParams();
            params2.height = (int) (25 * scale + 0.5f);
            params2.width = pixels;
            balken.setLayoutParams(params2);

            diaPic.setImageResource(player.getColor());
            if(player.bild!=null){
                diaPic.setImageBitmap(player.bild);
                diaPic.setForeground(null);
            }else{
                diaPic.setForeground(player.playerPic);
            }
            balken.setBackgroundResource(player.color2);
            String sh = " Shots // ";
            if(player.getShotCount()==1){
                sh = " Shot // ";
            }
            shots.setText(player.getShotCount()+sh+player.ml +"ml");
            name.setText(""+player.getName());
            return itemView;
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(SpielauswahlNeu.this, SpielerEinstellungen.class);
        i.putExtra("onBack",true);
        startActivity(i);
    }
}
