package com.drink.shotbotapp.shot_botapp;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.ArrayList;

public class Memory extends AppCompatActivity {

    TextView m1;
    TextView m2;
    TextView m3;
    TextView m4;
    TextView m5;
    TextView m6;
    TextView m7;
    TextView m8;
    TextView m9;
    TextView m10;
    TextView m11;
    TextView m12;
    TextView m13;
    TextView m14;
    TextView m15;
    TextView m16;
    TextView[] fields;
    boolean[] aufgedeckt;
    MemoryDeck deck;
    boolean first;
    int last;
    int last2;
    int status;
    ImageView ampel;
    String Status;
    Boolean Getrunken = true;
    ImageView playerpic;
    TextView playerName;
    ArrayList<Player> players;
    int currentPlayer = 0;
    boolean einstellungenSichtbar = false;
    int fuell = 2000;
    boolean begrenzt = false;
    int rounds;
    int count =0;
    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memory);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        status =0;
        last =-1;
        last2 =-1;
        first =false;
        rounds = getIntent().getIntExtra("Rounds",0);
        if(rounds >0){
            begrenzt=true;
        }
        players = PlayersClass.getPlayerList();
        BluetoothAdapterClass.setHandler(mHandler);
        ampel = findViewById(R.id.ampel);
        playerpic = findViewById(R.id.playerPicMem);
        playerName = findViewById(R.id.playerNameMem);
        aufgedeckt = new boolean[16];
        playerName.setText(players.get(currentPlayer).getName());
        playerpic.setImageResource(players.get(currentPlayer).getColor());
        if(players.get(currentPlayer).bild!=null){
            playerpic.setImageBitmap(players.get(currentPlayer).bild);
            playerpic.setForeground(null);
        }else{
            playerpic.setForeground(players.get(currentPlayer).playerPic);
        }
        GlobalStats.gameStartet("Memory");
        fields = new TextView[16];
        for(int i=0; i<aufgedeckt.length; i++){
            aufgedeckt[i]=false;
        }
        m1 = (TextView)findViewById(R.id.m1);
        m2 = (TextView)findViewById(R.id.m2);
        m3 = (TextView)findViewById(R.id.m3);
        m4 = (TextView)findViewById(R.id.m4);
        m5 = (TextView)findViewById(R.id.m5);
        m6 = (TextView)findViewById(R.id.m6);
        m7 = (TextView)findViewById(R.id.m7);
        m8 = (TextView)findViewById(R.id.m8);
        m9 = (TextView)findViewById(R.id.m9);
        m10 = (TextView)findViewById(R.id.m10);
        m11 = (TextView)findViewById(R.id.m11);
        m12 = (TextView)findViewById(R.id.m12);
        m13 = (TextView)findViewById(R.id.m13);
        m14 = (TextView)findViewById(R.id.m14);
        m15 = (TextView)findViewById(R.id.m15);
        m16 = (TextView)findViewById(R.id.m16);
        fields[0]= m1;
        fields[1]= m2;
        fields[2]= m3;
        fields[3]= m4;
        fields[4]= m5;
        fields[5]= m6;
        fields[6]= m7;
        fields[7]= m8;
        fields[8]= m9;
        fields[9]= m10;
        fields[10]= m11;
        fields[11]= m12;
        fields[12]= m13;
        fields[13]= m14;
        fields[14]= m15;
        fields[15]= m16;
        deck = new MemoryDeck();
        deck.shuffle();

        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });

        for(int i=0; i<fields.length; i++){
            final int a =i;
            fields[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(status==1){
                        // 2 Karten offen, wieder umdrehen

                            if(currentPlayer<players.size()-1){
                                currentPlayer ++;
                            }else{
                                currentPlayer =0;
                            }
                            playerName.setText(players.get(currentPlayer).getName());
                            playerpic.setImageResource(players.get(currentPlayer).getColor());
                            if(players.get(currentPlayer).bild!=null){
                                playerpic.setImageBitmap(players.get(currentPlayer).bild);
                                playerpic.setForeground(null);
                            }else{
                                playerpic.setForeground(players.get(currentPlayer).playerPic);
                            }

                        fields[last2].setForeground(null);
                        fields[last].setForeground(null);
                        fields[last2].setBackgroundResource(R.drawable.memoryback);
                        fields[last].setBackgroundResource(R.drawable.memoryback);
                        last=-1;
                        last2=-1;
                        status =0;
                    }else{
                        if(!aufgedeckt[a] && !first){
                            // Erste Karte aufgedeckt
                            aufgedeckt[a]=true;
                            fields[a].setBackgroundResource(R.drawable.memoryblank);

                            String uri = deck.cards[a].pic;
                            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                            Drawable draw = getResources().getDrawable(imageResource);

                            fields[a].setForeground(draw);
                            first=true;
                            last =a;
                        }else if(!aufgedeckt[a] && first){
                            aufgedeckt[a]=true;
                            fields[a].setBackgroundResource(R.drawable.memoryblank);

                            String uri = deck.cards[a].pic;
                            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                            Drawable draw = getResources().getDrawable(imageResource);

                            fields[a].setForeground(draw);
                            first=false;
                            if(deck.cards[a].value!=deck.cards[last].value){
                                //2.Karte aufgedeckt falsch

                                    players.get(currentPlayer).oneShot();
                                    if(!SystemState.isOffline()) {
                                        Getrunken = false;
                                        ampel.setImageResource(R.drawable.ampel_rot);
                                    }

                                aufgedeckt[a] = false;
                                aufgedeckt[last] = false;
                                status=1;
                                last2 = a;
                            }else{
                                //2.Karte aufgedeckt richtig
                                boolean bool =true;
                                for(int j=0; j<aufgedeckt.length; j++){
                                    if(!aufgedeckt[j]){
                                        bool = false;
                                    }
                                }

                                if(bool){
                                    count++;
                                    if(begrenzt && rounds == count){
                                        finish();
                                    }
                                    if(currentPlayer<players.size()-1){
                                        currentPlayer ++;
                                    }else{
                                        currentPlayer =0;
                                    }
                                    playerName.setText(players.get(currentPlayer).getName());
                                    playerpic.setImageResource(players.get(currentPlayer).getColor());
                                    if(players.get(currentPlayer).bild!=null){
                                        playerpic.setImageBitmap(players.get(currentPlayer).bild);
                                        playerpic.setForeground(null);
                                    }else{
                                        playerpic.setForeground(players.get(currentPlayer).playerPic);
                                    }
                                    for(int i=0; i<fields.length; i++){
                                        fields[i].setBackgroundResource(R.drawable.memoryback);
                                        fields[i].setForeground(null);
                                    }
                                    deck.shuffle();
                                    for(int i=0; i<aufgedeckt.length; i++){
                                        aufgedeckt[i] = false;
                                    }
                                    status =0;
                                    last =-1;
                                    last2 =-1;
                                    first =false;
                                }else{

                                        if(currentPlayer<players.size()-1){
                                            currentPlayer ++;
                                        }else{
                                            currentPlayer =0;
                                        }
                                        playerName.setText(players.get(currentPlayer).getName());
                                        playerpic.setImageResource(players.get(currentPlayer).getColor());
                                        if(players.get(currentPlayer).bild!=null){
                                            playerpic.setImageBitmap(players.get(currentPlayer).bild);
                                            playerpic.setForeground(null);
                                        }else{
                                            playerpic.setForeground(players.get(currentPlayer).playerPic);
                                        }

                                }
                            }
                        }
                    }

                }
            });
        }

    }
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bedienung.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Status = readMessage;
                    if(Status.contains("1")){
                        Getrunken = true;
                        ampel.setImageResource(R.drawable.ampel_gruen);
                        Status="";
                    }
                    break;
            }
        }
    };
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }
}
