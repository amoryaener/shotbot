package com.drink.shotbotapp.shot_botapp;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class DragDrop extends AppCompatActivity {

    private static final float FACTOR = 10.00f;
    Random Random;
    int grad = 0, grad_alt = 0;
    boolean is_spinning = false;
    LinearLayout minirad;
    ImageView arrow;
    ImageView rad;
    boolean mini = false;
    boolean buddy = false;
    ImageView dragdropr;
    RelativeLayout.LayoutParams dragParams;
    RelativeLayout drop;
    int x;
    int y;
    Context context;
    ImageView dragCard2;
    ImageView dragCard;
    ImageView dragCard3;
    ImageView dragCard4;
    ImageView dragCard5;
    ImageView dragCard6;
    ImageView dragCard7;
    ImageView dragCard8;
    ImageView dragCard9;
    ImageView dragCard10;
    ImageView dragCard11;
    ImageView dragCard12;
    ImageView dragCard13;
    ImageView dragCard14;
    ImageView dragCard15;
    ImageView dragCard16;
    ImageView dragCard17;
    ImageView dragCard18;
    ImageView dragCard19;
    ImageView dragCard20;
    ImageView dragCard21;
    ImageView dragCard22;
    ImageView dragCard23;
    ImageView dragCard24;
    ImageView dragCard25;
    ImageView dragCard26;
    ImageView dragCard27;
    ImageView dragCard28;
    ImageView dragCard29;
    ImageView dragCard30;
    ImageView dropFeld;
    ImageView ampel;
    int dropx1;
    int dropx2;
    int dropy1;
    int dropy2;
    ImageView[] cards;
    ImageView abc;
    CardDeckDrag deck;
    int count =0;
    ImageView bigCard;
    int currentPlayer =0;
    ArrayList<Player> players;
    ArrayList<Player> players2;
    TextView playername;
    String Status;
    Boolean Getrunken = true;
    PopupWindow mpopupPlayer;
    View popUpViewPlayer;
    PopupWindow mpopupPlayer2;
    View popUpViewPlayer2;
    ListView ergebnisList;
    ListView ergebnisList2;
    ImageView playerPic;
    ImageView popupPic;
    ImageView dragdropq;
    RelativeLayout cclay;
    Button ccstart;
    TextView ccv1;
    TextView ccv2;
    TextView ccv3;
    TextView ccv4;
    TextView ccv5;
    TextView ccv6;
    TextView ccv7;
    TextView ccv8;
    TextView ccv9;
    TextView ccv10;
    ImageView ccadd1;
    ImageView ccdel1;
    ImageView ccadd2;
    ImageView ccdel2;
    ImageView ccadd3;
    ImageView ccdel3;
    ImageView ccadd4;
    ImageView ccdel4;
    ImageView ccadd5;
    ImageView ccdel5;
    ImageView ccadd6;
    ImageView ccdel6;
    ImageView ccadd7;
    ImageView ccdel7;
    ImageView ccadd8;
    ImageView ccdel8;
    ImageView ccadd9;
    ImageView ccdel9;
    ImageView ccadd10;
    ImageView ccdel10;
    TextView cczugewiesen;
    TextView [] ccvalues;
    ImageView[] ccadds;
    ImageView[] ccdels;
    int gesCards=0;
    int [] selectedCard;
    Player[] buddyArray;
    ListView lv;
    PopupWindow mpopup;
    View popUpView;
    ImageView exit;
    ImageView stats;
    PopupWindow beendenPopup;
    View beendenView;
    Button beenden_ja;
    Button beenden_nein;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drag_drop);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        stats = findViewById(R.id.dragStats);
        Random = new Random();
        BluetoothAdapterClass.setHandler(mHandler);
        popUpView = getLayoutInflater().inflate(R.layout.activity_player_stats,
                null); // inflating popup layout
        mpopup = new PopupWindow(popUpView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        lv = (ListView)popUpView.findViewById(R.id.lvPlayerStats);
        exit = popUpView.findViewById(R.id.statsExit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpopup.dismiss();
            }
        });
        GlobalStats.gameStartet("Ring of Fire");
        dragdropr = findViewById(R.id.dragdropR);
        dragdropq = findViewById(R.id.dragdropQ);
        cclay = findViewById(R.id.cclay);
        cczugewiesen = findViewById(R.id.cczugewiesen);
        minirad = findViewById(R.id.minirad);
        rad = findViewById(R.id.drag_iv_wheel);
        arrow = findViewById(R.id.drag_iv_arrow);
        minirad.setVisibility(View.GONE);
        cclay.setVisibility(View.VISIBLE);
        ccstart = findViewById(R.id.ccstart);
        rad.setOnTouchListener(new OnSwipeTouchListener(DragDrop.this) {
            public void onSwipeTop() {
                if(!is_spinning){
                    rotate();
                }
            }
            public void onSwipeRight() {
                if(!is_spinning){
                    rotate();
                }
            }
            public void onSwipeLeft() {
                if(!is_spinning){
                    rotate();
                }
            }
            public void onSwipeBottom() {
                if(!is_spinning){
                    rotate();
                }
            }

        });
        buddyArray = new Player[2];
        ccstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(gesCards==30){
                    selectedCard = new int[10];
                    SharedPreferences sharedPrefs = getSharedPreferences("saveCard", 0);
                    sharedPrefs.edit().clear().commit();
                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    for(int i=0; i<10; i++){
                        selectedCard[i]= Integer.parseInt((String)ccvalues[i].getText());
                        editor.putInt("dragCards"+i,Integer.parseInt((String)ccvalues[i].getText()));
                    }
                    editor.putBoolean("dragSaved",true);
                    deck = new CardDeckDrag(selectedCard);
                    deck.shuffle();
                    cclay.setVisibility(View.GONE);
                    cclay.setClickable(false);
                    editor.commit();
                }
            }
        });
        ccvalues = new TextView[10];
        ccadds = new ImageView[10];
        ccdels = new ImageView[10];

        ccv1 = findViewById(R.id.ccvalue1);
        ccadd1 = findViewById(R.id.ccadd1);
        ccdel1 = findViewById(R.id.ccdel1);
        ccv2 = findViewById(R.id.ccvalue2);
        ccadd2 = findViewById(R.id.ccadd2);
        ccdel2 = findViewById(R.id.ccdel2);
        ccv3 = findViewById(R.id.ccvalue3);
        ccadd3 = findViewById(R.id.ccadd3);
        ccdel3 = findViewById(R.id.ccdel3);
        ccv4 = findViewById(R.id.ccvalue4);
        ccadd4 = findViewById(R.id.ccadd4);
        ccdel4 = findViewById(R.id.ccdel4);
        ccv5 = findViewById(R.id.ccvalue5);
        ccadd5 = findViewById(R.id.ccadd5);
        ccdel5 = findViewById(R.id.ccdel5);
        ccv6 = findViewById(R.id.ccvalue6);
        ccadd6 = findViewById(R.id.ccadd6);
        ccdel6 = findViewById(R.id.ccdel6);
        ccv7 = findViewById(R.id.ccvalue7);
        ccadd7 = findViewById(R.id.ccadd7);
        ccdel7 = findViewById(R.id.ccdel7);
        ccv8 = findViewById(R.id.ccvalue8);
        ccadd8 = findViewById(R.id.ccadd8);
        ccdel8 = findViewById(R.id.ccdel8);
        ccv9 = findViewById(R.id.ccvalue9);
        ccadd9 = findViewById(R.id.ccadd9);
        ccdel9 = findViewById(R.id.ccdel9);
        ccv10 = findViewById(R.id.ccvalue10);
        ccadd10 = findViewById(R.id.ccadd10);
        ccdel10 = findViewById(R.id.ccdel10);

        beendenView = getLayoutInflater().inflate(R.layout.beenden_popup,
                null); // inflating popup layout
        beendenPopup = new PopupWindow(beendenView, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        beendenPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        beenden_ja = beendenView.findViewById(R.id.beenden_ja);
        beenden_nein = beendenView.findViewById(R.id.beenden_nein);
        beenden_ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        beenden_nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beendenPopup.dismiss();
            }
        });

        ccvalues[0]=ccv1;
        ccadds[0]=ccadd1;
        ccdels[0]=ccdel1;
        ccvalues[1]=ccv2;
        ccadds[1]=ccadd2;
        ccdels[1]=ccdel2;
        ccvalues[2]=ccv3;
        ccadds[2]=ccadd3;
        ccdels[2]=ccdel3;
        ccvalues[3]=ccv4;
        ccadds[3]=ccadd4;
        ccdels[3]=ccdel4;
        ccvalues[4]=ccv5;
        ccadds[4]=ccadd5;
        ccdels[4]=ccdel5;
        ccvalues[5]=ccv6;
        ccadds[5]=ccadd6;
        ccdels[5]=ccdel6;
        ccvalues[6]=ccv7;
        ccadds[6]=ccadd7;
        ccdels[6]=ccdel7;
        ccvalues[7]=ccv8;
        ccadds[7]=ccadd8;
        ccdels[7]=ccdel8;
        ccvalues[8]=ccv9;
        ccadds[8]=ccadd9;
        ccdels[8]=ccdel9;
        ccvalues[9]=ccv10;
        ccadds[9]=ccadd10;
        ccdels[9]=ccdel10;
        for(int i=0; i<10; i++){
            final int i2 = i;
            ccadds[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(gesCards<30){
                        ccvalues[i2].setText(Integer.parseInt((String)ccvalues[i2].getText())+1+"");
                        gesCards++;
                        ccdels[i2].setImageResource(R.drawable.delred);
                        if(gesCards==30){
                            for(int j=0; j<10; j++){
                                ccadds[j].setImageResource(R.drawable.addgrey);
                            }
                        }
                        cczugewiesen.setText("Zugewiesen: "+gesCards+"/30");
                    }
                }
            });
            ccdels[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Integer.parseInt((String)ccvalues[i2].getText())>0){
                        ccvalues[i2].setText(Integer.parseInt((String)ccvalues[i2].getText())-1+"");
                        gesCards--;
                        if(Integer.parseInt((String)ccvalues[i2].getText())==0){
                            ccdels[i2].setImageResource(R.drawable.delgrey);
                        }
                        if(gesCards==29){
                            for(int j=0; j<10; j++){
                                ccadds[j].setImageResource(R.drawable.addgreen);
                            }
                        }
                        cczugewiesen.setText("Zugewiesen: "+gesCards);
                    }
                }
            });
        }
        players = PlayersClass.getPlayerList();
        playername = findViewById(R.id.dragPlayerName);
        playername.setText(players.get(currentPlayer).getName());
        playerPic = findViewById(R.id.playerPic);
        popUpViewPlayer = getLayoutInflater().inflate(R.layout.dragpopup,
                null); // inflating popup layout
        mpopupPlayer = new PopupWindow(popUpViewPlayer, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopupPlayer.setAnimationStyle(android.R.style.Animation_Dialog);
        popUpViewPlayer2 = getLayoutInflater().inflate(R.layout.dragbuddy,
                null); // inflating popup layout
        mpopupPlayer2 = new PopupWindow(popUpViewPlayer2, ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopupPlayer2.setAnimationStyle(android.R.style.Animation_Dialog);
        ergebnisList2 = popUpViewPlayer2.findViewById(R.id.dragList2);
        ergebnisList = popUpViewPlayer.findViewById(R.id.dragList);
        popupPic = popUpViewPlayer.findViewById(R.id.dragPopupPicErgebnis);
        //deck = new CardDeckDrag();
        //deck.shuffle();
        bigCard = findViewById(R.id.dragBigCard);
        bigCard.setVisibility(View.GONE);
        cards = new ImageView[30];
        drop = findViewById(R.id.dropzone);
        context = getBaseContext();
        dragCard = findViewById(R.id.dragCard);
        dragCard.setOnTouchListener(new MyTouchListener());
        dragCard2 = findViewById(R.id.dragCard2);
        dragCard2.setOnTouchListener(new MyTouchListener());
        dragCard3 = findViewById(R.id.dragCard3);
        dragCard3.setOnTouchListener(new MyTouchListener());
        dragCard4 = findViewById(R.id.dragCard4);
        dragCard4.setOnTouchListener(new MyTouchListener());
        dragCard5 = findViewById(R.id.dragCard5);
        dragCard5.setOnTouchListener(new MyTouchListener());
        dragCard6 = findViewById(R.id.dragCard6);
        dragCard6.setOnTouchListener(new MyTouchListener());
        dragCard7 = findViewById(R.id.dragCard7);
        dragCard7.setOnTouchListener(new MyTouchListener());
        dragCard8 = findViewById(R.id.dragCard8);
        dragCard8.setOnTouchListener(new MyTouchListener());
        dragCard9 = findViewById(R.id.dragCard9);
        dragCard9.setOnTouchListener(new MyTouchListener());
        dragCard10 = findViewById(R.id.dragCard10);
        dragCard10.setOnTouchListener(new MyTouchListener());
        dragCard11 = findViewById(R.id.dragCard11);
        dragCard11.setOnTouchListener(new MyTouchListener());
        dragCard12 = findViewById(R.id.dragCard12);
        dragCard12.setOnTouchListener(new MyTouchListener());
        dragCard13 = findViewById(R.id.dragCard14);
        dragCard13.setOnTouchListener(new MyTouchListener());
        dragCard14 = findViewById(R.id.dragCard15);
        dragCard14.setOnTouchListener(new MyTouchListener());
        dragCard15 = findViewById(R.id.dragCard16);
        dragCard15.setOnTouchListener(new MyTouchListener());
        dragCard16 = findViewById(R.id.dragCard17);
        dragCard16.setOnTouchListener(new MyTouchListener());
        dragCard17 = findViewById(R.id.dragCard18);
        dragCard17.setOnTouchListener(new MyTouchListener());
        dragCard18 = findViewById(R.id.dragCard19);
        dragCard18.setOnTouchListener(new MyTouchListener());
        dragCard19 = findViewById(R.id.dragCard20);
        dragCard19.setOnTouchListener(new MyTouchListener());
        dragCard20 = findViewById(R.id.dragCard21);
        dragCard20.setOnTouchListener(new MyTouchListener());
        dragCard21 = findViewById(R.id.dragCard22);
        dragCard21.setOnTouchListener(new MyTouchListener());
        dragCard22 = findViewById(R.id.dragCard23);
        dragCard22.setOnTouchListener(new MyTouchListener());
        dragCard23 = findViewById(R.id.dragCard24);
        dragCard23.setOnTouchListener(new MyTouchListener());
        dragCard24 = findViewById(R.id.dragCard25);
        dragCard24.setOnTouchListener(new MyTouchListener());
        dragCard25 = findViewById(R.id.dragCard26);
        dragCard25.setOnTouchListener(new MyTouchListener());
        dragCard26 = findViewById(R.id.dragCard27);
        dragCard26.setOnTouchListener(new MyTouchListener());
        dragCard27 = findViewById(R.id.dragCard28);
        dragCard27.setOnTouchListener(new MyTouchListener());
        dragCard28 = findViewById(R.id.dragCard29);
        dragCard28.setOnTouchListener(new MyTouchListener());
        dragCard29 = findViewById(R.id.dragCard30);
        dragCard29.setOnTouchListener(new MyTouchListener());
        dragCard30 = findViewById(R.id.dragCard31);
        dragCard30.setOnTouchListener(new MyTouchListener());
        dropFeld = findViewById(R.id.dropfeld);

        playerPic.setImageResource(players.get(0).getColor());
        if(players.get(0).bild!=null){
            playerPic.setImageBitmap(players.get(0).bild);
            playerPic.setForeground(null);
        }else{
            playerPic.setForeground(players.get(currentPlayer).playerPic);
        }

        ampel = findViewById(R.id.ampel);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) dropFeld.getLayoutParams();
        dropx1 = params.leftMargin;
        dropx2 = dropx1 + params.width;
        dropy1 = params.topMargin;
        dropy2 = dropy1 + params.height;
        setupListview();

        dragdropr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupPic.setImageResource(R.drawable.dragpopup);
                mpopupPlayer.showAtLocation(popUpViewPlayer, Gravity.CENTER, 0, 0); // Displaying popup
            }
        });
        dragdropq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupPic.setImageResource(R.drawable.dragpopup2);
                mpopupPlayer.showAtLocation(popUpViewPlayer, Gravity.CENTER, 0, 0); // Displaying popup
            }
        });
        bigCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mini){
                    mini = false;
                    //RAD
                }else{
                    if(Getrunken==true) {
                        bigCard.setVisibility(View.GONE);
                        if (currentPlayer < players.size() - 1) {
                            currentPlayer++;
                        } else {
                            currentPlayer = 0;
                        }
                        playername.setText(players.get(currentPlayer).getName());
                        playerPic.setImageResource(players.get(currentPlayer).getColor());
                        if (players.get(currentPlayer).bild != null) {
                            playerPic.setImageBitmap(players.get(currentPlayer).bild);
                            playerPic.setForeground(null);
                        } else {
                            playerPic.setForeground(players.get(currentPlayer).playerPic);
                        }
                    }
                }

            }
        });

        cards[0]= dragCard;
        cards[1]= dragCard2;
        cards[2]= dragCard3;
        cards[3]= dragCard4;
        cards[4]= dragCard5;
        cards[5]= dragCard6;
        cards[6]= dragCard7;
        cards[7]= dragCard8;
        cards[8]= dragCard9;
        cards[9]= dragCard10;
        cards[10]= dragCard11;
        cards[11]= dragCard12;
        cards[12]= dragCard13;
        cards[13]= dragCard14;
        cards[14]= dragCard15;
        cards[15]= dragCard16;
        cards[16]= dragCard17;
        cards[17]= dragCard18;
        cards[18]= dragCard19;
        cards[19]= dragCard20;
        cards[20]= dragCard21;
        cards[21]= dragCard22;
        cards[22]= dragCard23;
        cards[23]= dragCard24;
        cards[24]= dragCard25;
        cards[25]= dragCard26;
        cards[26]= dragCard27;
        cards[27]= dragCard28;
        cards[28]= dragCard29;
        cards[29]= dragCard30;

        stats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0);
                setupListview3();
            }
        });

        Random randomGen = new Random();
        for (int i=0; i<30; i++){
            int random = randomGen.nextInt(41);
            random = random-20;
            cards[i].setRotation((float)random);
        }

        drop.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch(event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // Do nothing
                        dragParams = new RelativeLayout.LayoutParams(70,100);
                        dragParams = (RelativeLayout.LayoutParams) abc.getLayoutParams();
                        abc.setLayoutParams(dragParams);

                        break;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        x = (int) event.getX();
                        y = (int) event.getY();
                        break;

                    case DragEvent.ACTION_DRAG_EXITED :
                        x = (int) event.getX();
                        y = (int) event.getY();
                        dragParams.setMargins(x, y, 0, 0);
                        abc.setLayoutParams(dragParams);
                        break;

                    case DragEvent.ACTION_DRAG_LOCATION  :
                        x = (int) event.getX();
                        y = (int) event.getY();
                        break;

                    case DragEvent.ACTION_DRAG_ENDED   :

                        // Do nothing
                        break;

                    case DragEvent.ACTION_DROP:
                        x = (int) event.getX();
                        y = (int) event.getY();
                        x = x-abc.getWidth()/2;
                        y = y-abc.getHeight()/2;
                        dragParams.setMargins(x, y, 0, 0);
                        abc.setLayoutParams(dragParams);
                        if(x >= dropx1 && x<= dropx2 && y>= dropy1 && y <= dropy2){
                            abc.setVisibility(View.GONE);
                            action();
                        }
                        else{
                            abc.setVisibility(View.VISIBLE);
                            drop.removeView(abc);
                            drop.addView(abc);
                        }

                        // Do nothing
                        break;
                    default: break;
                }
                return true;
            }
        });
        SharedPreferences sharedPrefs = getSharedPreferences("saveCard", 0);
        if(sharedPrefs.getBoolean("dragSave",true)){
            int ges =0;
            for(int i=0; i<10; i++){
                ccvalues[i].setText(""+sharedPrefs.getInt("dragCards"+i,0));
                ges+= sharedPrefs.getInt("dragCards"+i,0);
                if(ccvalues[i].getText().equals("0")){
                    ccdels[i].setImageResource(R.drawable.delgrey);
                }else{
                    ccdels[i].setImageResource(R.drawable.delred);
                }
            }
            gesCards =ges;
            if(gesCards==30){
                for(int i=0; i<10; i++){
                    ccadds[i].setImageResource(R.drawable.delgrey);
                }
            }
            cczugewiesen.setText("Zugewiesen: "+gesCards+"/30");
        }
    }

    public void action(){
        //if(Getrunken==true) {
            Card card = deck.getCard(count);
            count++;
            drop.removeView(bigCard);
            drop.addView(bigCard);
            bigCard.setImageResource(card.getPic2());
            bigCard.setVisibility(View.VISIBLE);
            if (card.value == 0) {//--> Minispiel
                drop.removeView(minirad);
                drop.addView(minirad);
                minirad.setVisibility(View.VISIBLE);
                /*Intent intent =new Intent(DragDrop.this, Quiz.class);
                intent.putExtra("Rounds",3);
                startActivity(intent);*/
            }
            if (card.value == 1) {                                           //--> Shot
                if(!SystemState.isOffline()){
                    ampel.setImageResource(R.drawable.ampel_rot);
                    Getrunken=false;
                }
                if(buddy){
                    if(players.get(currentPlayer).id ==buddyArray[0].id){
                        ArrayList<Player> loosers= new ArrayList<Player>();
                        loosers.add(players.get(currentPlayer));
                        loosers.add(buddyArray[1]);
                        MultiLight ml = new MultiLight();
                        ml.shots(loosers);
                        //MultiShot ms = new MultiShot();
                        //ms.shots(loosers);
                    }
                }else{
                    players.get(currentPlayer).oneShot();
                }
            }
            if (card.value == 2) {                                           //--> Männer
                if(!SystemState.isOffline()){
                    ampel.setImageResource(R.drawable.ampel_rot);
                    Getrunken=false;
                }
                ArrayList<Player> loosers = new ArrayList<Player>();
                for (int i = 0; i < players.size(); i++) {
                    if (players.get(i).getMorw().equals("m")) {
                        loosers.add(players.get(i));
                    }
                }
                if(buddy && buddyArray[0].morw.equals("m")){
                    if(!buddyArray[0].morw.equals(buddyArray[1].morw)){
                        loosers.add(buddyArray[1]);
                    }
                }
                MultiLight ml = new MultiLight();
                ml.shots(loosers);
               // MultiShot ms = new MultiShot();
                //ms.shots(loosers);
            }
            if (card.value == 3) {                                           //--> Frauen
                if(!SystemState.isOffline()){
                    ampel.setImageResource(R.drawable.ampel_rot);
                    Getrunken=false;
                }
                ArrayList<Player> loosers = new ArrayList<Player>();
                for (int i = 0; i < players.size(); i++) {
                    if (players.get(i).getMorw().equals("w")) {
                        loosers.add(players.get(i));
                    }
                }
                if(buddy && buddyArray[0].morw.equals("w")){
                    if(!buddyArray[0].morw.equals(buddyArray[1].morw)){
                        loosers.add(buddyArray[1]);
                    }
                }
                MultiLight ml = new MultiLight();
                ml.shots(loosers);
               // MultiShot ms = new MultiShot();
                //ms.shots(loosers);
            }
            if (card.value == 4) {                                           //--> Alle
                if(!SystemState.isOffline()){
                    ampel.setImageResource(R.drawable.ampel_rot);
                    Getrunken=false;
                }
                ArrayList<Player> loosers = new ArrayList<Player>();
                for (int i = 0; i < players.size(); i++) {
                    loosers.add(players.get(i));
                }
                MultiLight ml = new MultiLight();
                ml.shots(loosers);
               // MultiShot ms = new MultiShot();
               // ms.shots(loosers);
            }
            if (card.value == 5) {                                           //--> Random
                if(!SystemState.isOffline()){
                    ampel.setImageResource(R.drawable.ampel_rot);
                    Getrunken=false;
                }
                Random randomGen = new Random();
                int randnr = randomGen.nextInt(players.size());
                if(buddy){
                    if(players.get(randnr).id ==buddyArray[0].id){
                        ArrayList<Player> loosers= new ArrayList<Player>();
                        loosers.add(buddyArray[0]);
                        loosers.add(buddyArray[1]);
                        MultiLight ml = new MultiLight();
                        ml.shots(loosers);
                       // MultiShot ms = new MultiShot();
                       // ms.shots(loosers);
                    }
                }else{
                    players.get(randnr).oneShot();
                }
            }
            if (card.value == 6) {
            //Regel
            }
            if (card.value == 7) {
            //Voll
                ArrayList<Player> loosers = new ArrayList<Player>();
                int max = players.get(0).getShotCount();
                for(int i=0; i<players.size(); i++){
                    if(players.get(i).getShotCount()>max){
                        max = players.get(i).getShotCount();
                    }
                }
                for(int i=0; i<players.size(); i++){
                    if(players.get(i).getShotCount()==max){
                        loosers.add(players.get(i));
                    }
                }
                if(loosers.contains(buddyArray[0])&&!loosers.contains(buddyArray[1])){
                    loosers.add(buddyArray[1]);
                }
                MultiLight ml = new MultiLight();
                ml.shots(loosers);
                //MultiShot ms = new MultiShot();
                //ms.shots(loosers);
            }
            if (card.value == 8) {
            //Nüchtern
                ArrayList<Player> loosers = new ArrayList<Player>();
                int min = players.get(0).getShotCount();
                for(int i=0; i<players.size(); i++){
                    if(players.get(i).getShotCount()<min){
                        min = players.get(i).getShotCount();
                    }
                }
                for(int i=0; i<players.size(); i++){
                    if(players.get(i).getShotCount()==min){
                        loosers.add(players.get(i));
                    }
                }
                if(loosers.contains(buddyArray[0])&&!loosers.contains(buddyArray[1])){
                    loosers.add(buddyArray[1]);
                }
                MultiLight ml = new MultiLight();
                ml.shots(loosers);
                //MultiShot ms = new MultiShot();
                //ms.shots(loosers);
            }
            if (card.value == 9) {
            //Buddy
                players2 = new ArrayList<Player>();
                for(int i=0; i<players.size();i++){
                    players2.add(players.get(i));
                }
                players2.remove(players.get(currentPlayer));
                setupListview2();
                mpopupPlayer2.showAtLocation(popUpViewPlayer2, Gravity.CENTER, 0, 0); // Displaying popup
            }
    }

    private final class MyTouchListener implements OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                abc = (ImageView)view;
                ClipData data = ClipData.newPlainText("", "");
                DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
                        view);
                view.startDrag(data, shadowBuilder, view, 0);
                view.setVisibility(View.INVISIBLE);
                return true;
            } else {
                return false;
            }
        }
    }

    public void setBuddy(Player p){
        //0: Sucht sich Buddy aus
        //1: Bekommt auch Shot, wenn 0 Shot bekommt;
        buddyArray[0]= players.get(currentPlayer);
        buddyArray[1]= p;
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bedienung.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Status = readMessage;
                    if(Status.contains("1")){
                        Getrunken = true;
                        ampel.setImageResource(R.drawable.ampel_gruen);
                        Status="";
                    }
                    break;
            }
        }
    };

    public void setupListview(){
        try {
            ArrayAdapter<Player> adapter = new DragDrop.PlayerListAdapter();
            ergebnisList.setAdapter(adapter);

        } catch (Exception e) {
        }
    }

    public void setupListview2(){
        try {
            ArrayAdapter<Player> adapter2 = new DragDrop.PlayerListAdapter2();
            ergebnisList2.setAdapter(adapter2);

        } catch (Exception e) {
        }
    }

    private class PlayerListAdapter extends ArrayAdapter<Player> {
        public PlayerListAdapter() {
            super(DragDrop.this, R.layout.draglistentry, players);


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.draglistentry, parent, false);
            }

            //Find Item
            final Player player = players.get(position);

            //Insert in View
            TextView b = (TextView) itemView.findViewById(R.id.dragEntryName);
            b.setText(player.getName());

            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(buddy){
                        if(player.id ==buddyArray[0].id){
                            ArrayList<Player> loosers= new ArrayList<Player>();
                            loosers.add(player);
                            loosers.add(buddyArray[1]);
                            MultiLight ml = new MultiLight();
                            ml.shots(loosers);
                           // MultiShot ms = new MultiShot();
                            //ms.shots(loosers);

                        }
                    }else{
                        player.oneShot();
                    }
                    if(!SystemState.isOffline()){
                        ampel.setImageResource(R.drawable.ampel_rot);
                        Getrunken=false;
                    }
                    mpopupPlayer.dismiss();
                }
            });

            return itemView;
        }
    }
    private class PlayerListAdapter2 extends ArrayAdapter<Player> {
        public PlayerListAdapter2() {
            super(DragDrop.this, R.layout.draglistentry, players2);


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.draglistentry, parent, false);
            }

            //Find Item
            final Player player = players2.get(position);

            //Insert in View
            TextView b = (TextView) itemView.findViewById(R.id.dragEntryName);
            b.setText(player.getName());

            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buddy =true;
                    setBuddy(player);
                    mpopupPlayer2.dismiss();
                }
            });

            return itemView;
        }
    }

    public void setupListview3(){
        try {
            ArrayAdapter<Player> adapter = new PlayerListAdapter3();
            lv.setAdapter(adapter);

        } catch (Exception e) {
        }
    }

    private class PlayerListAdapter3 extends ArrayAdapter<Player> {
        public PlayerListAdapter3() {
            super(DragDrop.this, R.layout.balkendia, PlayersClass.getPlayerList());


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.balkendia, parent, false);
            }

            //Find Item
            final Player player = PlayersClass.getPlayerList().get(position);

            //Insert in View

            int maxW = 250;
            int maxShots = PlayersClass.maxShots();
            double verhaeltnis;
            if(maxShots!=0){
                verhaeltnis = (double)player.getShotCount()/(double)maxShots;
            }else{
                verhaeltnis = 0;
            }
            int width = (int)(maxW*verhaeltnis);
            final float scale = getContext().getResources().getDisplayMetrics().density;
            int pixels = (int) (width * scale + 0.5f);

            TextView balken = (TextView) itemView.findViewById(R.id.diaBalken);
            ImageView diaPic = itemView.findViewById(R.id.diaBild);
            TextView shots = itemView.findViewById(R.id.diaShots);
            TextView name = itemView.findViewById(R.id.diaName);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(pixels,(int) (25 * scale + 0.5f));
            ViewGroup.LayoutParams params2 = balken.getLayoutParams();
            params2.height = (int) (25 * scale + 0.5f);
            params2.width = pixels;
            balken.setLayoutParams(params2);

            diaPic.setImageResource(player.getColor());
            if(player.bild!=null){
                diaPic.setImageBitmap(player.bild);
                diaPic.setForeground(null);
            }else{
                diaPic.setForeground(player.playerPic);
            }
            balken.setBackgroundResource(player.color2);
            String sh = " Shots // ";
            if(player.getShotCount()==1){
                sh = " Shot // ";
            }
            shots.setText(player.getShotCount()+sh+player.ml +"ml");
            name.setText(""+player.getName());
            return itemView;
        }
    }

    public void rotate(){
        is_spinning = true;
        grad = grad_alt % 360;
        grad = Random.nextInt(3600) + 439;
        RotateAnimation rotate= new RotateAnimation(grad_alt, grad,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f );
        rotate.setDuration(4215);
        rotate.setFillAfter(true);
        rotate.setInterpolator(new DecelerateInterpolator());
        rotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                CurrentNumber(360 - (grad % 360));
                is_spinning =false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        rad.startAnimation(rotate);
    }

    private void CurrentNumber(int grad){

        minirad.setVisibility(View.GONE);

        if(grad >= 342 || grad <=18 ){
            Intent intent =new Intent(DragDrop.this, Quiz.class);
            intent.putExtra("Rounds",2*players.size());
            startActivity(intent);
        }
        if(grad >= 18 && grad <=54 ){
            Intent intent =new Intent(DragDrop.this, TimerSpiel.class);
            intent.putExtra("Rounds",2);
            startActivity(intent);
        }
        if(grad >= 54 && grad <= 90){
            Intent intent =new Intent(DragDrop.this, Gluecksrad.class);
            intent.putExtra("Rounds",players.size());
            startActivity(intent);
        }
        if(grad >= 90 && grad <= 126){
            Intent intent =new Intent(DragDrop.this, Draw.class);
            intent.putExtra("Rounds",1);
            startActivity(intent);
        }
        if(grad >= 126 && grad <= 162){
            Intent intent =new Intent(DragDrop.this, Memory.class);
            intent.putExtra("Rounds",1);
            startActivity(intent);
        }
        if(grad >= 162 && grad <= 198){
            Intent intent =new Intent(DragDrop.this, Hausnummer.class);
            intent.putExtra("Rounds",1);
            startActivity(intent);
        }
        if(grad >= 198 && grad <= 234){
            Intent intent =new Intent(DragDrop.this, WahrFalsch.class);
            intent.putExtra("Rounds",2*players.size());
            startActivity(intent);
        }
        if(grad >= 234 && grad <= 270){
            Intent intent =new Intent(DragDrop.this, BalkenSaufen.class);
            intent.putExtra("Rounds",2*players.size());
            startActivity(intent);
        }
        if(grad >= 270 && grad <= 306){
            Intent intent =new Intent(DragDrop.this, Meiern.class);
            intent.putExtra("Rounds",2*players.size());
            startActivity(intent);
        }
        if(grad >= 306 && grad <= 342){
            Intent intent =new Intent(DragDrop.this, HoeherTiefer.class);
            intent.putExtra("Rounds",2*players.size());
            startActivity(intent);
        }
    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        beendenPopup.showAtLocation(beendenView, Gravity.CENTER, 0, 0); // Displaying popup
    }
}
