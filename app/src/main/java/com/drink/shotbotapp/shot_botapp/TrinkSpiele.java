package com.drink.shotbotapp.shot_botapp;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class TrinkSpiele extends AppCompatActivity {
    private ViewPager viewPager;
    private slideAdapter myadapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trink_spiele);
        viewPager= (ViewPager) findViewById(R.id.ViewPager);
        myadapter = new slideAdapter(this);
        viewPager.setAdapter(myadapter);


    }

    private void msg(String s)
    {
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
    }
}
