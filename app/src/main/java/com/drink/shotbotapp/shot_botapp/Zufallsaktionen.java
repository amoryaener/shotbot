package com.drink.shotbotapp.shot_botapp;

import java.util.ArrayList;
import java.util.Random;

public class Zufallsaktionen {

    public int[] zwischenaktionen;
    public ArrayList<Player> players;

    public Zufallsaktionen(ArrayList<Player> p){
        this.players = p;
        this.zwischenaktionen = new int[4];
        this.zwischenaktionen[0]=R.drawable.zwischenaktion8;
        this.zwischenaktionen[1]=R.drawable.zwischenaktion2;
        this.zwischenaktionen[2]=R.drawable.zwischenaktion5;
        this.zwischenaktionen[3]=R.drawable.zwischenaktion6;
        //this.zwischenaktionen[4]=R.drawable.zwischenaktion7;
    }

    public boolean aktion(int random){
        boolean out = false;
        if(random==2){ //============================================> Männer
            ArrayList<Player> loosers = new ArrayList<Player>();
            for(int i=0; i<players.size(); i++){
                if(players.get(i).morw.equals("m")){
                    loosers.add(players.get(i));
                }
            }
            if(loosers.size()>0){
                if(loosers.size()==1){
                    loosers.get(0).oneShot();
                }else{
                    MultiLight ml = new MultiLight();
                    ml.shots(loosers);
                    //MultiShot ms = new MultiShot();
                    //ms.shots(loosers);
                }
                if(!SystemState.isOffline()) {
                    out = true;
                }
            }
        }else if(random==3){ //============================================> Frauen
            ArrayList<Player> loosers = new ArrayList<Player>();
            for(int i=0; i<players.size(); i++){
                if(players.get(i).morw.equals("w")){
                    loosers.add(players.get(i));
                }
            }
            if(loosers.size()>0){
                if(loosers.size()==1){
                    loosers.get(0).oneShot();
                }else{
                    MultiLight ml = new MultiLight();
                    ml.shots(loosers);
                    //MultiShot ms = new MultiShot();
                    //ms.shots(loosers);
                }
                if(!SystemState.isOffline()) {
                    out = true;
                }
            }
        }else if(random==0){ //============================================> Alle
            ArrayList<Player> loosers = new ArrayList<Player>();
            for(int i=0; i<players.size(); i++){
                loosers.add(players.get(i));
            }
            MultiLight ml = new MultiLight();
            ml.shots(loosers);
            //MultiShot ms = new MultiShot();
            //ms.shots(loosers);
            if(!SystemState.isOffline()) {
                out = true;
            }
        }else if(random==1){ //============================================> Zufall
            Random ran = new Random();
            players.get(ran.nextInt(players.size())).oneShot();
            if(!SystemState.isOffline()) {
                out = true;
            }
        }/*else if(random==4){ //============================================> Zufall
            Random ran = new Random();
            players.get(ran.nextInt(players.size())).oneShot();
            if(!SystemState.isOffline()) {
                out = true;
            }
        }*/
        return out;
    }

    public int getPic(int index){
        return this.zwischenaktionen[index];
    }

}
