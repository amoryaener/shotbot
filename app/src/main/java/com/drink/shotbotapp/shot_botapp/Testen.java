package com.drink.shotbotapp.shot_botapp;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Testen extends AppCompatActivity {

    TextView Text;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference Message = database.getReference("message");
    DatabaseReference Speichern = database.getReference("Speichern");
    String userId = Speichern.push().getKey();

    ImageButton btn_1St;
    ImageButton btn_2St;
    ImageButton btn_3St;
    ImageButton btn_4St;
    ImageButton btn_5St;
    ImageButton btn_6St;
    ImageButton btn_7St;
    ImageButton btn_8St;
    ImageButton btn_9St;
    ImageButton btn_10St;

    ImageButton btn_1P;
    ImageButton btn_2P;
    ImageButton btn_3P;
    ImageButton btn_4P;
    ImageButton btn_5P;
    ImageButton btn_6P;
    ImageButton btn_7P;
    ImageButton btn_8P;
    ImageButton btn_9P;
    ImageButton btn_10P;

    ImageButton btn_Zufall1;
    ImageButton btn_Zufall2;
    ImageButton btn_Mode3;
    ImageButton btn_Mode4;
    ImageButton btn_Mode5;

    ImageButton btn_LK1;
    ImageButton btn_LK2;
    ImageButton btn_LT1;
    ImageButton btn_LT2;

    public static final int MESSAGE_READ = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testen);

        Text = (TextView) findViewById(R.id.snapText);

        Message.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                Text.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });

        btn_1St = (ImageButton)findViewById(R.id.btn_1St);
        btn_2St = (ImageButton)findViewById(R.id.btn_2St);
        btn_3St = (ImageButton)findViewById(R.id.btn_3St);
        btn_4St = (ImageButton)findViewById(R.id.btn_4St);
        btn_5St = (ImageButton)findViewById(R.id.btn_5St);
        btn_6St = (ImageButton)findViewById(R.id.btn_6St);
        btn_7St = (ImageButton)findViewById(R.id.btn_7St);
        btn_8St = (ImageButton)findViewById(R.id.btn_8St);
        btn_9St = (ImageButton)findViewById(R.id.btn_9St);
        btn_10St = (ImageButton)findViewById(R.id.btn_10St);

        btn_1P = (ImageButton)findViewById(R.id.btn_1P);
        btn_2P = (ImageButton)findViewById(R.id.btn_2P);
        btn_3P = (ImageButton)findViewById(R.id.btn_3P);
        btn_4P = (ImageButton)findViewById(R.id.btn_4P);
        btn_5P = (ImageButton)findViewById(R.id.btn_5P);
        btn_6P = (ImageButton)findViewById(R.id.btn_6P);
        btn_7P = (ImageButton)findViewById(R.id.btn_7P);
        btn_8P = (ImageButton)findViewById(R.id.btn_8P);
        btn_9P = (ImageButton)findViewById(R.id.btn_9P);
        btn_10P = (ImageButton)findViewById(R.id.btn_10P);

        btn_Zufall1 = (ImageButton)findViewById(R.id.btn_Zufall1);
        btn_Zufall2 = (ImageButton)findViewById(R.id.btn_Zufall2);
        btn_Mode3 = (ImageButton)findViewById(R.id.btn_Mode3);
        btn_Mode4 = (ImageButton)findViewById(R.id.btn_Mode4);
        btn_Mode5 = (ImageButton)findViewById(R.id.btn_Mode5);

        btn_LK1 = (ImageButton)findViewById(R.id.btn_LK1);
        btn_LK2 = (ImageButton)findViewById(R.id.btn_LK2);
        btn_LT1 = (ImageButton)findViewById(R.id.btn_LT1);
        btn_LT2 = (ImageButton)findViewById(R.id.btn_LT2);

        BluetoothAdapterClass.setHandler(mHandler);

        btn_1St.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Speichern.child(userId).setValue("Fabian");
            }
        });

        btn_2St.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Speichern.setValue("Marina");
            }
        });

        btn_3St.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Speichern.child("Marina").setValue("Marina");
            }
        });

        btn_4St.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("4S");
            }
        });

        btn_5St.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("5S");
            }
        });

        btn_6St.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("6S");
            }
        });

        btn_7St.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("7S");
            }
        });

        btn_8St.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("8S");
            }
        });

        btn_9St.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("9S");
            }
        });

        btn_10St.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("10S");
            }
        });

//-------------------------------------------------------------

        btn_1P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("1P");
            }
        });

        btn_2P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("2P");
            }
        });

        btn_3P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("3P");
            }
        });

        btn_4P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("4P");
            }
        });

        btn_5P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("5P");
            }
        });

        btn_6P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("6P");
            }
        });

        btn_7P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("7P");
            }
        });

        btn_8P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("8P");
            }
        });

        btn_9P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("9P");
            }
        });

        btn_10P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("10P");
            }
        });
//-----------------------------------------------------------

        btn_Zufall1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("Z1");
            }
        });

        btn_Zufall2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("Z2");
            }
        });
//-----------------------------------------------------------

        btn_LK1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("i");
            }
        });
        btn_LK2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("k");
            }
        });
        btn_LT1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("t");
            }
        });
        btn_LT2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapterClass.WriteBT("g");
            }
        });

    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Text.setText("Mode: "+readMessage);
                    break;
            }
        }
    };


    }


